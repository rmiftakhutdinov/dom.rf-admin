package expectedConditions;

import framework.Helper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class a_work_with_alert extends Helper {

    @Test (description = "Создание объекта alert")
    public void simpleAlert(){

        driver.get("http://javascript.ru/alert"); // переход на страницу демонстрации

        WebElement alertButton = findElement(By.xpath("//input[@type='button'][@value='Запустить']"));

        Alert alert = driver.switchTo().alert(); // переключение на окно алерта

        alert.accept(); // выполнение

    }

    @Test (description = "Создание объекта alert через wait")
    public void waitAlert(){


        driver.get("http://javascript.ru/alert"); // переход на страницу демонстрации

        WebElement alertButton = driver.findElement(By.xpath("//input[@type='button'][@value='Запустить']"));

        alertButton.click(); // клик на кнопку

        Alert alert = (new WebDriverWait(driver, 10).until(ExpectedConditions.alertIsPresent()));

        alert.accept();

    }

}
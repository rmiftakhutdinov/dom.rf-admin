package expectedConditions;

import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class b_work_with_title extends Helper {


    @Test (description = "Простая смена страницы")
    public void simpleChangePage(){


        driver.get("http://33slona.ru"); // переход на страницу 33 Слона


        WebElement adCreate = driver.findElement(By.cssSelector("a.orange-button"));


        adCreate.click();


        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));


        addressInput.sendKeys("г Москва");


    }


    @Test (description = "Смена страницы через wait")
    public void changePageWithWait(){


        driver.get("http://33slona.ru"); // переход на страницу 33 Слона

        WebElement adCreate = driver.findElement(By.cssSelector("a.orange-button"));


        adCreate.click();


        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.titleIs("Подать объявление о сдаче квартиры без комиссий и посредников."));


        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));


        addressInput.sendKeys("г Москва");



    }


    @Test (description = "Title contains")
    public void title_contains(){

        driver.get("http://33slona.ru"); // переход на страницу 33 Слона

        WebElement adCreate = driver.findElement(By.cssSelector("a.orange-button"));


        adCreate.click();


        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.titleContains("Подать объявление о сдаче квартиры"));


        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));


        addressInput.sendKeys("г Москва");

    }


}

package expectedConditions;

import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class c_work_with_url extends Helper {


    @Test
    public void url_to_be(){

        driver.get("http://33slona.ru"); // переход на страницу 33 Слона

        WebElement adCreate = driver.findElement(By.cssSelector("a.orange-button"));


        adCreate.click();


        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.urlToBe("https://33slona.ru/add?offerType=sell&objType=flat"));

        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));


        addressInput.sendKeys("г Москва");

    }


    @Test
    public void url_contains(){


        driver.get("http://33slona.ru"); // переход на страницу 33 Слона

        WebElement adCreate = driver.findElement(By.cssSelector("a.orange-button"));


        adCreate.click();


        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.urlContains("https://33slona.ru/add?offerType="));

        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));


        addressInput.sendKeys("г Москва");

    }

}

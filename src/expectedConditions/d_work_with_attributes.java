package expectedConditions;

import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class d_work_with_attributes extends Helper{


    @Test(description = "By locator")
    public void attributeContainsByLocator(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.titleIs("Подать объявление о сдаче квартиры без комиссий и посредников."));

        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));

        addressInput.sendKeys("123");

        addressInput.sendKeys("г Москва");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.attributeContains(By.cssSelector("input.address-input"), "value", "г Москва"));


    }


    @Test (description = "Web Element")
    public void attributeContainsWebElement(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.titleIs("Подать объявление о сдаче квартиры без комиссий и посредников."));

        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));

        addressInput.sendKeys("123");

        addressInput.sendKeys("г Москва");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.attributeContains(addressInput, "value", "г Москва"));

    }


    @Test (description = "By locator")
    public void attributeToBeByLocator(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.titleIs("Подать объявление о сдаче квартиры без комиссий и посредников."));

        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));

        addressInput.sendKeys("123");

        //addressInput.clear();

        addressInput.sendKeys("г Москва");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.attributeToBe(By.cssSelector("input.address-input"), "value", "г Москва"));

    }


    @Test (description = "Web Element")
    public void attributeToBeWebElement(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.titleIs("Подать объявление о сдаче квартиры без комиссий и посредников."));

        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));

        addressInput.sendKeys("123");

        //addressInput.clear();

        addressInput.sendKeys("г Москва");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.attributeToBe(addressInput, "value", "г Москва"));

    }


    @Test(description = "Web Element")
    public void attributeToBeNotEmpty(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.titleIs("Подать объявление о сдаче квартиры без комиссий и посредников."));

        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));

        addressInput.sendKeys("г Москва");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.attributeToBeNotEmpty(addressInput, "value"));

    }


}

package expectedConditions;

import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class e_work_with_text extends Helper{


    @Test
    public void text_to_be(){


        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        WebElement h2 = driver.findElement(By.cssSelector("h2"));

        Assert.assertTrue(h2.getText().contains("Новое объявление"));

        /*new WebDriverWait(driver, 10)
                .until(ExpectedConditions.textToBe(By.cssSelector("h2"), "Новое объявление"));*/

    }


    @Test
    public void text_to_be_present_in_element(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        WebElement h2 = driver.findElement(By.cssSelector("h2"));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.textToBePresentInElement(h2, "Новое объявление"));

    }


    @Test
    public void text_to_be_present_in_element_located(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("h2"), "Новое объявление"));

    }


    @Test
    public void text_to_be_present_in_element_value(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("h2"), "Новое объявление"));


        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));

        addressInput.sendKeys("г Москва");


        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.textToBePresentInElementValue(addressInput, "г Москва"));

        // ТАК ЖЕ РАБОТАЕТ С BY LOCATOR

        // CHECK CONTAINS, NOT EQUALS!!!

    }


    @Test
    public void invisibility_of_element_with_text(){


        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.invisibilityOfElementWithText(By.cssSelector("h2"), "Новое объявление"));


    }
}

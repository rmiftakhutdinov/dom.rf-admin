package expectedConditions;

import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class f_work_with_presence extends Helper {


    @Test
    public void presence_of_element_located(){


        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input.address-input")));


    }


    @Test
    public void presence_of_all_elements_located_by(){


        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div.pure-u-1-5")));


    }


    @Test
    public void presence_of_nested_element_located_by(){

        // проверка на наличие вложенных элементов

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfNestedElementLocatedBy(By.cssSelector("div.metro-dropdown"), By.cssSelector("ul>li>span")));


        /*WebElement element = driver.findElement(By.cssSelector("div.metro-dropdown"));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfNestedElementLocatedBy(element, By.cssSelector("ul>li>span")));*/



        /*new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfNestedElementsLocatedBy(By.cssSelector("some_elements"), By.cssSelector("some_elements")));*/

    }


}

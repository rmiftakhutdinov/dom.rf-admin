package expectedConditions;

import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.List;

public class g_work_with_visibility extends Helper{


    @Test
    public void visibility_of_element_located(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input.address-input")));


    }

    @Test
    public void visibility_of(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        WebElement addressInput = driver.findElement(By.cssSelector("input.address-input"));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(addressInput));



        /*new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(addressInput)).sendKeys("г Москва");*/

    }

    @Test
    public void visibility_of_all_elements_located_by(){


        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div.advantages")));

    }

    @Test
    public void visibility_of_all_elements(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        List<WebElement> elements = driver.findElements(By.cssSelector("div.advantages"));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfAllElements(elements));

    }




    @Test
    public void invisibility_of(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        WebElement spinner = driver.findElement(By.cssSelector("div.full-slon-spinner"));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.invisibilityOf(spinner));

    }

    @Test
    public void invisibility_of_element_located(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.full-slon-spinner")));

    }

    @Test
    public void invisibility_of_element_with_text(){

        driver.get("https://realty.youla.io/?page=1");

        WebElement element = driver.findElement(By.xpath("//span[text()='Город']"));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOf(element)).click();

        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Москва']")));

        element.click();

        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.invisibilityOfElementWithText(By.xpath("//label"), "Москва"));

    }

    @Test
    public void invisibility_of_all_elements(){

        List<WebElement> elements = driver.findElements(By.cssSelector(""));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.invisibilityOfAllElements(elements));

    }

}

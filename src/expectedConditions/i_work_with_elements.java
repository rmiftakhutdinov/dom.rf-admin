package expectedConditions;

import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class i_work_with_elements extends Helper {

    @Test
    public void element_to_be_clickable() throws InterruptedException {



        By by = By.cssSelector("css.selector");

        click(by);



        WebElement element = driver.findElement(By.cssSelector("css.selector"));

        click((By) element);



    }

    @Test
    public void element_to_be_selected() throws InterruptedException {

        driver.get("https://realty.youla.io/?page=1");

        WebElement element = driver.findElement(By.xpath("//span[text()='Город']"));

        click((By) element);

        click(By.xpath("//input[@name='city_id[0]']/following-sibling::label"));

        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.elementToBeSelected(By.xpath("//input[@name='city_id[0]']")));


        /*WebElement element1 = driver.findElement(By.xpath("//input[@name='city_id[0]']"));
        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.elementToBeSelected(element1));*/

    }

    @Test
    public void element_selection_state_to_be() throws InterruptedException {

        driver.get("https://realty.youla.io/?page=1");

        WebElement element = driver.findElement(By.xpath("//span[text()='Город']"));

        click((By) element);


        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.elementSelectionStateToBe(By.xpath("//input[@name='city_id[0]']"),false));


        click(By.xpath("//input[@name='city_id[0]']/following-sibling::label"));

        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.elementSelectionStateToBe(By.xpath("//input[@name='city_id[0]']"), true));


        /*WebElement element1 = driver.findElement(By.xpath("//input[@name='city_id[0]']"));
        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.elementSelectionStateToBe(element1, true));*/
    }

    @Test
    public void numbers_of_elements_to_be(){

        driver.get("https://33slona.ru/add?offerType=rent&objType=flat");

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("div.advantages"), 5));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.numberOfElementsToBeLessThan(By.cssSelector("div.advantages"), 6));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("div.advantages"), 4));

    }

    @Test
    public void stalenessOf(){

        WebElement element = driver.findElement(By.cssSelector("css.selector"));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.stalenessOf(element)); // ожидание пока элемент исчезнет из DOM

    }


}

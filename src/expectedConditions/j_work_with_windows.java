package expectedConditions;

import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class j_work_with_windows extends Helper {

    @Test
    public void number_of_window_to_be() throws InterruptedException {

        driver.get("http://ahml.ru");

        click(By.xpath("//a[@href=' https://www.instagram.com/domrf/']"));

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.numberOfWindowsToBe(2));

    }

}

package framework;

import data.CommonData;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pageObjects.GlobalPage;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Random;

public class Helper {

    // Вызов драйвера
    protected WebDriver driver;

    // Выбор OS для запуска chromeDriver
    protected ChromeDriver getChromeDriver(String os){

        if (Objects.equals(os.toLowerCase(), "windows")){

                System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        } else {

                System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");

        }

        // Запуск тестов без открытия браузера
        /*ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        return new ChromeDriver(options);*/

        return new ChromeDriver();

    }

    // Выбор OS для запуска firefoxDriver
    protected FirefoxDriver getFirefoxDriver (String os) {

        if (Objects.equals(os.toLowerCase(), "windows")){

            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");

        } else {

            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver");

        }

        // Запуск тестов без открытия браузера
        /*ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        return new ChromeDriver(options);*/

        return new FirefoxDriver();

    }

    // Сообщение об ошибке
    protected void afterExeptionMessage(String message){

        throw new RuntimeException("\n" + message);

    }

    // Поиск и ожидание элемента
    protected WebElement findElement(By by){

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (java.util.NoSuchElementException ex){
            throw new RuntimeException("Элемент " + by + " не найден на странице");
        }

        return driver.findElement(by);

    }

    // Таймаут по заданному времени
    protected void sleep(int time) throws InterruptedException {

        Thread.sleep(time);

    }

    // Клик на элемент
    protected void click(By by) {

        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(by)).click();

    }

    // Ожидание элемента и клик по нему
    protected void click(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeClickable(by)).click();

    }

    // Ожидание элемента с выбором времени
    protected void waitBy(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.visibilityOfElementLocated(by));

    }

    // Ожидание элемента с установленным по дефолту временем
    protected void waitBy(By by){

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(by));

    }

    // Авторизация в Админке на дев.окружении: user admin@example.com
    protected void authorizationInAdminDev() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        CommonData user = new CommonData();

        findElement(page.userName).clear();
        findElement(page.userName).sendKeys(user.adminUserEmail);
        findElement(page.userPassword).clear();
        findElement(page.userPassword).sendKeys(user.adminUserPassDev);

        sleep(1500);
        click(page.submit);
        try {
            findElement(page.profileName);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найдено имя пользователя");
        }

    }

    // Авторизация в Админке на прод.окружении: user admin@example.com
    protected void authorizationInAdminProd() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        CommonData user = new CommonData();

        findElement(page.userName).clear();
        findElement(page.userName).sendKeys(user.adminUserEmail);
        findElement(page.userPassword).clear();
        findElement(page.userPassword).sendKeys(user.adminUserPassProd);

        sleep(1500);
        click(page.submit);
        try {
            findElement(page.profileName);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найдено имя пользователя");
        }

    }

    // Скролл к элементу
    protected void scrollToElement(By by) throws InterruptedException {

        WebElement element = driver.findElement(by);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        sleep(1000);

    }

    // Ожидание элемента на странице
    protected boolean isElementPresent(By selector){

        try{

            int timeouts = 10;
            new WebDriverWait(driver, timeouts).until(ExpectedConditions.presenceOfElementLocated(selector));
            return true;

        } catch (TimeoutException ex){

            return false;

        }

    }

    // Ожидание отсутствия элемента на странице
    protected boolean elementIsNotPresent(By selector){

        try{

            int timeouts = 3;
            new WebDriverWait(driver, timeouts).until(ExpectedConditions.presenceOfElementLocated(selector));
            return false;

        } catch (TimeoutException ex){

            return true;

        }

    }

    // Ожидание отсутствия элемента на странице
    protected boolean elementIsNotPresent(By selector, int time){

        try{

            new WebDriverWait(driver, time).until(ExpectedConditions.presenceOfElementLocated(selector));
            return false;

        } catch (TimeoutException ex){

            return true;

        }

    }

    // Добавление файла
    protected void setClipboardData(String string) {

        StringSelection stringSelection = new StringSelection(string);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

    }

    // Сендкейс - вставить текст
    protected void sendKeys(By by, String string) {

        findElement(by).clear();
        findElement(by).sendKeys(string);

    }

    // Сендкейс - найти элемент и нажать с клавиатуры
    protected void sendKeys(By by, Keys keys) {

        findElement(by).clear();
        findElement(by).sendKeys();

    }

    // Ожидание исчезновения элемента
    protected boolean elementIsNotVisible(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.invisibilityOfElementLocated(by));

        return false;

    }

    // Ожидание исчезновения элемента из дома
    protected void domElementIsNotVisible(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.stalenessOf(findElement(by)));

    }

    // Ожидание исчезновения элемента
    protected boolean elementIsNotVisible(By by){

        new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOfElementLocated(by));

        return false;

    }

    // Выбор селекта по value
    protected void selectByValue(By by, String string){

        Select select = new Select(findElement(by));
        select.selectByValue(string);

    }

    // Выбор селекта по тексту
    protected void selectByText(By by, String string){

        Select select = new Select(findElement(by));
        select.selectByVisibleText(string);

    }

    // Генерация случайных символов
    protected String randomString(String data, int length){

        /**
         * @data - принимает на вход символьный набор, из которого генерится строка
         * @length - длина выходного значения
         */

        StringBuffer string = new StringBuffer();
        for (int i=0; i < length; i++){

            string.append(data.charAt(new Random().nextInt(data.length())));

        }

        return string.toString();

    }

    // Получение текущей даты
    protected String getCurrentDate(){

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);

    }

    // метод проверки существования файла
    protected void fileExist(File file){

        try {
            Assert.assertTrue(file.exists());
        } catch (AssertionError ex){
            afterExeptionMessage("Файл не найден");
        }

    }

    // метод удаления файла
    protected void fileDelete(File file){

        try {
            Assert.assertTrue(file.delete());
        } catch (AssertionError ex){
            afterExeptionMessage("Файл не удалён");
        }

    }

}
package pageObjects;

import framework.Helper;
import org.openqa.selenium.By;

public class AhmlServicesPage extends Helper {

    public By filterByType = By.xpath("//div[@class='multi-filter-wrapper ng-scope serviceType-filter']/select");
    public By filterByStatus = By.xpath("//div[@class='multi-filter-wrapper ng-scope status-filter']/select");
    public By periodFilter = By.xpath("//div[@class='input-group-btn dropdown']/button");
    public By periodFilterAllTime = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[1]/a[text()='За все время']");
    public By periodFilterDiapason = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[2]/a[text()='Диапазон']");

    public By serviceNumber = By.xpath("//table[@class='table table-hover']/tbody/tr[1]/td[1]");

}

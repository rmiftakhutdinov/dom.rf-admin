package pageObjects;

import org.openqa.selenium.By;

public class ClientServicesPage {

    public By serviceNumber = By.xpath("//table[@class='table table-hover']/tbody/tr[1]/td[1]");

    public By showServicesDetail = By.xpath("//tbody/tr[1]/td[2]/a[contains(text(),'Детально')]");
    public By hideServicesDetail = By.xpath("//tbody/tr[1]/td[2]/a[contains(text(),'Свернуть')]");

    public By object = By.xpath("//tbody/tr[1]/td[3]/a");
    public By renter = By.xpath("//tbody/tr[1]/td[3]/div/small/a/nobr");

    public By invoiceNumber = By.xpath("//tr/td[4]/div[1]/span/a");

    public By typeFilter = By.xpath("//div[@class='multi-filter-wrapper ng-scope type-filter']/select");
    public By statusFilter = By.xpath("//div[@class='multi-filter-wrapper ng-scope status-filter']/select");
    public By paymentFilter = By.xpath("//div[@class='multi-filter-wrapper ng-scope paymentStatus-filter']/select");

    public By periodFilter = By.xpath("//div[@class='input-group-btn dropdown']/button");
    public By periodAllDate = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[1]/a[text()='За все время']");
    public By period7Days = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[2]/a[text()='7 дней']");
    public By period14Days = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[3]/a[text()='14 дней']");
    public By period30Days = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[4]/a[text()='30 дней']");
    public By period90Days = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[5]/a[text()='90 дней']");
    public By periodChoose = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[6]/a[text()='Диапазон']");
    public By periodChooseElement = By.xpath("//div[@class='multiTypeDateRange multi-filter-wrapper flex-row ng-scope createdAt-filter']");

    public By noServicesElement = By.xpath("//p[text()='Нет подходящих заказов']");

    public By rating1 = By.xpath("//div[@class='btn-toolbar multi-filter-wrapper ng-scope rating-filter']/div/label[1]");
    public By rating2 = By.xpath("//div[@class='btn-toolbar multi-filter-wrapper ng-scope rating-filter']/div/label[2]");
    public By rating3 = By.xpath("//div[@class='btn-toolbar multi-filter-wrapper ng-scope rating-filter']/div/label[3]");
    public By rating4 = By.xpath("//div[@class='btn-toolbar multi-filter-wrapper ng-scope rating-filter']/div/label[4]");
    public By rating5 = By.xpath("//div[@class='btn-toolbar multi-filter-wrapper ng-scope rating-filter']/div/label[5]");
    public By ratingAll = By.xpath("//div[@class='btn-toolbar multi-filter-wrapper ng-scope rating-filter']/div/label[6]");

    public By typeServices = By.xpath("//tbody/tr/td[2]/div[1]/div[1]");
    public By statusServices = By.xpath("//tbody/tr/td[4]/span");
    public By paymentStatusServices = By.xpath("//tbody/tr/td[4]/div/small");
    public By evaluationServices = By.xpath("//tbody/tr/td[4]/small/span");

}
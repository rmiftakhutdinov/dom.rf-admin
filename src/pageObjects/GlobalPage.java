package pageObjects;

import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;

public class GlobalPage extends Helper {

    public String pageURLDev = "http://dev-arenda.ahml.ru/admin/auth/login";
    public String pageURLProd = "http://arenda.ahml.ru/admin/auth/login";

    // Общие

        // методы проверки элемента раздела
        protected void objectsSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.objectsSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Объекты'");
            }

        }

        protected void ordersSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.ordersSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Заявки'");
            }

        }

        protected void rentersSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.rentersSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Арендаторы'");
            }

        }

        protected void invoiceSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.invoiceSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Счета'");
            }

        }

        protected void clientServicesSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.clientServicesSectionElement);
            } catch (TimeoutException ex){
                afterExeptionMessage("Не найден элемент раздела 'Все заказы'");
            }

        }

        protected void passesSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.passesSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Пропуска'");
            }

        }

        protected void ahmlServicesSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.ahmlServicesSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Заказы ДОМ.РФ'");
            }

        }

        protected void adsSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.adsSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Объявления'");
            }

        }

        protected void promosSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.promosSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Акции'");
            }

        }

        protected void notificationsClientSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.notificationsClientSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Уведомления клиентов'");
            }

        }

        protected void notificationsServiceSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.notificationsServiceSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Сервисные рассылки'");
            }

        }

        protected void statisticsSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.statisticsSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Сводка'");
            }

        }

        protected void documentsSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.documentsSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Документы'");
            }

        }

        protected void templatesSection(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.templatesSectionElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент раздела 'Шаблоны'");
            }

        }

        // метод вставки строки в путь
        public By searchNumber(String s){
            return By.xpath("//tbody/tr/td[2]/a[text()='" + s + "']");
        }

    public By userName = By.xpath("//input[@id='inputEmail']");
    public By userPassword = By.xpath("//input[@id='inputPassword']");
    public By submit = By.xpath("//button[@type='submit']");
    public By profileName = By.xpath("//a[text()='Администратор']");
    public By logout = By.xpath("//i[@class='fa et-down fa-sign-out']");
    public By authPageLogo = By.xpath("//a[@class='auth-page-logo logo']");
    public By searchInput = By.xpath("//input[@ng-model='filter.model']");
    public By filterReset = By.xpath("//a[text()='Сбросить фильтр']");
    public By spinner = By.cssSelector("i.fa.fa-spinner");
    public By add = By.xpath("//a[text()='Добавить...']");

    public By page1 = By.xpath("//li[@class='pagination-page ng-scope']/a[text()='1']");
    public By page2 = By.xpath("//li[@class='pagination-page ng-scope']/a[text()='2']");
    public By page1Active = By.xpath("//li[@class='pagination-page ng-scope active']/a[text()='1']");
    public By page2Active = By.xpath("//li[@class='pagination-page ng-scope active']/a[text()='2']");
    public By nextPage = By.xpath("//a[text()='Следующая']");
    public By prevPage = By.xpath("//a[text()='Предыдущая']");

    public By downloadReport = By.xpath("//a[@class='btn btn-default ng-isolate-scope'][text()='Выгрузить в XLSX']");
    public By downloadReportSpinner = By.xpath("//div[@class='button-spinner-container']");
    public String dirWork = "C:\\Users\\r.miftakhutdinov\\Downloads\\";

    public By periodFilter = By.xpath("//div[@class='input-group-btn dropdown']/button");
    public By periodDiapason1 = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[6]/a[text()='Диапазон']");
    public By periodDiapason2 = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[2]/a[text()='Диапазон']");
    public By periodDiapason3 = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[5]/a[text()='Диапазон']");
    public By periodInputDateFrom = By.xpath("//input[@id='mtlmDP']");
    public By periodInputDateTo = By.xpath("//input[@id='mtlmDP2']");
    public By periodAllDate = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[1]/a[text()='За все время']");
    public By period7Days = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[2]/a[text()='7 дней']");
    public By period14Days = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[3]/a[text()='14 дней']");
    public By period30Days = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[4]/a[text()='30 дней']");
    public By period90Days = By.xpath("//div[@class='input-group-btn dropdown open']/ul/li[5]/a[text()='90 дней']");
    public By nextMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[3]");
    public By prevMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[1]");
    public By setDateFrom = By.xpath("//div[@class='moment-picker-container month-view open']//tr[6]/td[1]");
    public By setDateTo = By.xpath("//div[@class='moment-picker-container month-view open']//tr[3]/td[1]");

    // Объекты
    public By objectsSection = By.xpath("//a[text()='Объекты']");
    public By objectsSectionElement = By.xpath("//h1[contains(text(),'Объекты')]");
    public By apartmentCardElement = By.xpath("//h1[contains(text(),'Апартамент')]");
    public By parkingCardElement = By.xpath("//h1[contains(text(),'Машиноместо')]");

        // метод ожидания номера апартамента
        protected void apartmentNumber(){

            ObjectsPage page = new ObjectsPage();

            try {
                waitBy(page.apartmentNumber, 10);
            } catch (Exception ex){
                afterExeptionMessage("В заданном фильтре нет подходящих апартаментов");
            }

        }

        // метод ожидания номера машиноместа
        protected void parkingNumber(){

            ObjectsPage page = new ObjectsPage();

            try {
                waitBy(page.parkingNumber, 10);
            } catch (Exception ex){
                afterExeptionMessage("В заданном фильтре нет подходящих машиномест");
            }

        }

        // метод ожидания элемента в карточке апартамента
        protected void apartmentCard(){

            GlobalPage page = new GlobalPage();

            try {
                waitBy(page.apartmentCardElement);
            } catch (NoSuchElementException ex){
                afterExeptionMessage("Не найден элемент карточки апартамента");
            }

        }

        // метод удаления черновика
        protected void deleteDraft(){

            ObjectsPage page = new ObjectsPage();

            click(page.dropDownMenuInContracts);
            click(page.deleteDraft);
            click(page.confirmDeleteDraftModal);
            elementIsNotVisible(page.confirmDeleteDraftModal);
            driver.navigate().refresh();

        }

    // Заявки
    public By ordersSection = By.xpath("//a[text()='Заявки']");
    public By ordersSectionElement = By.xpath("//h1[text()='Заявки']");
    public By addNewOrderElement = By.xpath("//h1[text()='Добавление заявки']");
    public By orderClientName = By.xpath("//form/div[3]//input");
    public By orderClientPhone = By.xpath("//form/div[5]//input");
    public By orderCardElement = By.xpath("//h1[contains(text(),'Заявка')]");

    public By interestedCheckboxes = By.xpath("//div[@class='checkbox']/label");

        // метод ожидания номера заявки
        protected void orderNumber(){

            OrdersPage page = new OrdersPage();

            try {
                waitBy(page.orderNumber, 10);
            } catch (Exception ex){
                afterExeptionMessage("В заданном фильтре нет подходящих заявок");
            }

        }

    // Арендаторы
    public By rentersSection = By.xpath("//a[text()='Арендаторы']");
    public By rentersSectionElement = By.xpath("//h1[text()='Арендаторы']");

    public By renterCardElement = By.xpath("//h1[contains(text(),'Арендатор')]");

    public By addNewRenterElement = By.xpath("//h1[text()='Добавление арендатора']");

    public By renterFamily = By.xpath("//input[@id='lastname']");
    public By renterName = By.xpath("//input[@id='firstname']");
    public By renterPatronymic = By.xpath("//input[@id='patronymic']");
    public By renterBornedAtDay = By.xpath("//input[@id='bornedAtDay']");
    public By renterBornedAtMonth = By.xpath("//input[@id='bornedAtMonth']");
    public By renterBornedAtYear = By.xpath("//input[@id='bornedAtYear']");
    public By renterPhone = By.xpath("//input[@id='phone']");
    public By renterEmail = By.xpath("//input[@id='email']");
    public By renterPassportNumber = By.xpath("//input[@id='passport[seriesNumber]']");
    public By renterPassportIssued = By.xpath("//textarea[@id='passport[issued]']");
    public By renterPassportIssuedDay = By.xpath("//input[@id='passport[issuedAtDay]']");
    public By renterPassportIssuedMonth = By.xpath("//input[@id='passport[issuedAtMonth]']");
    public By renterPassportIssuedYear = By.xpath("//input[@id='passport[issuedAtYear]']");
    public By renterPassportCode = By.xpath("//input[@id='passport[code]']");
    public By renterAddress = By.xpath("//textarea[@id='passport[registrationAddress]']");

    public By renterBankTitle = By.xpath("//input[@id='bankTitle']");
    public By renterBankAccount = By.xpath("//input[@id='bankAccountId']");
    public By renterBankCorrAccountId = By.xpath("//input[@id='bankCorrAccountId']");
    public By renterBankCorrespTitle = By.xpath("//input[@id='bankCorrespTitle']");
    public By renterBankBIK = By.xpath("//input[@id='bankBIK']");

    public By errorRenterPhone = By.xpath("//form/div[5]//div[contains(text(),'Используется другим арендатором')]");
    public By errorRenterEmail = By.xpath("//form/div[6]//div[contains(text(),'Используется другим арендатором')]");
    public By errorRenterPassportNumber = By.xpath("//form/div[8]//div[contains(text(),'Используется другим арендатором')]");

        // метод ожидания номера арендатора
        protected void renterNumberId(){

            RentersPage page = new RentersPage();

            try {
                waitBy(page.listRenterId, 10);
            } catch (Exception ex){
                afterExeptionMessage("В заданном фильтре нет подходящих арендаторов");
            }

        }

    // Счета
    public By invoiceSection = By.xpath("//a[contains(text(),'Счета')]");
    public By invoiceSectionElement = By.xpath("//h1[text()='Счета']");
    public By invoiceFromApartmentElement = By.xpath("//tbody/tr/td[1]/nobr/a[contains(text(),'250')]");
    public By invoiceFromParkingElement = By.xpath("//tbody/tr/td[1]/nobr/a[contains(text(),'A70')]");

        // метод ожидания номера счета
        protected void invoiceNumber(){

            InvoicePage page = new InvoicePage();

            try {
                waitBy(page.invoiceNumber, 10);
            } catch (Exception ex){
                afterExeptionMessage("В заданном фильтре нет подходящих счетов");
            }

        }

    // Заказы
    public By dropdownMenuServices = By.xpath("//a[@class='dropdown-toggle'][@name='qa-head-all-services']");

        // Все заказы клиентов
        public By clientServicesSection = By.xpath("//a[text()='Все заказы клиентов']");
        public By clientServicesSectionElement = By.xpath("//h1[text()='Все заказы клиентов']");

            // метод ожидания номера заказа клиента
            protected void clientServiceNumber(){

                ClientServicesPage page = new ClientServicesPage();

                try {
                    waitBy(page.serviceNumber, 10);
                } catch (Exception ex){
                    afterExeptionMessage("В заданном фильтре нет подходящих заказов");
                }

            }

        // Пропуска
        public By passesSection = By.xpath("//a[text()='Пропуска']");
        public By passesSectionElement = By.xpath("//h1[text()='Временные и разовые пропуска']");


        // Заказы ДОМ.РФ
        public By ahmlServicesSection = By.xpath("//a[text()='Заказы ДОМ.РФ']");
        public By ahmlServicesSectionElement = By.xpath("//h1[text()='Заказы ДОМ.РФ']");

            // метод ожидания номера внутреннего заказа
            protected void ahmlServiceNumber(){

                AhmlServicesPage page = new AhmlServicesPage();

                try {
                    waitBy(page.serviceNumber, 10);
                } catch (Exception ex){
                    afterExeptionMessage("В заданном фильтре нет подходящих заказов");
                }

            }

    // Уведомления
    public By dropdownMenuNotifications = By.xpath("//li[6]/a[contains(text(),'Уведомления')]");

        // Уведомления клиентов
        public By notificationsClientsSection = By.xpath("//a[text()='Уведомления клиентов']");
        public By notificationsClientSectionElement = By.xpath("//h1[text()='Уведомления']");

        // Сервисные рассылки
        public By notificationsServiceSection = By.xpath("//a[text()='Сервисные рассылки']");
        public By notificationsServiceSectionElement = By.xpath("//h1[text()='Сервисные рассылки']");

    // Сводка
    public By statisticsSection = By.xpath("//a[text()='Сводка']");
    public By statisticsSectionElement = By.xpath("//h1[text()='Сводка']");

    // Документы
    public By documentsSection = By.xpath("//a[text()='Документы']");
    public By documentsSectionElement = By.xpath("//h1[text()='Документы']");

    // Еще
    public By dropdownMenuMore = By.xpath("//a[contains(text(),'Еще')]");

        // Объявления
        public By adsSection = By.xpath("//a[text()='Объявления']");
        public By adsSectionElement = By.xpath("//h1[text()='Объявления']");

        public By addAdverttElement = By.xpath("//h1[contains(text(),'Добавление объявления')]");
        public By mainPhoneNumberInAddAdvert = By.xpath("//input[@id='input-area-0']");
        public By avitoPhoneNumberInAddAdvert = By.xpath("//input[@id='input-area-2']");
        public By cianPhoneNumberInAddAdvert = By.xpath("//input[@id='input-area-6']");
        public By yandexPhoneNumberInAddAdvert = By.xpath("//input[@id='input-area-5']");
        public By winnerPhoneNumberInAddAdvert = By.xpath("//input[@id='input-area-15']");
        public By otherPhoneNumberInAddAdvert = By.xpath("//input[@id='input-area-99999']");

        public By disableCreate = By.xpath("//button[contains(@class,'disabled')]");
        public By toastNewAdvert = By.xpath("//div[@class='toast-message'][text()='Объявление создано']");
        public By advertCardElement = By.xpath("//h1[contains(text(),'Объявление')]");

        // Акции
        public By promosSection = By.xpath("//a[text()='Акции']");
        public By promosSectionElement = By.xpath("//h1[text()='Акции от партнеров']");

        // Шаблоны
        public By templatesSection = By.xpath("//a[text()='Шаблоны']");
        public By templatesSectionElement = By.xpath("//h1[text()='Шаблоны']");

}
package pageObjects;

import org.openqa.selenium.By;

public class InvoicePage {

    // Общие
    public By toastChangePurpose = By.xpath("//div[@class='toast-message'][text()='Назначение изменено']");
    public By toastChangeSum = By.xpath("//div[@class='toast-message']");
    public By returnToastChangeSum(String string){
        return By.xpath(String.format("//div[@class='toast-message'][contains(text(),'%s')]", string));
    }
    public By toastNewComment = By.xpath("//div[@class='toast-message'][text()='Комментарий добавлен']");
    public By toastChangeStatus = By.xpath("//div[@class='toast-message'][text()='Статус успешно изменен']");

    // Страница добавления счета
    public By addInvoiceElement = By.xpath("//h1[text()='Новый счет']");
    public By dropDownRenter = By.xpath("//ol[@id='dynamic-options']/button");
    public By inputChoiseRenter = By.xpath("//input[@class='form-control']");
    public By objectNumberSelect = By.xpath("//form[@name='invoiceForm']/div[2]//select");
    public By invoicePurpose = By.xpath("//input[@name='purpose']");
    public By invoiceTypeSelect = By.xpath("//form[@name='invoiceForm']/div[4]//select");
    public By sumInvoice = By.xpath("//input[@name='sum']");
    public By periodCalendar1 = By.xpath("//input[@id='stlmDP']");
    public By periodCalendar2 = By.xpath("//input[@id='stlmDP2']");
    public By nextMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[3]");
    public By setDate1 = By.xpath("//div[@class='double-input-group']/div[1]//div[@class='moment-picker-specific-views']/table/tbody/tr[2]/td[1]");
    public By setDate2 = By.xpath("//div[@class='double-input-group']/div[2]//div[@class='moment-picker-specific-views']/table/tbody/tr[4]/td[7]");
    public By dateCreatedCalendar = By.xpath("//input[@id='formedAt']");
    public By setDateCreatedRentAndCommunal = By.xpath("//div[7]//div[@class='moment-picker top']//table/tbody/tr[3]/td[1]");
    public By setDateCreatedServicesAndOthers = By.xpath("//div[6]//div[@class='moment-picker top']//table/tbody/tr[3]/td[1]");
    public By addPhoto = By.xpath("//input[@id='input25']");
    public By deletePhoto = By.xpath("//div[1]/button//span[@title='Удалить']");
    public By createInvoice = By.xpath("//button[text()='Сформировать счет']");

    public By inputIndication_1_1 = By.xpath("//tr[2]/td[2]/input[@id='input13']");
    public By inputIndication_2_1 = By.xpath("//tr[3]/td[2]/input[@id='input13']");
    public By inputIndication_3_1 = By.xpath("//tr[5]/td[2]/input[@id='input13']");
    public By inputIndication_4_1 = By.xpath("//tr[6]/td[2]/input[@id='input13']");
    public By inputIndication_5_1 = By.xpath("//tr[8]/td[2]/input[@id='input13']");
    public By inputIndication_1_2 = By.xpath("//tr[2]/td[3]/input[@id='input3']");
    public By inputIndication_2_2 = By.xpath("//tr[3]/td[3]/input[@id='input3']");
    public By inputIndication_3_2 = By.xpath("//tr[5]/td[3]/input[@id='input3']");
    public By inputIndication_4_2 = By.xpath("//tr[6]/td[3]/input[@id='input3']");
    public By inputIndication_5_2 = By.xpath("//tr[8]/td[3]/input[@id='input3']");

    public By toastNewInvoice = By.xpath("//div[@class='toast-message'][text()='Счет успешно сформирован']");

    // Список счетов
    public By addNewInvoice = By.xpath("//a[text()='Новый счет']");
    public By statusFilter = By.xpath("//div[@class='multi-filter-wrapper ng-scope status-filter']/select");
    public By allStatuses = By.xpath("//div[@class='multi-filter-wrapper ng-scope status-filter']/select/option[1]");
    public By typeFilter = By.xpath("//div[@class='multi-filter-wrapper ng-scope type-filter']/select");
    public By delayedPaymentFrom = By.xpath("//input[@id='input15']");
    public By delayedPaymentTo = By.xpath("//input[@id='input16']");
    public By periodDiapasonElement = By.xpath("//div[@class='multiTypeDateRange multi-filter-wrapper flex-row ng-scope createdAt-filter']");

    public By calendarDateFrom = By.xpath("//input[@id='mtlmDP']");
    public By calendarDateTo = By.xpath("//input[@id='mtlmDP2']");
    public By prevMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[1]");
    public By setDateFrom = By.xpath("//div[@class='moment-picker-container month-view open']//tr[6]/td[1]");
    public By setDateTo = By.xpath("//div[@class='moment-picker-container month-view open']//tr[3]/td[1]");

    public By noInvoiceElement = By.xpath("//p[text()='Нет подходящих счетов']");

    public By invoiceNumber = By.xpath("//table[@class='table table-hover']/tbody/tr[1]/td[1]/nobr/a");
    public By typeInvoiceList = By.xpath("//tr//div[@class='type']/a");
    public By showInvoiceDetail = By.xpath("//tbody/tr[1]/td[2]/div[2]/a[contains(text(),'Детально')]");
    public By hideInvoiceDetail = By.xpath("//tbody/tr[1]/td[2]/div[2]/a[contains(text(),'Свернуть')]");
    public By coldWater = By.xpath("//tbody/tr/td[2]/div[2]/div/div[1]");
    public By hotWater = By.xpath("//tbody/tr/td[2]/div[2]/div/div[2]");
    public By electric = By.xpath("//tbody/tr/td[2]/div[2]/div/div[3]");
    public By heating = By.xpath("//tbody/tr/td[2]/div[2]/div/div[4]");
    public By wastewater = By.xpath("//tbody/tr/td[2]/div[2]/div/div[5]");
    public By heatingWater = By.xpath("//tbody/tr/td[2]/div[2]/div/div[6]");
    public By purposeInvoiceList = By.xpath("//div[@class='type']/small");
    public By sumInvoiceList = By.xpath("//div[@class='sum ng-binding']");
    public By objectList = By.xpath("//tbody/tr/td[3]/small/a");
    public By renterList = By.xpath("//tbody/tr/td[4]/small/a");
    public By renterPhoneNumberList = By.xpath("//tr[1]/td[4]/small/nobr/a");
    public By commentList = By.xpath("//tbody/tr/td[5]/small");

    public By statusListNotPaid = By.xpath("//tr/td[6]/div/span[contains(text(),'Не оплачен')]");
    public By statusListPaid = By.xpath("//tr/td[6]/div/span[contains(text(),'Оплачен')]");
    public By statusListCanceled = By.xpath("//tr/td[6]/div/span[contains(text(),'Отменен')]");
    public By statusListReturned = By.xpath("//tr/td[6]/div/span[contains(text(),'Возвращено')]");
    public By statusListToReturn = By.xpath("//tr/td[6]/div/span[contains(text(),'К возврату')]");
    public By paidFrom = By.xpath("//small/span");

    public By dropDownMenuList = By.xpath("//tr[1]/td[7]/div/a");
    public By listAddComment = By.xpath("//tr[1]/td[7]//a[contains(text(),'Добавить комментарий...')]");
    public By listChangePurpose = By.xpath("//tr[1]/td[7]//a[text()='Изменить назначение...']");
    public By listChangeSum = By.xpath("//tr[1]/td[7]//a[contains(text(),'Изменить сумму...')]");
    public By listPaid = By.xpath("//tr[1]/td[7]//a[text()='Оплачен']");
    public By listNotPaid = By.xpath("//tr[1]/td[7]//a[text()='Не оплачен']");
    public By listToReturn = By.xpath("//tr[1]/td[7]//a[text()='К возврату']");
    public By listReturned = By.xpath("//tr[1]/td[7]//a[text()='Возвращено']");
    public By listCanceled = By.xpath("//tr[1]/td[7]//a[text()='Отменен']");

    public By addCommentListModalElement = By.xpath("//div[@id='text_modal']//h4[contains(text(),'Комментарий к счету')]");
    public By changePurposeListModalElement = By.xpath("//div[@id='text_modal']//h4[contains(text(),'Назначение счета')]");
    public By textareaCommentAndPurposeModalList = By.xpath("//div[@id='text_modal']//textarea");
    public By cancelCommentAndPurposeModalList = By.xpath("//div[@id='text_modal']//a[text()='Отмена']");
    public By saveCommentAndPurposeModalList = By.xpath("//div[@id='text_modal']//a[text()='Сохранить']");

    // Карточка счета
    public By cardInvoiceElement = By.xpath("//h1[contains(text(),'Счет')]");
    public By cardChangePurpose = By.xpath("//a[text()='Изменить назначение...']");
    public By cardChangeSum = By.xpath("//a[text()='Изменить сумму...']");
    public By dropDownMenuStatusCard = By.xpath("//div/a[@data-toggle='dropdown']");
    public By cardPaid = By.xpath("//a[text()='Оплачен']");
    public By cardNotPaid = By.xpath("//a[text()='Не оплачен']");
    public By cardToReturn = By.xpath("//a[text()='К возврату']");
    public By cardReturned = By.xpath("//a[text()='Возвращено']");
    public By cardCanceled = By.xpath("//a[text()='Отменен']");

    public By changePurposeCardModalElement = By.xpath("//div[@id='purpose_modal']//h4");
    public By changeSumModalElement = By.xpath("//div[@id='payment_edit_modal']//h4");
    public By changeStatusModalElement = By.xpath("//div[@id='status_modal']//h4");
    public By textareaChangePurposeCardModal = By.xpath("//div[@id='purpose_modal']//textarea");
    public By inputChangeSumModal = By.xpath("//input[@id='input3']");
    public By cancelChangePurposeCardModal = By.xpath("//div[@id='purpose_modal']//a[text()='Отмена']");
    public By saveChangePurposeCardModal = By.xpath("//div[@id='purpose_modal']//a[text()='Сохранить']");
    public By cancelChangeSumModal = By.xpath("//div[@id='payment_edit_modal']//a[text()='Отмена']");
    public By saveChangeSumModal = By.xpath("//div[@id='payment_edit_modal']//a[text()='Сохранить']");
    public By cancelChangeStatusModal = By.xpath("//div[@id='status_modal']//a[text()='Отмена']");
    public By saveChangeStatusModal = By.xpath("//div[@id='status_modal']//a[text()='Изменить']");

    public By statusCardtNotPaid = By.xpath("//h1/small/span[contains(text(),'Не оплачен')]");
    public By statusCardPaid = By.xpath("//h1/small/span[contains(text(),'Оплачен')]");
    public By statusCardCanceled = By.xpath("//h1/small/span[contains(text(),'Отменен')]");
    public By statusCardReturned = By.xpath("//h1/small/span[contains(text(),'Возвращено')]");
    public By statusCardToReturn = By.xpath("//h1/small/span[contains(text(),'К возврату')]");

    public By textareaCommentCard = By.xpath("//div[@class='form-group']/textarea");
    public By cardAddComment = By.xpath("//button[contains(text(),'Добавить')]");

    public By renterCard = By.xpath("//div[2]/nobr");
    public By renterPhoneNumberCard = By.xpath("//small/nobr/a");
    public By objectCard = By.xpath("//div/span");
    public By typeInvoiceCard = By.xpath("//div[3]/div[2]");
    public By typeCommunalInvoiceCard = By.xpath(".//*[@id='options']/div/div/div/div[3]/div[2]");
    public By purposeInvoiceCard = By.xpath("//div[5]/div[2]");
    public By purposeCommunalInvoiceCard = By.xpath(".//*[@id='options']/div/div/div/div[6]/div[2]");
    public By sumInvoiceCard = By.xpath("//div[4]/div[2]");
    public By sumCommunalInvoiceCard = By.xpath(".//*[@id='options']/div/div/div/div[5]/div[2]");
    public By dateCreatedCard = By.xpath("//div[6]/div[2]");
    public By dateCommunalCreatedCard = By.xpath("//div[7]/div[2]");

    public By photo = By.xpath("//div[7]//div[1]/div/img");
    public By nextPhoto = By.xpath("//div[contains(@class,'handler--next')]");
    public By prevPhoto = By.xpath("//div[contains(@class,'handler--prev')]");
    public By openPhoto = By.xpath("//div[contains(@class,'tru-slider')]/img");
    public By pdf = By.xpath("//div[@class='form-file-preview ng-scope']/div/a");

    public By newCommentCard = By.xpath("//tbody/tr/td[3]");

}
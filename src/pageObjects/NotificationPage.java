package pageObjects;

import org.openqa.selenium.By;

public class NotificationPage {

    // Общие
    public By closeModal = By.xpath("//a[text()='Отмена']");

    // Список уведомлений
    public By addNotification = By.xpath("//a[text()='Новое уведомление...']");

    public By notificationNumber = By.xpath("//tbody/tr[1]/td[1]/a");

    public By notificationSendingToast = By.xpath("//div[@aria-label='Уведомление отправляется'][text()='Уведомление отправляется']");
    public By notificationSavingToast = By.xpath("//div[@aria-label='Уведомление сохранено'][text()='Уведомление сохранено']");
    public By notificationDeleteToast = By.xpath("//div[@aria-label='Уведомление удалено'][text()='Уведомление удалено']");
    public By noTextToast = By.xpath("//div[@aria-label='Нет текста уведомления'][text()='Нет текста уведомления']");
    public By noRecipientToast = By.xpath("//div[@aria-label='Не указано ни одного получателя'][text()='Не указано ни одного получателя']");

    // Страница добавления уведомления
    public By newNotificationElement = By.xpath("//h1[contains(text(),'Новое уведомление')]");
    public By sms = By.xpath("//label[text()='СМС']");
    public By push = By.xpath("//label[text()='Пуш']");
    public By email = By.xpath("//label[contains(text(),'почта')]");
    public By textarea = By.xpath("//textarea");
    public By maxSimbolSMS = By.xpath("//div[text()='Максимум 600 символов']");
    public By maxSimbolPush = By.xpath("//div[text()='Максимум 110 символов']");
    public By addRecipients = By.xpath("//a[contains(text(),'Добавить...')]");
    public By sendNotification = By.xpath("//button[contains(text(),'Отправить')]");
    public By saveByDraft = By.xpath("//button[text()='Сохранить как черновик']");

    public By chooseRecipientsModal = By.xpath("//h4[text()='Выбор получателей']");
    public By saveModal = By.xpath("//a[text()='Сохранить']");
    public By allCheckboxes = By.xpath("//th[1]//input[@type='checkbox']");
    public By oneCheckbox = By.xpath("//td/div/label/input");
    public By filterModal = By.xpath("//input[@id='search']");
    public By changeRecipients = By.xpath("//a[contains(text(),' Изменить...')]");

    // Карточка уведомления
    public By notificationCardElement = By.xpath("//h1[contains(text(),'Уведомление')]");
    public By deleteNotification = By.xpath("//a[text()='Удалить...']");
    public By deleteModalElement = By.xpath("//h4[text()='Удаление уведомления']");
    public By deleteConfirm = By.xpath("//a[text()='Удалить']");

    public By typeNotification = By.xpath("//div[3]/div[2]");
    public By textNotification = By.xpath("//div[4]/div[2]");
    public By recipientName = By.xpath("//nobr[1]");
    public By recipientPhone = By.xpath("//nobr[2]");
    public By sendNotificationFromCard = By.xpath("//a[text()='Отправить']");

    // Страница редактирования уведомления
    public By editNotificationElement = By.xpath("//h1[contains(text(),'Уведомление №')]");
    public By editNotification = By.xpath("//a[text()='Редактировать...']");

}
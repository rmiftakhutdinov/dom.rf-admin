package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.io.File;
import java.util.Scanner;

public class ObjectsPage {

    // Список объектов
    public By apartmentNumber = By.xpath("//td/a");
    public By parkingNumber = By.xpath("//tr[1]/td[2]/a");

    public By parkingsListElement = By.xpath("//th[text()='Арендатор']");
    public By apartmentsListElement = By.xpath("//th[text()='Арендатор, автор брони']");
    public By listSafeObjectNumber = By.xpath("//tbody/tr/td[2]");

    private String apartment() {
        String apartment = null;
        try {
            String txt = "files\\txtFiles\\objects\\textApartment.txt";
            File file = new File(txt);
            Scanner scanner = new Scanner(file);
            apartment = scanner.nextLine();
        } catch (Exception ex) {
            throw new RuntimeException("Путь к файлу с номером апартамента не найден");
        }
        return apartment;
    }

    public By listApartmentFloor = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[1]");
    public By listApartmentRooms = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[2]");
    public By listApartmentAreaTotal = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[3]");
    public By listApartmentViewWindow = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[4]");
    public By listApartmentDecoration = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[5]");
    public By listApartmentPrice = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[6]");
    public By listApartmentRenter = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[7]");
    public By listApartmentStatusFree = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[8]");
    public By listApartmentDropDownMenu = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[9]/div/a");

    private String parking() {
        String parking = null;
        try {
            String txt = "files\\txtFiles\\objects\\textParking.txt";
            File file = new File(txt);
            Scanner scanner = new Scanner(file);
            parking = scanner.nextLine();
        } catch (Exception ex) {
            throw new RuntimeException("Путь к файлу с номером машиноместа не найден");
        }
        return parking;
    }

    public By listParkingAreaTotal = By.xpath("//a[text()='"+ "A" + parking() +"']/parent::td/following-sibling::td[1]");
    public By listParkingPrice = By.xpath("//a[text()='"+ "A" + parking() +"']/parent::td/following-sibling::td[2]");
    public By listParkingRenter = By.xpath("//a[text()='"+ "A" + parking() +"']/parent::td/following-sibling::td[3]/div/span");
    public By listParkingStatusFree = By.xpath("//a[text()='"+ "A" + parking() +"']/parent::td/following-sibling::td[4]");
    public By listParkingDropDownMenu = By.xpath("//a[text()='"+ "A" + parking() +"']/parent::td/following-sibling::td[5]/div/a");

    public By searchNumberParking(String s){
        return By.xpath("//tbody/tr/td[2]/a[text()='" + "A" + s + "']");
    }

    public By openDropDownMenu = By.xpath("//div/a[@data-toggle='dropdown']");
    public By dropDownMenuOpenApartment = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[9]//a[text()='Открыть']");
    public By dropDownMenuEditApartment = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[9]//a[text()='Редактировать...']");
    public By dropDownMenuSettleRenter = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[9]//a[text()='Заселить...']");
    public By dropDownMenuCopyApartment = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[9]//a[text()='Копировать']");
    public By dropDownMenuAddAdvert = By.xpath("//a[text()='"+ apartment() +"']/parent::td/following-sibling::td[9]//a[text()='Создать объявление...']");

    public By dropDownMenuOpenParking = By.xpath("//a[text()='"+ "A" + parking() +"']/parent::td/following-sibling::td[5]//a[text()='Открыть']");
    public By dropDownMenuEditParking = By.xpath("//a[text()='"+ "A" + parking() +"']/parent::td/following-sibling::td[5]//a[text()='Редактировать...']");
    public By dropDownMenuLeaseObject = By.xpath("//a[text()='"+ "A" + parking() +"']/parent::td/following-sibling::td[5]//a[text()='Сдать...']");
    public By dropDownMenuCopyParking = By.xpath("//a[text()='"+ "A" + parking() +"']/parent::td/following-sibling::td[5]//a[text()='Копировать']");

    public By filterFloors = By.xpath("//div[contains(@class,'floor-filter')]/select");
    public By filterRooms = By.xpath("//div[contains(@class,'room-filter')]/select");
    public By filterDecoration = By.xpath("//div[contains(@class,'decoration-filter')]/select");
    public By filterStatus = By.xpath("//div[contains(@class,'status-filter')]/select");
    public By filterOrderStatus = By.xpath("//div[contains(@class,'requestStatus-filter')]/select");
    public By filterDate = By.xpath("//div[@class='input-group-btn dropdown']/button[contains(text(),'За все время')]");

    public By periodFilterStatusRent = By.xpath("//div[7]/div/div[@class='input-group-btn dropdown']/button");
    public By periodFilterStatusReserved = By.xpath("//div[6]/div/div[@class='input-group-btn dropdown']/button");
    public By periodFilterStatusPaused = By.xpath("//div[8]/div/div[@class='input-group-btn dropdown']/button");
    public By periodFilterStatusTermination = By.xpath("//div[9]/div/div[@class='input-group-btn dropdown']/button");

    // Страница добавления объекта
    public By addNewObjectElement = By.xpath("//h1[contains(text(),'Добавление объекта')]");

    public By objectParking = By.xpath("//label[text()='Машиноместо']");
    public By objectApartment = By.xpath("//label[text()='Апартамент']");
    public By objectParkingElement = By.xpath("//label[text()='Номер машиноместа']");
    public By objectApartmentElement = By.xpath("//label[text()='Номер апартамента']");
    public By changeTypeObject = By.xpath("//select[@ng-model='realtyType']");

    public By numberObject = By.xpath("//input[@id='input3']");
    public By price = By.xpath("//input[@id='input4']");
    public By deposit = By.xpath("//input[@name='deposit']");
    public By lease = By.xpath("//input[@name='lease']");

    public By floor = By.xpath("//input[@id='input9']");
    public By room0 = By.xpath("//div[@ng-show='!preloader']//div[@class='col-sm-10']/div[@class='btn-group']/label[1]");
    public By room1 = By.xpath("//div[@ng-show='!preloader']//div[@class='col-sm-10']/div[@class='btn-group']/label[2]");
    public By room2 = By.xpath("//div[@ng-show='!preloader']//div[@class='col-sm-10']/div[@class='btn-group']/label[3]");
    public By room3 = By.xpath("//div[@ng-show='!preloader']//div[@class='col-sm-10']/div[@class='btn-group']/label[4]");
    public By room4 = By.xpath("//div[@ng-show='!preloader']//div[@class='col-sm-10']/div[@class='btn-group']/label[5]");
    public By room5 = By.xpath("//div[@ng-show='!preloader']//div[@class='col-sm-10']/div[@class='btn-group']/label[6]");
    public By room6 = By.xpath("//div[@ng-show='!preloader']//div[@class='col-sm-10']/div[@class='btn-group']/label[7]");
    public By room7 = By.xpath("//div[@ng-show='!preloader']//div[@class='col-sm-10']/div[@class='btn-group']/label[8]");
    public By areaTotal = By.xpath("//input[@id='input13']");
    public By areaKitchen = By.xpath("//input[@name='areaKitchen']");
    public By areaLiving = By.xpath("//input[@name='areaLiving']");
    public By areaRooms1 = By.xpath("//div[@ng-show='obj.room']//span[1]/input");
    public By areaRooms2 = By.xpath("//div[@ng-show='obj.room']//span[2]/input");
    public By areaRooms3 = By.xpath("//div[@ng-show='obj.room']//span[3]/input");
    public By areaRooms4 = By.xpath("//div[@ng-show='obj.room']//span[4]/input");
    public By areaRooms5 = By.xpath("//div[@ng-show='obj.room']//span[5]/input");
    public By areaRooms6 = By.xpath("//div[@ng-show='obj.room']//span[6]/input");
    public By areaRooms7 = By.xpath("//div[@ng-show='obj.room']//span[7]/input");

    public By decorationBrown = By.xpath("//label[text()='Бежевый']");
    public By decorationWhite = By.xpath("//label[text()='Белый']");
    public By furnitureNo = By.xpath("//label[contains(text(),'Без')]");
    public By furnitureRussian = By.xpath("//label[text()='Отечественная']");
    public By furnitureIKEA = By.xpath("//label[text()='Икеа']");
    public By furnitureElite = By.xpath("//label[text()='Элитная']");
    public By repair = By.xpath("//label[text()='Обычный']");
    public By repairRussian = By.xpath("//label[text()='Советский']");
    public By repairEuro = By.xpath("//label[text()='Евроремонт']");
    public By repairElite = By.xpath("//label[text()='Элитный']");
    public By repairNeeded = By.xpath("//label[contains(text(),'Требует')]");
    public By balcony1 = By.xpath("//div[@class='btn-toolbar']/div[1]//label[text()='Балкон']");
    public By balcony2 = By.xpath("//div[@class='btn-toolbar']/div[2]//label[text()='Балкон']");
    public By balcony3 = By.xpath("//div[@class='btn-toolbar']/div[3]//label[text()='Балкон']");
    public By loggia1 = By.xpath("//div[@class='btn-toolbar']/div[1]//label[text()='Лоджия']");
    public By loggia2 = By.xpath("//div[@class='btn-toolbar']/div[2]//label[text()='Лоджия']");
    public By balconyNo1 = By.xpath("//div[@class='btn-toolbar']/div[1]//label[text()='Нет']");
    public By balconyNo2 = By.xpath("//div[@class='btn-toolbar']/div[2]//label[text()='Нет']");
    public By addBalcony = By.xpath("//a[contains(@ng-click,'balconies')][text()='Добавить']");
    public By deleteBalcony = By.xpath("//div[@class='btn-toolbar']/div[2]//a[contains(@ng-click,'balconies')][text()='Удалить']");
    public By bathroomCombined1 = By.xpath("//div[@class='btn-toolbar']/div[1]//label[text()='Совмещенный']");
    public By bathroomCombined2 = By.xpath("//div[@class='btn-toolbar']/div[2]//label[text()='Совмещенный']");
    public By bathroomCombined3 = By.xpath("//div[@class='btn-toolbar']/div[3]//label[text()='Совмещенный']");
    public By bathroomSeparated1 = By.xpath("//div[@class='btn-toolbar']/div[1]//label[text()='Раздельный']");
    public By bathroomSeparated2 = By.xpath("//div[@class='btn-toolbar']/div[2]//label[text()='Раздельный']");
    public By addBathroom = By.xpath("//a[contains(@ng-click,'wcs')][text()='Добавить']");
    public By deleteBathroom = By.xpath("//div[@class='btn-toolbar']/div[2]//a[contains(@ng-click,'wcs')][text()='Удалить']");
    public By viewWindowOnStreet = By.xpath("//label[1][contains(text(),'улицу')]");
    public By viewWindowOnCourtyard = By.xpath("//label[2][contains(text(),'двор')]");
    public By viewWindowOnBoth = By.xpath("//label[3][contains(text(),'На улицу')]");

    public By passportOfApartmentRus = By.xpath("//div[@id='ru_p']//div[contains(@id,'taTextElement')]");
    public By passportOfApartmentEng = By.xpath("//div[@id='en_p']//div[contains(@id,'taTextElement')]");
    public By changeLangToEngOfPassport = By.xpath("//a[@aria-controls='en_p']");
    public By changeLangToRusOfPassport = By.xpath("//a[@aria-controls='ru_p']");

    public By descriptionRus = By.xpath("//textarea[@ng-model='obj.description.ru']");
    public By descriptionEng = By.xpath("//textarea[@ng-model='obj.description.en']");
    public By changeLangToEngOfDescription = By.xpath("//a[@aria-controls='en_d']");
    public By changeLangToRusOfDescription = By.xpath("//a[@aria-controls='ru_d']");

    public By tour3D = By.xpath("//input[@id='input24']");

    public By addPhoto = By.xpath("//input[@id='input25']");
    public By dropDownMenuPhoto1 = By.xpath("//div[@class='form-file-preview ng-scope ng-isolate-scope']/div[1]/select");
    public By dropDownMenuPhoto2 = By.xpath("//div[@class='form-file-preview ng-scope ng-isolate-scope']/div[2]/select");
    public By dropDownMenuPhoto3 = By.xpath("//div[@class='form-file-preview ng-scope ng-isolate-scope']/div[3]/select");

    public By errorNumberApartment = By.xpath("//div[contains(text(),'Апартамент с таким номером')]");
    public By errorNumberParking = By.xpath("//div[contains(text(),'Машиноместо с таким номером')]");

    // Карточка объекта
    public By safeComplex = By.xpath("//div[text()='Лайнер']");
    public By safeAddress = By.xpath("//div[@id='options']//div[3]/div[@class='col-sm-10 ng-binding']");
    public By safeObjectNumber = By.xpath("//div[@id='options']//div[4]/div[@class='col-sm-10 ng-binding']");
    public By safeApartmentPrice = By.xpath("//div[@ng-if='realty.price']");
    public By safeDeposit = By.xpath("//div[@ng-if='realty.deposit']");
    public By safeLease = By.xpath("//div[@id='options']//div[7]/div[@class='col-sm-10 ng-binding']");
    public By safeNoAnimals = By.xpath("//div[text()='Без животных']");
    public By safeNoSmoking = By.xpath("//div[text()='Нельзя курить']");
    public By safeNoChildren = By.xpath("//div[text()='Без детей']");
    public By safeFloor = By.xpath("//div[@id='options']//div[9]/div[@class='col-sm-10 ng-binding']");
    public By safeRooms = By.xpath("//div[@id='options']//div[10]/div[@class='col-sm-10 ng-binding']");
    public By safeApartmentAreaTotal = By.xpath("//div[@id='options']//div[11]//div/div[1]/div[@class='col-sm-11 ng-binding']");
    public By safeAreaKitchen = By.xpath("//div[@id='options']//div[11]//div/div[2]/div[@class='col-sm-11 ng-binding']");
    public By safeAreaLiving = By.xpath("//div[@id='options']//div[11]//div/div[3]/div[@class='col-sm-11 ng-binding']");
    public By safeAreaRoom1 = By.xpath("//div[@ng-if='realty.areaRooms.length']/div[2]/span[1]");
    public By safeAreaRoom2 = By.xpath("//div[@ng-if='realty.areaRooms.length']/div[2]/span[2]");
    public By safeAreaRoom3 = By.xpath("//div[@ng-if='realty.areaRooms.length']/div[2]/span[3]");
    public By safeDecoration = By.xpath("//div[@id='options']//div[12]/div[@class='col-sm-10 ng-binding']");
    public By safeFurniture = By.xpath("//div[@id='options']//div[13]/div[@class='col-sm-10 ng-binding']");
    public By safeRepair = By.xpath("//div[@id='options']//div[14]/div[@class='col-sm-10 ng-binding']");
    public By safeBalcony = By.xpath("//div[@id='options']//div[15]/div[2]");
    public By safeBathroom = By.xpath("//div[@id='options']//div[16]/div[2]");
    public By safeViewWindow = By.xpath("//div[@id='options']//div[17]/div[@class='col-sm-10 ng-binding']");
    public By safeTV = By.xpath("//div[text()='Телевидение']");
    public By safeRefriegerator = By.xpath("//div[text()='Холодильник']");
    public By safeWasher = By.xpath("//div[text()='Стиральная машина']");
    public By safeInternet = By.xpath("//div[text()='Интернет']");
    public By safeDishwasher = By.xpath("//div[text()='Посудомоечная машина']");
    public By safeAppliances = By.xpath("//div[text()='Бытовая техника']");
    public By safeAirConditioning = By.xpath("//div[text()='Кондиционер']");
    public By safePanoramicWindows = By.xpath("//div[text()='Панорамное остекление']");
    public By safePassportOfApartment = By.xpath("//div[@id='options']//div[19]/div[@class='col-sm-10 ng-binding ng-scope']");
    public By safeDescription = By.xpath("//div[@id='options']//div[20]/div[@class='col-sm-10 ng-binding ng-scope']");
    public By safeTour3D = By.xpath("//div[@ng-if='realty.tour']");
    public By safeNamePhoto1 = By.xpath("//div[text()='Холл']");
    public By safeNamePhoto2 = By.xpath("//div[text()='Кухня']");

    public By safeParkingPrice = By.xpath("//div[@ng-if='parking.price']");
    public By safeParkingAreaTotal = By.xpath("//div[@id='options']//div[6]/div[@class='col-sm-10 ng-binding']");

    public By safeEditRequirementsToRenter = By.xpath("//div[@id='options']//div[8]/div[@class='col-sm-10 ng-scope']");
    public By safeEditFacilities = By.xpath("//div[@id='options']//div[18]/div[@class='col-sm-10 ng-scope']");
    public By safeEditNamePhoto1 = By.xpath("//div[text()='Кухня']");
    public By safeEditNamePhoto2 = By.xpath("//div[text()='Балкон']");
    public By safeEditNamePhoto3 = By.xpath("//div[text()='Холл']");

    public By toastNewObject = By.xpath("//div[@class='toast-message'][text()='Объект создан']");
    public By toastSaveObject = By.xpath("//div[@class='toast-message'][text()='Изменения сохранены']");
    public By toastPauseContract = By.xpath("//div[@class='toast-message'][text()='Действие договора приостановлено']");
    public By toastResumeContract = By.xpath("//div[@class='toast-message'][text()='Действие договора возобновлено']");

    public By renterOfApartment = By.xpath("//a[contains(text(),'Два апартамента')]");
    public By renterOfParking = By.xpath("//a[contains(text(),'Арендатор машиноместа')]");

    public By photo = By.xpath("//div[@class='form-file-preview ng-scope']/div[1]/div[@class='item-preview-inner']");
    public By nextPhoto = By.xpath("//div[contains(@class,'handler--next')]");
    public By prevPhoto = By.xpath("//div[contains(@class,'handler--prev')]");
    public By openPhoto = By.xpath("//div[contains(@class,'tru-slider')]/img");

    public By history = By.xpath("//a[text()='История']");
    public By historyElement = By.xpath("//li[@class='active']/a[text()='История']");
    public By orders = By.xpath("//li[@role='presentation']/a[text()='Заявки']");
    public By ordersElement = By.xpath("//li[@class='active']/a[text()='Заявки']");
    public By contracts = By.xpath("//a[text()='Договоры']");
    public By contractsElement = By.xpath("//li[@class='active']/a[text()='Договоры']");
    public By orderInOrders = By.xpath("//nobr[contains(text(),'Тест')]");
    public By renterInContractOfApartment = By.xpath("//nobr[contains(text(),'Два апартамента')]");
    public By renterInContractOfParking = By.xpath("//nobr[contains(text(),'Арендатор машиноместа')]");
    public By showPhotos = By.xpath("//tr[1]//a[contains(@ng-click,'item.showPhotos')]");
    public By photoInContracts = By.xpath("//div[@class='row files-row']/div[1]/a");

    public By changeStatus = By.xpath("//a[contains(text(),'Изменить статус')]");
    public By statusRented = By.xpath("//b[text()='Арендован']");
    public By statusNotActive = By.xpath("//b[text()='Неактивен']");
    public By statusReserved = By.xpath("//b[text()='Забронирован']");
    public By orderNumberInChangeStatusModal = By.xpath("//input[@ng-model='status.requestId']");
    public By calendarInChangeStatusModal = By.xpath("//input[@id='orderDP']");
    public By dateInChangeStatusModal = By.xpath("//div[@class='moment-picker-container month-view open']//tbody/tr[6]/td[1]");
    public By save = By.xpath("//a[text()='Сохранить']");

    public By dropDownMenuInContracts = By.xpath("//a[@class='btn btn-default dropdown-toggle']/span");
    public By finishContract = By.xpath("//a[text()='Завершить...']");
    public By editContract = By.xpath("//ul[@class='dropdown-menu']//a[contains(text(),'Редактировать')]");
    public By pauseContract = By.xpath("//a[text()='Приостановить...']");
    public By resumeContract = By.xpath("//a[text()='Возобновить...']");
    public By generateCommunalInvoice = By.xpath("//a[contains(text(),'Запрос выездного счета')]");
    public By finishContractConfirm = By.xpath("//a[text()='Завершить']");
    public By draft = By.xpath("//tr[1]/td[1]/div/span[contains(text(),'Черновик')]");
    public By deleteDraft = By.xpath("//a[contains(text(),'Удалить')]");
    public By confirmDeleteDraftModal = By.xpath("//div[@id='contract_delete_modal']/div/form/div/a[text()='Удалить']");

    public By modalPauseContractElement = By.xpath("//h4[text()='Приостановка действия договора']");
    public By modalResumeContractElement = By.xpath("//h4[text()='Возобновление действия договора']");
    public By inputIndicationModal = By.xpath("//div[1]/div[2]/input[@id='input13']");
    public By pauseContractConfirm = By.xpath("//div[@class='modal-footer']/a[text()='Приостановить']");
    public By resumeContractConfirm = By.xpath("//div[@class='modal-footer']/a[text()='Возобновить']");
    public By deleteFileInModal = By.xpath("//p[1]//i[@class='fa fa-close']");

    public By labelStatusRentedApartment = By.xpath("//span[@class='label label-success ng-binding'][contains(text(),'Арендован')]");
    public By labelStatusPausedApartment = By.xpath("//span[@class='label label-success ng-binding'][contains(text(),'Приостановлен')]");
    public By labelStatusNotActiveApartment = By.xpath("//span[@class='label label-success ng-binding'][contains(text(),'Неактивен')]");
    public By labelStatusReservedApartment = By.xpath("//span[@class='label label-success ng-binding'][contains(text(),'Забронирован')]");
    public By labelStatusRentedParking = By.xpath("//span[@class='label ng-binding'][contains(text(),'Арендован')]");
    public By labelStatusFreeApartment = By.xpath("//span[@class='label label-success ng-binding'][contains(text(),'Свободен')]");
    public By labelStatusFreeParking = By.xpath("//span[@class='label ng-binding'][contains(text(),'Свободен')]");

    public By ejectConfirm = By.xpath("//a[text()='Выселить']");
    public By settleModal = By.xpath("//div[@id='cant_moveout_modal']/div[@class='modal-dialog modal-sm']");
    public By communalModal = By.xpath("//div[@id='payments_modal']/div[@class='modal-dialog modal-md']");
    public By checkboxCall = By.xpath("//div[@class='checkbox']");
    public By nextStepModal = By.xpath("//a[text()='Далее']");
    public By generate = By.xpath("//div[@id='payments_modal']//a[text()='Отправить']");
    public By checkboxCleaning = By.xpath("//input[@type='checkbox']");
    public By onCleaningText = By.xpath("//span[contains(text(),'Дополнительно арендатору будет сформирован счет за генеральную уборку')]");
    public By offCleaningText = By.xpath("//span[contains(text(),'Счет за генеральную уборку сформирован не будет')]");
    public By closeModal = By.xpath("//div[@id='payments_modal']//a[text()='Закрыть']");
    public By toastGenerateDepartureInvoice = By.xpath("//div[@id='toast-container']//div[text()='Запрос на формирование выездного счета успешно отправлен']");

    // Страница редактирования объекта
    public By editObject = By.xpath("//a[contains(text(),' Редактировать')]");
    public By editObjectElement = By.xpath("//h1[contains(text(),'Редактирование объекта')]");
    public By deletePhoto1 = By.xpath("//div[@class='form-file-preview ng-scope ng-isolate-scope']/div[1]/button");

    // Страница заселения/сдачи
    public By settlementElement = By.xpath("//h1[contains(text(),'Заселение')]");

    public By settlePrice = By.xpath("//input[@id='amount']");
    public By addRoomer = By.xpath("//button[contains(text(),'Добавить жильца')]");
    public By roomer1Element = By.xpath("//h4[text()='Жилец 1']");
    public By roomer2Element = By.xpath("//h4[text()='Жилец 2']");
    public By deleteRoomer2 = By.xpath("//div[11]/div[3]//a[@data-target='#del_user']");
    public By deleteRoomerModalConfirm = By.xpath("//a[contains(text(),'жильца')]");
    public By deleteRoomerModalCancel = By.xpath("//a[text()='Отменить']");

    public By roomerFullName = By.xpath("//input[@ng-model='roomer.fullName']");
    public By bornedAtDay = By.xpath("//div[@id='roomers[0][bornedAt]']/input[1]");
    public By bornedAtMonth = By.xpath("//div[@id='roomers[0][bornedAt]']/input[2]");
    public By bornedAtYear = By.xpath("//div[@id='roomers[0][bornedAt]']/input[3]");
    public By roomerPhone = By.xpath("//input[@ng-model='roomer.phone']");
    public By roomerEmail = By.xpath("//input[@ng-model='roomer.email']");
    public By roomerPassportSeriesNumber = By.xpath("//input[@ng-model='roomer.passport.seriesNumber']");
    public By roomerPassportIssued = By.xpath("//textarea[@ng-model='roomer.passport.issued']");
    public By roomerIssuedDay = By.xpath("//div[@id='roomers[0][passport][issuedAt]']/input[1]");
    public By roomerIssuedMonth = By.xpath("//div[@id='roomers[0][passport][issuedAt]']/input[2]");
    public By roomerIssuedYear = By.xpath("//div[@id='roomers[0][passport][issuedAt]']/input[3]");
    public By roomerPassportCode = By.xpath("//input[@ng-model='roomer.passport.code']");

    public By priceInGraph = By.xpath("//tbody/tr[4]//input[@id='input13']");
    public By checkboxInGraph1 = By.xpath("//tbody/tr[4]//input[@type='checkbox']");
    public By checkboxInGraph2 = By.xpath("//tbody/tr[5]//input[@type='checkbox']");
    public By statusInGraph = By.xpath("//tbody/tr[5]//select");

    public By livingWithPets = By.xpath("//input[@id='withPets']");
    public By keys = By.xpath("//input[@ng-model='form.keysQuantity']");
    public By passes = By.xpath("//input[@ng-model='form.passQuantity']");

    public By leaseElement = By.xpath("//h1[contains(text(),'Сдача машиноместа')]");

    public By dropDownRenter = By.xpath("//ol[@id='dynamic-options']/button");
    public By renterInput = By.xpath("//button/span/span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]");
    public By inputChoiseRenter = By.xpath("//input[@class='form-control']");
    public By calendar = By.xpath("//input[@id='stlmDP2']");
    public By nextMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[3]");
    public By prevMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[1]");
    public By setDate = By.xpath("//div[@class='double-input-group']/div[2]//div[@class='moment-picker-specific-views']/table/tbody/tr[2]/td[1]");
    public By setEditDate = By.xpath("//div[@class='double-input-group']/div[2]//div[@class='moment-picker-specific-views']/table/tbody/tr[1]/td[1]");
    public By inputIndication1 = By.xpath("//input[@id='input13_0']");
    public By inputIndication2 = By.xpath("//input[@id='input13_1']");
    public By inputIndication3 = By.xpath("//input[@id='input13_2']");
    public By inputIndication4 = By.xpath("//input[@id='input13_3']");
    public By inputIndication5 = By.xpath("//input[@id='input13_4']");
    public By indication = By.xpath("//button[@title='Скопировать в поле ввода']");

    public By addPeriod = By.xpath("//a[text()='Добавить']");

    public By nextStep = By.xpath("//button[contains(text(),'Далее')]");
    public By nextStepApartmentElement = By.xpath("//h1[contains(text(),'Подтверждение заселения')]");
    public By nextStepParkingElement = By.xpath("//h1[contains(text(),'Подтверждение сдачи')]");

    public By step2Renter = By.xpath("//form[@name='contractForm']/div[1]/div[contains(@class,'col-sm-10')]");
    public By step2Adress = By.xpath("//form[@name='contractForm']/div[2]/div[contains(@class,'col-sm-10')]");
    public By step2NumberOfApartment = By.xpath("//form[@name='contractForm']/div[3]/div[contains(@class,'col-sm-10')]");
    public By step2SettlePrice = By.xpath("//form[@name='contractForm']/div[5]//div/span[2]");
    public By step2DateOfSigning = By.xpath("//form[@name='contractForm']/div[4]/div[contains(@class,'col-sm-10')]");
    public By step2DateOfEndRent = By.xpath("//form[@name='contractForm']/div[5]//div/span[1]");
    public By step2PriceInGraph = By.xpath("//tbody/tr[4]/td[3]/div");
    public By step2Indication1 = By.xpath("//div[@class='col-sm-10 ng-scope']/div[1]/div[2]/span");
    public By step2Indication2 = By.xpath("//div[@class='col-sm-10 ng-scope']/div[2]/div[2]/span");
    public By step2Indication3 = By.xpath("//div[@class='col-sm-10 ng-scope']/div[3]/div[2]/span");
    public By step2Indication4 = By.xpath("//div[@class='col-sm-10 ng-scope']/div[4]/div[2]/span");
    public By step2Indication5 = By.xpath("//div[@class='col-sm-10 ng-scope']/div[5]/div[2]/span");
    public By step2RoomerFullName = By.xpath("//div[@ng-switch='flags.confirmMode']/div[2]/div[contains(@class,'col-sm-10')]");
    public By step2RoomerBornedAt = By.xpath("//div[@ng-switch='flags.confirmMode']/div[3]/div[contains(@class,'col-sm-10')]");
    public By step2RoomerPhone = By.xpath("//div[@ng-switch='flags.confirmMode']/div[4]/div[contains(@class,'col-sm-10')]");
    public By step2RoomerEmail = By.xpath("//div[@ng-switch='flags.confirmMode']/div[5]/div[contains(@class,'col-sm-10')]");
    public By step2RoomerPassportSeriesNumber = By.xpath("//div[@ng-switch='flags.confirmMode']/div[7]/div[contains(@class,'col-sm-10')]");
    public By step2RoomerPassportIssued = By.xpath("//div[@ng-switch='flags.confirmMode']/div[8]/div[contains(@class,'col-sm-10')]");
    //public By step2RoomerIssuedDate = By.xpath("//div[@ng-switch='flags.confirmMode']/div[9]/div[contains(@class,'col-sm-10')]");
    public By step2RoomerIssuedDate = By.xpath("html/body/div[1]/div[2]/div[2]/div/div/form/div[10]/div[2]/div/div[9]/div[2]");
    public By step2RoomerPassportCode = By.xpath("//div[@ng-switch='flags.confirmMode']/div[10]/div[contains(@class,'col-sm-10')]");
    public By step2LivingWithPets = By.xpath("//span[text()='Проживание с животными']");
    public By step2Keys = By.xpath("//form/div[10]/div[4]/div[2]");
    public By step2Passes = By.xpath("//form/div[10]/div[5]/div[2]");

    public By backToStep1 = By.xpath("//a[contains(text(),'Назад')]");

    public By editContractApartmentElement = By.xpath("//h1[contains(text(),'Договор №250')]");
    public By editContractParkingElement = By.xpath("//h1[contains(text(),'Договор №A70')]");

    public By editPlanOfDepartureDate = By.xpath("//input[@id='stlmDP3']");
    public By editSetPlanOfDepartureDate = By.xpath("//div[@class='moment-picker-container month-view open']//tbody/tr[2]/td[1]");
    public By linkToInvoiceList = By.xpath("//p/a[@ui-sref='dashboard.invoiceList']");
    public By linkToInvoice = By.xpath("//tr[1]/td[1]/div/a");
    public By editPriceInGraph = By.xpath("//tbody/tr[6]//input[@id='input13']");
    public By editCheckboxInGraph = By.xpath("//tbody/tr[6]//input[@type='checkbox']");
    public By editStatusInGraph = By.xpath("//tbody/tr[6]//select");
    public By editSafeStatusInGraph = By.xpath("//tr[6]/td[3]/span/input");
    public By editDeletePhoto1 = By.xpath("//div[@class='form-file-preview ng-scope ng-isolate-scope']/div[1]/button");
    public By editDeletePhoto2 = By.xpath("//div[@class='form-file-preview ng-scope ng-isolate-scope']/div[2]/button");
    public By editSaveModalCancel = By.xpath("//a[text()='Отмена']");
    public By editSaveModalConfirm = By.xpath("//a[text()='Сохранить']");

    public By settleApartmentModal = By.xpath("//h4[text()='Заселение арендатора']");
    public By settleConfirm = By.xpath("//a[contains(text(),'Заселить')]");

    public By rentParkingModal = By.xpath("//h4[text()='Сдача машиноместа']");
    public By rentConfirm = By.xpath("//a[contains(text(),'Сдать')]");

    public By spinnerInGraph = By.xpath(".//*[@id='schedule__overlay']/i");

}
package pageObjects;

import org.openqa.selenium.By;

public class OrdersPage {

    // Список заявок
    public By orderNumber = By.xpath("//tr[1]/td[2]/a");
    public By listSafeOrderNumber = By.xpath("//tbody/tr/td[2]/a");
    public By listSafeClientFullName = By.xpath("//tbody/tr/td[3]/a");
    public By listSafeClientPhone = By.xpath("//tbody/tr/td[3]/nobr");
    public By listSafeApartmentType = By.xpath("//tbody/tr/td[4]/span");
    public By listSafeApartmentNumber = By.xpath("//tr//a[contains(@href,'/admin/realty-card/apartment/')]");
    public By listSafeComments = By.xpath("//tbody/tr/td[6]/em/small[text()='Этот комментарий должен быть виден в списке заявок и в карточке заявки']");
    public By listSafeNewComment = By.xpath("//tbody/tr/td[6]/em/small[text()='Комментарий, добавленный в карточке заявки']");
    public By listStatusLabelNewOrder = By.xpath("//tr/td[7]/span[text()='Новая']");
    public By listStatusLabelWaitingList = By.xpath("//tr/td[7]/span[text()='Лист ожидания']");
    public By listStatusLabelInvitedToShow = By.xpath("//tr/td[7]/span[text()='Приглашён на просмотр']");
    public By listStatusLabelCollectionDocuments = By.xpath("//tr/td[7]/span[text()='Сбор документов']");
    public By listStatusLabelThinks = By.xpath("//tr/td[7]/span[text()='Думает']");
    public By listStatusLabelSignedBA = By.xpath("//tr/td[7]/span[text()='Подписан ДБ']");
    public By listStatusLabelSignedLease = By.xpath("//tr/td[7]/span[text()='Подписан ДА']");
    public By listStatusLabelSecurity = By.xpath("//tr/td[7]/span[text()='Проверка СБ']");
    public By listStatusLabelNegative = By.xpath("//tr/td[7]/span[text()='Отказ']");

    public By listStatusText = By.xpath("//tbody/tr[1]/td[7]/small");
    public By listStatusTextAssignedRecall = By.xpath("//tbody/tr[1]/td[7]/small[1]");
    public By listStatusTextAssignedSettlement = By.xpath("//tbody[1]/tr/td[7]/small[2]");

    public By startProcessing = By.xpath("//button[text()='Начать обработку']");

    public By filterByStatus = By.xpath("//div[@class='multi-filter-wrapper ng-scope compositeStatus-filter']/select");
    public By filterByType = By.xpath("//div[@class='multi-filter-wrapper ng-scope targetRealtyRooms-filter']/select");

    // Страница добавления заявки
    public By dateOfOrderInput = By.xpath("//input[@id='formedAt']");
    public By prevMonth = By.xpath("//div[@class='moment-picker-container month-view open']/table/thead/tr[1]/th[1]");
    public By family = By.xpath("//form/div[2]//input");
    public By name = By.xpath("//form/div[3]//input");
    public By patronymic = By.xpath("//form/div[4]//input");
    public By phone = By.xpath("//form/div[5]//input");
    public By email = By.xpath("//form/div[6]//input");
    public By passportNumber = By.xpath("//form/div[7]//input");
    public By passportIssued = By.xpath("//form/div[8]//textarea");
    public By passportIssuedDay = By.xpath("//input[@placeholder='День']");
    public By passportIssuedMonth = By.xpath("//input[@placeholder='Мес']");
    public By passportIssuedYear = By.xpath("//input[@placeholder='Год']");
    public By passportPassportCode = By.xpath("//form/div[10]//input");
    public By snils = By.xpath("//form/div[11]//input");
    public By desiredApartmentAnyone = By.xpath("//label[text()='Любой']");
    public By desiredApartmentConcrete = By.xpath("//label[text()='Конкретный']");
    public By wishesAndComments = By.xpath("//textarea[@ng-model='order.comment']");
    public By addPhoto = By.xpath("//input[@id='input25']");
    public By apartNumberSelect = By.xpath("//div[@ng-if='order.apartSelected']/div[3]//select");
    public By withoutReserve = By.xpath("//label[text()='Без закрепления']");
    public By withReserve = By.xpath("//label[text()='Закрепить']");
    public By changeOrder = By.xpath("//label[text()='Заменить']");
    public By calendar = By.xpath("//input[@id='orderDP1']");
    public By nextMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[3]");
    public By setDate = By.xpath("//div[@class='moment-picker-container month-view open']//tbody/tr[3]/td[1]");

    // Карточка заявки
    public By safeCardStatusLabelNewOrder = By.xpath("//span[text()='Новая']");
    public By safeCardStatusLabelWaitingList = By.xpath("//span[text()='Лист ожидания']");
    public By safeCardStatusLabelInvitedToShow = By.xpath("//span[text()='Приглашён на просмотр']");
    public By safeCardStatusLabelCollectionDocuments = By.xpath("//span[text()='Сбор документов']");
    public By safeCardStatusLabelThinks = By.xpath("//span[text()='Думает']");
    public By safeCardStatusLabelSignedBA = By.xpath("//span[text()='Подписан ДБ']");
    public By safeCardStatusLabelSignedLease = By.xpath("//span[text()='Подписан ДА']");
    public By safeCardStatusLabelSecurity = By.xpath("//span[text()='Проверка СБ']");
    public By safeCardStatusLabelNegative = By.xpath("//span[text()='Отказ']");
    public By safeCardStatusLabelDelete = By.xpath("//span[text()='Удалена']");

    public By safeCardApartmentStatusLabel = By.xpath("//span[text()='Забронирован']");
    public By safeClientFullName = By.xpath("//div[@class='panel-body']/div[1]/p[1]");
    public By safeClientPhone = By.xpath("//div[@class='panel-body']/div[1]/p[1]/a[1]");
    public By safeClientEmail = By.xpath("//div[@class='panel-body']/div[1]/p[1]/a[2]");
    public By passportData = By.xpath("//div[@class='panel-body']/div[1]/p[2]/span");
    public By safeSnils = By.xpath("//div[@class='panel-body']/div[1]/p[3]/span");
    public By apartmentInOrderCard = By.xpath("//a[contains(@href,'/admin/realty-card/apartment/')]");
    public By safeComments = By.xpath("//div[@id='comments']//tbody/tr/td[3]");
    public By safeCardApartmentType = By.xpath("//div[@ng-if='order.targetRealtyRooms']");

    public By comments = By.xpath("//li[1][@role='presentation']/a");
    public By documents = By.xpath("//li[2][@role='presentation']/a");
    public By history = By.xpath("//li[3][@role='presentation']/a");

    public By historyStatusCreateOrder = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Создана заявка')]");
    public By historyStatusSecurity = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Паспортные данные отправлены на проверку в службу безопасности')]");
    public By historyStatusNewOrder = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Установлен статус «Новая»')]");
    public By historyStatusWaitingList = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Установлен статус «Лист ожидания»')]");
    public By historyStatusInvitedToShow = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Установлен статус «Приглашён на просмотр')]");
    public By historyStatusCollectionDocuments = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Установлен статус «Сбор документов»')]");
    public By historyStatusThinks = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Установлен статус «Думает»')]");
    public By historyStatusSignedBA = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Установлен статус «Подписан ДБ»')]");
    public By historyStatusSignedLease = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Установлен статус «Подписан ДА»')]");
    public By historyStatusNegative = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Установлен статус «Отказ»')]");
    public By historyStatusNegativeComment = By.xpath(".//*[@id='comments']/table/tbody/tr[1]/td[text()='Комментарий с причиной отказа']");
    public By historyStatusDelete = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Установлен статус «Удалена»')]");
    public By historyStatusRepeatCall = By.xpath("//span/i[text()='Клиент просит перезвонить, или необходим повторный звонок']");
    public By historyStatusNotAvailable = By.xpath("//span/i[text()='Клиент не берет трубку или недоступен']");
    public By historyStatusAssignedSettlement = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Назначено заселение')]");
    public By historyStatusRecallDone = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Выполнен перезвон')]");
    public By historyStatusSettlementDone = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Выполнено заселение')]");
    public By historyStatusRecallCanceled = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Отменен перезвон')]");
    public By historyStatusSettlementCanceled = By.xpath("//div[@id='history']//tbody/tr/td[3][contains(text(),'Отменено заселение')]");

    public By addRenter = By.xpath("//i[@class='fa fa-fw fa-user-plus']");
    public By editOrder = By.xpath("//a[contains(text(),'Редактировать...')]");
    public By sendToSecurity = By.xpath("//a[text()='Отправить в СБ']");

    public By toastSecurity = By.xpath("//div[@class='toast-message'][text()='Данные клиента отправлены на проверку службой безопасности']");

    public By photo = By.xpath("//div[@class='form-file-preview ng-scope']/div[1]/div[@class='item-preview-inner']");
    public By nextPhoto = By.xpath("//div[contains(@class,'handler--next')]");
    public By prevPhoto = By.xpath("//div[contains(@class,'handler--prev')]");
    public By openPhoto = By.xpath("//div[contains(@class,'tru-slider')]/img");

    public By commentsTextField = By.xpath("//textarea[@name='text']");
    public By addComment = By.xpath("//button[text()='Добавить']");
    public By newCommentElement = By.xpath("//td[text()='Комментарий, добавленный в карточке заявки']");

    public By changeStatus = By.xpath("//a[@class='btn btn-primary'][text()='Изменить статус...']");
    public By changeStatusToNew = By.xpath("//label/b[text()='Новая']");
    public By changeStatusToWaitingList = By.xpath("//label/b[text()='Лист ожидания']");
    public By changeStatusToInvitedToShow = By.xpath("//label/b[text()='Приглашён на просмотр']");
    public By changeStatusToCollectionDocuments = By.xpath("//label/b[text()='Сбор документов']");
    public By changeStatusToThinks = By.xpath("//label/b[text()='Думает']");
    public By changeStatusToSignedBA = By.xpath("//label/b[text()='Подписан ДБ']");
    public By changeStatusToSignedLease = By.xpath("//label/b[text()='Подписан ДА']");
    public By changeStatusToSecurity = By.xpath("//label/b[text()='Проверка СБ']");
    public By changeStatusToNegative = By.xpath("//label/b[text()='Отказ']");
    public By statusNegativeComment = By.xpath("//div[13]//textarea");
    public By changeStatusToDelete = By.xpath("//label/b[text()='Удалена']");
    public By cancelChangeStatus = By.xpath("//button[text()='Отмена']");
    public By saveChangeStatus = By.xpath("//button[contains(text(),'Сохранить')]");

    public By inviteToShowCalendar = By.xpath("//div[3]//span/input[@id='callDP']");
    public By inviteToShowTime = By.xpath("//div[3]//div[@class='form-inline']/div[2]//input");

    public By assignRecall = By.xpath("//a[contains(text(),'Назначить перезвон')]");
    public By assignSettlement = By.xpath("//a[contains(text(),'Назначить заселение')]");
    public By notAvailable = By.xpath("//div[1]/label/input[@name='reason_recall']");
    public By recallModalCalendar = By.xpath("//div[@id='recall_form_modal']//input[@id='callDP']");
    public By recallModalTime = By.xpath("//input[@ng-model='recallForm.time']");
    public By recallModalCancel = By.xpath("//div[@id='recall_form_modal']//a[text()='Отмена']");
    public By recallModalAssign = By.xpath("//div[@id='recall_form_modal']//a[text()='Назначить']");
    public By settlementModalCalendar = By.xpath("//div[@id='settlement_modal']//input[@ng-model='settlementForm.date']");
    public By settlementModalTime = By.xpath("//input[@ng-model='settlementForm.time']");
    public By settlementModalCancel = By.xpath("//div[@id='settlement_modal']//a[text()='Отмена']");
    public By settlementModalAssign = By.xpath("//div[@id='settlement_modal']//a[text()='Назначить']");
    public By recallTextNotAvailable = By.xpath("//small/i[text()='Клиент не берет трубку или недоступен']");
    public By recallTextRepeatCall = By.xpath("//small/i[text()='Клиент просит перезвонить, или необходим повторный звонок']");
    public By settlementText = By.xpath("//small[contains(text(),'Заселение')]");
    public By cancelRecall = By.xpath("//div[@class='col-md-3']/div[2]//a[text()='Отменить']");
    public By cancelSettlement = By.xpath("//div[@class='col-md-3']/div[3]//a[text()='Отменить']");
    public By doneRecall = By.xpath("//div[@class='col-md-3']/div[2]//a[text()='Выполнен']");
    public By doneSettlement = By.xpath("//div[@class='col-md-3']/div[3]//a[text()='Выполнен']");
    public By closeCancelRecallModal = By.xpath("//div[@id='call_del_modal']//a[text()='Закрыть']");
    public By closeCancelSettlementModal = By.xpath("//div[@id='settlement_del_modal']//a[text()='Закрыть']");
    public By confirmCancelRecallModal = By.xpath("//div[@id='call_del_modal']//a[text()='Отменить перезвон']");
    public By confirmCancelSettlementModal = By.xpath("//div[@id='settlement_del_modal']//a[text()='Отменить заселение']");

    public By statusTitleAssignedRecall = By.xpath("//small[contains(text(),'Назначен перезвон')]");
    public By statusTitleAssignedSettlement = By.xpath("//small[contains(text(),'Назначено заселение')]");

    public By info = By.xpath("//i[@class='-circle fa fa-fw fa-info']");
    public By stopProcessing = By.xpath("//button[contains(text(),'Остановить обработку')]");
    public By nextOrder = By.xpath("//button[1][contains(text(),'Перейти к следующей')]");
    public By infoModal = By.xpath("//div[@class='modal-body ng-scope']/p");
    public By closeInfoModal = By.xpath("//button[@class='close']");
    public By orderTitle = By.xpath("//div[@class='page-header']/h1");

    public By point = By.xpath("//a[text()='Указать...']");
    public By dateOfOrderCard = By.xpath("//div[@class='page-header']/h4");

    // Страница редактирования заявки
    public By editOrderElement = By.xpath("//h1[contains(text(),'Редактирование заявки')]");
    public By editDeletePhoto = By.xpath("//div[@class='form-file-preview ng-scope']/div[2]/button");

}
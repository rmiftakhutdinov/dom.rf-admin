package pageObjects;

import org.openqa.selenium.By;

public class PromosPage {

    // Страница добавления акции
    public By addPromoElement = By.xpath("//h1[text()='Добавление акции']");
    public By companyNameRus = By.xpath("//div[@id='ru_cn']/input");
    public By companyNameEng = By.xpath("//div[@id='en_cn']/input");
    public By changeLanguageCompanyNameToEng = By.xpath("//a[@aria-controls='en_cn']");
    public By changeLanguageCompanyNameToRus = By.xpath("//a[@aria-controls='ru_cn']");
    public By companySite = By.xpath("//input[@name='companySite']");

    public By promoNameRus = By.xpath("//div[@id='ru_sn']/textarea");
    public By promoNameEng = By.xpath("//div[@id='en_sn']/textarea");
    public By changeLanguagePromoNameToEng = By.xpath("//a[@aria-controls='en_sn']");
    public By changeLanguagePromoNameToRus = By.xpath("//a[@aria-controls='ru_sn']");
    public By promoDescriptionRus = By.xpath("//div[@id='ru_sd']/textarea");
    public By promoDescriptionEng = By.xpath("//div[@id='en_sd']/textarea");
    public By changeLanguagePromoDescriptionToEng = By.xpath("//a[@aria-controls='en_sd']");
    public By changeLanguagePromoDescriptionToRus = By.xpath("//a[@aria-controls='ru_sd']");
    public By promoCode = By.xpath("//input[@ng-model='form.promoCode']");
    public By address = By.xpath("//input[@ng-model='form.companyAddress']");

    public By addPromoWithPublishing = By.xpath("//button[contains(text(),'Добавить и опубликовать')]");
    public By addPromoWithoutPublishing = By.xpath("//button[2]");

    public By info = By.xpath("//label/i");
    public By infoText = By.xpath("//div[@class='tooltip-inner ng-binding']");

    public By calendar1 = By.xpath("//input[@id='specDP1']");
    public By calendar2 = By.xpath("//input[@id='specDP2']");
    public By nextMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[3]");
    public By setDate1 = By.xpath("//div[@class='double-input-group']/div[1]//div[@class='moment-picker-specific-views']/table/tbody/tr[6]/td[1]");
    public By setDate2 = By.xpath("//div[@class='double-input-group']/div[2]//div[@class='moment-picker-specific-views']/table/tbody/tr[3]/td[1]");

    public By addLogo = By.xpath("//input[@ng-model='logo']");
    public By addImage = By.xpath("//input[@name='background']");

    // Страница редактирования акции
    public By deleteLogo = By.xpath("//div[4]//span[@title='Удалить']");
    public By deleteImage = By.xpath("//div[11]//span[@title='Удалить']");

    public By savePromo = By.xpath("//button[contains(text(),'Сохранить')]");

    // Карточка акции
    public By promoCardElement = By.xpath("//h1[contains(text(),'Акция')]");
    public By statusPublished = By.xpath("//span[contains(text(),'Опубликована')]");
    public By statusNotPublished = By.xpath("//span[contains(text(),'Не опубликована')]");

    public By objectComplex = By.xpath("//div[@class='form-horizontal']/div[1]/div[2]");
    public By objectAddress = By.xpath("//div[@class='form-horizontal']/div[2]/div[2]");
    public By changeCompanyNameCardToEng = By.xpath("//div[@class='form-horizontal']/div[3]//a[@aria-controls='en']");
    public By changeCompanyNameCardToRus = By.xpath("//div[@class='form-horizontal']/div[3]//a[@aria-controls='ru']");
    public By companyNameCardRus = By.xpath("//div[@class='form-horizontal']/div[3]//div[@id='ru']/div");
    public By companyNameCardEng = By.xpath("//div[@class='form-horizontal']/div[3]//div[@id='en']/div");
    public By logoCard = By.xpath("//div[@class='form-horizontal']/div[4]//a/img");
    public By site = By.xpath("//div[@class='form-horizontal']/div[5]/div[2]/a");
    public By siteNewPromoElement = By.xpath("//div[@class='std-pg-header-link']/a[text()='Подобрать жильё']");
    public By siteEditPromoElement = By.xpath("//h1[text()='Вход в личный кабинет жильца']");
    public By changePromoNameCardToEng = By.xpath("//div[@class='form-horizontal']/div[6]//a[@aria-controls='en']");
    public By changePromoNameCardToRus = By.xpath("//div[@class='form-horizontal']/div[6]//a[@aria-controls='ru']");
    public By promoNameCardRus = By.xpath("//div[@class='form-horizontal']/div[6]//div[@id='run']/div");
    public By promoNameCardEng = By.xpath("//div[@class='form-horizontal']/div[6]//div[@id='enn']/div");
    public By changePromoDescriptionCardToEng = By.xpath("//div[@class='form-horizontal']/div[7]//a[@aria-controls='en']");
    public By changePromoDescriptionCardToRus = By.xpath("//div[@class='form-horizontal']/div[7]//a[@aria-controls='ru']");
    public By promoDescriptionCardRus = By.xpath("//div[@class='form-horizontal']/div[7]//div[@id='rud']/div");
    public By promoDescriptionCardEng = By.xpath("//div[@class='form-horizontal']/div[7]//div[@id='end']/div");
    public By promoCodeCard = By.xpath("//div[@class='form-horizontal']/div[8]/div[2]");
    public By addressCard = By.xpath("//div[@class='form-horizontal']/div[9]/div[2]");
    public By promoPeriodCard = By.xpath("//div[@class='form-horizontal']/div[10]/div[2]");
    public By infoCard = By.xpath("//small/i");
    public By imageCard = By.xpath("//div[@class='form-horizontal']/div[11]//a/img");
    public By openPhoto = By.xpath("//div/img");

    public By publish = By.xpath("//button[contains(text(),'Опубликовать')]");
    public By notPublish = By.xpath("//button[contains(text(),'Снять с публикации')]");
    public By edit = By.xpath("//a[text()='Редактировать...']");
    public By editElement = By.xpath("//h1[text()='Редактирование акции']");
    public By deletePromo = By.xpath("//a[text()='Удалить...']");
    public By modalDeleteConfirm = By.xpath("//a[text()='Удалить']");
    public By modalDeleteCancel = By.xpath("//a[text()='Отмена']");

    public By history = By.xpath("//li/a[@aria-controls='history']");
    public By options = By.xpath("//li/a[@aria-controls='options']");

    public By toastPublication = By.xpath("//div[@class='toast-message'][text()='Статус публикации изменен']");

    // Список акций
    public By promoNameLst = By.xpath("//tbody/tr[1]/td[2]/a");
    public By companyNameList = By.xpath("//tbody/tr[1]/td[2]/small");
    public By promoPeriodList = By.xpath("//tbody/tr[1]/td[3]");
    public By statusList = By.xpath("//tbody/tr[1]/td[4]/span");
    public By infoList = By.xpath("//thead/tr/th[5]/i");

    public By filterStatus = By.xpath("//div[contains(@class,'status-filter')]/select");

    public By sort = By.xpath("//tbody/tr[3]/td[5]/span/i");

    public By toastDeleted = By.xpath("//div[@class='toast-message'][text()='Акция удалена']");
    public By toastSorting = By.xpath("//div[@class='toast-message'][text()='Порядок сохранен']");

}
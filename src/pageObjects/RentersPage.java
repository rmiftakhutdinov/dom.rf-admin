package pageObjects;

import org.openqa.selenium.By;
import java.io.File;
import java.util.Scanner;

public class RentersPage {

    // Список арендаторов
    public By listRenterId = By.xpath("//tbody/tr/td[2]/a");
    public By listRenterFullName = By.xpath("//tbody/tr/td[3]/a");
    public By listRenterPhone = By.xpath("//tbody/tr/td[3]/nobr");
    public By listRenterApartment = By.xpath("//small/p/a[contains(text(),'ап. 250')]");
    public By listRenterParking = By.xpath("//small/p/a[contains(text(),'м/м A70')]");
    public By listComment = By.xpath("//tbody/tr/td[5]/em/small[text()='Комментарий, добавленный в карточке арендатора']");

    private String renter() {
        String renter = null;
        try {
            String txt = "files\\txtFiles\\renters\\textRenterID.txt";
            File file = new File(txt);
            Scanner scanner = new Scanner(file);
            renter = scanner.nextLine();
        } catch (Exception ex) {
            throw new RuntimeException("Путь к файлу с номером арендатора не найден");
        }
        return renter;
    }

    public By listSafeRenterFullName = By.xpath("//a[text()='"+ renter() +"']/parent::td/following-sibling::td[1]/a");
    public By listSafeRenterPhone = By.xpath("//a[text()='"+ renter() +"']/parent::td/following-sibling::td[1]/nobr");

    public By filterByObjects = By.xpath("//div[@class='multi-filter-wrapper ng-scope realtyType-filter']/select");
    public By filterByStatus = By.xpath("//div[@class='multi-filter-wrapper ng-scope realtyStatus-filter']/select");
    public By filterByDateResettled = By.xpath("//div[@class='multiTypeDateRange multi-filter-wrapper flex-row ng-scope resettledAt-filter']//button[contains(text(),'За все время')]");
    public By diapasonByDateResettled = By.xpath("//div[@class='multiTypeDateRange multi-filter-wrapper flex-row ng-scope resettledAt-filter']//a[text()='Диапазон']");
    public By calendarDateFrom = By.xpath("//input[@id='mtlmDP']");
    public By calendarDateTo = By.xpath("//input[@id='mtlmDP2']");
    public By nextMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[3]");
    public By prevMonth = By.xpath("//div[@class='moment-picker-container month-view open']//tr/th[1]");
    public By setDateFromResettled = By.xpath("//div[@class='multiTypeDateRange multi-filter-wrapper flex-row ng-scope resettledAt-filter']/div[1]/div[1]//tbody/tr[6]/td[1]");
    public By setDateToResettled = By.xpath("//div[@class='multiTypeDateRange multi-filter-wrapper flex-row ng-scope resettledAt-filter']/div[1]/div[2]//tbody/tr[3]/td[1]");

    // Добавление арендатора
    public By checkboxInvite = By.xpath("//input[@type='checkbox']");
    public By addPhoto = By.xpath("//input[@id='input25']");

    // Редактирование арендатора
    public By editDeletePhoto1 = By.xpath("//div[@class='form-file-preview ng-scope']/div[1]/button");
    public By editDeletePhoto2 = By.xpath("//div[@class='form-file-preview ng-scope']/div[2]/button");
    public By editErrorRenterPassportNumber = By.xpath("//form/div[7]//div[contains(text(),'Используется другим арендатором')]");

    // Карточка арендатора
    public By cardRenterFullName = By.xpath("//div[1][@class='col-md-6']/p[1]");
    public By cardRenterBornedAtDate = By.xpath("//div[1][@class='col-md-6']/p[1]/span");
    public By cardRenterPhone = By.xpath("//div[1][@class='col-md-6']/p[1]/a[1]");
    public By cardRenterEmail = By.xpath("//div[1][@class='col-md-6']/p[1]/a[2]");//div[@class='dateRangeSelect multi-filter-wrapper flex-row ng-scope isRentedAt-filter']//a[text()='Диапазон']
    public By cardRenterPassportData = By.xpath("//div[1][@class='col-md-6']/p[2]");
    public By cardRenterAddress = By.xpath("//div[@class='panel-body']/div[@class='col-md-6']/div[1]");
    public By cardRenterBankBIK = By.xpath("//div[@class='panel-body']/div[@class='col-md-6']/div[2]//span[1]");
    public By cardRenterBankCorrAccountId = By.xpath("//div[@class='panel-body']/div[@class='col-md-6']/div[2]//span[2]");
    public By cardRenterBankTitle = By.xpath("//div[@class='panel-body']/div[@class='col-md-6']/div[2]//span[3]");
    public By cardRenterBankAccount = By.xpath("//div[@class='panel-body']/div[@class='col-md-6']/div[2]//span[4]");
    public By cardRenterBankCorrespTitle = By.xpath("//div[@class='panel-body']/div[@class='col-md-6']/div[3]//span");
    public By noApartmentsElement = By.xpath("//p[contains(text(),'Нет апартаментов')]");
    public By noParkingElement = By.xpath("//p[contains(text(),'Нет машиномест')]");

    public By comments = By.xpath("//ul[@class='nav nav-tabs']/li[1]");
    public By documents = By.xpath("//ul[@class='nav nav-tabs']/li[2]");
    public By history = By.xpath("//ul[@class='nav nav-tabs']/li[3]");
    public By historyStatusNewRenter = By.xpath("//tbody/tr/td[3][text()='Создан арендатор']");

    public By editRenter = By.xpath("//i[@class='fa fa-fw fa-pencil']");
    public By editRenterElement = By.xpath("//h1[contains(text(),'Редактирование арендатора')]");

    public By sendInvite = By.xpath("//i[@class='fa fa-fw fa-envelope-o']");
    public By toastInvite = By.xpath("//div[@aria-label='Письмо успешно отправлено']");

    public By toSettle = By.xpath("//a[text()='Заселить…']");
    public By settlementElement = By.xpath("//h1[contains(text(),'Заселение')]");
    public By toLease = By.xpath("//a[text()='Сдать...']");
    public By leaseElement = By.xpath("//h1[contains(text(),'Сдача машиноместа')]");

    public By commentsTextField = By.xpath("//textarea[@name='text']");
    public By addComment = By.xpath("//button[text()='Добавить']");
    public By newCommentElement = By.xpath("//td[text()='Комментарий, добавленный в карточке арендатора']");

    public By photo = By.xpath(".//*[@id='docs']/div/div[1]//img");
    public By nextPhoto = By.xpath("//div[contains(@class,'handler--next')]");
    public By prevPhoto = By.xpath("//div[contains(@class,'handler--prev')]");
    public By openPhoto = By.xpath("//div[contains(@class,'tru-slider')]/img");

}
package pageObjects;

import org.openqa.selenium.By;

public class StatisticsPage {

    public By settlementToday = By.xpath("//div[@class='free-row']/a[1]/div[@class='text ng-binding'][text()='сегодня']");

    public By settlementTomorrow = By.xpath("//div[@class='free-row']/a[2]/div[@class='text ng-binding'][text()='завтра']");

    public By settlement30Days = By.xpath("//div[@class='free-row']/a[3]/div[@class='text ng-binding'][text()='в ближайшие 30 дней']");

    public By ejectmentToday = By.xpath("//div[@class='free-row']/a[4]/div[@class='text ng-binding'][text()='сегодня']");

    public By ejectmentTomorrow = By.xpath("//div[@class='free-row']/a[5]/div[@class='text ng-binding'][text()='завтра']");

    public By ejectment30Days = By.xpath("//div[@class='free-row']/a[6]/div[@class='text ng-binding'][text()='в ближайшие 30 дней']");

    public By monthStatistic = By.xpath("//label[contains(@class,'ng-untouched ng-valid ng-binding ng-scope ng-not-empty')][text()='Помесячно']");

    public By monthStatisticElement = By.xpath("//label[contains(@class,'active')][text()='Помесячно']");

    public By weekStatistic = By.xpath("//label[contains(@class,'ng-untouched ng-valid ng-binding ng-scope ng-not-empty')][text()='Понедельно']");

    public By weekStatisticElement = By.xpath("//label[contains(@class,'active')][text()='Понедельно']");

}
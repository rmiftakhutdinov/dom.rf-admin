package tests.clientServicesTests;

import framework.Configuration;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.ClientServicesPage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import static org.testng.Assert.assertTrue;

public class ClientServicesFilters extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Строка поиска в списке заказов")
    public void searchLineServices() throws InterruptedException {

        GlobalPage globalpage = new GlobalPage();
        ClientServicesPage page = new ClientServicesPage();

        objectsSection();
        click(globalpage.dropdownMenuServices);
        click(globalpage.clientServicesSection);
        clientServicesSection();

        waitBy(page.serviceNumber);

        sendKeys(globalpage.searchInput, "250-16000" + Keys.ENTER);
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        String[] serviceNumber = findElement(page.serviceNumber).getText().split("∟");
        Assert.assertTrue(serviceNumber[0].contains("250-16000"));

        sendKeys(globalpage.searchInput, "250" + Keys.ENTER);
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        waitBy(page.object);

        Set<String> apartments = driver.findElements(page.object).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        for (String apartment : apartments){

            String[] apart250 = apartment.replace(" ап. ", "").split(",");
            assertTrue(apart250[0].contains("250"));

        }

        sendKeys(globalpage.searchInput, "Мифтахутдинов Рамиль" + Keys.ENTER);
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        waitBy(page.renter);

        Set<String> renters = driver.findElements(page.renter).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        for (String renter : renters){

            assertTrue(renter.contains("Мифтахутдинов Рамиль Рустэмович"));

        }

    }

    @Test(description = "Фильтр по типам в списке заказов", priority = 1)
    public void TypeFilterServices() {

        GlobalPage globalpage = new GlobalPage();
        ClientServicesPage page = new ClientServicesPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuServices);
        click(globalpage.clientServicesSection);
        clientServicesSection();

        waitBy(page.serviceNumber);

        List<String> elements = driver.findElements(page.typeServices).stream().map(WebElement::getText).collect(Collectors.toList()); // собрали в коллекцию все элементы
        ArrayList<String> types = new ArrayList<>(); // создали массив

        for (String type : elements){

           String [] forSplit = type.split("\n"); // обрезали лишний текст у всех элементов
           types.add(forSplit[0]); // поместили в массив элементы с нужным текстом

        }

        Set<String> type = new HashSet<>(types); // собрали коллекцию уникальных элементов
        assertTrue(type.size() > 1); // проверка на кол-во уникальных элементов

        selectByValue(page.typeFilter, "string:clean");
        elementIsNotVisible(globalpage.spinner);
        waitBy(page.serviceNumber);

        Set<String> typeClean = driver.findElements(page.typeServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        for (String clean : typeClean){

            String splitString[] = clean.split("\n");
            Assert.assertTrue(splitString[0].contains("Уборка"));
            Assert.assertFalse(splitString[0].contains("Ремонт"));
            Assert.assertFalse(splitString[0].contains("Пропуска и ключи"));
            Assert.assertFalse(splitString[0].contains("Прочие"));

        }

        selectByValue(page.typeFilter, "string:repair");
        elementIsNotVisible(globalpage.spinner);
        waitBy(page.serviceNumber);

        Set<String> typeRepair = driver.findElements(page.typeServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        for (String repair : typeRepair){

            String splitString[] = repair.split("\n");
            Assert.assertFalse(splitString[0].contains("Уборка"));
            Assert.assertTrue(splitString[0].contains("Ремонт"));
            Assert.assertFalse(splitString[0].contains("Пропуска и ключи"));
            Assert.assertFalse(splitString[0].contains("Прочие"));

        }

        selectByValue(page.typeFilter, "string:pass");
        elementIsNotVisible(globalpage.spinner);
        waitBy(page.serviceNumber);

        Set<String> typePass = driver.findElements(page.typeServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        for (String pass : typePass){

            String splitString[] = pass.split("\n");
            Assert.assertFalse(splitString[0].contains("Уборка"));
            Assert.assertFalse(splitString[0].contains("Ремонт"));
            Assert.assertTrue(splitString[0].contains("Пропуска и ключи"));
            Assert.assertFalse(splitString[0].contains("Прочие"));

        }

        selectByValue(page.typeFilter, "string:other");
        elementIsNotVisible(globalpage.spinner);
        waitBy(page.serviceNumber);

        Set<String> typeOther = driver.findElements(page.typeServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        for (String other : typeOther){

            String splitString[] = other.split("\n");
            Assert.assertFalse(splitString[0].contains("Уборка"));
            Assert.assertFalse(splitString[0].contains("Ремонт"));
            Assert.assertFalse(splitString[0].contains("Пропуска и ключи"));
            Assert.assertTrue(splitString[0].contains("Прочие"));

        }

    }

    @Test(description = "Фильтр по статусам в списке заказов", priority = 2)
    public void statusFilterServices() {

        GlobalPage globalpage = new GlobalPage();
        ClientServicesPage page = new ClientServicesPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuServices);
        click(globalpage.clientServicesSection);
        clientServicesSection();

        waitBy(page.serviceNumber);

        Set<String> allStatuses = driver.findElements(page.statusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);

        selectByValue(page.statusFilter, "string:new");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> statusNotDone = driver.findElements(page.statusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(statusNotDone.contains("Не выполнен"));
        Assert.assertFalse(statusNotDone.contains("В обработке"));
        Assert.assertFalse(statusNotDone.contains("Выполнен"));
        Assert.assertFalse(statusNotDone.contains("Отменен"));

        selectByValue(page.statusFilter, "string:processed");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> statusInProgress = driver.findElements(page.statusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusInProgress.contains("Не выполнен"));
        Assert.assertTrue(statusInProgress.contains("В обработке"));
        Assert.assertFalse(statusInProgress.contains("Выполнен"));
        Assert.assertFalse(statusInProgress.contains("Отменен"));

        selectByValue(page.statusFilter, "string:ready");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> statusDone = driver.findElements(page.statusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusDone.contains("Не выполнен"));
        Assert.assertFalse(statusDone.contains("В обработке"));
        Assert.assertTrue(statusDone.contains("Выполнен"));
        Assert.assertFalse(statusDone.contains("Отменен"));

        selectByValue(page.statusFilter, "string:cancelled");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> statusCancelled = driver.findElements(page.statusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusCancelled.contains("Не выполнен"));
        Assert.assertFalse(statusCancelled.contains("В обработке"));
        Assert.assertFalse(statusCancelled.contains("Выполнен"));
        Assert.assertTrue(statusCancelled.contains("Отменен"));

    }

    @Test(description = "Фильтр по оплатам в списке заказов", priority = 3)
    public void paymentsFilterServices() {

        GlobalPage globalpage = new GlobalPage();
        ClientServicesPage page = new ClientServicesPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuServices);
        click(globalpage.clientServicesSection);
        clientServicesSection();

        waitBy(page.serviceNumber);

        Set<String> allPaymentStatuses = driver.findElements(page.paymentStatusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allPaymentStatuses.size() > 1);

        selectByValue(page.paymentFilter, "string:waiting");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> statusNotPaid = driver.findElements(page.paymentStatusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(statusNotPaid.contains("Не оплачен"));
        Assert.assertFalse(statusNotPaid.contains("Оплачен"));
        Assert.assertFalse(statusNotPaid.contains("Бесплатно"));
        Assert.assertFalse(statusNotPaid.contains("Возвращено"));
        Assert.assertFalse(statusNotPaid.contains("К возврату"));

        selectByValue(page.paymentFilter, "string:paid");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> statusPaid = driver.findElements(page.paymentStatusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusPaid.contains("Не оплачен"));
        Assert.assertTrue(statusPaid.contains("Оплачен"));
        Assert.assertFalse(statusPaid.contains("Бесплатно"));
        Assert.assertFalse(statusPaid.contains("Возвращено"));
        Assert.assertFalse(statusPaid.contains("К возврату"));

        Set<String> allLinks = driver.findElements(page.invoiceNumber).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allLinks.size() == 50);

        selectByValue(page.paymentFilter, "string:free");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> statusFree = driver.findElements(page.paymentStatusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusFree.contains("Не оплачен"));
        Assert.assertFalse(statusFree.contains("Оплачен"));
        Assert.assertTrue(statusFree.contains("Бесплатно"));
        Assert.assertFalse(statusFree.contains("Возвращено"));
        Assert.assertFalse(statusFree.contains("К возврату"));

        selectByValue(page.paymentFilter, "string:returned");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> statusReturned = driver.findElements(page.paymentStatusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusReturned.contains("Не оплачен"));
        Assert.assertFalse(statusReturned.contains("Оплачен"));
        Assert.assertFalse(statusReturned.contains("Бесплатно"));
        Assert.assertTrue(statusReturned.contains("Возвращено"));
        Assert.assertFalse(statusReturned.contains("К возврату"));

        selectByValue(page.paymentFilter, "string:toReturn");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> statusToReturn = driver.findElements(page.paymentStatusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusToReturn.contains("Не оплачен"));
        Assert.assertFalse(statusToReturn.contains("Оплачен"));
        Assert.assertFalse(statusToReturn.contains("Бесплатно"));
        Assert.assertFalse(statusToReturn.contains("Возвращено"));
        Assert.assertTrue(statusToReturn.contains("К возврату"));

    }

    @Test(description = "Фильтр по оценке в списке заказов", priority = 4)
    public void evaluationFilterServices() {

        GlobalPage globalpage = new GlobalPage();
        ClientServicesPage page = new ClientServicesPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuServices);
        click(globalpage.clientServicesSection);
        clientServicesSection();

        waitBy(page.serviceNumber);

        click(page.rating1);

        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> evaluation1 = driver.findElements(page.evaluationServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(evaluation1.contains("· 1"));
        Assert.assertFalse(evaluation1.contains("· 2"));
        Assert.assertFalse(evaluation1.contains("· 3"));
        Assert.assertFalse(evaluation1.contains("· 4"));
        Assert.assertFalse(evaluation1.contains("· 5"));

        click(page.rating2);

        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> evaluation2 = driver.findElements(page.evaluationServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(evaluation2.contains("· 1"));
        Assert.assertTrue(evaluation2.contains("· 2"));
        Assert.assertFalse(evaluation2.contains("· 3"));
        Assert.assertFalse(evaluation2.contains("· 4"));
        Assert.assertFalse(evaluation2.contains("· 5"));

        click(page.rating3);

        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> evaluation3 = driver.findElements(page.evaluationServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(evaluation3.contains("· 1"));
        Assert.assertFalse(evaluation3.contains("· 2"));
        Assert.assertTrue(evaluation3.contains("· 3"));
        Assert.assertFalse(evaluation3.contains("· 4"));
        Assert.assertFalse(evaluation3.contains("· 5"));

        click(page.rating4);

        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> evaluation4 = driver.findElements(page.evaluationServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(evaluation4.contains("· 1"));
        Assert.assertFalse(evaluation4.contains("· 2"));
        Assert.assertFalse(evaluation4.contains("· 3"));
        Assert.assertTrue(evaluation4.contains("· 4"));
        Assert.assertFalse(evaluation4.contains("· 5"));

        click(page.rating5);

        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        Set<String> evaluation5 = driver.findElements(page.evaluationServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(evaluation5.contains("· 1"));
        Assert.assertFalse(evaluation5.contains("· 2"));
        Assert.assertFalse(evaluation5.contains("· 3"));
        Assert.assertFalse(evaluation5.contains("· 4"));
        Assert.assertTrue(evaluation5.contains("· 5"));

    }

    @Test(description = "Фильтр по датам в списке заказов", priority = 5)
    public void dateFilterServices() {

        GlobalPage globalpage = new GlobalPage();
        ClientServicesPage page = new ClientServicesPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuServices);
        click(globalpage.clientServicesSection);
        clientServicesSection();

        waitBy(page.serviceNumber);

        click(page.periodFilter);
        click(page.period7Days);
        Assert.assertTrue(findElement(page.periodFilter).getText().contains("Последние 7 дней"));

        click(page.periodFilter);
        click(page.period14Days);
        Assert.assertTrue(findElement(page.periodFilter).getText().contains("Последние 14 дней"));

        click(page.periodFilter);
        click(page.period30Days);
        Assert.assertTrue(findElement(page.periodFilter).getText().contains("Последние 30 дней"));

        click(page.periodFilter);
        click(page.period90Days);
        Assert.assertTrue(findElement(page.periodFilter).getText().contains("Последние 90 дней"));

        click(page.periodFilter);
        click(page.periodChoose);
        Assert.assertTrue(findElement(page.periodChooseElement).isDisplayed());

        click(page.periodFilter);
        click(page.periodAllDate);
        Assert.assertTrue(findElement(page.periodFilter).getText().contains("За все время"));

    }

    @Test(description = "Сброс фильтров в списке заказов", priority = 6)
    public void resetFiltersServices() {

        GlobalPage globalpage = new GlobalPage();
        ClientServicesPage page = new ClientServicesPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuServices);
        click(globalpage.clientServicesSection);
        clientServicesSection();

        waitBy(page.serviceNumber);

        findElement(globalpage.searchInput).sendKeys("Всякая дребедень" + Keys.ENTER);
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner,30);

        selectByValue(page.typeFilter, "string:repair");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner,30);

        selectByValue(page.statusFilter, "string:ready");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        selectByValue(page.paymentFilter, "string:paid");
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        click(page.rating3);
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        click(page.periodFilter);
        click(page.period90Days);
        waitBy(globalpage.spinner);
        elementIsNotVisible(globalpage.spinner);

        waitBy(page.noServicesElement);

        click(globalpage.filterReset);
        try {
            waitBy(page.serviceNumber);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден элемент заказа");
        }

        Assert.assertTrue(findElement(globalpage.searchInput).getAttribute("value").equals(""));

        List<String> elements = driver.findElements(page.typeServices).stream().map(WebElement::getText).collect(Collectors.toList());
        ArrayList<String> types = new ArrayList<>();

        for (String type : elements){

            String [] forSplit = type.split("\n");
            types.add(forSplit[0]);

        }

        Set<String> type = new HashSet<>(types);
        assertTrue(type.size() > 1);

        Set<String> allStatuses = driver.findElements(page.statusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);

        Set<String> allPaymentStatuses = driver.findElements(page.paymentStatusServices).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allPaymentStatuses.size() > 1);

        Assert.assertTrue(findElement(page.ratingAll).getAttribute("class").contains("active"));

        Assert.assertTrue(findElement(page.periodFilter).getText().contains("За все время"));

    }

}
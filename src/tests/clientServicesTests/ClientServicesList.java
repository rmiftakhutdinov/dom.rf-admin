package tests.clientServicesTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.ClientServicesPage;
import pageObjects.InvoicePage;
import java.util.concurrent.TimeUnit;

public class ClientServicesList extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход по ссылкам в списке заказов")
    public void openLinksInServicesList() throws InterruptedException {

        GlobalPage globalpage = new GlobalPage();
        ClientServicesPage page = new ClientServicesPage();

        objectsSection();
        click(globalpage.dropdownMenuServices);
        click(globalpage.clientServicesSection);
        clientServicesSection();

        click(page.showServicesDetail);

        // Проверка на наличие текста в детализации заказа
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(findElement(By.xpath("//tbody/tr[1]/td[2]/div[3]/small")), "innerText"));

        // Проверка на наличие текста в итоговой сумме заказа
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(findElement(By.xpath("//tbody/tr/td[2]/div[1]/div[3]")), "innerText"));

        click(page.hideServicesDetail);

        String handleBefore = driver.getWindowHandle();

        click(page.object);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        apartmentCard();

        driver.close();
        driver.switchTo().window(handleBefore);

        try {
            waitBy(globalpage.clientServicesSectionElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден элемент раздела 'Все заказы'");
        }

        click(page.renter);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            waitBy(globalpage.renterCardElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден элемент карточки апартамента");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        try {
            waitBy(globalpage.clientServicesSectionElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден элемент раздела 'Все заказы'");
        }

        waitBy(page.paymentFilter);

        selectByValue(page.paymentFilter, "string:paid");
        elementIsNotVisible(globalpage.spinner);

        String invoiceNumber1 = findElement(page.invoiceNumber).getText().trim();

        click(page.invoiceNumber);
        try {
            waitBy(new InvoicePage().invoiceNumber);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден номер счета в списке счетов");
        }

        String invoiceNumber2 = findElement(By.xpath("//tbody/tr/td[1]/nobr/a")).getText().trim();
        Assert.assertTrue(invoiceNumber1.equals(invoiceNumber2));

    }

    @Test(description = "Пагинация в списке заказов", priority = 1)
    public void servicesListPagination() {

        GlobalPage globalpage = new GlobalPage();
        ClientServicesPage page = new ClientServicesPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuServices);
        click(globalpage.clientServicesSection);
        clientServicesSection();

        waitBy(globalpage.nextPage, 30);

        String page1 = new String(findElement(page.serviceNumber).getText());

        click(globalpage.nextPage,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        String page2 = new String(findElement(page.serviceNumber).getText());
        Assert.assertFalse(page1.equals(page2));

        click(globalpage.prevPage,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

        click(globalpage.page2,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        click(globalpage.page1,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

    }

}
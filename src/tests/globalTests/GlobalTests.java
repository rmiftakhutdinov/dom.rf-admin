package tests.globalTests;

import framework.Configuration;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import java.util.concurrent.TimeUnit;

public class GlobalTests extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход по всем разделам")
    public void openAllSections() {

        GlobalPage page = new GlobalPage();

        objectsSection();

        click(page.ordersSection);
        ordersSection();

        click(page.objectsSection);
        objectsSection();

        click(page.rentersSection);
        rentersSection();

        click(page.invoiceSection);
        invoiceSection();

        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        click(page.dropdownMenuServices);
        click(page.passesSection);
        passesSection();

        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        click(page.dropdownMenuNotifications);
        click(page.notificationsClientsSection);
        notificationsClientSection();

        click(page.dropdownMenuNotifications);
        click(page.notificationsServiceSection);
        notificationsServiceSection();

        click(page.statisticsSection);
        statisticsSection();

        click(page.documentsSection);
        documentsSection();

        click(page.dropdownMenuMore);
        click(page.promosSection);
        promosSection();

        click(page.dropdownMenuMore);
        click(page.templatesSection);
        templatesSection();

        click(page.dropdownMenuMore);
        click(page.adsSection);
        adsSection();

        click(page.logout);
        try {
            findElement(page.authPageLogo);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент на странице авторизации");
        }

    }

}
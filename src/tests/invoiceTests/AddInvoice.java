package tests.invoiceTests;

import framework.Configuration;
import org.openqa.selenium.*;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.InvoicePage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.*;

public class AddInvoice extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Добавление счета по апартаменту с типом 'Аренда'")
    public void addInvoiceApartmentRentType() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        objectsSection();
        click(globalpage.invoiceSection);
        invoiceSection();

        click(page.addNewInvoice);
        try {
            waitBy(page.addInvoiceElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления счета");
        }

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

        click(page.dropDownRenter);
        sendKeys(page.inputChoiseRenter, "202" + Keys.ENTER);

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);
        assertTrue(findElement(By.xpath("//button/span/span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]")).isDisplayed());

        selectByText(page.objectNumberSelect, "250");

        sendKeys(page.invoicePurpose, "Автотестовая аренда - апартамент");

        selectByValue(page.invoiceTypeSelect, "rent");

        click(page.periodCalendar1);
        click(page.nextMonth);
        click(page.setDate1);
        click(page.periodCalendar2);
        click(page.setDate2);

        sendKeys(page.sumInvoice, "55000,55");

        click(page.dateCreatedCalendar);
        click(page.nextMonth);
        click(page.setDateCreatedRentAndCommunal);
        String date1 = findElement(page.dateCreatedCalendar).getAttribute("value");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        click(page.deletePhoto);

        List<WebElement> images = driver.findElements(By.xpath("//div/img"));
        assertTrue(images.size() == 1);
        assertTrue(findElement(By.xpath("//span[@title='PDF']")).isDisplayed());

        String jpg2 = System.getProperty("user.dir") + "\\files\\images\\111.jpg";
        setClipboardData(jpg2);
        click(page.addPhoto);

        Robot robotJPG2 = new Robot();
        robotJPG2.delay(250);
        robotJPG2.keyPress(KeyEvent.VK_CONTROL);
        robotJPG2.keyPress(KeyEvent.VK_V);
        robotJPG2.keyRelease(KeyEvent.VK_V);
        robotJPG2.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG2.keyPress(KeyEvent.VK_ENTER);
        robotJPG2.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);

        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        click(page.createInvoice);
        try {
            waitBy(page.toastNewInvoice);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент тоста о добавлении счета");
        }

        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_apartment.txt";
        File file = new File(txt);
        String number = findElement(page.invoiceNumber).getText();

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(number);
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        assertTrue(findElement(page.typeInvoiceList).getText().contains("Аренда"));
        assertTrue(findElement(page.purposeInvoiceList).getText().equals("Автотестовая аренда - апартамент"));
        assertTrue(findElement(page.sumInvoiceList).getText().contains("55 000,55"));
        assertTrue(findElement(page.objectList).getText().contains("ап. 250"));
        assertTrue(findElement(page.renterList).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberList).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.statusListNotPaid).isDisplayed());

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.cardInvoiceElement).getText().replace("Счет ", "").replace("Не оплачен", "").trim().equals(number));
        assertTrue(findElement(page.statusCardtNotPaid).isDisplayed());
        assertTrue(findElement(page.renterCard).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberCard).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.objectCard).getText().contains("250"));
        assertTrue(findElement(page.typeInvoiceCard).getText().equals("Аренда"));
        assertTrue(findElement(page.purposeInvoiceCard).getText().equals("Автотестовая аренда - апартамент"));
        assertTrue(findElement(page.sumInvoiceCard).getText().contains("55 000,55"));
        String date2 = findElement(page.dateCreatedCard).getText();
        assertTrue(date1.contains(date2));

        List<WebElement> files = driver.findElements(By.xpath("//div/img"));
        assertTrue(files.size() == 2);
        assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

    }

    @Test(description = "Добавление счета по машиноместу с типом 'Аренда'", priority = 1)
    public void addInvoiceParkingRentType() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        click(page.addNewInvoice);
        try {
            waitBy(page.addInvoiceElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления счета");
        }

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

        click(page.dropDownRenter);
        sendKeys(page.inputChoiseRenter, "202" + Keys.ENTER);

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);
        assertTrue(findElement(By.xpath("//button/span/span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]")).isDisplayed());

        selectByText(page.objectNumberSelect, "A70");

        sendKeys(page.invoicePurpose, "Автотестовая аренда - машиноместо");

        List<WebElement> types = driver.findElements(By.xpath("//form[@name='invoiceForm']/div[4]//select/option"));

        for (WebElement type : types) {

            try {

                assertFalse(type.getAttribute("value").equals("communal"));

            } catch (AssertionError ex){

                afterExeptionMessage("В списке машиномест присутствует тип счета 'Коммунальные'");

            }

        }

        click(page.periodCalendar1);
        click(page.nextMonth);
        click(page.setDate1);
        click(page.periodCalendar2);
        click(page.setDate2);

        sendKeys(page.sumInvoice, "15000,55");

        click(page.dateCreatedCalendar);
        click(page.nextMonth);
        click(page.setDateCreatedRentAndCommunal);
        String date1 = findElement(page.dateCreatedCalendar).getAttribute("value");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        click(page.createInvoice);
        try {
            waitBy(page.toastNewInvoice);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент тоста о добавлении счета");
        }

        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_parking.txt";
        File file = new File(txt);
        String number = findElement(page.invoiceNumber).getText();

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(number);
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        assertTrue(findElement(page.typeInvoiceList).getText().contains("Аренда"));
        assertTrue(findElement(page.purposeInvoiceList).getText().equals("Автотестовая аренда - машиноместо"));
        assertTrue(findElement(page.sumInvoiceList).getText().contains("15 000,55"));
        assertTrue(findElement(page.objectList).getText().contains("м/м A70"));
        assertTrue(findElement(page.renterList).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberList).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.statusListNotPaid).isDisplayed());

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.cardInvoiceElement).getText().replace("Счет ", "").replace("Не оплачен", "").trim().equals(number));
        assertTrue(findElement(page.cardInvoiceElement).getText().contains("Счет A70"));
        assertTrue(findElement(page.statusCardtNotPaid).isDisplayed());
        assertTrue(findElement(page.renterCard).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberCard).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.objectCard).getText().contains("A70"));
        assertTrue(findElement(page.typeInvoiceCard).getText().equals("Аренда"));
        assertTrue(findElement(page.purposeInvoiceCard).getText().equals("Автотестовая аренда - машиноместо"));
        assertTrue(findElement(page.sumInvoiceCard).getText().contains("15 000,55"));
        String date2 = findElement(page.dateCreatedCard).getText();
        assertTrue(date1.contains(date2));

        List<WebElement> files = driver.findElements(By.xpath("//div/img"));
        assertTrue(files.size() == 2);
        assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

    }

    @Test(description = "Добавление счета по апартаменту с типом 'Коммунальные'", priority = 2)
    public void addInvoiceCommunalType() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        click(page.addNewInvoice);
        try {
            waitBy(page.addInvoiceElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления счета");
        }

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

        click(page.dropDownRenter);
        sendKeys(page.inputChoiseRenter, "202" + Keys.ENTER);

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);
        assertTrue(findElement(By.xpath("//button/span/span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]")).isDisplayed());

        selectByText(page.objectNumberSelect, "250");

        sendKeys(page.invoicePurpose, "Автотестовый коммунальный");

        selectByValue(page.invoiceTypeSelect, "communal");

        elementIsNotVisible(globalpage.spinner);

        click(page.periodCalendar1);
        click(page.nextMonth);
        click(page.setDate1);
        click(page.periodCalendar2);
        click(page.setDate2);

        sendKeys(page.inputIndication_1_1, "123");
        sendKeys(page.inputIndication_2_1, "123");
        sendKeys(page.inputIndication_3_1, "123");
        sendKeys(page.inputIndication_4_1, "123");
        sendKeys(page.inputIndication_5_1, "123");
        sendKeys(page.inputIndication_1_2, "125");
        sendKeys(page.inputIndication_2_2, "125");
        sendKeys(page.inputIndication_3_2, "125");
        sendKeys(page.inputIndication_4_2, "125");
        sendKeys(page.inputIndication_5_2, "333");

        elementIsNotVisible(globalpage.spinner);

        click(page.dateCreatedCalendar);
        click(page.nextMonth);
        click(page.setDateCreatedRentAndCommunal);
        String date1 = findElement(page.dateCreatedCalendar).getAttribute("value");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String sum = findElement(By.xpath("//div[2]/div[2]/input[@id='input3']")).getAttribute("value");

        click(page.createInvoice);
        try {
            waitBy(page.toastNewInvoice);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент тоста о добавлении счета");
        }

        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);

        assertTrue(findElement(page.invoiceNumber).getText().contains("250"));
        assertTrue(findElement(page.typeInvoiceList).getText().contains("Коммунальные"));
        assertTrue(findElement(page.purposeInvoiceList).getText().equals("Автотестовый коммунальный"));
        assertTrue(findElement(page.objectList).getText().contains("ап. 250"));
        assertTrue(findElement(page.renterList).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberList).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.statusListNotPaid).isDisplayed());

        String sumList = findElement(page.sumInvoiceList).getText();

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.cardInvoiceElement).getText().contains("Счет 250"));
        assertTrue(findElement(page.statusCardtNotPaid).isDisplayed());
        assertTrue(findElement(page.renterCard).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberCard).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.objectCard).getText().contains("250"));
        assertTrue(findElement(page.typeCommunalInvoiceCard).getText().equals("Коммунальные"));
        assertTrue(findElement(page.purposeCommunalInvoiceCard).getText().equals("Автотестовый коммунальный"));

        String sumCard = findElement(page.sumCommunalInvoiceCard).getText();
        String date2 = findElement(page.dateCommunalCreatedCard).getText();
        assertTrue(date1.contains(date2));
        assertTrue(sumCard.contains(sum));
        assertTrue(sumList.contains(sum));
        assertTrue(sumList.contains(sumCard));

        List<WebElement> files = driver.findElements(By.xpath("//div/img"));
        assertTrue(files.size() == 2);
        assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

    }

    @Test(description = "Добавление счета по апартаменту с типом 'Заказ услуг'", priority = 3)
    public void addInvoiceApartmentServiceType() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        click(page.addNewInvoice);
        try {
            waitBy(page.addInvoiceElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления счета");
        }

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

        click(page.dropDownRenter);
        sendKeys(page.inputChoiseRenter, "202" + Keys.ENTER);

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);
        assertTrue(findElement(By.xpath("//button/span/span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]")).isDisplayed());

        selectByText(page.objectNumberSelect, "250");

        sendKeys(page.invoicePurpose, "Автотестовые услуги - апартамент");

        selectByValue(page.invoiceTypeSelect, "service");

        elementIsNotVisible(page.periodCalendar1);

        sendKeys(page.sumInvoice, "5000,55");

        click(page.dateCreatedCalendar);
        click(page.nextMonth);
        click(page.setDateCreatedServicesAndOthers);
        String date1 = findElement(page.dateCreatedCalendar).getAttribute("value");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        click(page.createInvoice);
        try {
            waitBy(page.toastNewInvoice);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент тоста о добавлении счета");
        }

        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);

        assertTrue(findElement(page.invoiceNumber).getText().contains("250"));
        assertTrue(findElement(page.typeInvoiceList).getText().contains("Услуги"));
        assertTrue(findElement(page.purposeInvoiceList).getText().equals("Автотестовые услуги - апартамент"));
        assertTrue(findElement(page.sumInvoiceList).getText().contains("5 000,55"));
        assertTrue(findElement(page.objectList).getText().contains("ап. 250"));
        assertTrue(findElement(page.renterList).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberList).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.statusListNotPaid).isDisplayed());

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.cardInvoiceElement).getText().contains("Счет 250"));
        assertTrue(findElement(page.statusCardtNotPaid).isDisplayed());
        assertTrue(findElement(page.renterCard).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberCard).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.objectCard).getText().contains("250"));
        assertTrue(findElement(page.typeInvoiceCard).getText().equals("Услуги"));
        assertTrue(findElement(page.purposeInvoiceCard).getText().equals("Автотестовые услуги - апартамент"));
        assertTrue(findElement(page.sumInvoiceCard).getText().contains("5 000,55"));
        String date2 = findElement(page.dateCreatedCard).getText();
        assertTrue(date1.contains(date2));

        List<WebElement> files = driver.findElements(By.xpath("//div/img"));
        assertTrue(files.size() == 2);
        assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

    }

    @Test(description = "Добавление счета по машиноместу с типом 'Заказ услуг'", priority = 4)
    public void addInvoiceParkingServiceType() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        click(page.addNewInvoice);
        try {
            waitBy(page.addInvoiceElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления счета");
        }

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

        click(page.dropDownRenter);
        sendKeys(page.inputChoiseRenter, "202" + Keys.ENTER);

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);
        assertTrue(findElement(By.xpath("//button/span/span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]")).isDisplayed());

        selectByText(page.objectNumberSelect, "A70");

        sendKeys(page.invoicePurpose, "Автотестовые услуги - машиноместо");

        selectByValue(page.invoiceTypeSelect, "service");

        elementIsNotVisible(page.periodCalendar1);

        sendKeys(page.sumInvoice, "5000,55");

        click(page.dateCreatedCalendar);
        click(page.nextMonth);
        click(page.setDateCreatedServicesAndOthers);
        String date1 = findElement(page.dateCreatedCalendar).getAttribute("value");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        click(page.createInvoice);
        try {
            waitBy(page.toastNewInvoice);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент тоста о добавлении счета");
        }

        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);

        assertTrue(findElement(page.invoiceNumber).getText().contains("A70"));
        assertTrue(findElement(page.typeInvoiceList).getText().contains("Услуги"));
        assertTrue(findElement(page.purposeInvoiceList).getText().equals("Автотестовые услуги - машиноместо"));
        assertTrue(findElement(page.sumInvoiceList).getText().contains("5 000,55"));
        assertTrue(findElement(page.objectList).getText().contains("м/м A70"));
        assertTrue(findElement(page.renterList).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberList).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.statusListNotPaid).isDisplayed());

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.cardInvoiceElement).getText().contains("Счет A70"));
        assertTrue(findElement(page.statusCardtNotPaid).isDisplayed());
        assertTrue(findElement(page.renterCard).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberCard).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.objectCard).getText().contains("A70"));
        assertTrue(findElement(page.typeInvoiceCard).getText().equals("Услуги"));
        assertTrue(findElement(page.purposeInvoiceCard).getText().equals("Автотестовые услуги - машиноместо"));
        assertTrue(findElement(page.sumInvoiceCard).getText().contains("5 000,55"));
        String date2 = findElement(page.dateCreatedCard).getText();
        assertTrue(date1.contains(date2));

        List<WebElement> files = driver.findElements(By.xpath("//div/img"));
        assertTrue(files.size() == 2);
        assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

    }

    @Test(description = "Добавление счета по апартаменту с типом 'Прочее'", priority = 5)
    public void addInvoiceApartmentOtherType() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        click(page.addNewInvoice);
        try {
            waitBy(page.addInvoiceElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления счета");
        }

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

        click(page.dropDownRenter);
        sendKeys(page.inputChoiseRenter, "202" + Keys.ENTER);

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);
        assertTrue(findElement(By.xpath("//button/span/span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]")).isDisplayed());

        selectByText(page.objectNumberSelect, "250");

        sendKeys(page.invoicePurpose, "Автотестовые прочие - апартамент");

        selectByValue(page.invoiceTypeSelect, "other");

        elementIsNotVisible(page.periodCalendar1);

        sendKeys(page.sumInvoice, "0,55");

        click(page.dateCreatedCalendar);
        click(page.nextMonth);
        click(page.setDateCreatedServicesAndOthers);
        String date1 = findElement(page.dateCreatedCalendar).getAttribute("value");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        click(page.createInvoice);
        try {
            waitBy(page.toastNewInvoice);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент тоста о добавлении счета");
        }

        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);

        assertTrue(findElement(page.invoiceNumber).getText().contains("250"));
        assertTrue(findElement(page.typeInvoiceList).getText().contains("Прочее"));
        assertTrue(findElement(page.purposeInvoiceList).getText().equals("Автотестовые прочие - апартамент"));
        assertTrue(findElement(page.sumInvoiceList).getText().contains("0,55"));
        assertTrue(findElement(page.objectList).getText().contains("ап. 250"));
        assertTrue(findElement(page.renterList).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberList).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.statusListNotPaid).isDisplayed());

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.cardInvoiceElement).getText().contains("Счет 250"));
        assertTrue(findElement(page.statusCardtNotPaid).isDisplayed());
        assertTrue(findElement(page.renterCard).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberCard).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.objectCard).getText().contains("250"));
        assertTrue(findElement(page.typeInvoiceCard).getText().equals("Прочее"));
        assertTrue(findElement(page.purposeInvoiceCard).getText().equals("Автотестовые прочие - апартамент"));
        assertTrue(findElement(page.sumInvoiceCard).getText().contains("0,55"));
        String date2 = findElement(page.dateCreatedCard).getText();
        assertTrue(date1.contains(date2));

        List<WebElement> files = driver.findElements(By.xpath("//div/img"));
        assertTrue(files.size() == 2);
        assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

    }

    @Test(description = "Добавление счета по машиноместу с типом 'Прочее'", priority = 6)
    public void addInvoiceParkingOtherType() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        click(page.addNewInvoice);
        try {
            waitBy(page.addInvoiceElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления счета");
        }

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

        click(page.dropDownRenter);
        sendKeys(page.inputChoiseRenter, "202" + Keys.ENTER);

        elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);
        assertTrue(findElement(By.xpath("//button/span/span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]")).isDisplayed());

        selectByText(page.objectNumberSelect, "A70");

        sendKeys(page.invoicePurpose, "Автотестовые прочие - машиноместо");

        selectByValue(page.invoiceTypeSelect, "other");

        elementIsNotVisible(page.periodCalendar1);

        sendKeys(page.sumInvoice, "0,55");

        click(page.dateCreatedCalendar);
        click(page.nextMonth);
        click(page.setDateCreatedServicesAndOthers);
        String date1 = findElement(page.dateCreatedCalendar).getAttribute("value");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);
        elementIsNotVisible(By.xpath("//button[text()='Сформировать счет'][contains(@class,'disabled')]"), 30);

        click(page.createInvoice);
        try {
            waitBy(page.toastNewInvoice);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент тоста о добавлении счета");
        }

        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);

        assertTrue(findElement(page.invoiceNumber).getText().contains("A70"));
        assertTrue(findElement(page.typeInvoiceList).getText().contains("Прочее"));
        assertTrue(findElement(page.purposeInvoiceList).getText().equals("Автотестовые прочие - машиноместо"));
        assertTrue(findElement(page.sumInvoiceList).getText().contains("0,55"));
        assertTrue(findElement(page.objectList).getText().contains("м/м A70"));
        assertTrue(findElement(page.renterList).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberList).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.statusListNotPaid).isDisplayed());

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.cardInvoiceElement).getText().contains("Счет A70"));
        assertTrue(findElement(page.statusCardtNotPaid).isDisplayed());
        assertTrue(findElement(page.renterCard).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        assertTrue(findElement(page.renterPhoneNumberCard).getText().equals("+7 927 240-11-45"));
        assertTrue(findElement(page.objectCard).getText().contains("A70"));
        assertTrue(findElement(page.typeInvoiceCard).getText().equals("Прочее"));
        assertTrue(findElement(page.purposeInvoiceCard).getText().equals("Автотестовые прочие - машиноместо"));
        assertTrue(findElement(page.sumInvoiceCard).getText().contains("0,55"));
        String date2 = findElement(page.dateCreatedCard).getText();
        assertTrue(date1.contains(date2));

        List<WebElement> files = driver.findElements(By.xpath("//div/img"));
        assertTrue(files.size() == 2);
        assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

    }

}
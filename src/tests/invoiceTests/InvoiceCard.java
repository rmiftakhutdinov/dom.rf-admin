package tests.invoiceTests;

import framework.Configuration;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.InvoicePage;
import java.io.File;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.assertTrue;

public class InvoiceCard extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Просмотр добавленных файлов")
    public void viewAddedFiles() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        objectsSection();
        click(globalpage.invoiceSection);
        invoiceSection();

        waitBy(globalpage.searchInput);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_apartment.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);

        elementIsNotVisible(globalpage.spinner);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.cardInvoiceElement).getText().replace("Счет ", "").replace("Не оплачен", "").trim().equals(number));

        click(page.photo);
        try {
            waitBy(page.openPhoto);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Элемент открытого в галерее фото не найден");
        }

        String photo1 = new String(findElement(page.openPhoto).getAttribute("src"));
        findElement(page.nextPhoto).click();
        sleep(1000);
        String photo2 = new String(findElement(page.openPhoto).getAttribute("src"));
        Assert.assertFalse(photo1.equals(photo2));
        findElement(page.prevPhoto).click();
        sleep(1000);
        Assert.assertFalse(photo2.equals(photo1));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        String handleBefore = driver.getWindowHandle();
        String pdf = findElement(page.pdf).getAttribute("href");

        click(page.pdf);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        Assert.assertTrue(driver.getCurrentUrl().equals(pdf));
        driver.close();
        driver.switchTo().window(handleBefore);

        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

    }

    @Test(description = "Добавление комментария в карточке счета", priority = 1)
    public void addNewCommentInInvoiceCard() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        waitBy(globalpage.searchInput);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_apartment.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);

        elementIsNotVisible(globalpage.spinner);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        waitBy(page.textareaCommentCard);
        sendKeys(page.textareaCommentCard, "Комментарий из карточки счета");

        click(page.cardAddComment);
        try {
            waitBy(page.toastNewComment);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент тоста о добавлении комментария");
        }

        try {
            waitBy(page.newCommentCard);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент добавленного комментария");
        }

        assertTrue(findElement(page.newCommentCard).getText().equals("Комментарий из карточки счета"));

        driver.navigate().back();
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.commentList).getText().equals("Комментарий из карточки счета"));

    }

    @Test(description = "Изменение назначения в карточке счета", priority = 2)
    public void changePurposeInInvoiceCard() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        waitBy(globalpage.searchInput);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_apartment.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);

        elementIsNotVisible(globalpage.spinner);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        click(page.cardChangePurpose);
        try {
            waitBy(page.changePurposeCardModalElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент модального окна об изменении назначения");
        }

        click(page.cancelChangePurposeCardModal);
        elementIsNotVisible(page.cancelChangePurposeCardModal);

        click(page.cardChangePurpose);
        try {
            waitBy(page.changePurposeCardModalElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент модального окна об изменении назначения");
        }

        sendKeys(page.textareaChangePurposeCardModal, "Измененная автотестовая аренда - апартамент");

        click(page.saveChangePurposeCardModal);
        /*try {
            waitBy(page.toastChangePurpose);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент тоста об изменении назначения"); - имеется баг - выводится другой тост!!!
        }*/

        try {
            waitBy(By.xpath("//div[@id='options']//div[text()='Измененная автотестовая аренда - апартамент']"));
        } catch (TimeoutException ex) {
            afterExeptionMessage("Назначение счета не изменилось");
        }

        driver.navigate().back();
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.purposeInvoiceList).getText().equals("Измененная автотестовая аренда - апартамент"));

    }

    @Test(description = "Изменение суммы в карточке счета", priority = 3)
    public void changeSumInInvoiceCard() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        waitBy(globalpage.searchInput);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_apartment.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);

        elementIsNotVisible(globalpage.spinner);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        click(page.cardChangeSum);
        try {
            waitBy(page.changeSumModalElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент модального окна об изменении суммы");
        }

        click(page.cancelChangeSumModal);
        elementIsNotVisible(page.cancelChangeSumModal);

        click(page.cardChangeSum);
        try {
            waitBy(page.changeSumModalElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент модального окна об изменении суммы");
        }

        sendKeys(page.inputChangeSumModal, "77000,77");

        click(page.saveChangeSumModal);

        String a = findElement(page.toastChangeSum).getText().trim();
        a.replace(" №", "");
        waitBy(page.returnToastChangeSum(a));
        String text = "Счет №" + number + " изменен";

        try {
            assertTrue(a.equals(text));
        } catch (AssertionError ex) {
            afterExeptionMessage("Не найден элемент тоста об изменении суммы");
        }

        try {
            new WebDriverWait(driver, 10)
                    .until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath("//div[@class='form-horizontal']/div[4]/div[2]")), "77 000,77 \u20BD"));
        }catch (TimeoutException ex){
            afterExeptionMessage("Сумма счета не изменилась");
        }

        driver.navigate().back();
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.sumInvoiceList).getText().contains("77 000,77"));

    }

    @Test(description = "Смена статуса в карточке счета", priority = 4)
    public void changeStatusInInvoiceCard() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(globalpage.searchInput);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_apartment.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);

        elementIsNotVisible(globalpage.spinner);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        click(page.dropDownMenuStatusCard);
        click(page.cardPaid);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.cancelChangeStatusModal);
        elementIsNotVisible(page.cancelChangeStatusModal);

        click(page.dropDownMenuStatusCard);
        click(page.cardPaid);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.saveChangeStatusModal);
        try {
            waitBy(page.toastChangeStatus);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        try {
            waitBy(page.statusCardPaid);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Лейбл статуса счета не изменился на 'Оплачен'");
        }

        elementIsNotVisible(page.cardChangePurpose);
        elementIsNotVisible(page.cardChangeSum);

        driver.navigate().back();
        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.statusListPaid).isDisplayed());
        assertTrue(findElement(page.paidFrom).getText().equals("в АИЖК"));

        click(page.dropDownMenuList);

        elementIsNotVisible(page.listChangePurpose);
        elementIsNotVisible(page.listChangeSum);

        click(page.dropDownMenuList);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        click(page.dropDownMenuStatusCard);
        click(page.cardToReturn);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.saveChangeStatusModal);
        try {
            waitBy(page.toastChangeStatus);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        try {
            waitBy(page.statusCardToReturn);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Лейбл статуса счета не изменился на 'К возврату'");
        }

        elementIsNotVisible(page.cardChangePurpose);
        elementIsNotVisible(page.cardChangeSum);

        driver.navigate().back();
        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.statusListToReturn).isDisplayed());

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        click(page.dropDownMenuStatusCard);
        click(page.cardReturned);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.saveChangeStatusModal);
        try {
            waitBy(page.toastChangeStatus);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        try {
            waitBy(page.statusCardReturned);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Лейбл статуса счета не изменился на 'Возвращено'");
        }

        elementIsNotVisible(page.cardChangePurpose);
        elementIsNotVisible(page.cardChangeSum);

        driver.navigate().back();
        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.statusListReturned).isDisplayed());

        click(page.dropDownMenuList);

        elementIsNotVisible(page.listChangePurpose);
        elementIsNotVisible(page.listChangeSum);

        click(page.dropDownMenuList);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        click(page.dropDownMenuStatusCard);
        click(page.cardCanceled);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.saveChangeStatusModal);
        try {
            waitBy(page.toastChangeStatus);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        try {
            waitBy(page.statusCardCanceled);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Лейбл статуса счета не изменился на 'Отменен'");
        }

        elementIsNotVisible(page.cardChangePurpose);
        elementIsNotVisible(page.cardChangeSum);

        driver.navigate().back();
        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.statusListCanceled).isDisplayed());

        click(page.dropDownMenuList);

        elementIsNotVisible(page.listChangePurpose);
        elementIsNotVisible(page.listChangeSum);

        click(page.dropDownMenuList);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        click(page.dropDownMenuStatusCard);
        click(page.cardNotPaid);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.saveChangeStatusModal);
        try {
            waitBy(page.toastChangeStatus);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        try {
            waitBy(page.statusCardtNotPaid);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Лейбл статуса счета не изменился на 'Не оплачен'");
        }

        waitBy(page.cardChangePurpose);
        waitBy(page.cardChangeSum);

        driver.navigate().back();
        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.statusListNotPaid).isDisplayed());

        click(page.dropDownMenuList);

        waitBy(page.listChangePurpose);
        waitBy(page.listChangeSum);

    }

}
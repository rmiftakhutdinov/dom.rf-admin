package tests.invoiceTests;

import framework.Configuration;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.InvoicePage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import static org.testng.Assert.assertTrue;

public class InvoiceFilters extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Строка поиска в списке счетов")
    public void searchLineInvoice() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        objectsSection();
        click(globalpage.invoiceSection);
        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_parking.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);

        waitBy(globalpage.spinner);
        waitBy(page.invoiceNumber);
        assertTrue(findElement(page.invoiceNumber).getText().equals(number));

        sendKeys(globalpage.searchInput, "A70" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        List<WebElement> numbers = driver.findElements(page.invoiceNumber);
        for (WebElement invoiceNumber : numbers) {

            if (invoiceNumber.getText().contains("A70")){

                break;

            } else {

                afterExeptionMessage("Номер объекта не найден");
                driver.quit();

            }

        }

        sendKeys(globalpage.searchInput, "Мифтахутдинов Рамиль" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        List<WebElement> renters = driver.findElements(page.renterList);
        for (WebElement renter : renters) {

            if (renter.getText().trim().equals("Мифтахутдинов Рамиль Рустэмович")){

                break;

            } else {

                afterExeptionMessage("Арендатор не найден");
                driver.quit();

            }

        }

    }

    @Test(description = "Фильтр по статусам в списке счетов", priority = 1)
    public void statusFilterInvoice() {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(findElement(By.xpath("//tbody/tr/td[6]/div/span")), "innerText"));

        Set<String> allStatuses = driver.findElements(By.xpath("//tbody/tr/td[6]/div/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);

        selectByValue(page.statusFilter, "string:new");
        elementIsNotVisible(globalpage.spinner);
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(findElement(By.xpath("//tbody/tr/td[6]/div/span")), "innerText"));

        Set<String> statusNotPaid = driver.findElements(By.xpath("//tbody/tr/td[6]/div/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(statusNotPaid.contains("Не оплачен"));
        Assert.assertFalse(statusNotPaid.contains("Оплачен"));
        Assert.assertFalse(statusNotPaid.contains("К возврату"));
        Assert.assertFalse(statusNotPaid.contains("Возвращено"));
        Assert.assertFalse(statusNotPaid.contains("Отменен"));

        selectByValue(page.statusFilter, "string:paid");
        elementIsNotVisible(globalpage.spinner);
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(findElement(By.xpath("//tbody/tr/td[6]/div/span")), "innerText"));

        Set<String> statusPaid = driver.findElements(By.xpath("//tbody/tr/td[6]/div/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusPaid.contains("Не оплачен"));
        Assert.assertTrue(statusPaid.contains("Оплачен"));
        Assert.assertFalse(statusPaid.contains("К возврату"));
        Assert.assertFalse(statusPaid.contains("Возвращено"));
        Assert.assertFalse(statusPaid.contains("Отменен"));

        selectByValue(page.statusFilter, "string:toReturn");
        elementIsNotVisible(globalpage.spinner);
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(findElement(By.xpath("//tbody/tr/td[6]/div/span")), "innerText"));

        Set<String> statusToReturn = driver.findElements(By.xpath("//tbody/tr/td[6]/div/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusToReturn.contains("Не оплачен"));
        Assert.assertFalse(statusToReturn.contains("Оплачен"));
        Assert.assertTrue(statusToReturn.contains("К возврату"));
        Assert.assertFalse(statusToReturn.contains("Возвращено"));
        Assert.assertFalse(statusToReturn.contains("Отменен"));

        selectByValue(page.statusFilter, "string:returned");
        elementIsNotVisible(globalpage.spinner);
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(findElement(By.xpath("//tbody/tr/td[6]/div/span")), "innerText"));

        Set<String> statusReturned = driver.findElements(By.xpath("//tbody/tr/td[6]/div/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusReturned.contains("Не оплачен"));
        Assert.assertFalse(statusReturned.contains("Оплачен"));
        Assert.assertFalse(statusReturned.contains("К возврату"));
        Assert.assertTrue(statusReturned.contains("Возвращено"));
        Assert.assertFalse(statusReturned.contains("Отменен"));

        selectByValue(page.statusFilter, "string:cancel");
        elementIsNotVisible(globalpage.spinner);
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(findElement(By.xpath("//tbody/tr/td[6]/div/span")), "innerText"));

        Set<String> statusCancel = driver.findElements(By.xpath("//tbody/tr/td[6]/div/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusCancel.contains("Не оплачен"));
        Assert.assertFalse(statusCancel.contains("Оплачен"));
        Assert.assertFalse(statusCancel.contains("К возврату"));
        Assert.assertFalse(statusCancel.contains("Возвращено"));
        Assert.assertTrue(statusCancel.contains("Отменен"));

    }

    @Test(description = "Фильтр по типам в списке счетов", priority = 2)
    public void TypeFilterInvoice() {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        Set<String> allTypes = driver.findElements(page.typeInvoiceList).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allTypes.size() > 1);

        selectByValue(page.typeFilter, "string:rent");
        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        Set<String> typeRent = driver.findElements(page.typeInvoiceList).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        for (String rent : typeRent){

            String splitString[] = rent.split(","); // split - разбить строки, запятая - разделитель строк
            Assert.assertTrue(splitString[0].contains("Аренда"));
            Assert.assertFalse(splitString[0].contains("Коммунальные"));
            Assert.assertFalse(splitString[0].contains("Депозит"));
            Assert.assertFalse(splitString[0].contains("Услуги"));
            Assert.assertFalse(splitString[0].contains("Прочее"));

        }

        selectByValue(page.typeFilter, "string:communal");
        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        Set<String> typeCommunal = driver.findElements(page.typeInvoiceList).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        for (String communal : typeCommunal){

            String splitString[] = communal.split(",");
            Assert.assertFalse(splitString[0].contains("Аренда"));
            Assert.assertTrue(splitString[0].contains("Коммунальные"));
            Assert.assertFalse(splitString[0].contains("Депозит"));
            Assert.assertFalse(splitString[0].contains("Услуги"));
            Assert.assertFalse(splitString[0].contains("Прочее"));

        }

        click(page.showInvoiceDetail);
        waitBy(page.coldWater);
        waitBy(page.hotWater);
        waitBy(page.electric);
        waitBy(page.heating);
        waitBy(page.wastewater);
        waitBy(page.heatingWater);
        click(page.hideInvoiceDetail);
        elementIsNotVisible(page.hideInvoiceDetail);
        elementIsNotVisible(page.coldWater);

        selectByValue(page.typeFilter, "string:deposit");
        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        Set<String> typeDeposit = driver.findElements(page.typeInvoiceList).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(typeDeposit.contains("Аренда"));
        Assert.assertFalse(typeDeposit.contains("Коммунальные"));
        Assert.assertTrue(typeDeposit.contains("Депозит"));
        Assert.assertFalse(typeDeposit.contains("Услуги"));
        Assert.assertFalse(typeDeposit.contains("Прочее"));

        selectByValue(page.typeFilter, "string:service");
        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        Set<String> typeService = driver.findElements(page.typeInvoiceList).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(typeService.contains("Аренда"));
        Assert.assertFalse(typeService.contains("Коммунальные"));
        Assert.assertFalse(typeService.contains("Депозит"));
        Assert.assertTrue(typeService.contains("Услуги"));
        Assert.assertFalse(typeService.contains("Прочее"));

        selectByValue(page.typeFilter, "string:other");
        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        Set<String> typeOther = driver.findElements(page.typeInvoiceList).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(typeOther.contains("Аренда"));
        Assert.assertFalse(typeOther.contains("Коммунальные"));
        Assert.assertFalse(typeOther.contains("Депозит"));
        Assert.assertFalse(typeOther.contains("Услуги"));
        Assert.assertTrue(typeOther.contains("Прочее"));

    }

    @Test(description = "Фильтр по просрочке оплаты в списке счетов", priority = 3)
    public void delayedPaymentsFilterInvoice() {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        sendKeys(page.delayedPaymentFrom, "50");
        sendKeys(page.delayedPaymentTo, "100");
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBeNotEmpty(findElement(By.xpath("//tbody/tr/td[6]/div/span")), "innerText"));

        Set<String> delayedElements = driver.findElements(By.xpath("//tbody/tr/td[6]/div//small")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        for (String delayed : delayedElements){

            String a = delayed.replace("Просрочен на ", "").replace(" дней", "").replace(" дня", "").replace(" день", "").trim();
            int i = Integer.parseInt(a);

            if (i < 50 || i > 100){

                afterExeptionMessage("В диапазон просроченных дней попал другой день");
                driver.quit();

            }

        }

    }

    @Test(description = "Фильтр по датам в списке счетов", priority = 4)
    public void dateFilterInvoice() {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        click(globalpage.periodFilter);
        click(globalpage.period7Days);
        Assert.assertTrue(findElement(globalpage.periodFilter).getText().contains("Последние 7 дней"));

        click(globalpage.periodFilter);
        click(globalpage.period14Days);
        Assert.assertTrue(findElement(globalpage.periodFilter).getText().contains("Последние 14 дней"));

        click(globalpage.periodFilter);
        click(globalpage.period30Days);
        Assert.assertTrue(findElement(globalpage.periodFilter).getText().contains("Последние 30 дней"));

        click(globalpage.periodFilter);
        click(globalpage.period90Days);
        Assert.assertTrue(findElement(globalpage.periodFilter).getText().contains("Последние 90 дней"));

        click(globalpage.periodFilter);
        click(globalpage.periodDiapason1);
        Assert.assertTrue(findElement(page.periodDiapasonElement).isDisplayed());

        click(globalpage.periodFilter);
        click(globalpage.periodAllDate);
        Assert.assertTrue(findElement(globalpage.periodFilter).getText().contains("За все время"));

    }

    @Test(description = "Сброс фильтров в списке счетов", priority = 5)
    public void resetFiltersInvoice() {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        waitBy(globalpage.searchInput);

        findElement(globalpage.searchInput).sendKeys("Всякая дребедень" + Keys.ENTER);
        elementIsNotVisible(globalpage.spinner,30);

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);

        selectByValue(page.typeFilter, "string:communal");
        elementIsNotVisible(globalpage.spinner);

        sendKeys(page.delayedPaymentFrom, "50");
        sendKeys(page.delayedPaymentTo, "100");
        elementIsNotVisible(globalpage.spinner);

        click(globalpage.periodFilter);
        click(globalpage.period90Days);
        elementIsNotVisible(globalpage.spinner);

        waitBy(page.noInvoiceElement);

        click(globalpage.filterReset);
        try {
            waitBy(page.invoiceNumber);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден элемент счета");
        }

        Assert.assertTrue(findElement(globalpage.searchInput).getAttribute("value").equals(""));

        Set<String> statusNotPaid = driver.findElements(By.xpath("//tbody/tr/td[6]/div/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(statusNotPaid.contains("Не оплачен"));
        Assert.assertFalse(statusNotPaid.contains("Oплачен"));
        Assert.assertFalse(statusNotPaid.contains("К возврату"));
        Assert.assertFalse(statusNotPaid.contains("Возвращено"));
        Assert.assertFalse(statusNotPaid.contains("Отменён"));

        Set<String> allTypes = driver.findElements(page.typeInvoiceList).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allTypes.size() > 1);

        Assert.assertTrue(findElement(page.delayedPaymentFrom).getAttribute("value").equals(""));
        Assert.assertTrue(findElement(page.delayedPaymentTo).getAttribute("value").equals(""));

        Assert.assertTrue(findElement(globalpage.periodFilter).getText().contains("За все время"));

    }

}
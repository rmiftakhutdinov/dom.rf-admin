package tests.invoiceTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.InvoicePage;
import java.io.File;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.assertTrue;

public class InvoiceList extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход по ссылкам в списке счетов")
    public void penLinksInInvoiceList() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        objectsSection();
        click(globalpage.invoiceSection);
        invoiceSection();

        waitBy(globalpage.searchInput);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_parking.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);

        elementIsNotVisible(globalpage.spinner);

        assertTrue(findElement(page.invoiceNumber).getText().equals(number));

        click(page.typeInvoiceList);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        driver.navigate().back();
        waitBy(page.invoiceNumber);

        click(page.objectList);
        String handleBefore = driver.getWindowHandle();
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            waitBy(globalpage.parkingCardElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Элемент карточки машиноместа не найден");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.renterList);
        try {
            waitBy(globalpage.renterCardElement, 60);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Элемент карточки арендатора не найден");
        }

    }

    @Test(description = "Добавление комментария в списке счетов", priority = 1)
    public void addNewCommentInInvoiceList() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        waitBy(globalpage.searchInput);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_parking.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);

        elementIsNotVisible(globalpage.spinner);

        click(page.dropDownMenuList);
        click(page.listAddComment);
        try {
            waitBy(page.addCommentListModalElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент модального окна о добавлении комментария");
        }

        click(page.cancelCommentAndPurposeModalList);
        elementIsNotVisible(page.addCommentListModalElement);

        click(page.dropDownMenuList);
        click(page.listAddComment);
        try {
            waitBy(page.addCommentListModalElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент модального окна о добавлении комментария");
        }

        sendKeys(page.textareaCommentAndPurposeModalList, "Комментарий из списка счетов");

        click(page.saveCommentAndPurposeModalList);
        try {
            waitBy(page.toastNewComment);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент тоста о добавлении комментария");
        }

        driver.navigate().refresh();
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.commentList).getText().equals("Комментарий из списка счетов"));

        click(page.invoiceNumber);
        try {
            waitBy(page.newCommentCard);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент добавленного комментария");
        }

        assertTrue(findElement(page.newCommentCard).getText().equals("Комментарий из списка счетов"));

    }

    @Test(description = "Изменение назначения в списке счетов", priority = 2)
    public void changePurposeInInvoiceList() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        waitBy(globalpage.searchInput);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_parking.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);

        elementIsNotVisible(globalpage.spinner);

        click(page.dropDownMenuList);
        click(page.listChangePurpose);
        try {
            waitBy(page.changePurposeListModalElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент модального окна об изменении назначения");
        }

        click(page.cancelCommentAndPurposeModalList);
        elementIsNotVisible(page.cancelCommentAndPurposeModalList);

        click(page.dropDownMenuList);
        click(page.listChangePurpose);
        try {
            waitBy(page.changePurposeListModalElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент модального окна об изменении назначения");
        }

        sendKeys(page.textareaCommentAndPurposeModalList, "Измененная автотестовая аренда - машиноместо");

        click(page.saveCommentAndPurposeModalList);
        try {
            waitBy(page.toastChangePurpose);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент тоста об изменении назначения");
        }

        driver.navigate().refresh();
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.purposeInvoiceList).getText().equals("Измененная автотестовая аренда - машиноместо"));

        click(page.invoiceNumber);
        try {
            waitBy(page.purposeInvoiceCard);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент назначения счета");
        }

        assertTrue(findElement(page.purposeInvoiceCard).getText().equals("Измененная автотестовая аренда - машиноместо"));

    }

    @Test(description = "Изменение суммы в списке счетов", priority = 3)
    public void changeSumInInvoiceList() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        waitBy(globalpage.searchInput);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_parking.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);

        elementIsNotVisible(globalpage.spinner);

        click(page.dropDownMenuList);
        click(page.listChangeSum);
        try {
            waitBy(page.changeSumModalElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент модального окна об изменении суммы");
        }

        click(page.cancelChangeSumModal);
        elementIsNotVisible(page.cancelChangeSumModal);

        click(page.dropDownMenuList);
        click(page.listChangeSum);
        try {
            waitBy(page.changeSumModalElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент модального окна об изменении суммы");
        }

        waitBy(By.xpath("//label[text()='Сумма счета']"));
        sendKeys(page.inputChangeSumModal, "22000,22");

        elementIsNotVisible(By.xpath("//div[@id='payment_edit_modal']//a[@disabled][text()='Сохранить']"));

        click(page.saveChangeSumModal);
        try {
            waitBy(page.toastChangeSum);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент тоста об изменении суммы");
        }

        driver.navigate().refresh();
        waitBy(page.invoiceNumber);

        assertTrue(findElement(page.sumInvoiceList).getText().contains("22 000,22 \u20BD"));

        click(page.invoiceNumber);
        try {
            waitBy(page.sumInvoiceCard);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент суммы счета");
        }

        assertTrue(findElement(page.sumInvoiceCard).getText().equals("22 000,22 \u20BD"));

    }

    @Test(description = "Смена статуса в списке счетов", priority = 4)
    public void changeStatusInInvoiceList() throws Exception {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\invoice\\invoice_parking.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        sendKeys(globalpage.searchInput, number + Keys.ENTER);
        waitBy(globalpage.spinner);
        waitBy(page.invoiceNumber);

        click(page.dropDownMenuList);
        click(page.listPaid);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.cancelChangeStatusModal);
        elementIsNotVisible(page.cancelChangeStatusModal);

        click(page.dropDownMenuList);
        click(page.listPaid);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.saveChangeStatusModal);
        try {
            waitBy(page.toastChangeStatus);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        try {
            waitBy(page.statusListPaid);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Лейбл статуса счета не изменился на 'Оплачен'");
        }

        assertTrue(findElement(page.paidFrom).getText().equals("в АИЖК"));

        click(page.dropDownMenuList);

        elementIsNotVisible(page.listChangePurpose);
        elementIsNotVisible(page.listChangeSum);

        click(page.dropDownMenuList);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.statusCardPaid).isDisplayed());
        elementIsNotVisible(page.cardChangePurpose);
        elementIsNotVisible(page.cardChangeSum);

        driver.navigate().back();
        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        click(page.dropDownMenuList);
        click(page.listToReturn);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.saveChangeStatusModal);
        try {
            waitBy(page.toastChangeStatus);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        try {
            waitBy(page.statusListToReturn);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Лейбл статуса счета не изменился на 'К возврату'");
        }

        click(page.dropDownMenuList);

        elementIsNotVisible(page.listChangePurpose);
        elementIsNotVisible(page.listChangeSum);

        click(page.dropDownMenuList);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.statusCardToReturn).isDisplayed());
        elementIsNotVisible(page.cardChangePurpose);
        elementIsNotVisible(page.cardChangeSum);

        driver.navigate().back();
        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        click(page.dropDownMenuList);
        click(page.listReturned);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.saveChangeStatusModal);
        try {
            waitBy(page.toastChangeStatus);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        try {
            waitBy(page.statusListReturned);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Лейбл статуса счета не изменился на 'Возвращено'");
        }

        click(page.dropDownMenuList);

        elementIsNotVisible(page.listChangePurpose);
        elementIsNotVisible(page.listChangeSum);

        click(page.dropDownMenuList);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.statusCardReturned).isDisplayed());
        elementIsNotVisible(page.cardChangePurpose);
        elementIsNotVisible(page.cardChangeSum);

        driver.navigate().back();
        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        click(page.dropDownMenuList);
        click(page.listCanceled);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.saveChangeStatusModal);
        try {
            waitBy(page.toastChangeStatus);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        try {
            waitBy(page.statusListCanceled);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Лейбл статуса счета не изменился на 'Отменен'");
        }

        click(page.dropDownMenuList);

        elementIsNotVisible(page.listChangePurpose);
        elementIsNotVisible(page.listChangeSum);

        click(page.dropDownMenuList);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.statusCardCanceled).isDisplayed());
        elementIsNotVisible(page.cardChangePurpose);
        elementIsNotVisible(page.cardChangeSum);

        driver.navigate().back();
        elementIsNotVisible(globalpage.spinner);

        click(page.statusFilter);
        click(page.allStatuses);

        elementIsNotVisible(globalpage.spinner);
        waitBy(page.invoiceNumber);

        click(page.dropDownMenuList);
        click(page.listNotPaid);
        try {
            waitBy(page.changeStatusModalElement);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент модального окна о смене статуса");
        }

        click(page.saveChangeStatusModal);
        try {
            waitBy(page.toastChangeStatus);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        try {
            waitBy(page.statusListNotPaid);
        } catch (RuntimeException ex) {
            afterExeptionMessage("Лейбл статуса счета не изменился на 'Возвращено'");
        }

        click(page.dropDownMenuList);

        waitBy(page.listChangePurpose);
        waitBy(page.listChangeSum);

        click(page.dropDownMenuList);

        click(page.invoiceNumber);
        try {
            waitBy(page.cardInvoiceElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент карточки счета");
        }

        assertTrue(findElement(page.statusCardtNotPaid).isDisplayed());
        waitBy(page.cardChangePurpose);
        waitBy(page.cardChangeSum);

    }

    @Test(description = "Пагинация в списке счетов", priority = 5)
    public void invoiceListPagination() {

        GlobalPage globalpage = new GlobalPage();
        InvoicePage page = new InvoicePage();

        click(globalpage.objectsSection);
        click(globalpage.invoiceSection);
        invoiceSection();

        waitBy(globalpage.nextPage, 30);

        String page1 = new String(findElement(page.invoiceNumber).getAttribute("href"));

        click(globalpage.nextPage,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        String page2 = new String(findElement(page.invoiceNumber).getAttribute("href"));
        Assert.assertFalse(page1.equals(page2));

        click(globalpage.prevPage,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

        click(globalpage.page2,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        click(globalpage.page1,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

    }

}
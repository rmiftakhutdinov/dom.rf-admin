package tests.invoiceTests;

import framework.Configuration;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.openqa.selenium.*;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.InvoicePage;
import java.util.concurrent.TimeUnit;

@Ignore
public class TestForRoskap extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Добавление счета по апартаменту с типом 'Аренда'")
    public void addInvoiceApartmentRentType(){

        InvoicePage page = new InvoicePage();

        int i;

        for (i=0; i<100; i++){

            driver.get("http://dev-arenda.ahml.ru/admin/invoice-edit?id=0");
            try {
                waitBy(page.addInvoiceElement);
            } catch (NoSuchElementException ex) {
                afterExeptionMessage("Не найден элемент страницы добавления счета");
            }

            elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

            click(page.dropDownRenter);
            sendKeys(page.inputChoiseRenter, "276" + Keys.ENTER);

            elementIsNotVisible(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

            sendKeys(page.invoicePurpose, "Счет для Роскапа");

            String sum = randomString("12345", 1);
            sendKeys(page.sumInvoice, sum);

            click(page.createInvoice);
            try {
                waitBy(page.toastNewInvoice);
            } catch (NoSuchElementException ex) {
                afterExeptionMessage("Не найден элемент тоста о добавлении счета");
            }

        }

    }

}
package tests.notificationsTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.NotificationPage;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ModalWindowByAddRecipients extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Строка поиска в модальном окне выбора получателей")
    public void searchLineInModalWindow() {

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        objectsSection();

        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателей");
        }

        // Поиск по ФИО
        sendKeys(page.filterModal, "Мифтахутдинов Рамиль Рустэмович");
        Assert.assertTrue(findElement(By.xpath("//tr/td[3]/nobr")).getText().contains("Мифтахутдинов Рамиль Рустэмович"));

        // Поиск по номеру апартамента
        findElement(page.filterModal).clear();
        sendKeys(page.filterModal, "250");
        List<WebElement> apartments = driver.findElements(By.xpath("//tr/td[2]/span"));
        for (WebElement apartment : apartments){
            if (apartment.isDisplayed()) {
                if (findElement(By.xpath("//tr/td[2]/span")).getText().contains("250")) {
                    break;
                }
            }
        }

        // Поиск по номеру машиноместа
        findElement(page.filterModal).clear();
        sendKeys(page.filterModal, "A70");
        List<WebElement> parkings = driver.findElements(By.xpath("//tr/td[2]/span"));
        for (WebElement parking : parkings){
            if (parking.isDisplayed()) {
                if (findElement(By.xpath("//tr/td[2]/span")).getText().contains("A70")) {
                    break;
                }
            }
        }

        // Поиск по этажу
        findElement(page.filterModal).clear();
        sendKeys(page.filterModal, "15");
        List<WebElement> recipients = driver.findElements(By.xpath("//tbody/tr"));
        for (WebElement recipient : recipients){
            Assert.assertTrue(recipient.getText().contains("15"));
        }

        click(page.closeModal);
        elementIsNotVisible(page.chooseRecipientsModal);

    }

    @Test(description = "Выбор получателей по этажу", priority = 1)
    public void choiceRecipientsByClickFloor() {

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателей");
        }

        try {
            findElement(By.xpath("//tr/td[4]/span/a[contains(text(),'15')]")).click();
        } catch (NoSuchElementException ex){
            throw new RuntimeException("Не найден элемент 15-го этажа");
        }

        List<String> strings = driver.findElements(By.xpath("//tbody/tr/td[4]//a[contains(text(),'15')]"))
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());

        for (String string : strings){

            Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[4]//a[contains(text(),'" + string + "')]/ancestor::tr/td[1]/div//input")).isSelected());

        }

    }

}
package tests.notificationsTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.NotificationPage;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class NotificationCard extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Удаление СМС уведомления")
    public void deleteSMSNotification() {

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        objectsSection();
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        try {
            findElement(By.xpath("//tr/td[4]/small/a[text()='Автотестовая СМС-ка для одного получателя']")).click();
        } catch (NoSuchElementException ex){
            throw new RuntimeException("Не найден элемент текста СМС уведомления");
        }

        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

        Assert.assertTrue(findElement(page.typeNotification).getText().equals("СМС"));
        Assert.assertTrue(findElement(page.textNotification).getText().equals("Автотестовая СМС-ка для одного получателя"));
        Assert.assertTrue(findElement(page.recipientName).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(page.recipientPhone).getText().equals("+7 927 240-11-45"));

        click(page.deleteNotification);
        try {
            waitBy(page.deleteModalElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления уведомления");
        }

        click(page.closeModal);
        elementIsNotVisible(page.deleteModalElement);

        click(page.deleteNotification);
        try {
            waitBy(page.deleteModalElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления уведомления");
        }

        click(page.deleteConfirm);
        try {
            waitBy(page.notificationDeleteToast);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста удаления уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

    }

    @Test(description = "Удаление ПУШ уведомления", priority = 1)
    public void deletePushNotification() {

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        try {
            findElement(By.xpath("//tr/td[4]/small/a[text()='Автотестовый ПУШ для одного получателя']")).click();
        } catch (NoSuchElementException ex){
            throw new RuntimeException("Не найден элемент текста ПУШ уведомления");
        }

        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

        Assert.assertTrue(findElement(page.typeNotification).getText().equals("Пуш"));
        Assert.assertTrue(findElement(page.textNotification).getText().equals("Автотестовый ПУШ для одного получателя"));
        Assert.assertTrue(findElement(page.recipientName).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(page.recipientPhone).getText().equals("+7 927 240-11-45"));

        click(page.deleteNotification);
        try {
            waitBy(page.deleteModalElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления уведомления");
        }

        click(page.closeModal);
        elementIsNotVisible(page.deleteModalElement);

        click(page.deleteNotification);
        try {
            waitBy(page.deleteModalElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления уведомления");
        }

        click(page.deleteConfirm);
        try {
            waitBy(page.notificationDeleteToast);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста удаления уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

    }

    @Test(description = "Удаление черновика СМС", priority = 2)
    public void deleteSMSDraft() {

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        try {
            findElement(By.xpath("//tr/td[4]/small/a[text()='Автотестовый черновик СМС с текстом и получателем']")).click();
        } catch (NoSuchElementException ex){
            throw new RuntimeException("Не найден элемент текста черновика СМС");
        }

        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

        Assert.assertTrue(findElement(page.typeNotification).getText().equals("СМС"));
        Assert.assertTrue(findElement(page.textNotification).getText().equals("Автотестовый черновик СМС с текстом и получателем"));
        Assert.assertTrue(findElement(page.recipientName).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(page.recipientPhone).getText().equals("+7 927 240-11-45"));

        click(page.deleteNotification);
        try {
            waitBy(page.deleteModalElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления уведомления");
        }

        click(page.closeModal);
        elementIsNotVisible(page.deleteModalElement);

        click(page.deleteNotification);
        try {
            waitBy(page.deleteModalElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления уведомления");
        }

        click(page.deleteConfirm);
        try {
            waitBy(page.notificationDeleteToast);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста удаления уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

    }

    @Test(description = "Удаление черновика ПУШ", priority = 3)
    public void deletePushDraft() {

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        try {
            findElement(By.xpath("//tr/td[4]/small/a[text()='Автотестовый черновик ПУШ с текстом и получателем']")).click();
        } catch (NoSuchElementException ex){
            throw new RuntimeException("Не найден элемент текста черновика ПУШ");
        }

        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

        Assert.assertTrue(findElement(page.typeNotification).getText().equals("Пуш"));
        Assert.assertTrue(findElement(page.textNotification).getText().equals("Автотестовый черновик ПУШ с текстом и получателем"));
        Assert.assertTrue(findElement(page.recipientName).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(page.recipientPhone).getText().equals("+7 927 240-11-45"));

        click(page.deleteNotification);
        try {
            waitBy(page.deleteModalElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления уведомления");
        }

        click(page.closeModal);
        elementIsNotVisible(page.deleteModalElement);

        click(page.deleteNotification);
        try {
            waitBy(page.deleteModalElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления уведомления");
        }

        click(page.deleteConfirm);
        try {
            waitBy(page.notificationDeleteToast);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста удаления уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

    }

    @Test(description = "Редактирование и сохранение черновика без получателя", priority = 4)
    public void editAndSaveDraftWithoutRecipient(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));
        Assert.assertTrue(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателя");
        }

        sendKeys(page.filterModal, "Мифтахутдинов Рамиль");
        click(page.oneCheckbox);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeSelected(page.oneCheckbox));

        click(page.saveModal);
        try {
            waitBy(By.xpath("//span[contains(text(),'1 получатель')]"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранного получателя");
        }

        elementIsNotVisible(page.chooseRecipientsModal);
        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
        Assert.assertFalse(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.saveByDraft);
        try {
            waitBy(page.notificationSavingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста о сохранении уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("СМС"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr[1]")).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr/a")).getText().equals("+7 927 240-11-45"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Черновик"));

        click(page.notificationNumber);
        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

        click(page.sendNotificationFromCard);
        try {
            waitBy(page.noTextToast);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста об ошибке отправки уведомления");
        }

        click(page.editNotification);
        try {
            waitBy(page.editNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы редактирования уведомления");
        }

        click(page.push);

        Assert.assertTrue(findElement(page.push).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolPush).getText().equals("Максимум 110 символов"));

        sendKeys(page.textarea, "Отредактированный черновик с добавлением текста и удалением получателя");

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.changeRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателя");
        }

        sendKeys(page.filterModal, "Мифтахутдинов Рамиль");
        click(page.oneCheckbox);
        Assert.assertFalse(findElement(page.oneCheckbox).isSelected());

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            waitBy(By.xpath("//span[contains(text(),'Нет получателей')]"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранного получателя");
        }

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.saveByDraft);
        try {
            waitBy(page.notificationSavingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста о сохранении уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("Пуш"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Отредактированный черновик с добавлением текста и удалением получателя"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Черновик"));

    }

    @Test(description = "Отправка уведомления из формы редактирования уведомления", priority = 5)
    public void sendNotificationFromEditNotificationForm(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        click(page.notificationNumber);
        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

        click(page.sendNotificationFromCard);
        try {
            waitBy(page.noRecipientToast);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста об ошибке отправки уведомления");
        }

        click(page.editNotification);
        try {
            waitBy(page.editNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы редактирования уведомления");
        }

        click(page.sms);

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));

        findElement(page.textarea).clear();
        sendKeys(page.textarea, "Отредактированный черновик с добавлением получателя");

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателя");
        }

        sendKeys(page.filterModal, "Мифтахутдинов Рамиль");
        click(page.oneCheckbox);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeSelected(page.oneCheckbox));

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            waitBy(By.xpath("//span[contains(text(),'1 получатель')]"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранного получателя");
        }

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.sendNotification);
        try {
            waitBy(page.notificationSendingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста об отправке уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("СМС"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Отредактированный черновик с добавлением получателя"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr[1]")).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr/a")).getText().equals("+7 927 240-11-45"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Отправлено"));

    }

    @Test(description = "Редактирование и сохранение черновика без текста", priority = 6)
    public void editAndSaveDraftWithoutText(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        try {
            findElement(By.xpath("//tr/td[4]/small/a[text()='Автотестовый черновик СМС без получателя']")).click();
        } catch (NoSuchElementException ex){
            throw new RuntimeException("Не найден элемент текста черновика СМС");
        }

        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

        click(page.editNotification);
        try {
            waitBy(page.editNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы редактирования уведомления");
        }

        findElement(page.textarea).clear();

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателя");
        }

        sendKeys(page.filterModal, "Мифтахутдинов Рамиль");
        click(page.oneCheckbox);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeSelected(page.oneCheckbox));

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            waitBy(By.xpath("//span[contains(text(),'1 получатель')]"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранного получателя");
        }

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
        Assert.assertFalse(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.saveByDraft);
        try {
            waitBy(page.notificationSavingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста о сохранении уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("СМС"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr[1]")).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr/a")).getText().equals("+7 927 240-11-45"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Черновик"));

    }

    @Test(description = "Редактирование и сохранение черновика со всеми данными", priority = 7)
    public void editAndSaveDraftWithTextAndRecipients(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        click(page.notificationNumber);
        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

        click(page.editNotification);
        try {
            waitBy(page.editNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы редактирования уведомления");
        }

        sendKeys(page.textarea, "Отредактированный черновик с добавлением текста");

        click(page.changeRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателя");
        }

        click(page.allCheckboxes);

        List<WebElement> checkboxesOn = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement a : checkboxesOn){
            Assert.assertTrue(a.isSelected());
        }

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            if (findElement(By.xpath("//div[2]/span/span")).getText().contains("получателей")){
                Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
            } else {
                waitBy(By.xpath("//span[contains(text(),'получателя')]"));
            }
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранных получателей");
        }

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
        Assert.assertFalse(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.saveByDraft);
        try {
            waitBy(page.notificationSavingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста о сохранении уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("СМС"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Отредактированный черновик с добавлением текста"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//i")).getText().contains("арендатор"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Черновик"));

    }

    @Test(description = "Отправка уведомления из карточки уведомления", priority = 8)
    public void sendNotificationFromNotificationCard() {

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        click(page.notificationNumber);
        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

        click(page.sendNotificationFromCard);
        try {
            waitBy(page.notificationSendingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста об отправке уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

    }

}
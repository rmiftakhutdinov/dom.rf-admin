package tests.notificationsTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.NotificationPage;
import java.util.concurrent.TimeUnit;

public class NotificationsList extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход в карточку уведомления")
    public void openNotificationCard() {

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        objectsSection();
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        click(page.notificationNumber);
        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

        driver.navigate().back();
        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        try {
            findElement(By.xpath("//tr/td[4]/small/a[contains(text(),'Автотест')]")).click();
        } catch (NoSuchElementException ex){
            throw new RuntimeException("Не найден элемент текста СМС уведомления");
        }

        try {
            waitBy(page.notificationCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки уведомления");
        }

    }

    @Test(description = "Пагинация в списке уведомлений", priority = 1)
    public void notificationListPagination() {

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден номер уведомления");
        }

        waitBy(globalpage.nextPage, 30);

        String page1 = new String(findElement(page.notificationNumber).getAttribute("href"));

        click(globalpage.nextPage,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        String page2 = new String(findElement(page.notificationNumber).getAttribute("href"));
        Assert.assertFalse(page1.equals(page2));

        click(globalpage.prevPage,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

        click(globalpage.page2,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        click(globalpage.page1,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

    }

}
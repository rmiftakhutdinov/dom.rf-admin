package tests.notificationsTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.NotificationPage;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SendAndSaveNotification extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Отправка нового СМС уведомления всем арендаторам")
    public void sendNewSMSToAllRenters(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        objectsSection();
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));

        sendKeys(page.textarea, "Автотестовая СМС-ка для всех получателей");

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателей");
        }

        click(page.allCheckboxes);

        List<WebElement> checkboxesOn = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement a : checkboxesOn){
            Assert.assertTrue(a.isSelected());
        }

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            if (findElement(By.xpath("//div[2]/span/span")).getText().contains("получателей")){
                Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
            } else {
                waitBy(By.xpath("//span[contains(text(),'получателя')]"));
            }
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранных получателей");
        }

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.sendNotification);
        try {
            waitBy(page.notificationSendingToast, 300);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден элемент тоста об отправке уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("СМС"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Автотестовая СМС-ка для всех получателей"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//i")).getText().contains("арендатор"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Отправлено"));

    }

    @Test(description = "Отправка нового СМС уведомления одному арендатору", priority = 1)
    public void sendNewSMSToOneRenter(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));

        sendKeys(page.textarea, "Автотестовая СМС-ка для одного получателя");

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателей");
        }

        click(page.closeModal);
        elementIsNotVisible(page.chooseRecipientsModal);

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателей");
        }

        sendKeys(page.filterModal, "Мифтахутдинов Рамиль");
        click(page.oneCheckbox);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeSelected(page.oneCheckbox));

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            waitBy(By.xpath("//span[contains(text(),'1 получатель')]"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранного получателя");
        }

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        findElement(page.textarea).clear();

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        sendKeys(page.textarea, "Автотестовая СМС-ка для одного получателя");

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.sendNotification);
        try {
            waitBy(page.notificationSendingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста об отправке уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("СМС"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Автотестовая СМС-ка для одного получателя"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr[1]")).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr/a")).getText().equals("+7 927 240-11-45"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Отправлено"));

    }

    @Test(description = "Отправка нового ПУШ уведомления всем арендаторам", priority = 2)
    public void sendNewPushToAllRenters(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));

        click(page.push);

        Assert.assertTrue(findElement(page.push).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolPush).getText().equals("Максимум 110 символов"));

        sendKeys(page.textarea, "Автотестовый ПУШ для всех получателей");

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателей");
        }

        click(page.allCheckboxes);

        List<WebElement> checkboxesOn = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement a : checkboxesOn){
            Assert.assertTrue(a.isSelected());
        }

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            if (findElement(By.xpath("//div[2]/span/span")).getText().contains("получателей")){
                Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
            } else {
                waitBy(By.xpath("//span[contains(text(),'получателя')]"));
            }
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранных получателей");
        }

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.sendNotification);
        try {
            waitBy(page.notificationSendingToast, 300);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста об отправке уведомления");
        }

        try {
            waitBy(page.notificationNumber, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("Пуш"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Автотестовый ПУШ для всех получателей"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//i")).getText().contains("арендатор"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Отправлено"));

    }

    @Test(description = "Отправка нового ПУШ уведомления одному арендатору", priority = 3)
    public void sendNewPushToOneRenter(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));

        click(page.push);

        Assert.assertTrue(findElement(page.push).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolPush).getText().equals("Максимум 110 символов"));

        sendKeys(page.textarea, "Автотестовый ПУШ для одного получателя");

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателя");
        }

        sendKeys(page.filterModal, "Мифтахутдинов Рамиль");
        click(page.oneCheckbox);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeSelected(page.oneCheckbox));

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            waitBy(By.xpath("//span[contains(text(),'1 получатель')]"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранного получателя");
        }

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        findElement(page.textarea).clear();

        sendKeys(page.textarea, "Автотестовый ПУШ для одного получателя");

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));

        click(page.sendNotification);
        try {
            waitBy(page.notificationSendingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста об отправке уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("Пуш"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Автотестовый ПУШ для одного получателя"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr[1]")).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr/a")).getText().equals("+7 927 240-11-45"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Отправлено"));

    }

    @Test(description = "Сохранение черновика СМС только с текстом", priority = 4)
    public void saveByDraftSMSOnlyWithText(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));
        Assert.assertTrue(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        sendKeys(page.textarea, "Автотестовый черновик СМС без получателя");

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
        Assert.assertFalse(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.saveByDraft);
        try {
            waitBy(page.notificationSavingToast, 300);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден элемент тоста о сохранении уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("СМС"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Автотестовый черновик СМС без получателя"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Черновик"));

    }

    @Test(description = "Сохранение черновика СМС только с получателем", priority = 5)
    public void saveByDraftSMSOnlyWithRecipient(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));
        Assert.assertTrue(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателя");
        }

        sendKeys(page.filterModal, "Мифтахутдинов Рамиль");
        click(page.oneCheckbox);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeSelected(page.oneCheckbox));

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            waitBy(By.xpath("//span[contains(text(),'1 получатель')]"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранного получателя");
        }

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
        Assert.assertFalse(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.saveByDraft);
        try {
            waitBy(page.notificationSavingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста о сохранении уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("СМС"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr[1]")).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr/a")).getText().equals("+7 927 240-11-45"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Черновик"));

    }

    @Test(description = "Сохранение черновика СМС с текстом и получателем", priority = 6)
    public void saveByDraftSMSWithTextAndRecipient(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));
        Assert.assertTrue(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        sendKeys(page.textarea, "Автотестовый черновик СМС с текстом и получателем");

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателя");
        }

        sendKeys(page.filterModal, "Мифтахутдинов Рамиль");
        click(page.oneCheckbox);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeSelected(page.oneCheckbox));

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            waitBy(By.xpath("//span[contains(text(),'1 получатель')]"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранного получателя");
        }

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
        Assert.assertFalse(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.saveByDraft);
        try {
            waitBy(page.notificationSavingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста о сохранении уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("СМС"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Автотестовый черновик СМС с текстом и получателем"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr[1]")).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr/a")).getText().equals("+7 927 240-11-45"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Черновик"));

    }

    @Test(description = "Сохранение черновика ПУШ только с текстом", priority = 7)
    public void saveByDraftPushOnlyWithText(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));
        Assert.assertTrue(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.push);

        Assert.assertTrue(findElement(page.push).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolPush).getText().equals("Максимум 110 символов"));
        Assert.assertTrue(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        sendKeys(page.textarea, "Автотестовый черновик ПУШ без получателя");

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
        Assert.assertFalse(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.saveByDraft);
        try {
            waitBy(page.notificationSavingToast, 300);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден элемент тоста о сохранении уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("Пуш"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Автотестовый черновик ПУШ без получателя"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Черновик"));

    }

    @Test(description = "Сохранение черновика ПУШ только с получателем", priority = 8)
    public void saveByDraftPushOnlyWithRecipient(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));
        Assert.assertTrue(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.push);

        Assert.assertTrue(findElement(page.push).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolPush).getText().equals("Максимум 110 символов"));
        Assert.assertTrue(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателя");
        }

        sendKeys(page.filterModal, "Мифтахутдинов Рамиль");
        click(page.oneCheckbox);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeSelected(page.oneCheckbox));

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            waitBy(By.xpath("//span[contains(text(),'1 получатель')]"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранного получателя");
        }

        Assert.assertTrue(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
        Assert.assertFalse(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.saveByDraft);
        try {
            waitBy(page.notificationSavingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста о сохранении уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("Пуш"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr[1]")).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr/a")).getText().equals("+7 927 240-11-45"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Черновик"));

    }

    @Test(description = "Сохранение черновика ПУШ с текстом и получателем", priority = 9)
    public void saveByDraftPushWithTextAndRecipient(){

        GlobalPage globalpage = new GlobalPage();
        NotificationPage page = new NotificationPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuNotifications);
        click(globalpage.notificationsClientsSection);
        notificationsClientSection();

        click(page.addNotification);
        try {
            waitBy(page.newNotificationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления уведомления");
        }

        Assert.assertTrue(findElement(page.sms).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolSMS).getText().equals("Максимум 600 символов"));
        Assert.assertTrue(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.push);

        Assert.assertTrue(findElement(page.push).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.maxSimbolPush).getText().equals("Максимум 110 символов"));
        Assert.assertTrue(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        sendKeys(page.textarea, "Автотестовый черновик ПУШ с текстом и получателем");

        click(page.addRecipients);
        try {
            waitBy(page.chooseRecipientsModal);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент окна выбора получателя");
        }

        sendKeys(page.filterModal, "Мифтахутдинов Рамиль");
        click(page.oneCheckbox);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeSelected(page.oneCheckbox));

        click(page.saveModal);

        elementIsNotVisible(page.chooseRecipientsModal);

        try {
            waitBy(By.xpath("//span[contains(text(),'1 получатель')]"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент выбранного получателя");
        }

        Assert.assertFalse(findElement(page.sendNotification).getAttribute("class").contains("disabled"));
        Assert.assertFalse(findElement(page.saveByDraft).getAttribute("class").contains("disabled"));

        click(page.saveByDraft);
        try {
            waitBy(page.notificationSavingToast, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста о сохранении уведомления");
        }

        try {
            waitBy(page.notificationNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент номера уведомления");
        }

        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[3]/small")).getText().equals("Пуш"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[4]//a")).getText().equals("Автотестовый черновик ПУШ с текстом и получателем"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr[1]")).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[5]//nobr/a")).getText().equals("+7 927 240-11-45"));
        Assert.assertTrue(findElement(By.xpath("//tr[1]/td[6]//span")).getText().contains("Черновик"));

    }

}
package tests.objectsTests;

import framework.Configuration;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.ObjectsPage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AddAndEditContracts extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Заселение в апартамент №250")
    public void addContractInApartment() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        objectsSection();

        waitBy(page.apartmentNumber);

        findElement(globalpage.searchInput).sendKeys("250" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(globalpage.searchNumber("250"));
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

        if (elementIsNotPresent(page.labelStatusFreeApartment)){

            click(page.contracts);
            click(page.dropDownMenuInContracts);
            click(page.finishContract);

            sleep(1000);

            if(findElement(page.finishContractConfirm).isDisplayed()){

                click(page.finishContractConfirm);

            } else {

                waitBy(page.settleModal);
                click(page.checkboxCall);
                new WebDriverWait(driver, 30).until(ExpectedConditions.elementSelectionStateToBe(By.xpath("//input[@type='checkbox']"), true));
                click(page.ejectConfirm);

            }

            waitBy(page.changeStatus);

        }

        waitBy(page.labelStatusFreeApartment);
        click(page.changeStatus);
        click(page.statusRented);
        click(page.save);
        waitBy(page.settlementElement, 60);

        click(page.dropDownRenter, 30);
        findElement(page.inputChoiseRenter).sendKeys("202" + Keys.ENTER);
        Assert.assertTrue(findElement(page.renterInput).isDisplayed());
        String dateSigning = findElement(By.xpath("//input[@id='signDP']")).getText();
        click(page.calendar, 30);
        click(page.nextMonth);
        click(page.setDate);
        String endDate = findElement(By.xpath("//input[@id='stlmDP2']")).getAttribute("value");
        sendKeys(page.settlePrice, "150000");

        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOf(findElement(By.xpath("//div[@id='schedule__overlay']/i"))));

        sendKeys(page.priceInGraph, "55555");
        click(page.checkboxInGraph1);
        Assert.assertTrue(findElement(page.checkboxInGraph1).isSelected());
        click(page.checkboxInGraph2);
        Assert.assertTrue(findElement(page.checkboxInGraph2).isSelected());
        selectByValue(page.statusInGraph, "paid");

        scrollToElement(page.nextStep);
        click(page.inputIndication1);
        click(page.indication);
        String indication1 = findElement(page.inputIndication1).getAttribute("value");
        sleep(500);
        click(page.indication);
        String indication2 = findElement(page.inputIndication2).getAttribute("value");
        sleep(500);
        click(page.indication);
        String indication3 = findElement(page.inputIndication3).getAttribute("value");
        sleep(500);
        click(page.indication);
        String indication4 = findElement(page.inputIndication4).getAttribute("value");
        sleep(500);
        click(page.indication);
        String indication5 = findElement(page.inputIndication5).getAttribute("value");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        click(page.addRoomer);
        Assert.assertTrue(findElement(page.roomer1Element).isDisplayed());

        click(page.addRoomer);
        Assert.assertTrue(findElement(page.roomer2Element).isDisplayed());

        click(page.deleteRoomer2);
        click(page.deleteRoomerModalCancel,30);
        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOf(findElement(page.deleteRoomerModalCancel)));
        click(page.deleteRoomer2,30);
        click(page.deleteRoomerModalConfirm,30);

        elementIsNotVisible(page.roomer2Element,30);

        findElement(page.roomerFullName).sendKeys("Иванов Иван Иванович");
        findElement(page.bornedAtDay).sendKeys("01");
        findElement(page.bornedAtMonth).sendKeys("01");
        findElement(page.bornedAtYear).sendKeys("2000");
        findElement(page.roomerPhone).sendKeys("9876543210");
        findElement(page.roomerEmail).sendKeys("zhilec1@aizhk.ru");
        findElement(page.roomerPassportSeriesNumber).sendKeys("1234 ABC5678");
        sleep(500);
        findElement(page.roomerPassportIssued).sendKeys("Выдан Президентом Российской Федерации В.В. Путиным");
        findElement(page.roomerIssuedDay).sendKeys("12");
        findElement(page.roomerIssuedMonth).sendKeys("12");
        findElement(page.roomerIssuedYear).sendKeys("2012");
        findElement(page.roomerPassportCode).sendKeys("123123");

        click(page.livingWithPets);

        sendKeys(page.keys, "1");
        sendKeys(page.passes, "1");

        Assert.assertTrue(findElement(page.livingWithPets).isSelected());

        click(page.nextStep);
        try {
            waitBy(page.nextStepApartmentElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент страницы подтверждения заселения не найден");
        }

        Assert.assertTrue(findElement(page.step2Renter).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(page.step2Adress).getText().contains("Ходынский бульвар, д.2"));
        Assert.assertTrue(findElement(page.step2NumberOfApartment).getText().contains("250"));
        Assert.assertTrue(findElement(page.step2SettlePrice).getText().contains("150 000"));
        Assert.assertTrue(findElement(page.step2DateOfSigning).getText().contains(dateSigning));
        Assert.assertTrue(findElement(page.step2DateOfEndRent).getText().contains(endDate));
        Assert.assertTrue(findElement(page.step2PriceInGraph).getText().contains("55 555"));

        Assert.assertTrue(findElement(page.step2Indication1).getText().contains(indication1));
        Assert.assertTrue(findElement(page.step2Indication2).getText().contains(indication2));
        Assert.assertTrue(findElement(page.step2Indication3).getText().contains(indication3));
        Assert.assertTrue(findElement(page.step2Indication4).getText().contains(indication4));
        Assert.assertTrue(findElement(page.step2Indication5).getText().contains(indication5));

        List<WebElement> images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 2);
        Assert.assertTrue(findElement(By.xpath("//span[@title='PDF']")).isDisplayed());

        Assert.assertTrue(findElement(page.step2RoomerFullName).getText().contains("Иванов Иван Иванович"));
        Assert.assertTrue(findElement(page.step2RoomerBornedAt).getText().contains("01.01.2000"));
        Assert.assertTrue(findElement(page.step2RoomerPhone).getText().contains("+7 987 654-32-10"));
        Assert.assertTrue(findElement(page.step2RoomerEmail).getText().contains("zhilec1@aizhk.ru"));
        Assert.assertTrue(findElement(page.step2RoomerPassportSeriesNumber).getText().contains("1234 ABC5678"));
        Assert.assertTrue(findElement(page.step2RoomerPassportIssued).getText().contains("Выдан Президентом Российской Федерации В.В. Путиным"));
        Assert.assertTrue(findElement(page.step2RoomerIssuedDate).getText().contains("12.12.2012"));
        Assert.assertTrue(findElement(page.step2RoomerPassportCode).getText().contains("123-123"));
        Assert.assertTrue(findElement(page.step2LivingWithPets).getText().contains("Проживание с животными"));
        Assert.assertTrue(findElement(page.step2Keys).getText().contains("1"));
        Assert.assertTrue(findElement(page.step2Passes).getText().contains("1"));

        click(page.backToStep1);
        try {
            waitBy(page.settlementElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент страницы 1-го шага заселения не найден");
        }

        click(page.nextStep);
        try {
            waitBy(page.nextStepApartmentElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент страницы подтверждения заселения не найден");
        }

        click(globalpage.submit);
        try {
            waitBy(page.settleApartmentModal);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент модального окна подтверждения заселения не найден");
        }

        click(page.settleConfirm);

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(page.labelStatusRentedApartment)));
        click(page.contracts);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//span[contains(text(),'Действующий')]")));

    }

    @Test(description = "Редактирование договора апартамента №250", priority = 1)
    public void editContractInApartment() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber);

        findElement(globalpage.searchInput).sendKeys("250" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(globalpage.searchNumber("250"));
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

        for (;;){

            click(page.contracts, 30);

            if (isElementPresent(page.draft)) {

                deleteDraft();

            } else {

                break;

            }

        }

        click(page.dropDownMenuInContracts);
        click(page.editContract);
        waitBy(page.editContractApartmentElement);

        click(page.calendar);
        click(page.setEditDate);
        String endDate = findElement(By.xpath("//input[@id='stlmDP2']")).getAttribute("value");
        elementIsNotVisible(page.spinnerInGraph);

        click(page.addPeriod);
        elementIsNotVisible(page.spinnerInGraph);

        click(page.editPlanOfDepartureDate);
        click(page.prevMonth);
        click(page.editSetPlanOfDepartureDate);
        String planOfDepartureDate = findElement(page.editPlanOfDepartureDate).getAttribute("value");

        click(By.xpath("//form[@name='contractForm']"));
        elementIsNotVisible(page.spinnerInGraph);

        String handleBefore = driver.getWindowHandle();

        click(page.linkToInvoiceList);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String invoiceList = "http://dev-arenda.ahml.ru/admin/invoice-list";
        Assert.assertTrue(driver.getCurrentUrl().contains(invoiceList));

        invoiceSection();

        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.linkToInvoice);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String invoice = "http://dev-arenda.ahml.ru/admin/invoice-list?search=250";
        Assert.assertTrue(driver.getCurrentUrl().contains(invoice));
        try {
            waitBy(globalpage.invoiceFromApartmentElement,60);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент выставленного счёта в разделе 'Счета'");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        sendKeys(page.editPriceInGraph, "55555");
        click(page.editCheckboxInGraph);
        Assert.assertTrue(findElement(page.editCheckboxInGraph).isSelected());
        selectByValue(page.editStatusInGraph, "paid");

        List<WebElement> photos = driver.findElements(By.xpath("//div[@class='form-file-preview ng-scope ng-isolate-scope']/div[@class='item-preview col-sm-3 ng-scope']"));
        WebElement photoPDF = photos.get(0).findElement(By.xpath("//div[contains(@class,'item-preview-inner')]/a"));

        if(photoPDF != null){
            click(page.editDeletePhoto2);
        } else {
            click(page.editDeletePhoto1);
        }

        String jpg = System.getProperty("user.dir") + "\\files\\images\\111.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        click(page.addRoomer);
        Assert.assertTrue(findElement(page.roomer2Element).isDisplayed());

        click(page.deleteRoomer2);
        click(page.deleteRoomerModalCancel,30);
        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOf(findElement(page.deleteRoomerModalCancel)));
        click(page.deleteRoomer2,30);
        click(page.deleteRoomerModalConfirm,30);

        elementIsNotVisible(page.roomer2Element,30);

        sendKeys(page.roomerFullName, "Петров Петр Петрович");
        sendKeys(page.bornedAtDay, "12");
        sendKeys(page.bornedAtMonth, "12");
        sendKeys(page.bornedAtYear, "1980");
        sendKeys(page.roomerPhone, "5555555555");
        sendKeys(page.roomerEmail, "editZhilec@aizhk.ru");
        sendKeys(page.roomerPassportSeriesNumber, "4321 XYZ9999");
        sleep(500);
        sendKeys(page.roomerPassportIssued, "Выдан Премьер-министром Российской Федерации Д.А. Медведевым");
        sendKeys(page.roomerIssuedDay, "01");
        sendKeys(page.roomerIssuedMonth, "01");
        sendKeys(page.roomerIssuedYear, "2015");
        sendKeys(page.roomerPassportCode, "321321");

        click(page.livingWithPets);

        findElement(page.keys).clear();
        findElement(page.passes).clear();

        click(globalpage.submit);

        click(page.editSaveModalCancel);
        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOf(findElement(page.editSaveModalCancel)));
        click(globalpage.submit);
        click(page.editSaveModalConfirm);

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(page.labelStatusRentedApartment)));

        // Сверка параметров отредактированного договора
        click(page.contracts);
        click(page.dropDownMenuInContracts);
        click(page.editContract);
        waitBy(page.editContractApartmentElement);

        Assert.assertTrue(findElement(page.calendar).getAttribute("value").contains(endDate));
        Assert.assertTrue(findElement(page.editPlanOfDepartureDate).getAttribute("value").contains(planOfDepartureDate));
        Assert.assertTrue(findElement(page.editSafeStatusInGraph).getAttribute("value").contains("55 555"));

        List<WebElement> images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 2);
        Assert.assertTrue(findElement(By.xpath("//span[@title='PDF']")).isDisplayed());

        Assert.assertTrue(findElement(page.roomerFullName).getAttribute("value").contains("Петров Петр Петрович"));
        Assert.assertTrue(findElement(page.bornedAtDay).getAttribute("value").contains("12"));
        Assert.assertTrue(findElement(page.bornedAtMonth).getAttribute("value").contains("12"));
        Assert.assertTrue(findElement(page.bornedAtYear).getAttribute("value").contains("1980"));
        Assert.assertTrue(findElement(page.roomerPhone).getAttribute("value").contains("+7 555 555-55-55"));
        Assert.assertTrue(findElement(page.roomerEmail).getAttribute("value").contains("editZhilec@aizhk.ru"));
        Assert.assertTrue(findElement(page.roomerPassportSeriesNumber).getAttribute("value").contains("4321 XYZ9999"));
        Assert.assertTrue(findElement(page.roomerPassportIssued).getAttribute("value").contains("Выдан Премьер-министром Российской Федерации Д.А. Медведевым"));
        Assert.assertTrue(findElement(page.roomerIssuedDay).getAttribute("value").contains("01"));
        Assert.assertTrue(findElement(page.roomerIssuedMonth).getAttribute("value").contains("01"));
        Assert.assertTrue(findElement(page.roomerIssuedYear).getAttribute("value").contains("2015"));
        Assert.assertTrue(findElement(page.roomerPassportCode).getAttribute("value").contains("321-321"));

        Assert.assertFalse(findElement(page.livingWithPets).isSelected());
        Assert.assertTrue(findElement(page.keys).getAttribute("value").contains("0"));
        Assert.assertTrue(findElement(page.passes).getAttribute("value").contains("0"));

    }

    @Test(description = "Сдача машиноместа №А70", priority = 2)
    public void addContractInParking() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber);

        selectByValue(page.changeTypeObject, "string:parkings");

        waitBy(page.apartmentNumber);
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("A70" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(globalpage.searchNumber("A70"));
        try {
            waitBy(globalpage.parkingCardElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент карточки машиноместа не найден");
        }

        if (elementIsNotPresent(page.labelStatusFreeParking)){

            click(page.contracts);
            click(page.dropDownMenuInContracts);
            click(page.finishContract);

            sleep(1000);

            findElement(page.finishContractConfirm).isDisplayed();
            click(page.finishContractConfirm);

            waitBy(page.changeStatus);

        }

        Assert.assertTrue(isElementPresent(page.labelStatusFreeParking));
        click(page.changeStatus);
        click(page.statusRented);
        click(page.save);
        waitBy(page.leaseElement, 60);

        click(page.dropDownRenter);
        findElement(page.inputChoiseRenter).sendKeys("202" + Keys.ENTER);
        Assert.assertTrue(findElement(page.renterInput).isDisplayed());
        String dateSigning = findElement(By.xpath("//input[@id='signDP']")).getText();
        click(page.calendar);
        click(page.nextMonth);
        click(page.setDate);
        String endDate = findElement(By.xpath("//input[@id='stlmDP2']")).getAttribute("value");
        sendKeys(page.settlePrice, "30000");

        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOf(findElement(By.xpath("//div[@id='schedule__overlay']/i"))));

        sendKeys(page.priceInGraph, "33333");
        click(page.checkboxInGraph1);
        Assert.assertTrue(findElement(page.checkboxInGraph1).isSelected());
        click(page.checkboxInGraph2);
        Assert.assertTrue(findElement(page.checkboxInGraph2).isSelected());
        selectByValue(page.statusInGraph, "paid");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        click(page.nextStep);
        try {
            waitBy(page.nextStepParkingElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент страницы подтверждения сдачи м/м не найден");
        }

        Assert.assertTrue(findElement(page.step2Renter).getText().contains("Мифтахутдинов Рамиль Рустэмович"));
        Assert.assertTrue(findElement(page.step2Adress).getText().contains("Ходынский бульвар, д.2"));
        Assert.assertTrue(findElement(page.step2NumberOfApartment).getText().contains("A70"));
        Assert.assertTrue(findElement(page.step2SettlePrice).getText().contains("30 000"));
        Assert.assertTrue(findElement(page.step2DateOfSigning).getText().contains(dateSigning));
        Assert.assertTrue(findElement(page.step2DateOfEndRent).getText().contains(endDate));
        Assert.assertTrue(findElement(page.step2PriceInGraph).getText().contains("33 333"));

        List<WebElement> images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 2);
        Assert.assertTrue(findElement(By.xpath("//span[@title='PDF']")).isDisplayed());

        click(page.backToStep1);
        try {
            waitBy(page.leaseElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент страницы 1-го шага сдачи м/м не найден");
        }

        click(page.nextStep);
        try {
            waitBy(page.nextStepParkingElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент страницы подтверждения сдачи м/м не найден");
        }

        click(globalpage.submit);
        try {
            waitBy(page.rentParkingModal);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент модального окна подтверждения сдачи м/м не найден");
        }

        click(page.rentConfirm);

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(page.labelStatusRentedParking)));
        click(page.contracts);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//span[contains(text(),'Действующий')]")));

    }

    @Test(description = "Редактирование договора машиноместа №А70", priority = 3)
    public void editContractInParking() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(globalpage.searchInput);

        selectByValue(page.changeTypeObject, "string:parkings");

        waitBy(page.apartmentNumber);
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("A70" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(page.apartmentNumber);
        try {
            waitBy(globalpage.parkingCardElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент карточки машиноместа не найден");
        }

        click(page.contracts);
        click(page.dropDownMenuInContracts);
        click(page.editContract);
        waitBy(page.editContractParkingElement);
        elementIsNotVisible(page.spinnerInGraph);

        click(page.calendar);
        click(page.setEditDate);
        String endDate = findElement(By.xpath("//input[@id='stlmDP2']")).getAttribute("value");
        elementIsNotVisible(page.spinnerInGraph);

        click(page.addPeriod);
        elementIsNotVisible(page.spinnerInGraph);

        click(page.editPlanOfDepartureDate);
        click(page.prevMonth);
        click(page.editSetPlanOfDepartureDate);
        String planOfDepartureDate = findElement(page.editPlanOfDepartureDate).getAttribute("value");

        click(By.xpath("//form[@name='contractForm']"));
        elementIsNotVisible(page.spinnerInGraph);

        String handleBefore = driver.getWindowHandle();

        click(page.linkToInvoiceList);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String invoiceList = "http://dev-arenda.ahml.ru/admin/invoice-list";
        Assert.assertTrue(driver.getCurrentUrl().contains(invoiceList));

        invoiceSection();

        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.linkToInvoice);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String invoice = "http://dev-arenda.ahml.ru/admin/invoice-list?search=A70";
        Assert.assertTrue(driver.getCurrentUrl().contains(invoice));
        try {
            waitBy(globalpage.invoiceFromParkingElement);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент выставленного счёта в разделе 'Счета'");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        sendKeys(page.editPriceInGraph, "11111");
        click(page.editCheckboxInGraph);
        Assert.assertTrue(findElement(page.editCheckboxInGraph).isSelected());
        selectByValue(page.editStatusInGraph, "paid");

        List<WebElement> photos = driver.findElements(By.xpath("//div[@class='form-file-preview ng-scope ng-isolate-scope']/div[@class='item-preview col-sm-3 ng-scope']"));
        WebElement photoPDF = photos.get(0).findElement(By.xpath("//div[contains(@class,'item-preview-inner')]/a"));

        if(photoPDF != null){
            click(page.editDeletePhoto2);
        } else {
            click(page.editDeletePhoto1);
        }

        String jpg = System.getProperty("user.dir") + "\\files\\images\\111.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        elementIsNotVisible(By.xpath("//div/button[contains(@class,'disabled')]"));

        click(globalpage.submit);

        click(page.editSaveModalCancel);
        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOf(findElement(page.editSaveModalCancel)));
        click(globalpage.submit);
        click(page.editSaveModalConfirm);

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(page.labelStatusRentedParking)));

        // Сверка параметров отредактированного договора
        click(page.contracts);
        click(page.dropDownMenuInContracts);
        click(page.editContract);
        waitBy(page.editContractParkingElement);

        Assert.assertTrue(findElement(page.calendar).getAttribute("value").contains(endDate));
        Assert.assertTrue(findElement(page.editPlanOfDepartureDate).getAttribute("value").contains(planOfDepartureDate));
        Assert.assertTrue(findElement(page.editSafeStatusInGraph).getAttribute("value").contains("11 111"));

        List<WebElement> images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 2);
        Assert.assertTrue(findElement(By.xpath("//span[@title='PDF']")).isDisplayed());

    }

    @Test(description = "Приостановление договора апартамента", priority = 4)
    public void pauseContractApartment() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber);

        findElement(globalpage.searchInput).sendKeys("250" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(globalpage.searchNumber("250"));
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

        click(page.contracts);
        click(page.dropDownMenuInContracts);
        click(page.pauseContract);
        try {
            waitBy(page.modalPauseContractElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент модально окна приостановления договора не найден");
        }

        elementIsNotVisible(globalpage.spinner);

        click(page.inputIndicationModal);
        click(page.indication);
        sleep(500);
        click(page.indication);
        sleep(500);
        click(page.indication);
        sleep(500);
        click(page.indication);
        sleep(500);
        click(page.indication);

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        elementIsNotVisible(By.xpath("//div[@id='pause_modal']//a[@disabled='disabled']"));

        click(page.deleteFileInModal);

        click(page.pauseContractConfirm);

        try {
            waitBy(page.toastPauseContract);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент тоста о приостановлении договора не найден");
        }

        try {
            waitBy(page.labelStatusPausedApartment);
        } catch (TimeoutException ex){
            afterExeptionMessage("Статус апартамента не изменился на 'Приостановлен'");
        }

        Assert.assertTrue(findElement(By.xpath("//h4/small/span[contains(text(),'Приостановлен')]")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//div/span[contains(text(),'Приостановлен')]")).isDisplayed());

        elementIsNotVisible(page.changeStatus);

        click(page.showPhotos);
        try {
            waitBy(page.photoInContracts);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Элемент фото в договоре не найден");
        }

        List<WebElement> images = driver.findElements(By.xpath("//div[@class='row files-row']/div/a/img"));
        Assert.assertTrue(images.size() == 3);

        List<WebElement> pdfs = driver.findElements(By.xpath("//div[@class='row files-row']//div[text()='PDF']"));
        Assert.assertTrue(pdfs.size() == 2);

        driver.navigate().back();

        waitBy(page.apartmentNumber);

        Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[10]/span[contains(text(),'Приостановлен')]")).isDisplayed());

        click(page.openDropDownMenu);
        Assert.assertTrue(findElement(By.xpath("//a[contains(@ng-style,'block')][contains(text(),'Заселить')]")).isDisplayed());

    }

    @Test(description = "Возобновление договора апартамента", priority = 5)
    public void resumeContractApartment() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber);

        findElement(globalpage.searchInput).sendKeys("250" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(globalpage.searchNumber("250"));
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

        click(page.contracts);
        click(page.dropDownMenuInContracts);

        Assert.assertTrue(findElement(page.resumeContract).isDisplayed());
        elementIsNotPresent(page.editContract);
        elementIsNotPresent(page.generateCommunalInvoice);

        click(page.resumeContract);
        try {
            waitBy(page.modalResumeContractElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент модально окна возобновления договора не найден");
        }

        elementIsNotVisible(globalpage.spinner);

        click(page.inputIndicationModal);
        click(page.indication);
        sleep(500);
        click(page.indication);
        sleep(500);
        click(page.indication);
        sleep(500);
        click(page.indication);
        sleep(500);
        click(page.indication);

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        elementIsNotVisible(By.xpath("//div[@id='pause_modal']//a[@disabled='disabled']"));

        click(page.resumeContractConfirm);
        try {
            waitBy(page.toastResumeContract);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент тоста о возобновлении договора не найден");
        }

        try {
            waitBy(page.labelStatusRentedApartment);
        } catch (TimeoutException ex){
            afterExeptionMessage("Статус апартамента не изменился на 'Арендован'");
        }

        Assert.assertTrue(findElement(By.xpath("//h4/small/span[contains(text(),'Действующий')]")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//div/span[contains(text(),'Действующий')]")).isDisplayed());

        isElementPresent(page.changeStatus);

        click(page.showPhotos);
        try {
            waitBy(page.photoInContracts);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Элемент фото в договоре не найден");
        }

        List<WebElement> images = driver.findElements(By.xpath("//div[@class='row files-row']/div/a/img"));
        Assert.assertTrue(images.size() == 5);

        List<WebElement> pdfs = driver.findElements(By.xpath("//div[@class='row files-row']//div[text()='PDF']"));
        Assert.assertTrue(pdfs.size() == 3);

        driver.navigate().back();

        waitBy(page.apartmentNumber);

        Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[10]/span[contains(text(),'Арендован')]")).isDisplayed());

    }

    @Test(description = "Отправка запроса на выездной счет", priority = 6)
    public void setDepartureInvoice() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber);

        findElement(globalpage.searchInput).sendKeys("250" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(globalpage.searchNumber("250"));
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

        click(page.contracts);
        click(page.dropDownMenuInContracts);
        click(page.generateCommunalInvoice);

        waitBy(page.communalModal);
        click(page.closeModal);

        elementIsNotVisible(page.closeModal,30);
        click(page.dropDownMenuInContracts);
        click(page.generateCommunalInvoice);

        waitBy(page.communalModal);
        click(page.generate);

        try {
            waitBy(page.toastGenerateDepartureInvoice);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент тоста о формировании выездного счета не найден");
        }

        elementIsNotVisible(page.generate, 30);
        waitBy(globalpage.apartmentCardElement);

    }

}
package tests.objectsTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.ObjectsPage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

@Ignore
public class AddAndEditObjects extends GlobalPage {

    // Данный функционал скрыт для комплекса "Лайнер"

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Выбор чекбоксов и переключение радиобаттонов апартамента")
    public void chooseCheckboxesAndRadiobuttonsInApartment() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        objectsSection();

        findElement(globalpage.add).click();
        try {
            findElement(page.addNewObjectElement);
        } catch (RuntimeException ex){
            afterExeptionMessage("Не найден элемент на странице добавления объекта");
        }

        click(page.objectParking);
        waitBy(page.objectParkingElement);
        click(page.objectApartment);
        waitBy(page.objectApartmentElement);

        waitBy(By.xpath("//input[@type='checkbox']"),30);

        // Выбор и прокликивание всех чекбоксов на странице
        List<WebElement> checkboxesOn = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement a : checkboxesOn){
            a.click();
            Assert.assertTrue(a.isSelected()); // проверка на присутствие отметок в чекбоксах
        }

        // Переключение кол-ва комнат
        findElement(page.room0).click();
        Assert.assertTrue(findElement(page.room0).getAttribute("class").contains("active"));

        findElement(page.room1).click();
        Assert.assertTrue(findElement(page.room1).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.areaRooms1).isDisplayed());

        findElement(page.room2).click();
        Assert.assertTrue(findElement(page.room2).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.areaRooms2).isDisplayed());

        findElement(page.room3).click();
        Assert.assertTrue(findElement(page.room3).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.areaRooms3).isDisplayed());

        findElement(page.room4).click();
        Assert.assertTrue(findElement(page.room4).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.areaRooms4).isDisplayed());

        findElement(page.room5).click();
        Assert.assertTrue(findElement(page.room5).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.areaRooms5).isDisplayed());

        findElement(page.room6).click();
        Assert.assertTrue(findElement(page.room6).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.areaRooms6).isDisplayed());

        findElement(page.room7).click();
        Assert.assertTrue(findElement(page.room7).getAttribute("class").contains("active"));
        Assert.assertTrue(findElement(page.areaRooms7).isDisplayed());

        // Выбор и прокликивание всех чекбоксов на странице
        List<WebElement> checkboxesOff = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement b : checkboxesOff){
            b.click();
            Assert.assertFalse(b.isSelected()); // проверка на отсутствие отметок в чекбоксах
        }

        // Отделка
        findElement(page.decorationBrown).click();
        Assert.assertTrue(findElement(page.decorationBrown).getAttribute("class").contains("active"));

        findElement(page.decorationWhite).click();
        Assert.assertTrue(findElement(page.decorationWhite).getAttribute("class").contains("active"));

        // Мебель
        findElement(page.furnitureNo).click();
        Assert.assertTrue(findElement(page.furnitureNo).getAttribute("class").contains("active"));

        findElement(page.furnitureRussian).click();
        Assert.assertTrue(findElement(page.furnitureRussian).getAttribute("class").contains("active"));

        findElement(page.furnitureIKEA).click();
        Assert.assertTrue(findElement(page.furnitureIKEA).getAttribute("class").contains("active"));

        findElement(page.furnitureElite).click();
        Assert.assertTrue(findElement(page.furnitureElite).getAttribute("class").contains("active"));

        // Ремонт
        findElement(page.repair).click();
        Assert.assertTrue(findElement(page.repair).getAttribute("class").contains("active"));

        findElement(page.repairRussian).click();
        Assert.assertTrue(findElement(page.repairRussian).getAttribute("class").contains("active"));

        findElement(page.repairEuro).click();
        Assert.assertTrue(findElement(page.repairEuro).getAttribute("class").contains("active"));

        findElement(page.repairElite).click();
        Assert.assertTrue(findElement(page.repairElite).getAttribute("class").contains("active"));

        findElement(page.repairNeeded).click();
        Assert.assertTrue(findElement(page.repairNeeded).getAttribute("class").contains("active"));

        // Балкон
        findElement(page.loggia1).click();
        Assert.assertTrue(findElement(page.loggia1).getAttribute("class").contains("active"));

        findElement(page.balcony1).click();
        Assert.assertTrue(findElement(page.balcony1).getAttribute("class").contains("active"));

        findElement(page.balconyNo1).click();
        Assert.assertTrue(findElement(page.balconyNo1).getAttribute("class").contains("active"));

        findElement(page.addBalcony).click();
        Assert.assertTrue(findElement(page.balcony2).isDisplayed());

        findElement(page.addBalcony).click();
        Assert.assertTrue(findElement(page.balcony3).isDisplayed());

        findElement(page.loggia2).click();
        Assert.assertTrue(findElement(page.loggia2).getAttribute("class").contains("active"));

        findElement(page.balconyNo2).click();
        Assert.assertTrue(findElement(page.balconyNo2).getAttribute("class").contains("active"));

        findElement(page.balcony2).click();
        Assert.assertTrue(findElement(page.balcony2).getAttribute("class").contains("active"));

        findElement(page.deleteBalcony).click();
        elementIsNotVisible(page.balcony3,5);
        findElement(page.deleteBalcony).click();
        elementIsNotVisible(page.balcony2,5);

        // Санузел
        findElement(page.bathroomSeparated1).click();
        Assert.assertTrue(findElement(page.bathroomSeparated1).getAttribute("class").contains("active"));

        findElement(page.bathroomCombined1).click();
        Assert.assertTrue(findElement(page.bathroomCombined1).getAttribute("class").contains("active"));

        findElement(page.addBathroom).click();
        Assert.assertTrue(findElement(page.bathroomCombined2).isDisplayed());

        findElement(page.addBathroom).click();
        Assert.assertTrue(findElement(page.bathroomCombined3).isDisplayed());

        findElement(page.bathroomSeparated2).click();
        Assert.assertTrue(findElement(page.bathroomSeparated2).getAttribute("class").contains("active"));

        findElement(page.bathroomCombined2).click();
        Assert.assertTrue(findElement(page.bathroomCombined2).getAttribute("class").contains("active"));

        findElement(page.deleteBathroom).click();
        elementIsNotVisible(page.bathroomCombined3,5);
        findElement(page.deleteBathroom).click();
        elementIsNotVisible(page.bathroomCombined2,5);

        // Вид из окна
        findElement(page.viewWindowOnCourtyard).click();
        Assert.assertTrue(findElement(page.viewWindowOnCourtyard).getAttribute("class").contains("active"));

        findElement(page.viewWindowOnStreet).click();
        Assert.assertTrue(findElement(page.viewWindowOnStreet).getAttribute("class").contains("active"));

        findElement(page.viewWindowOnBoth).click();
        Assert.assertTrue(findElement(page.viewWindowOnBoth).getAttribute("class").contains("active"));

    }

    @Test(description = "Добавление апартамента", priority = 1)
    public void addApartment() throws InterruptedException, AWTException, FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        click(globalpage.add);
        try {
            waitBy(page.objectApartmentElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы добавления апартамента не найден");
        }

        waitBy(By.xpath("//input[@type='checkbox']"),30);

        List<WebElement> checkboxesOn = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement a : checkboxesOn){
            a.click();
            Assert.assertTrue(a.isSelected());
        }

        findElement(page.price).sendKeys("50000");
        Assert.assertTrue(findElement(page.deposit).getAttribute("value").equals("100"));
        findElement(page.deposit).clear();
        findElement(page.deposit).sendKeys("50");
        Assert.assertTrue(findElement(page.lease).getAttribute("value").equals("6"));
        findElement(page.lease).clear();
        findElement(page.lease).sendKeys("12");

        findElement(page.floor).sendKeys("15");
        Assert.assertTrue(findElement(page.room1).getAttribute("class").contains("active"));
        findElement(page.room3).click();
        findElement(page.areaTotal).sendKeys("125.65");
        findElement(page.areaKitchen).sendKeys("20.08");
        findElement(page.areaLiving).sendKeys("95.7");
        findElement(page.areaRooms1).sendKeys("30");
        findElement(page.areaRooms2).sendKeys("25.88");
        findElement(page.areaRooms3).sendKeys("44.55");

        Assert.assertTrue(findElement(page.decorationWhite).getAttribute("class").contains("active"));
        findElement(page.decorationBrown).click();
        Assert.assertTrue(findElement(page.furnitureIKEA).getAttribute("class").contains("active"));
        findElement(page.furnitureElite).click();
        Assert.assertTrue(findElement(page.repairEuro).getAttribute("class").contains("active"));
        findElement(page.repairRussian).click();
        Assert.assertTrue(findElement(page.balcony1).getAttribute("class").contains("active"));
        elementIsNotVisible(page.deleteBalcony,5);
        findElement(page.loggia1).click();
        findElement(page.addBalcony).click();
        findElement(page.addBalcony).click();
        findElement(page.balconyNo2).click();
        Assert.assertTrue(findElement(page.bathroomCombined1).getAttribute("class").contains("active"));
        elementIsNotVisible(page.deleteBathroom,5);
        findElement(page.bathroomSeparated1).click();
        findElement(page.addBathroom).click();
        Assert.assertTrue(findElement(page.viewWindowOnStreet).getAttribute("class").contains("active"));
        findElement(page.viewWindowOnBoth).click();

        findElement(page.changeLangToEngOfPassport).click();
        findElement(page.passportOfApartmentEng).sendKeys("Passport of apartment");
        findElement(page.changeLangToRusOfPassport).click();
        findElement(page.passportOfApartmentRus).sendKeys("Паспорт апартамента");

        findElement(page.changeLangToEngOfDescription).click();
        sleep(1000);
        findElement(page.descriptionEng).sendKeys("Description of apartment");
        findElement(page.changeLangToRusOfDescription).click();
        sleep(1000);
        findElement(page.descriptionRus).sendKeys("Описание апартамента" + " Описание апартамента" + " Описание апартамента" + " Описание апартамента");
        sleep(1000);
        findElement(page.tour3D).sendKeys("https://arenda.ahml.ru/");

        // Добавление файлов
        String jpg = System.getProperty("user.dirWork") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String png = System.getProperty("user.dirWork") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        // Выбор названия фото
        selectByValue(page.dropDownMenuPhoto1, "4");
        selectByValue(page.dropDownMenuPhoto2, "9");

        // Путь к файлу
        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textApartment.txt";
        File file = new File(txt);

        // Чтение из файла
        Scanner scanner = new Scanner(file);
        String a = scanner.nextLine();
        int apartNumber = Integer.parseInt(a);

        // Увеличение номера объекта на один, если такой номер уже существует
        for(;;){
            sendKeys(page.numberObject, String.valueOf(apartNumber));
            click(globalpage.submit);
            sleep(2000);
            if (elementIsNotPresent(page.errorNumberApartment)){
                break;
            } else {
                apartNumber++;
                sleep(1000);
            }
        }

        // Запись в файл
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(String.valueOf(apartNumber));
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        try {
            findElement(page.toastNewObject);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент тоста о добавлении объекта не найден");
        }

        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

    }

    @Test(description = "Сверка параметров добавленного апартамента", priority = 2)
    public void verificationParamsNewApartment() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textApartment.txt";
        File file = new File(txt);
        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.apartmentNumber, 30);
        Assert.assertTrue(findElement(page.apartmentsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean apartmentNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeObjectNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                Assert.assertTrue(findElement(page.listApartmentFloor).getText().contains("15"));
                Assert.assertTrue(findElement(page.listApartmentRooms).getText().contains("3"));
                Assert.assertTrue(findElement(page.listApartmentAreaTotal).getText().contains("125,65"));
                Assert.assertTrue(findElement(page.listApartmentViewWindow).getText().contains("На улицу и двор"));
                Assert.assertTrue(findElement(page.listApartmentDecoration).getText().contains("Бежевый"));
                Assert.assertTrue(findElement(page.listApartmentPrice).getText().contains("50 000"));
                Assert.assertTrue(findElement(page.listApartmentStatusFree).getText().contains("Свободен"));

                click(globalpage.searchNumber(number));
                try {
                    waitBy(globalpage.apartmentCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Элемент карточки апартамента не найден");
                }

                apartmentNumber = false;
                break;

            }

        }

        if (apartmentNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        Assert.assertTrue(findElement(page.safeComplex).getText().contains("Лайнер"));
        Assert.assertTrue(findElement(page.safeAddress).getText().contains("Москва, Ходынский бульвар, 2"));
        Assert.assertTrue(findElement(page.safeObjectNumber).getText().contains(number));
        Assert.assertTrue(findElement(page.safeApartmentPrice).getText().contains("50 000"));
        Assert.assertTrue(findElement(page.safeDeposit).getText().contains("50%"));
        Assert.assertTrue(findElement(page.safeLease).getText().contains("12"));
        Assert.assertTrue(findElement(page.safeNoAnimals).getText().contains("Без животных"));
        Assert.assertTrue(findElement(page.safeNoSmoking).getText().contains("Нельзя курить"));
        Assert.assertTrue(findElement(page.safeNoChildren).getText().contains("Без детей"));
        Assert.assertTrue(findElement(page.safeFloor).getText().contains("15"));
        Assert.assertTrue(findElement(page.safeRooms).getText().contains("3"));
        Assert.assertTrue(findElement(page.safeApartmentAreaTotal).getText().contains("125.65"));
        Assert.assertTrue(findElement(page.safeAreaKitchen).getText().contains("20.08"));
        Assert.assertTrue(findElement(page.safeAreaLiving).getText().contains("95.7"));
        Assert.assertTrue(findElement(page.safeAreaRoom1).getText().contains("30"));
        Assert.assertTrue(findElement(page.safeAreaRoom2).getText().contains("25.88"));
        Assert.assertTrue(findElement(page.safeAreaRoom3).getText().contains("44.55"));
        Assert.assertTrue(findElement(page.safeDecoration).getText().contains("Бежевый"));
        Assert.assertTrue(findElement(page.safeFurniture).getText().contains("Элитная"));
        Assert.assertTrue(findElement(page.safeRepair).getText().contains("Советский"));
        Assert.assertTrue(findElement(page.safeBalcony).getText().contains("Балкон и Лоджия"));
        Assert.assertTrue(findElement(page.safeBathroom).getText().contains("Совмещённый и Раздельный"));
        Assert.assertTrue(findElement(page.safeViewWindow).getText().contains("На улицу и двор"));
        Assert.assertTrue(findElement(page.safeTV).getText().contains("Телевидение"));
        Assert.assertTrue(findElement(page.safeRefriegerator).getText().contains("Холодильник"));
        Assert.assertTrue(findElement(page.safeWasher).getText().contains("Стиральная машина"));
        Assert.assertTrue(findElement(page.safeInternet).getText().contains("Интернет"));
        Assert.assertTrue(findElement(page.safeDishwasher).getText().contains("Посудомоечная машина"));
        Assert.assertTrue(findElement(page.safeAppliances).getText().contains("Бытовая техника"));
        Assert.assertTrue(findElement(page.safeAirConditioning).getText().contains("Кондиционер"));
        Assert.assertTrue(findElement(page.safePanoramicWindows).getText().contains("Панорамное остекление"));
        Assert.assertTrue(findElement(page.safePassportOfApartment).getText().contains("Паспорт апартамента"));
        Assert.assertTrue(findElement(page.safeDescription).getText().equals("Описание апартамента Описание апартамента Описание апартамента Описание апартамента"));
        Assert.assertTrue(findElement(page.safeTour3D).getText().contains("https://arenda.ahml.ru/"));
        Assert.assertTrue(findElement(page.safeNamePhoto1).getText().contains("Холл"));
        Assert.assertTrue(findElement(page.safeNamePhoto2).getText().contains("Кухня"));

    }

    @Test(description = "Редактирование апартамента", priority = 3)
    public void editApartment() throws InterruptedException, FileNotFoundException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textApartment.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.apartmentNumber, 30);
        Assert.assertTrue(findElement(page.apartmentsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(page.apartmentNumber);
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

        click(page.editObject);
        try {
            waitBy(page.editObjectElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы редактирования объекта не найден");
        }

        waitBy(By.xpath("//input[@type='checkbox']"),30);

        List<WebElement> checkboxesOn = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement a : checkboxesOn){
            a.click();
            Assert.assertFalse(a.isSelected());
        }

        findElement(page.price).clear();
        findElement(page.price).sendKeys("100000");
        findElement(page.deposit).clear();
        findElement(page.deposit).sendKeys("100");
        findElement(page.lease).clear();
        findElement(page.lease).sendKeys("6");

        findElement(page.floor).clear();
        findElement(page.floor).sendKeys("10");
        findElement(page.room0).click();
        findElement(page.areaTotal).clear();
        findElement(page.areaTotal).sendKeys("333.33");
        findElement(page.areaKitchen).clear();
        findElement(page.areaKitchen).sendKeys("100");
        findElement(page.areaLiving).clear();
        findElement(page.areaLiving).sendKeys("150.15");

        findElement(page.decorationWhite).click();
        findElement(page.furnitureIKEA).click();
        findElement(page.repairEuro).click();
        findElement(page.balcony1).click();
        findElement(page.balconyNo2).click();
        findElement(page.bathroomSeparated2).click();
        findElement(page.addBathroom).click();
        findElement(page.viewWindowOnCourtyard).click();

        findElement(page.changeLangToEngOfPassport).click();
        Assert.assertTrue(findElement(page.passportOfApartmentEng).getText().contains("Passport of apartment"));
        findElement(page.changeLangToRusOfPassport).click();
        findElement(page.passportOfApartmentRus).clear();
        findElement(page.passportOfApartmentRus).sendKeys("Паспорт апартамента отредактирован");

        findElement(page.changeLangToEngOfDescription).click();
        sleep(1000);
        Assert.assertTrue(findElement(page.descriptionEng).getAttribute("value").contains("Description of apartment"));
        findElement(page.changeLangToRusOfDescription).click();
        sleep(1000);
        findElement(page.descriptionRus).clear();
        findElement(page.descriptionRus).sendKeys("Описание апартамента отредактировано" + " Описание апартамента отредактировано" + " Описание апартамента отредактировано");
        sleep(1000);
        findElement(page.tour3D).clear();
        findElement(page.tour3D).sendKeys("https://arenda.ahml.ru/ - сайт АИЖК");

        findElement(page.deletePhoto1).click();

        String jpg = System.getProperty("user.dirWork") + "\\files\\images\\111.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        String png = System.getProperty("user.dirWork") + "\\files\\images\\222.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        selectByValue(page.dropDownMenuPhoto2, "11");
        selectByValue(page.dropDownMenuPhoto3, "4");

        sleep(1000);

        int apartNumber = Integer.parseInt(number) + 1;
        String a = String.valueOf(apartNumber);

        findElement(page.numberObject).clear();
        findElement(page.numberObject).sendKeys(a);

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(String.valueOf(apartNumber));
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        try {
            click(globalpage.submit);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки 'Сохранить' не найден");
        }

        try {
            waitBy(page.toastSaveObject);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент тоста о сохранении объекта не найден");
        }

        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

    }

    @Test(description = "Сверка параметров отредактированнго апартамента", priority = 4)
    public void verificationParamsEditApartment() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textApartment.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.apartmentNumber, 30);
        Assert.assertTrue(findElement(page.apartmentsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean apartmentNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeObjectNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                Assert.assertTrue(findElement(page.listApartmentFloor).getText().contains("10"));
                Assert.assertTrue(findElement(page.listApartmentRooms).getText().contains("Студия"));
                Assert.assertTrue(findElement(page.listApartmentAreaTotal).getText().contains("333,33"));
                Assert.assertTrue(findElement(page.listApartmentViewWindow).getText().contains("Во двор"));
                Assert.assertTrue(findElement(page.listApartmentDecoration).getText().contains("Белый"));
                Assert.assertTrue(findElement(page.listApartmentPrice).getText().contains("100 000"));
                Assert.assertTrue(findElement(page.listApartmentStatusFree).getText().contains("Свободен"));

                click(globalpage.searchNumber(number));
                try {
                    waitBy(globalpage.apartmentCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Элемент карточки апартамента не найден");
                }

                apartmentNumber = false;
                break;

            }

        }

        if (apartmentNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        Assert.assertTrue(findElement(page.safeComplex).getText().contains("Лайнер"));
        Assert.assertTrue(findElement(page.safeAddress).getText().contains("Москва, Ходынский бульвар, 2"));
        Assert.assertTrue(findElement(page.safeObjectNumber).getText().contains(number));
        Assert.assertTrue(findElement(page.safeApartmentPrice).getText().contains("100 000"));
        Assert.assertTrue(findElement(page.safeDeposit).getText().contains("100%"));
        Assert.assertTrue(findElement(page.safeLease).getText().contains("6"));
        Assert.assertTrue(findElement(page.safeEditRequirementsToRenter).getText().contains("Нет"));
        Assert.assertTrue(findElement(page.safeFloor).getText().contains("10"));
        Assert.assertTrue(findElement(page.safeRooms).getText().contains("Cтудия"));
        Assert.assertTrue(findElement(page.safeApartmentAreaTotal).getText().contains("333.33"));
        Assert.assertTrue(findElement(page.safeAreaKitchen).getText().contains("100"));
        Assert.assertTrue(findElement(page.safeAreaLiving).getText().contains("150.15"));
        Assert.assertTrue(findElement(page.safeDecoration).getText().contains("Белый"));
        Assert.assertTrue(findElement(page.safeFurniture).getText().contains("Икеа"));
        Assert.assertTrue(findElement(page.safeRepair).getText().contains("Евроремонт"));
        Assert.assertTrue(findElement(page.safeBalcony).getText().contains("Балкон"));
        Assert.assertTrue(findElement(page.safeBathroom).getText().contains("Совмещённый и 2 раздельных"));
        Assert.assertTrue(findElement(page.safeViewWindow).getText().contains("Во двор"));
        Assert.assertTrue(findElement(page.safeEditFacilities).getText().contains("Нет"));
        Assert.assertTrue(findElement(page.safePassportOfApartment).getText().contains("Паспорт апартамента отредактирован"));
        Assert.assertTrue(findElement(page.safeDescription).getText().equals("Описание апартамента отредактировано Описание апартамента отредактировано Описание апартамента отредактировано"));
        Assert.assertTrue(findElement(page.safeTour3D).getText().contains("https://arenda.ahml.ru/ - сайт АИЖК"));
        Assert.assertTrue(findElement(page.safeEditNamePhoto1).getText().contains("Кухня"));
        Assert.assertTrue(findElement(page.safeEditNamePhoto2).getText().contains("Балкон"));
        Assert.assertTrue(findElement(page.safeEditNamePhoto3).getText().contains("Холл"));

    }

    @Test(description = "Добавление машиноместа", priority = 5)
    public void addParking() throws InterruptedException, AWTException, FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        click(globalpage.add);
        click(page.objectParking);
        try {
            waitBy(page.objectParkingElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы добавления машиноместа не найден");
        }

        findElement(page.price).sendKeys("15000");
        findElement(page.areaTotal).sendKeys("20");

        String jpg = System.getProperty("user.dirWork") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        String png = System.getProperty("user.dirWork") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textParking.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String a = scanner.nextLine();
        int parkingNumberInt = Integer.parseInt(a);

        for(;;){
            sendKeys(page.numberObject, "A" + String.valueOf(parkingNumberInt));
            click(globalpage.submit);
            sleep(2000);
            if (!isElementPresent(page.errorNumberParking)){
                break;
            } else {
                parkingNumberInt++;
                sleep(1000);
            }
        }

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(String.valueOf(parkingNumberInt));
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        try {
            findElement(page.toastNewObject);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент тоста о добавлении объекта не найден");
        }

        try {
            waitBy(globalpage.parkingCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки машиноместа не найден");
        }

    }

    @Test(description = "Сверка параметров добавленного машиноместа", priority = 6)
    public void verificationParamsNewParking() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        selectByValue(page.changeTypeObject, "string:parkings");

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textParking.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.apartmentNumber, 30);
        driver.navigate().refresh();
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("A" + number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean apartmentNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeObjectNumber);
        for (WebElement element : elements) {

            if (element.getText().equals("A" + number)) {

                Assert.assertTrue(findElement(page.listParkingAreaTotal).getText().contains("20"));
                Assert.assertTrue(findElement(page.listParkingPrice).getText().contains("15 000"));
                Assert.assertTrue(findElement(page.listParkingStatusFree).getText().contains("Свободен"));

                click(page.searchNumberParking(number));
                try {
                    waitBy(globalpage.parkingCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Элемент карточки машиноместа не найден");
                }

                apartmentNumber = false;
                break;

            }

        }

        if (apartmentNumber) {
            afterExeptionMessage("Номер машиноместа не найден");
        }

        Assert.assertTrue(findElement(page.safeComplex).getText().contains("Лайнер"));
        Assert.assertTrue(findElement(page.safeAddress).getText().contains("Москва, Ходынский бульвар, 2"));

        Assert.assertTrue(findElement(page.safeObjectNumber).getText().contains("A" + number));
        Assert.assertTrue(findElement(page.safeParkingPrice).getText().contains("15 000"));
        Assert.assertTrue(findElement(page.safeParkingAreaTotal).getText().contains("20"));

        List<WebElement>images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 2);

    }

    @Test(description = "Редактирование машиноместа", priority = 7)
    public void editParking() throws InterruptedException, FileNotFoundException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        selectByValue(page.changeTypeObject, "string:parkings");

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textParking.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.apartmentNumber, 30);
        driver.navigate().refresh();
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("A" + number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(page.apartmentNumber);
        try {
            waitBy(globalpage.parkingCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки машиноместа не найден");
        }

        click(page.editObject);
        try {
            waitBy(page.editObjectElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы редактирования объекта не найден");
        }

        findElement(page.price).clear();
        findElement(page.price).sendKeys("30000");

        findElement(page.areaTotal).clear();
        findElement(page.areaTotal).sendKeys("33.33");

        findElement(page.deletePhoto1).click();

        String jpg = System.getProperty("user.dirWork") + "\\files\\images\\111.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        String png = System.getProperty("user.dirWork") + "\\files\\images\\222.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        int apartNumber = Integer.parseInt(number) + 1;
        String a = String.valueOf(apartNumber);

        findElement(page.numberObject).clear();
        findElement(page.numberObject).sendKeys("A" + a);

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(String.valueOf(apartNumber));
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        try {
            click(globalpage.submit);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки 'Сохранить' не найден");
        }

        try {
            waitBy(page.toastSaveObject);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент тоста о сохранении объекта не найден");
        }

        try {
            waitBy(globalpage.parkingCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки машиноместа не найден");
        }

    }

    @Test(description = "Сверка параметров отредактированного машиноместа", priority = 8)
    public void verificationParamsEditParking() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        selectByValue(page.changeTypeObject, "string:parkings");

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textParking.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.apartmentNumber, 30);
        driver.navigate().refresh();
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("A" + number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean apartmentNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeObjectNumber);
        for (WebElement element : elements) {

            if (element.getText().equals("A" + number)) {

                Assert.assertTrue(findElement(page.listParkingAreaTotal).getText().contains("33,33"));
                Assert.assertTrue(findElement(page.listParkingPrice).getText().contains("30 000"));
                Assert.assertTrue(findElement(page.listParkingStatusFree).getText().contains("Свободен"));

                click(page.searchNumberParking(number));
                try {
                    waitBy(globalpage.parkingCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Элемент карточки машиноместа не найден");
                }

                apartmentNumber = false;
                break;

            }

        }

        if (apartmentNumber) {
            afterExeptionMessage("Номер машиноместа не найден");
        }

        Assert.assertTrue(findElement(page.safeComplex).getText().contains("Лайнер"));
        Assert.assertTrue(findElement(page.safeAddress).getText().contains("Москва, Ходынский бульвар, 2"));

        Assert.assertTrue(findElement(page.safeObjectNumber).getText().contains("A" + number));
        Assert.assertTrue(findElement(page.safeParkingPrice).getText().contains("30 000"));
        Assert.assertTrue(findElement(page.safeParkingAreaTotal).getText().contains("33.33"));

        List<WebElement>images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 3);

    }

}
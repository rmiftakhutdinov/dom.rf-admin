package tests.objectsTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.ObjectsPage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.assertTrue;

public class ObjectsCard extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход по ссылкам в карточке апартамента")
    public void openLinksInApartmentCard() throws InterruptedException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        objectsSection();

        waitBy(page.apartmentNumber, 30);

        findElement(globalpage.searchInput).sendKeys("221" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(globalpage.searchNumber("221"));
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

        String handleBefore = driver.getWindowHandle();

        findElement(page.renterOfApartment).click();
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            findElement(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки арендатора не найден");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.photo);
        try {
            waitBy(page.openPhoto);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент открытого в галерее фото не найден");
        }

        String photo1 = new String(findElement(page.openPhoto).getAttribute("src"));
        findElement(page.nextPhoto).click();
        sleep(1000);
        String photo2 = new String(findElement(page.openPhoto).getAttribute("src"));
        Assert.assertFalse(photo1.equals(photo2));
        findElement(page.prevPhoto).click();
        sleep(1000);
        Assert.assertFalse(photo2.equals(photo1));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        click(page.history);
        try {
            waitBy(page.historyElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент вкладки 'История' не найден");
        }

        click(page.renterOfApartment);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            findElement(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки арендатора не найден");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.orders);
        try {
            waitBy(page.ordersElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент вкладки 'Заявки' не найден");
        }

        click(page.orderInOrders);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки заявки не найден");
        }

        driver.navigate().back();

        click(page.contracts);
        try {
            waitBy(page.contractsElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент вкладки 'Договоры' не найден");
        }

        click(page.renterInContractOfApartment);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            findElement(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки арендатора не найден");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.showPhotos);
        try {
            waitBy(page.photoInContracts);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент фото в договоре не найден");
        }

        click(page.photoInContracts);
        try {
            waitBy(page.openPhoto);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент открытого в галерее фото не найден");
        }

        String photoInContracts1 = new String(findElement(page.openPhoto).getAttribute("src"));
        findElement(page.nextPhoto).click();
        sleep(1000);
        String photoInContracts2 = new String(findElement(page.openPhoto).getAttribute("src"));
        Assert.assertFalse(photoInContracts1.equals(photoInContracts2));
        findElement(page.prevPhoto).click();
        sleep(1000);
        Assert.assertFalse(photoInContracts2.equals(photoInContracts1));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        findElement(page.showPhotos).click();
        elementIsNotVisible(page.photoInContracts, 10);

    }

    @Test(description = "Смена статуса апартамента №200 на 'Неактивен'", priority = 1)
    public void changeStatusApartmentToNotActive() throws InterruptedException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber);

        findElement(globalpage.searchInput).sendKeys("200" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(globalpage.searchNumber("200"));
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

        if (isElementPresent(page.labelStatusRentedApartment)){
            Assert.assertTrue(isElementPresent(page.labelStatusRentedApartment));
            findElement(page.contracts).click();
            findElement(page.dropDownMenuInContracts).click();
            findElement(page.finishContract).click();

            sleep(1000);

            if(findElement(page.finishContractConfirm).isDisplayed()){

                click(page.finishContractConfirm);

            } else {

                waitBy(page.settleModal);
                findElement(page.checkboxCall).click();
                new WebDriverWait(driver, 30).until(ExpectedConditions.elementSelectionStateToBe(By.xpath("//input[@type='checkbox']"), true));
                click(page.ejectConfirm);

            }

            waitBy(page.changeStatus);

        }

        click(page.changeStatus);
        click(page.statusNotActive);
        click(page.save);
        try {
            waitBy(page.labelStatusNotActiveApartment, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент статуса апартамента 'Неактивен'");
        }

    }

    @Test(description = "Смена статуса апартамента №200 на 'Забронирован'", priority = 2)
    public void changeStatusApartmentToReserved() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        click(globalpage.ordersSection);
        click(globalpage.add);
        waitBy(globalpage.addNewOrderElement);

        findElement(globalpage.orderClientName).sendKeys("Тест");
        findElement(globalpage.orderClientPhone).sendKeys("9876543210");

        List<WebElement> labels = driver.findElements(globalpage.interestedCheckboxes);
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        click(globalpage.submit);
        waitBy(globalpage.orderCardElement);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        // Вырезание лишних символов из строки
        String orderNumber = findElement(By.xpath("//h1")).getText().replace("Заявка №", "").replace(" Новая", "").trim();

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(orderNumber);
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber);

        findElement(globalpage.searchInput).sendKeys("200" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(globalpage.searchNumber("200"));
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

        click(page.changeStatus);
        click(page.statusReserved);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        findElement(page.orderNumberInChangeStatusModal).sendKeys(number);
        click(page.calendarInChangeStatusModal);
        click(page.dateInChangeStatusModal);
        click(page.save);
        try {
            waitBy(page.labelStatusReservedApartment, 60);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент статуса апартамента 'Забронирован'");
        }

    }

    @Test(description = "Переход по ссылкам в карточке машиноместа", priority = 3)
    public void openLinksInParkingCard() throws InterruptedException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        selectByValue(page.changeTypeObject, "string:parkings");

        waitBy(page.apartmentNumber, 30);
        driver.navigate().refresh();
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("A21" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(globalpage.searchNumber("A21"));
        try {
            waitBy(globalpage.parkingCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки машиноместа не найден");
        }

        String handleBefore = driver.getWindowHandle();

        click(page.renterOfParking);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            waitBy(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки машиноместа не найден");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.photo);
        try {
            waitBy(page.openPhoto);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент открытого в галерее фото не найден");
        }

        String photo1 = new String(findElement(page.openPhoto).getAttribute("src"));
        findElement(page.nextPhoto).click();
        sleep(1000);
        String photo2 = new String(findElement(page.openPhoto).getAttribute("src"));
        Assert.assertFalse(photo1.equals(photo2));
        findElement(page.prevPhoto).click();
        sleep(1000);
        Assert.assertFalse(photo2.equals(photo1));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        click(page.history);
        try {
            waitBy(page.historyElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент вкладки 'История' не найден");
        }

        click(page.renterOfParking);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            waitBy(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки арендатора не найден");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.contracts);
        try {
            waitBy(page.contractsElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент вкладки 'Договоры' не найден");
        }

        click(page.renterInContractOfParking);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            waitBy(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки арендатора не найден");
        }
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.showPhotos);
        try {
            waitBy(page.photoInContracts);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент фото в договоре не найден");
        }

        click(page.photoInContracts);
        try {
            waitBy(page.openPhoto);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент открытого в галерее фото не найден");
        }

        String photoInContracts1 = new String(findElement(page.openPhoto).getAttribute("src"));
        findElement(page.nextPhoto).click();
        sleep(1000);
        String photoInContracts2 = new String(findElement(page.openPhoto).getAttribute("src"));
        Assert.assertFalse(photoInContracts1.equals(photoInContracts2));
        findElement(page.prevPhoto).click();
        sleep(1000);
        Assert.assertFalse(photoInContracts2.equals(photoInContracts1));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        findElement(page.showPhotos).click();
        elementIsNotVisible(page.photoInContracts, 10);

    }

}
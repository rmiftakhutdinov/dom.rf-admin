package tests.objectsTests;

import framework.Configuration;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.ObjectsPage;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ObjectsFilters extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Строка поиска в списке апартаментов")
    public void searchLineApartments() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        objectsSection();

        waitBy(page.apartmentNumber, 30);

        findElement(globalpage.searchInput).sendKeys("250" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.apartmentNumber).getText().equals("250"));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("89272401145" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(By.xpath("//div[@ng-if='!!obj.author']/a")).getText().equals("+7 927 240-11-45"));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("Мифтахутдинов Рамиль" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(By.xpath("//span[@ng-switch='obj.author.type']/a")).getText().equals("Мифтахутдинов Рамиль Рустэмович"));

    }

    @Test(description = "Фильтр по этажам в списке апартаментов", priority = 1)
    public void floorsFilterApartments() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber, 30);

        // Сбор этажей в коллекцию + удаление лишних пробелов
        Set<String> allFloors = driver.findElements(By.xpath("//tbody/tr/td[3]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allFloors.contains("2"));
        Assert.assertTrue(allFloors.contains("3"));
        Assert.assertTrue(allFloors.contains("4"));

        // Сбор этажей в коллекцию без удаления лишних пробелов
        // List<String> floors = driver.findElements(By.xpath("//tbody/tr/td[3]")).stream().map(WebElement::getText).collect(Collectors.toList());

        driver.navigate().refresh();
        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterFloors, "number:2");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> floor2 = driver.findElements(By.xpath("//tbody/tr/td[3]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(floor2.contains("2"));
        Assert.assertFalse(floor2.contains("3"));
        Assert.assertFalse(floor2.contains("4"));

        driver.navigate().refresh();
        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterFloors, "number:3");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> floor3 = driver.findElements(By.xpath("//tbody/tr/td[3]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(floor3.contains("2"));
        Assert.assertTrue(floor3.contains("3"));
        Assert.assertFalse(floor3.contains("4"));

    }

    @Test(description = "Фильтр по комнатам в списке апартаментов", priority = 2)
    public void roomsFilterApartments() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber, 30);

        Set<String> allRooms = driver.findElements(By.xpath("//tbody/tr/td[4]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allRooms.contains("Студия"));
        Assert.assertTrue(allRooms.contains("2"));
        Assert.assertTrue(allRooms.contains("3"));

        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterRooms, "string:studio");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> room2 = driver.findElements(By.xpath("//tbody/tr/td[4]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(room2.contains("Студия"));
        Assert.assertFalse(room2.contains("2"));
        Assert.assertFalse(room2.contains("3"));

        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterRooms, "string:three");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> room3 = driver.findElements(By.xpath("//tbody/tr/td[4]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(room3.contains("Студия"));
        Assert.assertFalse(room3.contains("2"));
        Assert.assertTrue(room3.contains("3"));

    }

    @Test(description = "Фильтр по цветам отделки в списке апартаментов", priority = 3)
    public void decorationFilterApartments() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber, 30);

        Set<String> allDecorations = driver.findElements(By.xpath("//tbody/tr/td[7]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allDecorations.contains("Белый"));
        Assert.assertTrue(allDecorations.contains("Бежевый"));

        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterDecoration, "string:beige");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> beige = driver.findElements(By.xpath("//tbody/tr/td[7]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(beige.contains("Бежевый"));
        Assert.assertFalse(beige.contains("Белый"));

        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterDecoration, "string:white");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> white = driver.findElements(By.xpath("//tbody/tr/td[7]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(white.contains("Бежевый"));
        Assert.assertTrue(white.contains("Белый"));

    }

    @Test(description = "Фильтр по статусам в списке апартаментов", priority = 4)
    public void statusFilterApartments() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber, 30);

        Set<String> allStatuses = driver.findElements(By.xpath("//tbody/tr/td[10]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);

        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterStatus, "string:free");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> statusFree = driver.findElements(By.xpath("//tbody/tr/td[10]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(statusFree.contains("Свободен"));
        Assert.assertFalse(statusFree.contains("Арендован"));
        Assert.assertFalse(statusFree.contains("Забронирован"));
        Assert.assertFalse(statusFree.contains("Неактивен"));
        Assert.assertFalse(statusFree.contains("Приостановлен"));

        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterStatus, "string:withoutReserve");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> statusReserve = driver.findElements(By.xpath("//tbody/tr/td[10]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusReserve.contains("Свободен"));
        Assert.assertFalse(statusReserve.contains("Арендован"));
        Assert.assertTrue(statusReserve.contains("Забронирован"));
        Assert.assertFalse(statusReserve.contains("Неактивен"));
        Assert.assertFalse(statusReserve.contains("Приостановлен"));

        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterStatus, "string:inactive");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> statusIinactive = driver.findElements(By.xpath("//tbody/tr/td[10]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusIinactive.contains("Свободен"));
        Assert.assertFalse(statusIinactive.contains("Арендован"));
        Assert.assertFalse(statusIinactive.contains("Забронирован"));
        Assert.assertTrue(statusIinactive.contains("Неактивен"));
        Assert.assertFalse(statusIinactive.contains("Приостановлен"));

        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterStatus, "string:block");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> statusBlock = driver.findElements(By.xpath("//tbody/tr/td[10]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusBlock.contains("Свободен"));
        Assert.assertTrue(statusBlock.contains("Арендован"));
        Assert.assertFalse(statusBlock.contains("Забронирован"));
        Assert.assertFalse(statusBlock.contains("Неактивен"));
        Assert.assertFalse(statusBlock.contains("Приостановлен"));

        Assert.assertTrue(findElement(page.filterDate).isDisplayed());

        waitBy(page.apartmentNumber,30);

        selectByValue(page.filterStatus, "string:paused");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> statusPaused = driver.findElements(By.xpath("//tbody/tr/td[10]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusPaused.contains("Свободен"));
        Assert.assertFalse(statusPaused.contains("Арендован"));
        Assert.assertFalse(statusPaused.contains("Забронирован"));
        Assert.assertFalse(statusPaused.contains("Неактивен"));
        Assert.assertTrue(statusPaused.contains("Приостановлен"));

    }

    @Test(description = "Сброс фильтров в списке апартаментов", priority = 5)
    public void resetFiltersAparments() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber, 30);

        findElement(globalpage.searchInput).sendKeys("Всякая дребедень" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        selectByValue(page.filterFloors, "number:2");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        selectByValue(page.filterRooms, "string:studio");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        selectByValue(page.filterStatus, "string:free");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        click(globalpage.filterReset);
        try {
            waitBy(page.apartmentNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден объект в разделе 'Объекты'");
        }

        Assert.assertTrue(findElement(globalpage.searchInput).getAttribute("value").equals(""));

        Set<String> allFloors = driver.findElements(By.xpath("//tbody/tr/td[3]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allFloors.size() > 1);

        Set<String> allRooms = driver.findElements(By.xpath("//tbody/tr/td[4]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allRooms.size() > 1);

        Set<String> allStatuses = driver.findElements(By.xpath("//tbody/tr/td[10]")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);

    }

    @Test(description = "Строка поиска в списке машиномест", priority = 6)
    public void searchLineParkings() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber, 30);

        selectByValue(page.changeTypeObject, "string:parkings");

        waitBy(page.apartmentNumber, 30);
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("A70" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.apartmentNumber).getText().equals("A70"));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("89272401145" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(By.xpath("//div[@ng-if='!!obj.author']/a")).getText().equals("+7 927 240-11-45"));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("Мифтахутдинов Рамиль" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(By.xpath("//span[@ng-switch='obj.author.type']/a")).getText().equals("Мифтахутдинов Рамиль Рустэмович"));

    }

    @Test(description = "Фильтр по статусам в списке машиномест", priority = 7)
    public void statusFilterParkings() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();
        waitBy(page.apartmentNumber, 30);

        selectByValue(page.changeTypeObject, "string:parkings");

        waitBy(page.apartmentNumber, 30);
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        Set<String> allStatuses = driver.findElements(By.xpath("//tbody/tr/td[6]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);

        selectByValue(page.filterStatus, "string:free");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> statusFree = driver.findElements(By.xpath("//tbody/tr/td[6]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(statusFree.contains("Свободен"));
        Assert.assertFalse(statusFree.contains("Арендован"));

        selectByValue(page.filterStatus, "string:block");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.apartmentNumber, 30);

        Set<String> statusBlock = driver.findElements(By.xpath("//tbody/tr/td[6]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusBlock.contains("Свободен"));
        Assert.assertTrue(statusBlock.contains("Арендован"));

        Assert.assertTrue(findElement(page.filterDate).isEnabled());

    }

    @Test(description = "Сброс фильтров в списке машиномест", priority = 8)
    public void resetFiltersParkings() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(page.apartmentNumber, 30);

        selectByValue(page.changeTypeObject, "string:parkings");

        waitBy(page.apartmentNumber, 30);
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("Всякая дребедень" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        selectByValue(page.filterStatus, "string:free");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        click(globalpage.filterReset);
        try {
            waitBy(page.apartmentNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден объект в разделе 'Объекты'");
        }

        Assert.assertTrue(findElement(globalpage.searchInput).getAttribute("value").equals(""));

        Set<String> allStatuses = driver.findElements(By.xpath("//tbody/tr/td[6]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);

    }

}
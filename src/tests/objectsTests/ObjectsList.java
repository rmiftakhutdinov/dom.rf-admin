package tests.objectsTests;

import framework.Configuration;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.ObjectsPage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class ObjectsList extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход по ссылкам в списке апартаментов")
    public void openLinksInApartmentsList() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        objectsSection();

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\objects\\textApartment.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.openDropDownMenu, 30);
        Assert.assertTrue(findElement(page.apartmentsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean objectNumber = true;
        List<WebElement> elements1 = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements1) {

            if (element.getText().equals(number)) {

                click(page.listApartmentDropDownMenu);
                click(page.dropDownMenuOpenApartment);
                try {
                    waitBy(globalpage.apartmentCardElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент карточки апартамента не найден");
                }

                objectNumber = false;
                break;

            }

        }

        if (objectNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        driver.navigate().back();

        waitBy(page.apartmentNumber);

        List<WebElement> elements2 = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements2) {

            if (element.getText().equals(number)) {

                click(page.listApartmentDropDownMenu);
                click(page.dropDownMenuEditApartment);
                try {
                    waitBy(page.editObjectElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент страницы редактирования объекта не найден");
                }

                objectNumber = false;
                break;

            }

        }

        if (objectNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        driver.navigate().back();

        waitBy(page.apartmentNumber);

        List<WebElement> elements3 = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements3) {

            if (element.getText().equals(number)) {

                click(page.listApartmentDropDownMenu);
                click(page.dropDownMenuSettleRenter);
                try {
                    waitBy(page.settlementElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент страницы заселения арендатора не найден");
                }

                objectNumber = false;
                break;

            }

        }

        if (objectNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        driver.navigate().back();

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("221");

        List<WebElement> elements4 = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements4) {

            if (element.getText().equals("221")) {

                click(page.listApartmentRenter);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент карточки арендатора не найден");
                }

                objectNumber = false;
                break;

            }

        }

        if (objectNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

    }

    // Для комплекса "Лайнер" данный функционал скрыт
    @Ignore
    @Test(description = "Копирование апартамента", priority = 1)
    public void copyApartment() throws InterruptedException, FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textApartment.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.openDropDownMenu, 30);
        Assert.assertTrue(findElement(page.apartmentsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean apartmentNumber = true;
        List<WebElement> elements = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                click(page.listApartmentDropDownMenu);
                click(page.dropDownMenuCopyApartment);
                try {
                    waitBy(page.objectApartmentElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент страницы добавления апартамента не найден");
                }

                apartmentNumber = false;
                break;

            }

        }

        if (apartmentNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        String idCopyObject = "http://dev-arenda.ahml.ru/admin/realty-add?isCopy";
        Assert.assertTrue(driver.getCurrentUrl().contains(idCopyObject));

        int apartNumber = Integer.parseInt(number);

        for(;;){
            sendKeys(page.numberObject, String.valueOf(apartNumber));
            elementIsNotVisible(globalpage.disableCreate, 30);
            click(globalpage.submit);
            sleep(2000);
            if (!isElementPresent(page.errorNumberApartment)){
                break;
            } else {
                apartNumber--;
                sleep(1000);
            }
        }

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(String.valueOf(apartNumber));
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        try {
            waitBy(page.toastNewObject);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент тоста о создании объекта не найден");
        }

        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

    }

    // Для комплекса "Лайнер" данный функционал скрыт
    @Ignore
    @Test(description = "Сверка параметров скопированного апартамента", priority = 2)
    public void verificationParamsCopyApartment() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textApartment.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.apartmentNumber, 30);
        Assert.assertTrue(findElement(page.apartmentsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean apartmentNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeObjectNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                click(globalpage.searchNumber(number));
                try {
                    waitBy(globalpage.apartmentCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Элемент карточки апартамента не найден");
                }

                apartmentNumber = false;
                break;

            }

        }

        if (apartmentNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        Assert.assertTrue(findElement(page.safeComplex).getText().contains("Лайнер"));
        Assert.assertTrue(findElement(page.safeAddress).getText().contains("Москва, Ходынский бульвар, 2"));
        Assert.assertTrue(findElement(page.safeObjectNumber).getText().contains(number));
        Assert.assertTrue(findElement(page.safeApartmentPrice).getText().contains("100 000"));
        Assert.assertTrue(findElement(page.safeDeposit).getText().contains("100%"));
        Assert.assertTrue(findElement(page.safeLease).getText().contains("6"));
        Assert.assertTrue(findElement(page.safeEditRequirementsToRenter).getText().contains("Нет"));
        Assert.assertTrue(findElement(page.safeFloor).getText().contains("10"));
        Assert.assertTrue(findElement(page.safeRooms).getText().contains("Cтудия"));
        Assert.assertTrue(findElement(page.safeApartmentAreaTotal).getText().contains("333.33"));
        Assert.assertTrue(findElement(page.safeAreaKitchen).getText().contains("100"));
        Assert.assertTrue(findElement(page.safeAreaLiving).getText().contains("150.15"));
        Assert.assertTrue(findElement(page.safeDecoration).getText().contains("Белый"));
        Assert.assertTrue(findElement(page.safeFurniture).getText().contains("Икеа"));
        Assert.assertTrue(findElement(page.safeRepair).getText().contains("Евроремонт"));
        Assert.assertTrue(findElement(page.safeBalcony).getText().contains("Балкон"));
        Assert.assertTrue(findElement(page.safeBathroom).getText().contains("Совмещённый и 2 раздельных"));
        Assert.assertTrue(findElement(page.safeViewWindow).getText().contains("Во двор"));
        Assert.assertTrue(findElement(page.safeEditFacilities).getText().contains("Нет"));
        Assert.assertTrue(findElement(page.safePassportOfApartment).getText().contains("Паспорт апартамента отредактирован"));
        Assert.assertTrue(findElement(page.safeDescription).getText().contains("Описание апартамента отредактировано"));
        Assert.assertTrue(findElement(page.safeTour3D).getText().contains("https://arenda.ahml.ru/ - сайт АИЖК"));
        Assert.assertTrue(findElement(page.safeEditNamePhoto1).getText().contains("Кухня"));
        Assert.assertTrue(findElement(page.safeEditNamePhoto2).getText().contains("Балкон"));
        Assert.assertTrue(findElement(page.safeEditNamePhoto3).getText().contains("Холл"));

    }

    @Test(description = "Создание объявления из апартамента", priority = 3)
    public void addAdvertFromApartment() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textApartment.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.openDropDownMenu, 30);
        Assert.assertTrue(findElement(page.apartmentsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean apartmentNumber = true;
        List<WebElement> elements = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                click(page.listApartmentDropDownMenu);
                click(page.dropDownMenuAddAdvert);
                try {
                    waitBy(globalpage.addAdverttElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент страницы добавления объявления не найден");
                }

                apartmentNumber = false;
                break;

            }

        }

        if (apartmentNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        String idAddAdvertFromApartment = "http://dev-arenda.ahml.ru/admin/ads-add?isFromRealty";
        Assert.assertTrue(driver.getCurrentUrl().contains(idAddAdvertFromApartment));

        findElement(globalpage.mainPhoneNumberInAddAdvert).sendKeys("1234567890");
        findElement(globalpage.avitoPhoneNumberInAddAdvert).sendKeys("1111111111");
        findElement(globalpage.cianPhoneNumberInAddAdvert).sendKeys("2222222222");
        findElement(globalpage.yandexPhoneNumberInAddAdvert).sendKeys("3333333333");
        findElement(globalpage.winnerPhoneNumberInAddAdvert).sendKeys("4444444444");
        findElement(globalpage.otherPhoneNumberInAddAdvert).sendKeys("5555555555");

        elementIsNotVisible(globalpage.disableCreate, 30);

        click(globalpage.submit);
        try {
            waitBy(globalpage.toastNewAdvert);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент тоста о создании объявления не найден");
        }

        try {
            waitBy(globalpage.advertCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент карточки объявления не найден");
        }

    }

    @Test(description = "Переход по ссылкам в списке машиномест", priority = 4)
    public void openLinksInParkingsList() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        selectByValue(page.changeTypeObject, "string:parkings");

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\objects\\textParking.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.apartmentNumber, 30);
        driver.navigate().refresh();
        waitBy(page.parkingsListElement);
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("A" + number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean objectNumber = true;
        List<WebElement> elements1 = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements1) {

            if (element.getText().equals("A" + number)) {

                click(page.listParkingDropDownMenu);
                click(page.dropDownMenuOpenParking);
                try {
                    waitBy(globalpage.parkingCardElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент карточки машиноместа не найден");
                }

                objectNumber = false;
                break;

            }

        }

        if (objectNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        driver.navigate().back();

        waitBy(page.apartmentNumber);

        List<WebElement> elements2 = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements2) {

            if (element.getText().equals("A" + number)) {

                click(page.listParkingDropDownMenu);
                click(page.dropDownMenuEditParking);
                try {
                    waitBy(page.editObjectElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент страницы редактирования объекта не найден");
                }

                objectNumber = false;
                break;

            }

        }

        if (objectNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        driver.navigate().back();

        waitBy(page.apartmentNumber);

        List<WebElement> elements3 = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements3) {

            if (element.getText().equals("A" + number)) {

                click(page.listParkingDropDownMenu);
                click(page.dropDownMenuLeaseObject);
                try {
                    waitBy(page.leaseElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент страницы сдачи машиноместа не найден");
                }

                objectNumber = false;
                break;

            }

        }

        if (objectNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        driver.navigate().back();

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("A70");

        waitBy(page.apartmentNumber);

        List<WebElement> elements4 = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements4) {

            if (element.getText().equals("A" + number)) {

                click(page.listParkingRenter);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент карточки арендатора не найден");
                }

                objectNumber = false;
                break;

            }

        }

        if (objectNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

    }

    // Для комплекса "Лайнер" данный функционал скрыт
    @Ignore
    @Test(description = "Копирование машиноместа", priority = 5)
    public void copyParking() throws InterruptedException, FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        selectByValue(page.changeTypeObject, "string:parkings");

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textParking.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.apartmentNumber, 30);
        driver.navigate().refresh();
        waitBy(page.parkingsListElement);
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("A" + number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean apartmentNumber = true;
        List<WebElement> elements = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements) {

            if (element.getText().equals("A" + number)) {

                click(page.listParkingDropDownMenu);
                click(page.dropDownMenuCopyParking);
                try {
                    waitBy(page.objectParkingElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент страницы добавления машиноместа не найден");
                }

                apartmentNumber = false;
                break;

            }

        }

        if (apartmentNumber) {
            afterExeptionMessage("Номер апартамента не найден");
        }

        String idCopyObject = "http://dev-arenda.ahml.ru/admin/realty-add?isCopy";
        Assert.assertTrue(driver.getCurrentUrl().contains(idCopyObject));

        int parkingNumberInt = Integer.parseInt(number);

        for(;;){
            sendKeys(page.numberObject, "A" + String.valueOf(parkingNumberInt));
            elementIsNotVisible(globalpage.disableCreate, 30);
            click(globalpage.submit);
            sleep(2000);
            if (!isElementPresent(page.errorNumberParking)){
                break;
            } else {
                parkingNumberInt--;
                sleep(1000);
            }
        }

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(String.valueOf(parkingNumberInt));
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        try {
            waitBy(page.toastNewObject);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент тоста о создании объекта не найден");
        }

        waitBy(globalpage.parkingCardElement);
        Assert.assertTrue(findElement(globalpage.parkingCardElement).isDisplayed());

    }

    // Для комплекса "Лайнер" данный функционал скрыт
    @Ignore
    @Test(description = "Сверка параметров скопированного машиноместа", priority = 6)
    public void verificationParamsCopyParking() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        selectByValue(page.changeTypeObject, "string:parkings");

        String txt = System.getProperty("user.dirWork") + "\\files\\txtFiles\\objects\\textParking.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        waitBy(page.apartmentNumber, 30);
        driver.navigate().refresh();
        waitBy(page.parkingsListElement);
        Assert.assertTrue(findElement(page.parkingsListElement).isDisplayed());

        findElement(globalpage.searchInput).sendKeys("A" + number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.apartmentNumber);

        boolean apartmentNumber = true;
        List<WebElement> elements = driver.findElements(page.listSafeObjectNumber);

        for (WebElement element : elements) {

            if (element.getText().equals("A" + number)) {

                Assert.assertTrue(findElement(page.listParkingAreaTotal).getText().contains("33,33"));
                Assert.assertTrue(findElement(page.listParkingPrice).getText().contains("30 000"));
                Assert.assertTrue(findElement(page.listParkingStatusFree).getText().contains("Свободен"));

                click(globalpage.searchNumber("A" + number));
                try {
                    waitBy(globalpage.parkingCardElement);
                } catch (NoSuchElementException ex){
                    afterExeptionMessage("Элемент карточки машиноместа не найден");
                }

                apartmentNumber = false;
                break;

            }

        }

        if (apartmentNumber) {
            afterExeptionMessage("Номер машиноместа не найден");
        }

        Assert.assertTrue(findElement(page.safeComplex).getText().contains("Лайнер"));
        Assert.assertTrue(findElement(page.safeAddress).getText().contains("Москва, Ходынский бульвар, 2"));

        Assert.assertTrue(findElement(page.safeObjectNumber).getText().contains("A" + number));
        Assert.assertTrue(findElement(page.safeParkingPrice).getText().contains("30 000"));
        Assert.assertTrue(findElement(page.safeParkingAreaTotal).getText().contains("33.33"));

        List<WebElement> images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size()==3);

    }

    @Test(description = "Пагинация в списке объектов", priority = 7)
    public void objectsListPagination() {

        GlobalPage globalpage = new GlobalPage();
        ObjectsPage page = new ObjectsPage();

        click(globalpage.statisticsSection);
        click(globalpage.objectsSection);
        objectsSection();

        waitBy(globalpage.nextPage, 30);

        String page1 = new String(findElement(page.apartmentNumber).getAttribute("href"));

        click(globalpage.nextPage,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        String page2 = new String(findElement(page.apartmentNumber).getAttribute("href"));
        Assert.assertFalse(page1.equals(page2));

        click(globalpage.prevPage,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

        click(globalpage.page2,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        click(globalpage.page1,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

    }

}
package tests.ordersTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.OrdersPage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.assertTrue;

public class AddAndEditOrder extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Добавление заявки с любым апартаментом")
    public void addOrderWithAnyoneApartment() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        objectsSection();
        click(globalpage.ordersSection);
        ordersSection();

        click(globalpage.add);
        try {
            waitBy(globalpage.addNewOrderElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления заявки");
        }

        click(page.dateOfOrderInput);
        click(page.prevMonth);
        click(page.setDate);

        String dateOfOrder = findElement(page.dateOfOrderInput).getAttribute("value");

        sendKeys(page.family, "Тестовый");
        sendKeys(page.name, "Клиент");
        sendKeys(page.patronymic, "Заявкович");
        sendKeys(page.phone, "0987654321");
        sendKeys(page.email, "test@email.com");
        sendKeys(page.passportNumber, "1234 ABCD");
        sleep(500);
        sendKeys(page.passportIssued, "выдан Советом Народных Комиссаров СССР");
        sendKeys(page.passportIssuedDay, "01");
        sendKeys(page.passportIssuedMonth, "05");
        sendKeys(page.passportIssuedYear, "1990");
        sendKeys(page.passportPassportCode, "тук-тук");
        sendKeys(page.snils, "12345678910");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        Assert.assertTrue(findElement(page.desiredApartmentAnyone).getAttribute("class").contains("active"));

        List<WebElement> labels = driver.findElements(globalpage.interestedCheckboxes);
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        sleep(500);
        sendKeys(page.wishesAndComments, "Этот комментарий должен быть виден в списке заявок и в карточке заявки");

        elementIsNotVisible(By.xpath("//button[contains(@class,'disabled')]"),30);

        click(globalpage.submit);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        String orderNumber = findElement(By.xpath("//h1")).getText().replace("Заявка №", "").replace(" Новая", "").trim();

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(orderNumber);
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        Assert.assertTrue(findElement(page.point).isDisplayed());

        String dateOfOrderCard = findElement(page.dateOfOrderCard).getText().replace("от ", "").trim();
        assertTrue(dateOfOrder.contains(dateOfOrderCard));

        click(globalpage.ordersSection);

        waitBy(By.xpath("//tbody/tr[1]/td[2]"));

        String dateOfOrderList[] = findElement(By.xpath("//tbody/tr[1]/td[2]")).getText().split("\n");
        assertTrue(dateOfOrderCard.contains(dateOfOrderList[1]));


    }

    @Test(description = "Сверка параметров добавленной заявки с любым апартаментом", priority = 1)
    public void verificationParamsNewOrderWithAnyoneApartment() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.orderNumber);

        boolean orderNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeOrderNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                Assert.assertTrue(findElement(page.listSafeClientFullName).getText().contains("Тестовый Клиент Заявкович"));
                Assert.assertTrue(findElement(page.listSafeClientPhone).getText().contains("+7 987 654-32-10"));
                Assert.assertTrue(findElement(page.listSafeApartmentType).getText().contains("2, 3, Ст, МСт"));
                Assert.assertTrue(findElement(page.listSafeComments).getText().contains("Этот комментарий должен быть виден в списке заявок и в карточке заявки"));
                Assert.assertTrue(findElement(page.listStatusLabelNewOrder).getText().contains("Новая"));

                click(globalpage.searchNumber(number), 30);
                try {
                    waitBy(globalpage.orderCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки заявки");
                }

                orderNumber = false;
                break;

            }

        }

        if (orderNumber) {
            afterExeptionMessage("Номер заявки не найден");
        }

        Assert.assertTrue(findElement(page.safeCardStatusLabelNewOrder).getText().contains("Новая"));
        Assert.assertTrue(findElement(page.safeClientFullName).getText().contains("Тестовый Клиент Заявкович"));
        Assert.assertTrue(findElement(page.safeClientPhone).getText().contains("+7 987 654-32-10"));
        Assert.assertTrue(findElement(page.safeClientEmail).getText().contains("test@email.com"));
        Assert.assertTrue(findElement(page.passportData).getText().contains("1234 ABCD, выдан Советом Народных Комиссаров СССР 01.05.1990, тук-тук"));
        Assert.assertTrue(findElement(page.safeSnils).getText().contains("Иностранный гражданин"));
        Assert.assertTrue(findElement(page.safeCardApartmentType).getText().replace("Интересуется", "").trim().contains("2, 3, Ст, МСт"));

        Assert.assertTrue(findElement(page.safeComments).getText().contains("Этот комментарий должен быть виден в списке заявок и в карточке заявки"));

        click(page.documents);
        List<WebElement> images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 2);
        Assert.assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusCreateOrder).getText().contains("Создана заявка"));

        click(page.point);
        try {
            waitBy(page.editOrderElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы редактирования заявки");
        }

    }

    @Test(description = "Добавление заявки с конкретным апартаментом без закрепления", priority = 2)
    public void addOrderWithConcreteApartmentNoReserve() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        click(globalpage.add);
        try {
            waitBy(globalpage.addNewOrderElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления заявки");
        }

        click(page.dateOfOrderInput);
        click(page.prevMonth);
        click(page.setDate);

        String dateOfOrder = findElement(page.dateOfOrderInput).getAttribute("value");

        sendKeys(page.family, "Тестовый");
        sendKeys(page.name, "Клиент");
        sendKeys(page.patronymic, "Заявкович");
        sendKeys(page.phone, "0987654321");
        sendKeys(page.email, "test@email.com");
        sendKeys(page.passportNumber, "1234 ABCD");
        sleep(500);
        sendKeys(page.passportIssued, "выдан Советом Народных Комиссаров СССР");
        sendKeys(page.passportIssuedDay, "01");
        sendKeys(page.passportIssuedMonth, "05");
        sendKeys(page.passportIssuedYear, "1990");
        sendKeys(page.passportPassportCode, "тук-тук");
        sendKeys(page.snils, "12345678910");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        Assert.assertTrue(findElement(page.desiredApartmentAnyone).getAttribute("class").contains("active"));

        click(page.desiredApartmentConcrete);
        waitBy(page.withoutReserve);
        Assert.assertTrue(findElement(page.withoutReserve).getAttribute("class").contains("active"));

        List<WebElement> labels = driver.findElements(globalpage.interestedCheckboxes);
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        sleep(500);
        sendKeys(page.wishesAndComments, "Этот комментарий должен быть виден в списке заявок и в карточке заявки");

        click(globalpage.submit);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        String orderNumber = findElement(By.xpath("//h1")).getText().replace("Заявка №", "").replace(" Новая", "").trim();

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(orderNumber);
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        String dateOfOrderCard = findElement(page.dateOfOrderCard).getText().replace("от ", "").trim();
        assertTrue(dateOfOrder.contains(dateOfOrderCard));

        click(globalpage.ordersSection);

        waitBy(By.xpath("//tbody/tr[1]/td[2]"));

        String dateOfOrderList[] = findElement(By.xpath("//tbody/tr[1]/td[2]")).getText().split("\n");
        assertTrue(dateOfOrderCard.contains(dateOfOrderList[1]));

    }

    @Test(description = "Сверка параметров добавленной заявки с конкретным апартаментом без закрепления", priority = 3)
    public void verificationParamsNewOrderWithConcreteApartmentNoReserve() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.orderNumber);

        boolean orderNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeOrderNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                Assert.assertTrue(findElement(page.listSafeClientFullName).getText().contains("Тестовый Клиент Заявкович"));
                Assert.assertTrue(findElement(page.listSafeClientPhone).getText().contains("+7 987 654-32-10"));
                Assert.assertTrue(findElement(page.listSafeApartmentType).getText().contains("2, 3, Ст, МСт"));
                Assert.assertTrue(findElement(page.listSafeApartmentNumber).getAttribute("href").contains("/admin/realty-card/apartment/227?n=1"));
                Assert.assertTrue(findElement(page.listSafeComments).getText().contains("Этот комментарий должен быть виден в списке заявок и в карточке заявки"));
                Assert.assertTrue(findElement(page.listStatusLabelNewOrder).getText().contains("Новая"));

                click(globalpage.searchNumber(number), 30);
                try {
                    waitBy(globalpage.orderCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки заявки");
                }

                orderNumber = false;
                break;

            }

        }

        if (orderNumber) {
            afterExeptionMessage("Номер заявки не найден");
        }

        Assert.assertTrue(findElement(page.safeCardStatusLabelNewOrder).getText().contains("Новая"));
        Assert.assertTrue(findElement(page.safeClientFullName).getText().contains("Тестовый Клиент Заявкович"));
        Assert.assertTrue(findElement(page.safeClientPhone).getText().contains("+7 987 654-32-10"));
        Assert.assertTrue(findElement(page.safeClientEmail).getText().contains("test@email.com"));
        Assert.assertTrue(findElement(page.passportData).getText().contains("1234 ABCD, выдан Советом Народных Комиссаров СССР 01.05.1990, тук-тук"));
        Assert.assertTrue(findElement(page.safeSnils).getText().contains("Иностранный гражданин"));
        Assert.assertTrue(findElement(page.apartmentInOrderCard).getAttribute("href").contains("/admin/realty-card/apartment/227?n=1"));
        Assert.assertTrue(findElement(page.safeCardApartmentType).getText().replace("Интересуется", "").trim().contains("2, 3, Ст, МСт"));

        Assert.assertTrue(findElement(page.safeComments).getText().contains("Этот комментарий должен быть виден в списке заявок и в карточке заявки"));

        click(page.documents);
        List<WebElement> images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 2);
        Assert.assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusCreateOrder).getText().contains("Создана заявка"));

    }

    @Test(description = "Добавление заявки с закреплением конкретного апартамента", priority = 4)
    public void addOrderWithReserveConcreteApartment() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        click(globalpage.add);
        try {
            waitBy(globalpage.addNewOrderElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления заявки");
        }

        click(page.dateOfOrderInput);
        click(page.prevMonth);
        click(page.setDate);

        String dateOfOrder = findElement(page.dateOfOrderInput).getAttribute("value");

        sendKeys(page.family, "Тестовый");
        sendKeys(page.name, "Клиент");
        sendKeys(page.patronymic, "Заявкович");
        sendKeys(page.phone, "0987654321");
        sendKeys(page.email, "test@email.com");
        sendKeys(page.passportNumber, "1234 ABCD");
        sleep(500);
        sendKeys(page.passportIssued, "выдан Советом Народных Комиссаров СССР");
        sendKeys(page.passportIssuedDay, "01");
        sendKeys(page.passportIssuedMonth, "05");
        sendKeys(page.passportIssuedYear, "1990");
        sendKeys(page.passportPassportCode, "тук-тук");
        sendKeys(page.snils, "12345678910");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        Assert.assertTrue(findElement(page.desiredApartmentAnyone).getAttribute("class").contains("active"));

        click(page.desiredApartmentConcrete);
        waitBy(page.withoutReserve);
        Assert.assertTrue(findElement(page.withoutReserve).getAttribute("class").contains("active"));

        boolean a = true;

        List<WebElement> apartments = driver.findElements(By.xpath("//div[@ng-if='order.apartSelected']/div[3]//select/option"));
        for (WebElement element : apartments){

            String apartment = element.getText();
            selectByText(page.apartNumberSelect, apartment);
            int apart = Integer.parseInt(apartment);

            if (apart <= 283){

                if (findElement(By.xpath("//div[14]//span/span")).getText().contains("Свободен")
                        || findElement(By.xpath("//div[14]//span/span")).getText().contains("Расторжение")
                        || findElement(By.xpath("//div[14]//span/span")).getText().contains("Неактивен")){

                    try {
                        if (findElement(page.withReserve).getAttribute("disabled").equals("null")){
                            a = false;
                            break;
                        }
                    } catch (NullPointerException ex){
                        a = false;
                        break;
                    }

                }

            } else {

                apart++;

            }

        }

        if (a) {
            afterExeptionMessage("Свободный апартамент не найден");
        }

        click(page.withReserve);
        String date1 = findElement(page.calendar).getAttribute("value");
        click(page.calendar);
        click(page.nextMonth);
        findElement(page.setDate).click();
        String date2 = findElement(page.calendar).getAttribute("value");
        Assert.assertFalse(date1.contains(date2));

        List<WebElement> labels = driver.findElements(globalpage.interestedCheckboxes);
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        sleep(500);
        sendKeys(page.wishesAndComments, "Этот комментарий должен быть виден в списке заявок и в карточке заявки");

        click(globalpage.submit);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        String orderNumber = findElement(By.xpath("//h1")).getText().replace("Заявка №", "").replace(" Новая", "").trim();

        try {
            FileWriter writer = new FileWriter(file);
            writer.write(orderNumber);
            writer.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }

        String dateOfOrderCard = findElement(page.dateOfOrderCard).getText().replace("от ", "").trim();
        assertTrue(dateOfOrder.contains(dateOfOrderCard));

        click(globalpage.ordersSection);

        waitBy(By.xpath("//tbody/tr[1]/td[2]"));

        String dateOfOrderList[] = findElement(By.xpath("//tbody/tr[1]/td[2]")).getText().split("\n");
        assertTrue(dateOfOrderCard.contains(dateOfOrderList[1]));

    }

    @Test(description = "Сверка параметров добавленной заявки с закреплением конкретного апартамента", priority = 5)
    public void verificationParamsNewOrderWithReserveConcreteApartment() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.orderNumber);

        boolean orderNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeOrderNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                Assert.assertTrue(findElement(page.listSafeClientFullName).getText().contains("Тестовый Клиент Заявкович"));
                Assert.assertTrue(findElement(page.listSafeClientPhone).getText().contains("+7 987 654-32-10"));
                Assert.assertTrue(findElement(page.listSafeApartmentType).getText().contains("2, 3, Ст, МСт"));
                Assert.assertTrue(findElement(page.listSafeApartmentNumber).getAttribute("href").contains("/admin/realty-card/apartment/"));
                Assert.assertTrue(findElement(page.listSafeComments).getText().contains("Этот комментарий должен быть виден в списке заявок и в карточке заявки"));
                Assert.assertTrue(findElement(page.listStatusLabelNewOrder).getText().contains("Новая"));

                click(globalpage.searchNumber(number), 30);
                try {
                    waitBy(globalpage.orderCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки заявки");
                }

                orderNumber = false;
                break;

            }

        }

        if (orderNumber) {
            afterExeptionMessage("Номер заявки не найден");
        }

        Assert.assertTrue(findElement(page.safeCardStatusLabelNewOrder).getText().contains("Новая"));
        Assert.assertTrue(findElement(page.safeClientFullName).getText().contains("Тестовый Клиент Заявкович"));
        Assert.assertTrue(findElement(page.safeClientPhone).getText().contains("+7 987 654-32-10"));
        Assert.assertTrue(findElement(page.safeClientEmail).getText().contains("test@email.com"));
        Assert.assertTrue(findElement(page.passportData).getText().contains("1234 ABCD, выдан Советом Народных Комиссаров СССР 01.05.1990, тук-тук"));
        Assert.assertTrue(findElement(page.safeSnils).getText().contains("Иностранный гражданин"));
        Assert.assertTrue(findElement(page.apartmentInOrderCard).getAttribute("href").contains("/admin/realty-card/apartment/"));
        //Assert.assertTrue(findElement(page.safeCardApartmentStatusLabel).getText().contains("Забронирован"));
        Assert.assertTrue(findElement(page.safeCardApartmentType).getText().replace("Интересуется", "").trim().contains("2, 3, Ст, МСт"));
        Assert.assertTrue(findElement(page.safeComments).getText().contains("Этот комментарий должен быть виден в списке заявок и в карточке заявки"));

        click(page.documents);
        List<WebElement> images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 2);
        Assert.assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusCreateOrder).getText().contains("Создана заявка"));

    }

    @Test(description = "Редактирование заявки", priority = 6)
    public void editOrder() throws InterruptedException, FileNotFoundException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.orderNumber);

        boolean orderNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeOrderNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                click(globalpage.searchNumber(number), 30);
                try {
                    waitBy(globalpage.orderCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки заявки");
                }

                orderNumber = false;
                break;

            }

        }

        if (orderNumber) {
            afterExeptionMessage("Номер заявки не найден");
        }

        click(page.editOrder);
        waitBy(page.editOrderElement);

        click(page.dateOfOrderInput);
        click(page.prevMonth);
        click(page.setDate);

        String dateOfOrder = findElement(page.dateOfOrderInput).getAttribute("value");

        findElement(page.family).clear();
        sendKeys(page.family, "Сидоров");
        findElement(page.name).clear();
        sendKeys(page.name, "Иван");
        findElement(page.patronymic).clear();
        sendKeys(page.patronymic, "Петрович");
        findElement(page.phone).clear();
        sendKeys(page.phone, "0123456789");
        findElement(page.email).clear();
        sendKeys(page.email, "edit@email.com");
        findElement(page.passportNumber).clear();
        sendKeys(page.passportNumber, "ABCD 1234");
        findElement(page.passportIssued).clear();
        sleep(500);
        sendKeys(page.passportIssued, "выдан Председателем Компартии СССР");
        findElement(page.passportIssuedDay).clear();
        sendKeys(page.passportIssuedDay, "05");
        findElement(page.passportIssuedMonth).clear();
        sendKeys(page.passportIssuedMonth, "01");
        findElement(page.passportIssuedYear).clear();
        sendKeys(page.passportIssuedYear, "1980");
        findElement(page.passportPassportCode).clear();
        sendKeys(page.passportPassportCode, "раз-раз");
        click(By.xpath("//div[11]/div[2]/div[@class='checkbox']/label"));

        click(page.editDeletePhoto);

        String jpg = System.getProperty("user.dir") + "\\files\\images\\111.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String png = System.getProperty("user.dir") + "\\files\\images\\222.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        Assert.assertTrue(findElement(page.desiredApartmentConcrete).getAttribute("class").contains("active"));
        click(page.desiredApartmentAnyone);
        Assert.assertTrue(findElement(page.desiredApartmentAnyone).getAttribute("class").contains("active"));
        click(page.desiredApartmentConcrete);
        waitBy(page.apartNumberSelect);
        Assert.assertTrue(findElement(page.desiredApartmentConcrete).getAttribute("class").contains("active"));

        elementIsNotVisible(globalpage.spinner);
        waitBy(By.xpath("//label[contains(@class,'active')][text()='Закрепить']"));

        click(page.withoutReserve);
        Assert.assertTrue(findElement(page.withoutReserve).getAttribute("class").contains("active"));
        elementIsNotVisible(page.calendar);

        click(globalpage.submit);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        String dateOfOrderCard = findElement(page.dateOfOrderCard).getText().replace("от ", "").trim();
        assertTrue(dateOfOrder.contains(dateOfOrderCard));

        click(globalpage.ordersSection);

        waitBy(By.xpath("//tbody/tr[1]/td[2]"));

        String dateOfOrderList[] = findElement(By.xpath("//tbody/tr[1]/td[2]")).getText().split("\n");
        assertTrue(dateOfOrderCard.contains(dateOfOrderList[1]));

    }

    @Test(description = "Сверка параметров отредактированной заявки", priority = 7)
    public void verificationParamsEditOrder() throws InterruptedException, FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.orderNumber);

        boolean orderNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeOrderNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                Assert.assertTrue(findElement(page.listSafeClientFullName).getText().contains("Сидоров Иван Петрович"));
                Assert.assertTrue(findElement(page.listSafeClientPhone).getText().contains("+7 123 456-78-90"));
                Assert.assertTrue(findElement(page.listSafeApartmentType).getText().contains("2, 3, Ст, МСт"));
                Assert.assertTrue(findElement(page.listSafeApartmentNumber).getAttribute("href").contains("/admin/realty-card/apartment/"));
                Assert.assertTrue(findElement(page.listSafeComments).getText().contains("Этот комментарий должен быть виден в списке заявок и в карточке заявки"));
                Assert.assertTrue(findElement(page.listStatusLabelNewOrder).getText().contains("Новая"));

                click(globalpage.searchNumber(number), 30);
                try {
                    waitBy(globalpage.orderCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки заявки");
                }

                orderNumber = false;
                break;

            }

        }

        if (orderNumber) {
            afterExeptionMessage("Номер заявки не найден");
        }

        Assert.assertTrue(findElement(page.safeCardStatusLabelNewOrder).getText().contains("Новая"));
        Assert.assertTrue(findElement(page.safeClientFullName).getText().contains("Сидоров Иван Петрович"));
        Assert.assertTrue(findElement(page.safeClientPhone).getText().contains("+7 123 456-78-90"));
        Assert.assertTrue(findElement(page.safeClientEmail).getText().contains("edit@email.com"));
        Assert.assertTrue(findElement(page.passportData).getText().contains("ABCD 1234, выдан Председателем Компартии СССР 05.01.1980, раз-раз"));
        Assert.assertTrue(findElement(page.safeSnils).getText().contains("123-456-789 10"));
        Assert.assertTrue(findElement(page.apartmentInOrderCard).getAttribute("href").contains("/admin/realty-card/apartment/"));
        //Assert.assertTrue(findElement(page.safeCardApartmentStatusLabel).getText().contains("Забронирован"));
        Assert.assertTrue(findElement(page.safeCardApartmentType).getText().replace("Интересуется", "").trim().contains("2, 3, Ст, МСт"));

        String handleBefore = driver.getWindowHandle();
        String apartNumber = findElement(page.apartmentInOrderCard).getAttribute("href");

        click(page.apartmentInOrderCard);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }
        String urlApartment = driver.getCurrentUrl();
        Assert.assertTrue(urlApartment.contains(apartNumber));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.documents);
        List<WebElement> images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 3);
        Assert.assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

        click(page.photo);
        try {
            waitBy(page.openPhoto);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент открытого в галерее фото не найден");
        }

        String photo1 = new String(findElement(page.openPhoto).getAttribute("src"));
        click(page.nextPhoto);
        sleep(1000);
        String photo2 = new String(findElement(page.openPhoto).getAttribute("src"));
        Assert.assertFalse(photo1.equals(photo2));
        click(page.prevPhoto);
        sleep(1000);
        Assert.assertFalse(photo2.equals(photo1));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusCreateOrder).getText().contains("Создана заявка"));

        click(page.comments);
        Assert.assertTrue(findElement(page.safeComments).getText().contains("Этот комментарий должен быть виден в списке заявок и в карточке заявки"));

    }

}
package tests.ordersTests;

import framework.Configuration;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.OrdersPage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.assertTrue;

public class OrderCard extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход в карточку апартамента по ссылке из карточки заявки")
    public void toApartmentCardFromOrderCard() throws InterruptedException, FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        objectsSection();
        click(globalpage.ordersSection);
        ordersSection();

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.orderNumber);

        boolean orderNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeOrderNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                click(globalpage.searchNumber(number), 30);
                try {
                    waitBy(globalpage.orderCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки заявки");
                }

                orderNumber = false;
                break;

            }

        }

        if (orderNumber) {
            afterExeptionMessage("Номер заявки не найден");
        }

        click(page.apartmentInOrderCard);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

    }

    //Переделать тест под анкету
    @Ignore
    @Test(description = "Отправка паспортных данных в СБ", priority = 1)
    public void sendPassportDataToSecurity() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.orderNumber);

        boolean orderNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeOrderNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                click(globalpage.searchNumber(number), 30);
                try {
                    waitBy(globalpage.orderCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки заявки");
                }

                orderNumber = false;
                break;

            }

        }

        if (orderNumber) {
            afterExeptionMessage("Номер заявки не найден");
        }

        click(page.sendToSecurity);
        try {
            waitBy(page.toastSecurity);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент тоста об отправке данных в СБ не найден");
        }
        Assert.assertTrue(findElement(page.safeCardStatusLabelSecurity).getText().equals("Проверка СБ"));

        click(page.history);
        try {
            waitBy(page.historyStatusSecurity);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент статуса заявки не найден");
        }

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusLabelSecurity);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент статуса заявки не найден");
        }

    }

    @Test(description = "Добавление комментария в карточке заявки", priority = 2)
    public void addCommentInOrderCard() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.orderNumber);

        boolean orderNumber = true;

        List<WebElement> elements = driver.findElements(page.listSafeOrderNumber);
        for (WebElement element : elements) {

            if (element.getText().equals(number)) {

                click(globalpage.searchNumber(number), 30);
                try {
                    waitBy(globalpage.orderCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки заявки");
                }

                orderNumber = false;
                break;

            }

        }

        if (orderNumber) {
            afterExeptionMessage("Номер заявки не найден");
        }

        sendKeys(page.commentsTextField, "Комментарий, добавленный в карточке заявки");
        click(page.addComment);
        try {
            waitBy(page.newCommentElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент добавленного комментария в карточке заявки");
        }

        click(globalpage.ordersSection);
        try {
            waitBy(page.listSafeNewComment);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент добавленного комментария в списке заявок");
        }

    }

    @Test(description = "Смена статусов заявки", priority = 3)
    public void changeOrderStatuses() {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        click(globalpage.add);
        try {
            waitBy(globalpage.addNewOrderElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления заявки");
        }

        sendKeys(globalpage.orderClientName, "Клиент");
        sendKeys(globalpage.orderClientPhone, "0987654321");

        List<WebElement> labels = driver.findElements(globalpage.interestedCheckboxes);
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        click(globalpage.submit);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.changeStatus);
        click(page.cancelChangeStatus);
        elementIsNotVisible(page.cancelChangeStatus);

        click(page.changeStatus);
        click(page.changeStatusToWaitingList,30);

        click(page.saveChangeStatus);
        try {
            waitBy(page.safeCardStatusLabelWaitingList);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в карточке заявки");
        }

        click(page.history);
        try {
            waitBy(page.historyStatusWaitingList);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в истории заявки");
        }

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusLabelWaitingList);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в списке заявок");
        }

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.changeStatus);
        click(page.changeStatusToInvitedToShow,30);
        click(page.inviteToShowCalendar);
        click(page.nextMonth);
        click(page.setDate);

        // Имеется баг - в списке заявок не выводится год. После исправления убрать replace.
        String dateModal = findElement(page.inviteToShowCalendar).getAttribute("value").replace(" 2018", "").trim();

        sendKeys(page.inviteToShowTime, "1500");

        String timeModal = findElement(page.inviteToShowTime).getAttribute("value");

        click(page.saveChangeStatus);
        try {
            waitBy(page.safeCardStatusLabelInvitedToShow);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в карточке заявки");
        }

        click(page.history);
        try {
            waitBy(page.historyStatusInvitedToShow);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в истории заявки");
        }

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusLabelInvitedToShow);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в списке заявок");
        }

        String dateAndTime = findElement(By.xpath("//tbody/tr[1]/td[7]/small")).getText().replace(":", "");

        assertTrue(dateAndTime.contains(dateModal));
        assertTrue(dateAndTime.contains(timeModal));

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.changeStatus);
        click(page.changeStatusToCollectionDocuments,30);

        click(page.saveChangeStatus);
        try {
            waitBy(page.safeCardStatusLabelCollectionDocuments);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в карточке заявки");
        }

        click(page.history);
        try {
            waitBy(page.historyStatusCollectionDocuments);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в истории заявки");
        }

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusLabelCollectionDocuments);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в списке заявок");
        }

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.changeStatus);
        click(page.changeStatusToThinks,30);

        click(page.saveChangeStatus);
        try {
            waitBy(page.safeCardStatusLabelThinks);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в карточке заявки");
        }

        click(page.history);
        try {
            waitBy(page.historyStatusThinks);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в истории заявки");
        }

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusLabelThinks);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в списке заявок");
        }

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.changeStatus);
        click(page.changeStatusToSignedBA,30);

        click(page.saveChangeStatus);
        try {
            waitBy(page.safeCardStatusLabelSignedBA);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в карточке заявки");
        }

        click(page.history);
        try {
            waitBy(page.historyStatusSignedBA);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в истории заявки");
        }

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusLabelSignedBA);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в списке заявок");
        }

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.changeStatus);
        click(page.changeStatusToSignedLease,30);

        click(page.saveChangeStatus);
        try {
            waitBy(page.safeCardStatusLabelSignedLease);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в карточке заявки");
        }

        elementIsNotVisible(page.editOrder);

        click(page.history);
        try {
            waitBy(page.historyStatusSignedLease);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в истории заявки");
        }

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusLabelSignedLease);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в списке заявок");
        }

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.changeStatus);
        click(page.changeStatusToNegative,30);

        sendKeys(page.statusNegativeComment, "Комментарий с причиной отказа");
        click(page.saveChangeStatus);
        try {
            waitBy(page.safeCardStatusLabelNegative);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в карточке заявки");
        }

        elementIsNotVisible(page.editOrder);

        click(page.history);
        try {
            waitBy(page.historyStatusNegative);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в истории заявки");
        }

        click(page.comments);
        try {
            waitBy(page.historyStatusNegativeComment);
        } catch (TimeoutException ex) {
            afterExeptionMessage("Не найден элемент комментария об отказе во вкладке 'Комментарии специалиста'");
        }

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusLabelNegative);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в списке заявок");
        }

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.changeStatus);
        click(page.changeStatusToNew,30);

        click(page.saveChangeStatus);
        try {
            waitBy(page.safeCardStatusLabelNewOrder);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в карточке заявки");
        }

        click(page.history);
        try {
            waitBy(page.historyStatusNewOrder);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в истории заявки");
        }

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusLabelNewOrder);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в списке заявок");
        }

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.changeStatus);
        click(page.changeStatusToDelete,30);

        click(page.saveChangeStatus);
        try {
            waitBy(page.safeCardStatusLabelDelete);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в карточке заявки");
        }

        elementIsNotVisible(page.editOrder);

        click(page.history);
        try {
            waitBy(page.historyStatusDelete);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент изменённого статуса в истории заявки");
        }

    }

    @Test(description = "Создание арендатора из карточки заявки", priority = 4)
    public void addRenterFromOrderCard() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        click(globalpage.add);
        try {
            waitBy(globalpage.addNewOrderElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления заявки");
        }

        sendKeys(page.family, "Тестовый");
        sendKeys(page.name, "Клиент");
        sendKeys(page.patronymic, "Заявкович");
        sendKeys(page.phone, "0987654321");
        sendKeys(page.email, "test@email.com");
        sendKeys(page.passportNumber, "1234 ABCD");
        sleep(500);
        sendKeys(page.passportIssued, "выдан Советом Народных Комиссаров СССР");
        sendKeys(page.passportIssuedDay, "01");
        sendKeys(page.passportIssuedMonth, "05");
        sendKeys(page.passportIssuedYear, "1990");
        sendKeys(page.passportPassportCode, "тук-тук");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        List<WebElement> labels = driver.findElements(globalpage.interestedCheckboxes);
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        click(globalpage.submit);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.addRenter)).build().perform();
        String tooltip = new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.tooltip-inner"))).getText();
        Assert.assertTrue(tooltip.equals("Создать арендатора на основе данных клиента из заявки"));

        click(page.addRenter);
        try {
            waitBy(globalpage.addNewRenterElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления арендатора");
        }

        Assert.assertTrue(findElement(globalpage.renterFamily).getAttribute("value").contains("Тестовый"));
        Assert.assertTrue(findElement(globalpage.renterName).getAttribute("value").contains("Клиент"));
        Assert.assertTrue(findElement(globalpage.renterPatronymic).getAttribute("value").contains("Заявкович"));
        Assert.assertTrue(findElement(globalpage.renterPhone).getAttribute("value").contains("+7 987 654-32-10"));
        Assert.assertTrue(findElement(globalpage.renterEmail).getAttribute("value").contains("test@email.com"));
        Assert.assertTrue(findElement(globalpage.renterPassportNumber).getAttribute("value").contains("1234 ABCD"));
        Assert.assertTrue(findElement(globalpage.renterPassportIssued).getAttribute("value").contains("выдан Советом Народных Комиссаров СССР"));
        Assert.assertTrue(findElement(globalpage.renterPassportIssuedDay).getAttribute("value").contains("01"));
        Assert.assertTrue(findElement(globalpage.renterPassportIssuedMonth).getAttribute("value").contains("05"));
        Assert.assertTrue(findElement(globalpage.renterPassportIssuedYear).getAttribute("value").contains("1990"));
        Assert.assertTrue(findElement(globalpage.renterPassportCode).getAttribute("value").contains("тук-тук"));

        List<WebElement> images=driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 2);
        Assert.assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

        sendKeys(globalpage.renterBornedAtDay, "12");
        sendKeys(globalpage.renterBornedAtMonth, "12");
        sendKeys(globalpage.renterBornedAtYear, "1980");
        sendKeys(globalpage.renterAddress, "улица Кучера, дом Петуха, квартира Цыпленка, 4 звонка");
        sendKeys(globalpage.renterBankTitle, "Банк");
        sendKeys(globalpage.renterBankAccount, "Счет");
        sendKeys(globalpage.renterBankCorrAccountId, "Корр.счет");
        sendKeys(globalpage.renterBankBIK, "БИК");

        for(;;){
            click(globalpage.submit);
            if (elementIsNotPresent(globalpage.errorRenterPhone)){
                break;
            } else {
                String phone = randomString("9876543210", 10);
                findElement(globalpage.renterPhone).clear();
                sendKeys(globalpage.renterPhone, phone);
            }
        }

        for(;;){
            click(globalpage.submit);
            if (elementIsNotPresent(globalpage.errorRenterEmail)){
                break;
            } else {
                String email = randomString("abcdefghijklmnopqrstuvwxyz", 7) + "@email.com";
                findElement(globalpage.renterEmail).clear();
                sendKeys(globalpage.renterEmail, email);
            }
        }

        for(;;){
            click(globalpage.submit);
            if (elementIsNotPresent(globalpage.errorRenterPassportNumber)){
                break;
            } else {
                String passportNumber = randomString("1234567890", 4) + " " + randomString("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 4);
                findElement(globalpage.renterPassportNumber).clear();
                sendKeys(globalpage.renterPassportNumber, passportNumber);
            }
        }

        try {
            waitBy(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки арендатора");
        }

    }

    @Test(description = "Назначение, выполнение и отмена перезвона, если клиент недоступен", priority = 5)
    public void assignationAndAcceptAndCancelRecallNotAvailable() {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        click(globalpage.add);
        try {
            waitBy(globalpage.addNewOrderElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления заявки");
        }

        sendKeys(globalpage.orderClientName, "Клиент");
        sendKeys(globalpage.orderClientPhone, "0987654321");

        List<WebElement> labels = driver.findElements(globalpage.interestedCheckboxes);
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        click(globalpage.submit);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.assignRecall);
        click(page.recallModalCancel);
        elementIsNotVisible(page.recallModalCancel);
        click(page.assignRecall);
        click(page.notAvailable);
        click(page.recallModalAssign);
        elementIsNotVisible(page.recallModalAssign,30);
        Assert.assertTrue(findElement(page.statusTitleAssignedRecall).isDisplayed());
        Assert.assertTrue(findElement(page.recallTextNotAvailable).isDisplayed());

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusNotAvailable).isDisplayed());

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusText);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент текста назначенного перезвона");
        }
        Assert.assertTrue(findElement(page.listStatusText).getText().contains("Назначен перезвон"));

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.cancelRecall);
        click(page.closeCancelRecallModal);
        elementIsNotVisible(page.closeCancelRecallModal);
        click(page.cancelRecall);
        click(page.confirmCancelRecallModal);
        elementIsNotVisible(page.confirmCancelRecallModal,30);
        elementIsNotVisible(page.recallTextRepeatCall,30);
        elementIsNotVisible(page.statusTitleAssignedRecall,30);

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusRecallCanceled).isDisplayed());

        click(globalpage.ordersSection);
        try {
            waitBy(page.orderNumber);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент заявки");
        }
        elementIsNotVisible(page.listStatusText);

        click(page.orderNumber);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.assignRecall);
        click(page.notAvailable);
        click(page.recallModalAssign);
        click(page.doneRecall);
        elementIsNotVisible(page.doneRecall,30);
        elementIsNotVisible(page.recallTextNotAvailable,30);
        elementIsNotVisible(page.statusTitleAssignedRecall);

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusRecallDone).isDisplayed());

        click(globalpage.ordersSection);
        try {
            waitBy(page.orderNumber);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент заявки");
        }
        elementIsNotVisible(page.listStatusText);

    }

    @Test(description = "Назначение, выполнение и отмена перезвона, если требуется повторный звонок", priority = 6)
    public void assignationAndAcceptAndCancelRecallRepeatCall() {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        click(globalpage.add);
        try {
            waitBy(globalpage.addNewOrderElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления заявки");
        }

        sendKeys(globalpage.orderClientName, "Клиент");
        sendKeys(globalpage.orderClientPhone, "0987654321");

        List<WebElement> labels = driver.findElements(globalpage.interestedCheckboxes);
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        click(globalpage.submit);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.assignRecall);
        String date1 = findElement(page.recallModalCalendar).getAttribute("value");
        click(page.recallModalCalendar);
        click(page.nextMonth);
        click(page.setDate);
        String date2 = findElement(page.recallModalCalendar).getAttribute("value");
        Assert.assertFalse(date1.contains(date2));
        findElement(page.recallModalTime).clear();
        sendKeys(page.recallModalTime, "1500");
        click(page.recallModalAssign);
        elementIsNotVisible(page.recallModalAssign,30);
        Assert.assertTrue(findElement(page.statusTitleAssignedRecall).isDisplayed());
        Assert.assertTrue(findElement(page.recallTextRepeatCall).isDisplayed());

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusRepeatCall).isDisplayed());

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusText);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент текста назначенного перезвона");
        }
        Assert.assertTrue(findElement(page.listStatusText).getText().contains("Назначен перезвон"));

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.cancelRecall);
        click(page.closeCancelRecallModal);
        elementIsNotVisible(page.closeCancelRecallModal);
        click(page.cancelRecall);
        click(page.confirmCancelRecallModal);
        elementIsNotVisible(page.confirmCancelRecallModal,30);
        elementIsNotVisible(page.recallTextRepeatCall,30);
        elementIsNotVisible(page.statusTitleAssignedRecall);

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusRecallCanceled).isDisplayed());

        click(globalpage.ordersSection);
        try {
            waitBy(page.orderNumber);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент заявки");
        }
        elementIsNotVisible(page.listStatusText);

        click(page.orderNumber);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.assignRecall);
        click(page.recallModalAssign);
        click(page.doneRecall);
        elementIsNotVisible(page.doneRecall,30);
        elementIsNotVisible(page.recallTextNotAvailable,30);
        elementIsNotVisible(page.statusTitleAssignedRecall);

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusRecallDone).isDisplayed());

        click(globalpage.ordersSection);
        try {
            waitBy(page.orderNumber);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент заявки");
        }
        elementIsNotVisible(page.listStatusText);

    }

    @Test(description = "Назначение, выполнение и отмена заселения", priority = 7)
    public void assignationAndAcceptAndCancelSettlement() {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        click(globalpage.add);
        try {
            waitBy(globalpage.addNewOrderElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления заявки");
        }

        sendKeys(globalpage.orderClientName, "Клиент");
        sendKeys(globalpage.orderClientPhone, "0987654321");

        List<WebElement> labels = driver.findElements(globalpage.interestedCheckboxes);
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        click(globalpage.submit);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.assignSettlement);
        click(page.settlementModalCancel);
        elementIsNotVisible(page.settlementModalCancel);
        click(page.assignSettlement);
        String date1 = findElement(page.settlementModalCalendar).getAttribute("value");
        click(page.settlementModalCalendar);
        click(page.nextMonth);
        click(page.setDate);
        String date2 = findElement(page.settlementModalCalendar).getAttribute("value");
        Assert.assertFalse(date1.contains(date2));
        findElement(page.settlementModalTime).clear();
        sendKeys(page.settlementModalTime, "1500");
        click(page.settlementModalAssign);
        elementIsNotVisible(page.settlementModalAssign,30);
        Assert.assertTrue(findElement(page.statusTitleAssignedSettlement).isDisplayed());
        Assert.assertTrue(findElement(page.settlementText).isDisplayed());

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusAssignedSettlement).isDisplayed());

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusText);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент текста назначенного заселения");
        }
        Assert.assertTrue(findElement(page.listStatusText).getText().contains("Назначено заселение"));

        click(page.orderNumber,30);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.cancelSettlement);
        click(page.closeCancelSettlementModal);
        elementIsNotVisible(page.closeCancelSettlementModal);
        click(page.cancelSettlement);
        click(page.confirmCancelSettlementModal);
        elementIsNotVisible(page.confirmCancelSettlementModal,30);
        elementIsNotVisible(page.settlementText,30);
        elementIsNotVisible(page.statusTitleAssignedSettlement);

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusSettlementCanceled).isDisplayed());

        click(globalpage.ordersSection);
        try {
            waitBy(page.orderNumber);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент заявки");
        }
        elementIsNotVisible(page.listStatusText);

        click(page.orderNumber);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.assignSettlement);
        click(page.settlementModalAssign);
        click(page.doneSettlement);
        elementIsNotVisible(page.doneSettlement,30);
        elementIsNotVisible(page.settlementText,30);
        elementIsNotVisible(page.statusTitleAssignedSettlement);

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusSettlementDone).isDisplayed());

        click(globalpage.ordersSection);
        try {
            waitBy(page.orderNumber);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент заявки");
        }
        elementIsNotVisible(page.listStatusText);

    }

    @Test(description = "Назначение перезвона и заселения", priority = 8)
    public void assignationRecallAndSettlement() {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        click(globalpage.add);
        try {
            waitBy(globalpage.addNewOrderElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления заявки");
        }

        sendKeys(globalpage.orderClientName, "Клиент");
        sendKeys(globalpage.orderClientPhone, "0987654321");

        List<WebElement> labels = driver.findElements(globalpage.interestedCheckboxes);
        for (WebElement label : labels){

            label.click();

        }

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement checkbox : checkboxes){

            assertTrue(checkbox.isSelected());

        }

        click(globalpage.submit);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.assignRecall);
        String recallDate1 = findElement(page.recallModalCalendar).getAttribute("value");
        click(page.recallModalCalendar);
        click(page.nextMonth);
        click(page.setDate);
        String recallDate2 = findElement(page.recallModalCalendar).getAttribute("value");
        Assert.assertFalse(recallDate1.contains(recallDate2));
        findElement(page.recallModalTime).clear();
        sendKeys(page.recallModalTime, "1500");
        click(page.recallModalAssign);
        elementIsNotVisible(page.recallModalAssign,30);
        Assert.assertTrue(findElement(page.statusTitleAssignedRecall).isDisplayed());
        Assert.assertTrue(findElement(page.recallTextRepeatCall).isDisplayed());

        click(page.assignSettlement);
        String settlementDate1 = findElement(page.settlementModalCalendar).getAttribute("value");
        click(page.settlementModalCalendar);
        click(page.nextMonth);
        click(page.setDate);
        String settlementDate2 = findElement(page.settlementModalCalendar).getAttribute("value");
        Assert.assertFalse(settlementDate1.contains(settlementDate2));
        findElement(page.settlementModalTime).clear();
        sendKeys(page.settlementModalTime, "1500");
        click(page.settlementModalAssign);
        elementIsNotVisible(page.settlementModalAssign,30);
        Assert.assertTrue(findElement(page.statusTitleAssignedSettlement).isDisplayed());
        Assert.assertTrue(findElement(page.settlementText).isDisplayed());

        click(globalpage.ordersSection);
        try {
            waitBy(page.listStatusTextAssignedRecall);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент текста назначенного перезвона");
        }
        try {
            waitBy(page.listStatusTextAssignedSettlement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент текста назначенного заселения");
        }
        Assert.assertTrue(findElement(page.listStatusTextAssignedRecall).getText().contains("Назначен перезвон"));
        Assert.assertTrue(findElement(page.listStatusTextAssignedSettlement).getText().contains("Назначено заселение"));

    }

}
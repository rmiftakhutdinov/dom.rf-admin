package tests.ordersTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.GlobalPage;
import pageObjects.OrdersPage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class OrdersFilters extends GlobalPage {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Строка поиска в списке заявок")
    public void searchLineOrders() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        objectsSection();
        click(globalpage.ordersSection);
        ordersSection();

        waitBy(page.orderNumber);

        String txt = System.getProperty("user.dir") + "\\files\\txtFiles\\orders\\textOrder.txt";
        File file = new File(txt);

        Scanner scanner = new Scanner(file);
        String number = scanner.nextLine();

        findElement(globalpage.searchInput).sendKeys(number + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.orderNumber).getText().equals(number));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("192" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.listSafeApartmentNumber).getText().contains("192"));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("89876543210" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(By.xpath("//tbody/tr[1]/td[3]/nobr")).getText().equals("+7 987 654-32-10"));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("Тестовый Клиент Заявкович" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.listSafeClientFullName).getText().contains("Тестовый Клиент Заявкович"));

    }

    @Test(description = "Фильтр по статусам в списке заявок", priority = 1)
    public void statusFilterOrders() {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        waitBy(page.orderNumber);

        Set<String> allStatuses = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);
        //Assert.assertFalse(allStatuses.contains("Удалена"));
        // имеется баг - удаленные заявки должны выводиться только при выбранном фильтре "Удалена"
        // после исправления раскомментить

        selectByValue(page.filterByStatus, "string:status_new");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> statusNew = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(statusNew.contains("Новая"));
        Assert.assertFalse(statusNew.contains("Лист ожидания"));
        Assert.assertFalse(statusNew.contains("Приглашён на просмотр"));
        Assert.assertFalse(statusNew.contains("Сбор документов"));
        Assert.assertFalse(statusNew.contains("Думает"));
        Assert.assertFalse(statusNew.contains("Проверка СБ"));
        Assert.assertFalse(statusNew.contains("Подписан ДБ"));
        Assert.assertFalse(statusNew.contains("Подписан ДА"));
        Assert.assertFalse(statusNew.contains("Отказ"));
        Assert.assertFalse(statusNew.contains("Удалена"));

        selectByValue(page.filterByStatus, "string:status_waitingList");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> statusWaitingList = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusWaitingList.contains("Новая"));
        Assert.assertTrue(statusWaitingList.contains("Лист ожидания"));
        Assert.assertFalse(statusWaitingList.contains("Приглашён на просмотр"));
        Assert.assertFalse(statusWaitingList.contains("Сбор документов"));
        Assert.assertFalse(statusWaitingList.contains("Думает"));
        Assert.assertFalse(statusWaitingList.contains("Проверка СБ"));
        Assert.assertFalse(statusWaitingList.contains("Подписан ДБ"));
        Assert.assertFalse(statusWaitingList.contains("Подписан ДА"));
        Assert.assertFalse(statusWaitingList.contains("Отказ"));
        Assert.assertFalse(statusWaitingList.contains("Удалена"));

        selectByValue(page.filterByStatus, "string:status_invitedToShow");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> statusInvitedToShow = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusInvitedToShow.contains("Новая"));
        Assert.assertFalse(statusInvitedToShow.contains("Лист ожидания"));
        Assert.assertTrue(statusInvitedToShow.contains("Приглашён на просмотр"));
        Assert.assertFalse(statusInvitedToShow.contains("Сбор документов"));
        Assert.assertFalse(statusInvitedToShow.contains("Думает"));
        Assert.assertFalse(statusInvitedToShow.contains("Проверка СБ"));
        Assert.assertFalse(statusInvitedToShow.contains("Подписан ДБ"));
        Assert.assertFalse(statusInvitedToShow.contains("Подписан ДА"));
        Assert.assertFalse(statusInvitedToShow.contains("Отказ"));
        Assert.assertFalse(statusInvitedToShow.contains("Удалена"));

        selectByValue(page.filterByStatus, "string:status_documentsGathering");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> statusCollectionDocuments = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusCollectionDocuments.contains("Новая"));
        Assert.assertFalse(statusCollectionDocuments.contains("Лист ожидания"));
        Assert.assertFalse(statusCollectionDocuments.contains("Приглашён на просмотр"));
        Assert.assertTrue(statusCollectionDocuments.contains("Сбор документов"));
        Assert.assertFalse(statusCollectionDocuments.contains("Думает"));
        Assert.assertFalse(statusCollectionDocuments.contains("Проверка СБ"));
        Assert.assertFalse(statusCollectionDocuments.contains("Подписан ДБ"));
        Assert.assertFalse(statusCollectionDocuments.contains("Подписан ДА"));
        Assert.assertFalse(statusCollectionDocuments.contains("Отказ"));
        Assert.assertFalse(statusCollectionDocuments.contains("Удалена"));

        selectByValue(page.filterByStatus, "string:status_thinking");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> statusThinks = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusThinks.contains("Новая"));
        Assert.assertFalse(statusThinks.contains("Лист ожидания"));
        Assert.assertFalse(statusThinks.contains("Приглашён на просмотр"));
        Assert.assertFalse(statusThinks.contains("Сбор документов"));
        Assert.assertTrue(statusThinks.contains("Думает"));
        Assert.assertFalse(statusThinks.contains("Проверка СБ"));
        Assert.assertFalse(statusThinks.contains("Подписан ДБ"));
        Assert.assertFalse(statusThinks.contains("Подписан ДА"));
        Assert.assertFalse(statusThinks.contains("Отказ"));
        Assert.assertFalse(statusThinks.contains("Удалена"));

        selectByValue(page.filterByStatus, "string:status_security");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> statusSecurity = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusSecurity.contains("Новая"));
        Assert.assertFalse(statusSecurity.contains("Лист ожидания"));
        Assert.assertFalse(statusSecurity.contains("Приглашён на просмотр"));
        Assert.assertFalse(statusSecurity.contains("Сбор документов"));
        Assert.assertFalse(statusSecurity.contains("Думает"));
        Assert.assertTrue(statusSecurity.contains("Проверка СБ"));
        Assert.assertFalse(statusSecurity.contains("Подписан ДБ"));
        Assert.assertFalse(statusSecurity.contains("Подписан ДА"));
        Assert.assertFalse(statusSecurity.contains("Отказ"));
        Assert.assertFalse(statusSecurity.contains("Удалена"));

        selectByValue(page.filterByStatus, "string:status_bookingContractSigned");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> statusSignedBA = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusSignedBA.contains("Новая"));
        Assert.assertFalse(statusSignedBA.contains("Лист ожидания"));
        Assert.assertFalse(statusSignedBA.contains("Приглашён на просмотр"));
        Assert.assertFalse(statusSignedBA.contains("Сбор документов"));
        Assert.assertFalse(statusSignedBA.contains("Думает"));
        Assert.assertFalse(statusSignedBA.contains("Проверка СБ"));
        Assert.assertTrue(statusSignedBA.contains("Подписан ДБ"));
        Assert.assertFalse(statusSignedBA.contains("Подписан ДА"));
        Assert.assertFalse(statusSignedBA.contains("Отказ"));
        Assert.assertFalse(statusSignedBA.contains("Удалена"));

        selectByValue(page.filterByStatus, "string:status_settlement");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> statusSignedLease = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusSignedLease.contains("Новая"));
        Assert.assertFalse(statusSignedLease.contains("Лист ожидания"));
        Assert.assertFalse(statusSignedLease.contains("Приглашён на просмотр"));
        Assert.assertFalse(statusSignedLease.contains("Сбор документов"));
        Assert.assertFalse(statusSignedLease.contains("Думает"));
        Assert.assertFalse(statusSignedLease.contains("Проверка СБ"));
        Assert.assertFalse(statusSignedLease.contains("Подписан ДБ"));
        Assert.assertTrue(statusSignedLease.contains("Подписан ДА"));
        Assert.assertFalse(statusSignedLease.contains("Отказ"));
        Assert.assertFalse(statusSignedLease.contains("Удалена"));

        selectByValue(page.filterByStatus, "string:status_refuse");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> statusNegative = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusNegative.contains("Новая"));
        Assert.assertFalse(statusNegative.contains("Лист ожидания"));
        Assert.assertFalse(statusNegative.contains("Приглашён на просмотр"));
        Assert.assertFalse(statusNegative.contains("Сбор документов"));
        Assert.assertFalse(statusNegative.contains("Думает"));
        Assert.assertFalse(statusNegative.contains("Проверка СБ"));
        Assert.assertFalse(statusNegative.contains("Подписан ДБ"));
        Assert.assertFalse(statusNegative.contains("Подписан ДА"));
        Assert.assertTrue(statusNegative.contains("Отказ"));
        Assert.assertFalse(statusNegative.contains("Удалена"));

        selectByValue(page.filterByStatus, "string:status_deleted");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> statusDeleted = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusDeleted.contains("Новая"));
        Assert.assertFalse(statusDeleted.contains("Лист ожидания"));
        Assert.assertFalse(statusDeleted.contains("Приглашён на просмотр"));
        Assert.assertFalse(statusDeleted.contains("Сбор документов"));
        Assert.assertFalse(statusDeleted.contains("Думает"));
        Assert.assertFalse(statusDeleted.contains("Проверка СБ"));
        Assert.assertFalse(statusDeleted.contains("Подписан ДБ"));
        Assert.assertFalse(statusDeleted.contains("Подписан ДА"));
        Assert.assertFalse(statusDeleted.contains("Отказ"));
        Assert.assertTrue(statusDeleted.contains("Удалена"));

        selectByValue(page.filterByStatus, "string:call_call");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        int elementsRecallInFilterCount = Integer.parseInt(findElement(By.xpath("//select/optgroup/option[1]")).getText().trim().replace("Нужно перезвонить · ",""));
        List<WebElement> elementsRecall = driver.findElements(By.xpath("//tbody/tr/td[7]"));
        Assert.assertTrue(elementsRecall.size() == elementsRecallInFilterCount);
        for (WebElement element : elementsRecall){
            Assert.assertTrue(element.getText().contains("Назначен перезвон"));
        }

        selectByValue(page.filterByStatus, "string:settlement_settlement");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        int elementsSettlementInFilterCount = Integer.parseInt(findElement(By.xpath("//select/optgroup/option[2]")).getText().trim().replace("Нужно заселить · ",""));
        List<WebElement> elementsSettlement = driver.findElements(By.xpath("//tbody/tr/td[7]"));
        Assert.assertTrue(elementsSettlement.size() == elementsSettlementInFilterCount);
        for (WebElement element : elementsSettlement){
            Assert.assertTrue(element.getText().contains("Назначено заселение"));
        }

    }

    @Test(description = "Фильтр по типу апартамента в списке заявок", priority = 2)
    public void apartmentTypeFilterOrders() {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        waitBy(page.orderNumber);

        Set<String> allTypes = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allTypes.size() > 1);

        selectByValue(page.filterByType, "string:studio");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> typeStudio = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(typeStudio.contains("Ст"));

        selectByValue(page.filterByType, "string:smallStudio");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> typeStudioSmall = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(typeStudioSmall.contains("МСт"));

        selectByValue(page.filterByType, "string:two");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> type2rooms = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(type2rooms.contains("2"));

        selectByValue(page.filterByType, "string:three");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.orderNumber);

        Set<String> type3rooms = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(type3rooms.contains("3"));

    }

    @Test(description = "Сброс фильтров в списке заявок", priority = 3)
    public void resetFiltersOrders() {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        waitBy(page.orderNumber);

        findElement(globalpage.searchInput).sendKeys("Всякая дребедень" + Keys.ENTER);
        elementIsNotVisible(globalpage.spinner,30);

        selectByValue(page.filterByStatus, "string:status_invitedToShow");
        elementIsNotVisible(globalpage.spinner,30);

        selectByValue(page.filterByType, "string:two");
        elementIsNotVisible(globalpage.spinner,30);

        click(globalpage.filterReset);
        try {
            waitBy(page.orderNumber);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент раздела 'Заявки'");
        }

        Assert.assertTrue(findElement(globalpage.searchInput).getAttribute("value").equals(""));

        Set<String> allStatuses = driver.findElements(By.xpath("//tbody/tr/td[7]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);
        //Assert.assertFalse(allStatuses.contains("Удалена"));
        // имеется баг - удаленные заявки должны выводиться только при выбранном фильтре "Удалена"
        // после исправления раскомментить

        Set<String> allTypes = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allTypes.size() > 1);

    }

}
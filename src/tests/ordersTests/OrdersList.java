package tests.ordersTests;

import framework.Configuration;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.OrdersPage;
import java.util.concurrent.TimeUnit;

public class OrdersList extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход по ссылкам в списке заявок")
    public void openLinksInOrdersList() throws InterruptedException {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        objectsSection();
        click(globalpage.ordersSection);
        ordersSection();

        click(page.listSafeClientFullName);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        driver.navigate().back();
        ordersSection();

        click(page.listSafeApartmentNumber);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

    }

    @Test(description = "Обработка заявок",priority = 1)
    public void ordersProcessing() {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        click(page.startProcessing);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        click(page.info);
        try {
            waitBy(page.infoModal);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }
        Assert.assertTrue(findElement(page.infoModal).getText().contains("На обработку одной заявки дается 12 минут"));

        click(page.closeInfoModal);
        elementIsNotVisible(page.infoModal);

        String order1 = findElement(page.orderTitle).getText();

        click(page.nextOrder);
        try {
            waitBy(globalpage.orderCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки заявки");
        }

        String order2 = findElement(page.orderTitle).getText();
        Assert.assertFalse(order1.contains(order2));

        click(page.stopProcessing);
        ordersSection();

    }

    @Test(description = "Пагинация в списке заявок", priority = 2)
    public void ordersListPagination() {

        GlobalPage globalpage = new GlobalPage();
        OrdersPage page = new OrdersPage();

        click(globalpage.objectsSection);
        click(globalpage.ordersSection);
        ordersSection();

        waitBy(globalpage.nextPage, 30);

        String page1 = new String(findElement(page.orderNumber).getAttribute("href"));

        click(globalpage.nextPage,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        String page2 = new String(findElement(page.orderNumber).getAttribute("href"));
        Assert.assertFalse(page1.equals(page2));

        click(globalpage.prevPage,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

        click(globalpage.page2,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        click(globalpage.page1,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

    }

}
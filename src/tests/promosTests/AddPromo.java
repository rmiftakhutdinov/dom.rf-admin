package tests.promosTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.PromosPage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class AddPromo extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Добавление акции без публикации")
    public void addPromoWithoutPublishing() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        objectsSection();
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        click(globalpage.add);
        try {
            waitBy(page.addPromoElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления акции");
        }

        click(page.changeLanguageCompanyNameToEng);
        sendKeys(page.companyNameEng, "Autotest company");
        click(page.changeLanguageCompanyNameToRus);
        sendKeys(page.companyNameRus,"Автотестовая компания");

        String logo = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(logo);
        click(page.addLogo);

        Robot robotLogo = new Robot();
        robotLogo.delay(250);
        robotLogo.keyPress(KeyEvent.VK_CONTROL);
        robotLogo.keyPress(KeyEvent.VK_V);
        robotLogo.keyRelease(KeyEvent.VK_V);
        robotLogo.keyRelease(KeyEvent.VK_CONTROL);
        robotLogo.keyPress(KeyEvent.VK_ENTER);
        robotLogo.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);

        sendKeys(page.companySite,"https://arenda.ahml.ru/");

        click(page.changeLanguagePromoNameToEng);
        sendKeys(page.promoNameEng, "Autotest promo without publishing");
        click(page.changeLanguagePromoNameToRus);
        sendKeys(page.promoNameRus,"Автотестовая акция без публикации");

        click(page.changeLanguagePromoDescriptionToEng);
        sendKeys(page.promoDescriptionEng, "33 Elephants - Online real estate transactions start here!");
        click(page.changeLanguagePromoDescriptionToRus);
        sendKeys(page.promoDescriptionRus,"33 Слона - онлайн сделки с недвижимостью начинаются здесь!");

        sendKeys(page.promoCode,"33SLONA-QA");
        sendKeys(page.address,"Казань, Баумана, 44/8, офис 510");

        click(page.calendar1);
        click(page.setDate1);
        String date1 = findElement(page.calendar1).getAttribute("value");

        click(page.calendar2);
        click(page.nextMonth);
        click(page.setDate2);
        String date2 = findElement(page.calendar2).getAttribute("value");

        click(By.xpath("//input[@type='checkbox']"));
        Assert.assertTrue(findElement(By.xpath("//input[@type='checkbox']")).isSelected());

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.info)).build().perform();
        String tooltip = new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(page.infoText)).getText();
        Assert.assertTrue(tooltip.equals("Акция будет автоматически снята с публикации в 00:00 на следующий день после окончания"));

        Assert.assertTrue(findElement(page.addPromoWithoutPublishing).getAttribute("class").contains("disabled"));
        Assert.assertTrue(findElement(page.addPromoWithPublishing).getAttribute("class").contains("disabled"));

        String image = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(image);
        click(page.addImage);

        Robot robotImage = new Robot();
        robotImage.delay(250);
        robotImage.keyPress(KeyEvent.VK_CONTROL);
        robotImage.keyPress(KeyEvent.VK_V);
        robotImage.keyRelease(KeyEvent.VK_V);
        robotImage.keyRelease(KeyEvent.VK_CONTROL);
        robotImage.keyPress(KeyEvent.VK_ENTER);
        robotImage.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        elementIsNotVisible(By.xpath("//button[2][contains(@class,'disabled')]"));
        elementIsNotVisible(By.xpath("//button[contains(text(),'Добавить и опубликовать')][contains(@class,'disabled')]"));

        click(page.addPromoWithoutPublishing);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        waitBy(page.statusNotPublished);

        String dateCard = findElement(page.promoPeriodCard).getText();
        Assert.assertTrue(dateCard.contains(date1));
        Assert.assertTrue(dateCard.contains(date2));

        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        String dateList = findElement(page.promoPeriodList).getText();
        Assert.assertTrue(dateCard.contains(dateList));

    }

    @Test(description = "Сверка параметров акции без публикации", priority = 1)
    public void verificationParamsPromoWithoutPublishing() {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        Assert.assertTrue(findElement(page.promoNameLst).getText().contains("Автотестовая акция без публикации"));
        Assert.assertTrue(findElement(page.companyNameList).getText().contains("Автотестовая компания"));
        Assert.assertTrue(findElement(page.statusList).getText().contains("Не опубликована"));

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.infoList)).build().perform();
        String tooltipList = new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(page.infoText)).getText().replace("\n", " ");
        Assert.assertTrue(tooltipList.contains("Для сортировки акций выберите в фильтре «Опубликована»"));

        click(page.promoNameLst);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        waitBy(page.statusNotPublished);
        Assert.assertTrue(findElement(page.objectComplex).getText().contains("Лайнер"));
        Assert.assertTrue(findElement(page.objectAddress).getText().contains("Москва, Ходынский бульвар, 2"));

        click(page.changeCompanyNameCardToEng);
        Assert.assertTrue(findElement(page.companyNameCardEng).getText().contains("Autotest company"));
        click(page.changeCompanyNameCardToRus);
        Assert.assertTrue(findElement(page.companyNameCardRus).getText().contains("Автотестовая компания"));

        String logo1 = findElement(page.logoCard).getAttribute("src");
        click(page.logoCard);
        String logo2 = findElement(page.openPhoto).getAttribute("src");
        Assert.assertTrue(logo1.equals(logo2));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        click(page.site);
        try {
            waitBy(page.siteNewPromoElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент сайта акции");
        }

        driver.navigate().back();
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        click(page.changePromoNameCardToEng);
        Assert.assertTrue(findElement(page.promoNameCardEng).getText().contains("Autotest promo without publishing"));
        click(page.changePromoNameCardToRus);
        Assert.assertTrue(findElement(page.promoNameCardRus).getText().contains("Автотестовая акция без публикации"));

        click(page.changePromoDescriptionCardToEng);
        Assert.assertTrue(findElement(page.promoDescriptionCardEng).getText().contains("33 Elephants - Online real estate transactions start here!"));
        click(page.changePromoDescriptionCardToRus);
        Assert.assertTrue(findElement(page.promoDescriptionCardRus).getText().contains("33 Слона - онлайн сделки с недвижимостью начинаются здесь!"));

        Assert.assertTrue(findElement(page.promoCodeCard).getText().contains("33SLONA-QA"));
        Assert.assertTrue(findElement(page.addressCard).getText().contains("Казань, Баумана, 44/8, офис 510"));
        Assert.assertTrue(findElement(page.promoPeriodCard).getText().contains("Акция будет снята с публикации после ее окончания"));

        actions.moveToElement(findElement(page.infoCard)).build().perform();
        String tooltipCard = new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(page.infoText)).getText().replace("\n", " ");
        Assert.assertTrue(tooltipCard.contains("Акция будет автоматически снята с публикации в 00:00 на следующий день после окончания"));

        String image1 = findElement(page.imageCard).getAttribute("src");
        click(page.imageCard);
        String image2 = findElement(page.openPhoto).getAttribute("src");
        Assert.assertTrue(image1.equals(image2));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        click(page.history);
        Assert.assertTrue(findElement(By.xpath("//div[@class='col-md-9']/ul/li[2]")).getAttribute("class").equals("active"));
        Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[3][text()='Создана акция']")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[3][text()='Установлен статус «Не опубликована»']")).isDisplayed());

    }

    @Test(description = "Добавление акции с публикацией", priority = 2)
    public void addPromoWithPublishing() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        click(globalpage.add);
        try {
            waitBy(page.addPromoElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы добавления акции");
        }

        click(page.changeLanguageCompanyNameToEng);
        sendKeys(page.companyNameEng, "Autotest company");
        click(page.changeLanguageCompanyNameToRus);
        sendKeys(page.companyNameRus,"Автотестовая компания");

        String logo = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(logo);
        click(page.addLogo);

        Robot robotLogo = new Robot();
        robotLogo.delay(250);
        robotLogo.keyPress(KeyEvent.VK_CONTROL);
        robotLogo.keyPress(KeyEvent.VK_V);
        robotLogo.keyRelease(KeyEvent.VK_V);
        robotLogo.keyRelease(KeyEvent.VK_CONTROL);
        robotLogo.keyPress(KeyEvent.VK_ENTER);
        robotLogo.keyRelease(KeyEvent.VK_ENTER);

        sleep(3000);

        sendKeys(page.companySite,"https://arenda.ahml.ru/");

        click(page.changeLanguagePromoNameToEng);
        sendKeys(page.promoNameEng, "Autotest promo with publishing");
        click(page.changeLanguagePromoNameToRus);
        sendKeys(page.promoNameRus,"Автотестовая акция с публикацией");

        click(page.changeLanguagePromoDescriptionToEng);
        sendKeys(page.promoDescriptionEng, "33 Elephants - Online real estate transactions start here!");
        click(page.changeLanguagePromoDescriptionToRus);
        sendKeys(page.promoDescriptionRus,"33 Слона - онлайн сделки с недвижимостью начинаются здесь!");

        sendKeys(page.promoCode,"33SLONA-QA");
        sendKeys(page.address,"Казань, Баумана, 44/8, офис 510");

        click(page.calendar1);
        click(page.setDate1);
        String date1 = findElement(page.calendar1).getAttribute("value");

        click(page.calendar2);
        click(page.nextMonth);
        click(page.setDate2);
        String date2 = findElement(page.calendar2).getAttribute("value");


        click(By.xpath("//input[@type='checkbox']"));
        Assert.assertTrue(findElement(By.xpath("//input[@type='checkbox']")).isSelected());

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.info)).build().perform();
        String tooltip = new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(page.infoText)).getText();
        Assert.assertTrue(tooltip.equals("Акция будет автоматически снята с публикации в 00:00 на следующий день после окончания"));

        Assert.assertTrue(findElement(page.addPromoWithoutPublishing).getAttribute("class").contains("disabled"));
        Assert.assertTrue(findElement(page.addPromoWithPublishing).getAttribute("class").contains("disabled"));

        String image = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(image);
        click(page.addImage);

        Robot robotImage = new Robot();
        robotImage.delay(250);
        robotImage.keyPress(KeyEvent.VK_CONTROL);
        robotImage.keyPress(KeyEvent.VK_V);
        robotImage.keyRelease(KeyEvent.VK_V);
        robotImage.keyRelease(KeyEvent.VK_CONTROL);
        robotImage.keyPress(KeyEvent.VK_ENTER);
        robotImage.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        elementIsNotVisible(By.xpath("//button[2][contains(@class,'disabled')]"));
        elementIsNotVisible(By.xpath("//button[contains(text(),'Добавить и опубликовать')][contains(@class,'disabled')]"));

        click(page.addPromoWithPublishing);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        waitBy(page.statusPublished);

        String dateCard = findElement(page.promoPeriodCard).getText();
        Assert.assertTrue(dateCard.contains(date1));
        Assert.assertTrue(dateCard.contains(date2));

        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        String dateList = findElement(page.promoPeriodList).getText();
        Assert.assertTrue(dateCard.contains(dateList));

    }

    @Test(description = "Сверка параметров акции с публикацией", priority = 3)
    public void verificationParamsPromoWithPublishing() {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        Assert.assertTrue(findElement(page.promoNameLst).getText().contains("Автотестовая акция с публикацией"));
        Assert.assertTrue(findElement(page.companyNameList).getText().contains("Автотестовая компания"));
        Assert.assertTrue(findElement(page.statusList).getText().contains("Опубликована"));

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.infoList)).build().perform();
        String tooltipList = new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(page.infoText)).getText().replace("\n", " ");
        Assert.assertTrue(tooltipList.contains("Для сортировки акций выберите в фильтре «Опубликована»"));

        click(page.promoNameLst);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        waitBy(page.statusPublished);
        Assert.assertTrue(findElement(page.objectComplex).getText().contains("Лайнер"));
        Assert.assertTrue(findElement(page.objectAddress).getText().contains("Москва, Ходынский бульвар, 2"));

        click(page.changeCompanyNameCardToEng);
        Assert.assertTrue(findElement(page.companyNameCardEng).getText().contains("Autotest company"));
        click(page.changeCompanyNameCardToRus);
        Assert.assertTrue(findElement(page.companyNameCardRus).getText().contains("Автотестовая компания"));

        String logo1 = findElement(page.logoCard).getAttribute("src");
        click(page.logoCard);
        String logo2 = findElement(page.openPhoto).getAttribute("src");
        Assert.assertTrue(logo1.equals(logo2));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        click(page.site);
        try {
            waitBy(page.siteNewPromoElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент сайта акции");
        }

        driver.navigate().back();
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        click(page.changePromoNameCardToEng);
        Assert.assertTrue(findElement(page.promoNameCardEng).getText().contains("Autotest promo with publishing"));
        click(page.changePromoNameCardToRus);
        Assert.assertTrue(findElement(page.promoNameCardRus).getText().contains("Автотестовая акция с публикацией"));

        click(page.changePromoDescriptionCardToEng);
        Assert.assertTrue(findElement(page.promoDescriptionCardEng).getText().contains("33 Elephants - Online real estate transactions start here!"));
        click(page.changePromoDescriptionCardToRus);
        Assert.assertTrue(findElement(page.promoDescriptionCardRus).getText().contains("33 Слона - онлайн сделки с недвижимостью начинаются здесь!"));

        Assert.assertTrue(findElement(page.promoCodeCard).getText().contains("33SLONA-QA"));
        Assert.assertTrue(findElement(page.addressCard).getText().contains("Казань, Баумана, 44/8, офис 510"));
        Assert.assertTrue(findElement(page.promoPeriodCard).getText().contains("Акция будет снята с публикации после ее окончания"));

        actions.moveToElement(findElement(page.infoCard)).build().perform();
        String tooltipCard = new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(page.infoText)).getText().replace("\n", " ");
        Assert.assertTrue(tooltipCard.contains("Акция будет автоматически снята с публикации в 00:00 на следующий день после окончания"));

        String image1 = findElement(page.imageCard).getAttribute("src");
        click(page.imageCard);
        String image2 = findElement(page.openPhoto).getAttribute("src");
        Assert.assertTrue(image1.equals(image2));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        click(page.history);
        Assert.assertTrue(findElement(By.xpath("//div[@class='col-md-9']/ul/li[2]")).getAttribute("class").equals("active"));
        Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[3][text()='Создана акция']")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[3][text()='Установлен статус «Опубликована»']")).isDisplayed());

    }

}
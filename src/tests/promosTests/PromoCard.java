package tests.promosTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.PromosPage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class PromoCard extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Смена статуса акции")
    public void changePromoStatus() {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        objectsSection();
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(page.promoNameLst);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        click(page.notPublish);
        try {
            waitBy(page.toastPublication);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        waitBy(page.toastPublication);
        waitBy(page.statusNotPublished);

        click(page.history);
        Assert.assertTrue(findElement(By.xpath("//div[@class='col-md-9']/ul/li[2]")).getAttribute("class").equals("active"));
        Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[3][text()='Установлен статус «Не опубликована»']")).isDisplayed());

        click(page.publish);
        try {
            waitBy(page.toastPublication);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста о смене статуса");
        }

        waitBy(page.toastPublication);
        waitBy(page.statusPublished);

        Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[3][text()='Установлен статус «Опубликована»']")).isDisplayed());

        click(page.options);
        Assert.assertTrue(findElement(By.xpath("//div[@class='col-md-9']/ul/li[1]")).getAttribute("class").equals("active"));

    }

    @Test(description = "Редактирование акции", priority = 1)
    public void editPromo() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(page.promoNameLst);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        click(page.edit);
        try {
            waitBy(page.editElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент страницы редактирования акции");
        }

        click(page.changeLanguageCompanyNameToEng);
        findElement(page.companyNameEng).clear();
        sendKeys(page.companyNameEng, "Autotest edit company");
        click(page.changeLanguageCompanyNameToRus);
        findElement(page.companyNameRus).clear();
        sendKeys(page.companyNameRus,"Автотестовая отредактированная компания");

        click(page.deleteLogo);
        elementIsNotVisible(page.deleteLogo);

        String logo = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(logo);
        click(page.addLogo);

        Robot robotLogo = new Robot();
        robotLogo.delay(250);
        robotLogo.keyPress(KeyEvent.VK_CONTROL);
        robotLogo.keyPress(KeyEvent.VK_V);
        robotLogo.keyRelease(KeyEvent.VK_V);
        robotLogo.keyRelease(KeyEvent.VK_CONTROL);
        robotLogo.keyPress(KeyEvent.VK_ENTER);
        robotLogo.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        elementIsNotVisible(By.xpath("//button[contains(text(),'Сохранить')][contains(@class,'disabled')]"));

        findElement(page.companySite).clear();
        sendKeys(page.companySite,"https://arenda.ahml.ru/renter");

        click(page.changeLanguagePromoNameToEng);
        findElement(page.promoNameEng).clear();
        sendKeys(page.promoNameEng, "Autotest edit promo");
        click(page.changeLanguagePromoNameToRus);
        findElement(page.promoNameRus).clear();
        sendKeys(page.promoNameRus,"Автотестовая отредактированная акция");

        click(page.changeLanguagePromoDescriptionToEng);
        findElement(page.promoDescriptionEng).clear();
        sendKeys(page.promoDescriptionEng, "33 Elephants - edited!");
        click(page.changeLanguagePromoDescriptionToRus);
        findElement(page.promoDescriptionRus).clear();
        sendKeys(page.promoDescriptionRus,"33 Слона - отредактировано!");

        findElement(page.promoCode).clear();
        sendKeys(page.promoCode,"33SLONA-QA - отредактировано");
        findElement(page.address).clear();
        sendKeys(page.address,"Казань, Баумана, 44/8, офис 510 - отредактировано!");

        click(page.calendar1);
        click(page.setDate1);
        String date1 = findElement(page.calendar1).getAttribute("value");

        click(page.calendar2);
        click(page.nextMonth);
        click(page.setDate2);
        String date2 = findElement(page.calendar2).getAttribute("value");

        click(By.xpath("//input[@type='checkbox']"));
        Assert.assertFalse(findElement(By.xpath("//input[@type='checkbox']")).isSelected());

        Assert.assertFalse(findElement(page.savePromo).getAttribute("class").contains("disabled"));

        click(page.deleteImage);
        elementIsNotVisible(page.deleteImage);

        String image = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(image);
        click(page.addImage);

        Robot robotImage = new Robot();
        robotImage.delay(250);
        robotImage.keyPress(KeyEvent.VK_CONTROL);
        robotImage.keyPress(KeyEvent.VK_V);
        robotImage.keyRelease(KeyEvent.VK_V);
        robotImage.keyRelease(KeyEvent.VK_CONTROL);
        robotImage.keyPress(KeyEvent.VK_ENTER);
        robotImage.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        elementIsNotVisible(By.xpath("//button[contains(text(),'Сохранить')][contains(@class,'disabled')]"));

        click(page.savePromo);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        String dateCard = findElement(page.promoPeriodCard).getText();
        Assert.assertTrue(dateCard.contains(date1));
        Assert.assertTrue(dateCard.contains(date2));

        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        String dateList = findElement(page.promoPeriodList).getText();
        Assert.assertTrue(dateCard.contains(dateList));

    }

    @Test(description = "Сверка параметров отредактированной акции", priority = 2)
    public void verificationParamsEditPromo() {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        Assert.assertTrue(findElement(page.promoNameLst).getText().contains("Автотестовая отредактированная акция"));
        Assert.assertTrue(findElement(page.companyNameList).getText().contains("Автотестовая отредактированная компания"));
        Assert.assertTrue(findElement(page.statusList).getText().contains("Опубликована"));

        click(page.promoNameLst);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        Assert.assertTrue(findElement(page.objectComplex).getText().contains("Лайнер"));
        Assert.assertTrue(findElement(page.objectAddress).getText().contains("Москва, Ходынский бульвар, 2"));

        click(page.changeCompanyNameCardToEng);
        Assert.assertTrue(findElement(page.companyNameCardEng).getText().contains("Autotest edit company"));
        click(page.changeCompanyNameCardToRus);
        Assert.assertTrue(findElement(page.companyNameCardRus).getText().contains("Автотестовая отредактированная компания"));

        String logo1 = findElement(page.logoCard).getAttribute("src");
        click(page.logoCard);
        String logo2 = findElement(page.openPhoto).getAttribute("src");
        Assert.assertTrue(logo1.equals(logo2));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        click(page.site);
        try {
            waitBy(page.siteEditPromoElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент сайта акции");
        }

        driver.get("http://dev-arenda.ahml.ru/admin/special-list");
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        click(page.promoNameLst);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        click(page.changePromoNameCardToEng);
        Assert.assertTrue(findElement(page.promoNameCardEng).getText().contains("Autotest edit promo"));
        click(page.changePromoNameCardToRus);
        Assert.assertTrue(findElement(page.promoNameCardRus).getText().contains("Автотестовая отредактированная акция"));

        click(page.changePromoDescriptionCardToEng);
        Assert.assertTrue(findElement(page.promoDescriptionCardEng).getText().contains("33 Elephants - edited!"));
        click(page.changePromoDescriptionCardToRus);
        Assert.assertTrue(findElement(page.promoDescriptionCardRus).getText().contains("33 Слона - отредактировано!"));

        Assert.assertTrue(findElement(page.promoCodeCard).getText().contains("33SLONA-QA - отредактировано"));
        Assert.assertTrue(findElement(page.addressCard).getText().contains("Казань, Баумана, 44/8, офис 510 - отредактировано!"));
        Assert.assertFalse(findElement(page.promoPeriodCard).getText().contains("Акция будет снята с публикации после ее окончания"));

        String image1 = findElement(page.imageCard).getAttribute("src");
        click(page.imageCard);
        String image2 = findElement(page.openPhoto).getAttribute("src");
        Assert.assertTrue(image1.equals(image2));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        click(page.history);
        Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[3][text()='Установлен статус «Не опубликована»']")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//tbody/tr/td[3][contains(text(),'Период акции')]")).isDisplayed());

    }

    @Test(description = "Удаление опубликованной акции", priority = 3)
    public void deletePublishPromo() {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        Assert.assertTrue(findElement(page.promoNameLst).getText().contains("Автотестовая отредактированная акция"));
        Assert.assertTrue(findElement(page.companyNameList).getText().contains("Автотестовая отредактированная компания"));
        Assert.assertTrue(findElement(page.statusList).getText().contains("Опубликована"));

        click(page.promoNameLst);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        try {
            waitBy(page.statusPublished);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Статус акции неверный");
        }

        click(page.deletePromo);
        try {
            waitBy(page.modalDeleteCancel);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления акции");
        }

        click(page.modalDeleteCancel);
        elementIsNotVisible(page.modalDeleteCancel);

        click(page.deletePromo);
        try {
            waitBy(page.modalDeleteConfirm);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления акции");
        }

        click(page.modalDeleteConfirm);
        try {
            waitBy(page.toastDeleted);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста об удалении акции");
        }

        promosSection();

    }

    @Test(description = "Удаление не опубликованной акции", priority = 4)
    public void deleteNotPublishPromo() {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        Assert.assertTrue(findElement(page.promoNameLst).getText().contains("Автотестовая акция без публикации"));
        Assert.assertTrue(findElement(page.companyNameList).getText().contains("Автотестовая компания"));
        Assert.assertTrue(findElement(page.statusList).getText().contains("Не опубликована"));

        click(page.promoNameLst);
        try {
            waitBy(page.promoCardElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент карточки акции");
        }

        try {
            waitBy(page.statusNotPublished);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Статус акции неверный");
        }

        click(page.deletePromo);
        try {
            waitBy(page.modalDeleteCancel);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления акции");
        }

        click(page.modalDeleteCancel);
        elementIsNotVisible(page.modalDeleteCancel);

        click(page.deletePromo);
        try {
            waitBy(page.modalDeleteConfirm);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент модального окна удаления акции");
        }

        click(page.modalDeleteConfirm);
        try {
            waitBy(page.toastDeleted);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста об удалении акции");
        }

        promosSection();

    }

}
package tests.promosTests;

import framework.Configuration;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.PromosPage;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class PromosFilters extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();


        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Строка поиска в списке акций")
    public void searchLinePromos() {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        objectsSection();
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        sendKeys(globalpage.searchInput, "Акция для автотестирования" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.promoNameLst).getText().equals("Акция для автотестирования"));

        sendKeys(globalpage.searchInput, "НЕ УДАЛЯТЬ!!!" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.companyNameList).getText().equals("НЕ УДАЛЯТЬ!!!"));

    }

    @Test(description = "Фильтр по статусам в списке акций", priority = 1)
    public void statusFilterPromos() {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        waitBy(page.promoNameLst);

        Set<String> allStatuses = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);
        Assert.assertFalse(allStatuses.contains("Удалена"));

        selectByValue(page.filterStatus, "string:publish");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.promoNameLst);

        Set<String> statusPublish = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(statusPublish.contains("Опубликована"));
        Assert.assertFalse(statusPublish.contains("Не опубликована"));
        Assert.assertFalse(statusPublish.contains("Удалена"));

        selectByValue(page.filterStatus, "string:unpublish");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.promoNameLst);

        Set<String> statusUnpublish = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusUnpublish.contains("Опубликована"));
        Assert.assertTrue(statusUnpublish.contains("Не опубликована"));
        Assert.assertFalse(statusUnpublish.contains("Удалена"));

        selectByValue(page.filterStatus, "string:delete");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.promoNameLst);

        Set<String> statusDelete = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertFalse(statusDelete.contains("Опубликована"));
        Assert.assertFalse(statusDelete.contains("Не опубликована"));
        Assert.assertTrue(statusDelete.contains("Удалена"));

    }

    @Test(description = "Сброс фильтров в списке акций", priority = 2)
    public void resetFiltersPromos() {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        click(globalpage.objectsSection);
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        elementIsNotVisible(globalpage.spinner);

        findElement(globalpage.searchInput).sendKeys("Всякая дребедень" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        selectByValue(page.filterStatus, "string:publish");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        click(globalpage.filterReset);
        try {
            waitBy(page.promoNameLst);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент акции в списке");
        }

        Assert.assertTrue(findElement(globalpage.searchInput).getAttribute("value").equals(""));

        Set<String> allStatuses = driver.findElements(By.xpath("//tbody/tr/td[4]/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        Assert.assertTrue(allStatuses.size() > 1);
        Assert.assertFalse(allStatuses.contains("Удалена"));

    }

}
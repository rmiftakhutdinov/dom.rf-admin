package tests.promosTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.PromosPage;
import java.util.concurrent.TimeUnit;

public class SortingPromos extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getFirefoxDriver(Configuration.os);
        driver.manage().window().maximize();


        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Сортировка акций")
    public void sortingPromos() {

        GlobalPage globalpage = new GlobalPage();
        PromosPage page = new PromosPage();

        objectsSection();
        click(globalpage.dropdownMenuMore);
        click(globalpage.promosSection);
        promosSection();

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));

        selectByValue(page.filterStatus, "string:publish");
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        waitBy(page.promoNameLst);

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.sort)).build().perform();
        String tooltipSort = new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(page.infoText)).getText().replace("\n", " ");
        Assert.assertTrue(tooltipSort.contains("Потяните для сортировки"));

        WebElement sortAt = driver.findElement(By.xpath("//tbody/tr[1]/td[5]/span"));
        WebElement sortTo = driver.findElement(By.xpath("//tbody/tr[5]/td[5]/span"));

        actions.clickAndHold(sortAt).moveToElement(sortTo).release(sortTo).build().perform();

        try {
            waitBy(page.toastSorting);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент тоста об изменении порядка сортирорвки акций");
        }

    }

}
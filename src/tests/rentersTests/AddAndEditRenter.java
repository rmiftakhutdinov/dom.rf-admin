package tests.rentersTests;

import framework.Configuration;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.RentersPage;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class AddAndEditRenter extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown() {

        driver.quit();

    }

    @Test(description = "Добавление арендатора")
    public void addRenter() throws InterruptedException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        objectsSection();
        click(globalpage.rentersSection);
        rentersSection();

        click(globalpage.add);
        try {
            waitBy(globalpage.addNewRenterElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы добавления арендатора");
        }

        click(page.checkboxInvite);
        new WebDriverWait(driver, 30).until(ExpectedConditions.elementSelectionStateToBe(By.xpath("//input[@type='checkbox']"), true));

        sendKeys(globalpage.renterFamily, "Автотестовый");
        sendKeys(globalpage.renterName, "Арендатор");
        sendKeys(globalpage.renterPatronymic, "Объектович");
        sendKeys(globalpage.renterBornedAtDay, "12");
        sendKeys(globalpage.renterBornedAtMonth, "12");
        sendKeys(globalpage.renterBornedAtYear, "1980");
        sendKeys(globalpage.renterPhone, "0987654321");
        sendKeys(globalpage.renterEmail, "test@email.com");
        sendKeys(globalpage.renterPassportNumber, "1234 ABCD");
        sleep(500);
        sendKeys(globalpage.renterPassportIssued, "Советом Народных Комиссаров СССР");
        sendKeys(globalpage.renterPassportIssuedDay, "01");
        sendKeys(globalpage.renterPassportIssuedMonth, "05");
        sendKeys(globalpage.renterPassportIssuedYear, "1990");
        sendKeys(globalpage.renterPassportCode, "код-код");
        sendKeys(globalpage.renterAddress, "адрес регистрации арендатора");
        sendKeys(globalpage.renterBankTitle, "Банк");
        sendKeys(globalpage.renterBankAccount, "Счет");
        sendKeys(globalpage.renterBankCorrAccountId, "Корр.счет");
        sendKeys(globalpage.renterBankCorrespTitle, "Банк-корреспондент");
        sendKeys(globalpage.renterBankBIK, "БИК");

        String jpg = System.getProperty("user.dir") + "\\files\\images\\JPG.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String png = System.getProperty("user.dir") + "\\files\\images\\PNG.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String pdf = System.getProperty("user.dir") + "\\files\\images\\PDF.pdf";
        setClipboardData(pdf);
        click(page.addPhoto);

        Robot robotPDF = new Robot();
        robotPDF.delay(250);
        robotPDF.keyPress(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_V);
        robotPDF.keyRelease(KeyEvent.VK_CONTROL);
        robotPDF.keyPress(KeyEvent.VK_ENTER);
        robotPDF.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        String phone = randomString("9876543210", 10);

        for (; ; ) {
            click(globalpage.submit);
            if (elementIsNotPresent(globalpage.errorRenterPhone)) {
                break;
            } else {
                findElement(globalpage.renterPhone).clear();
                sendKeys(globalpage.renterPhone, phone);
            }
            phone = findElement(globalpage.renterPhone).getAttribute("value");
        }

        String txtRenterPhone = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterPhone.txt";
        File fileRenterPhone = new File(txtRenterPhone);

        try {
            FileWriter writer = new FileWriter(fileRenterPhone);
            writer.write(phone);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        String email = randomString("abcdefghijklmnopqrstuvwxyz", 7) + "@email.com";

        for (; ; ) {
            click(globalpage.submit);
            if (elementIsNotPresent(globalpage.errorRenterEmail)) {
                break;
            } else {
                findElement(globalpage.renterEmail).clear();
                sendKeys(globalpage.renterEmail, email);
            }
        }

        String txtRenterEmail = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterEmail.txt";
        File fileRenterEmail = new File(txtRenterEmail);

        try {
            FileWriter writer = new FileWriter(fileRenterEmail);
            writer.write(email);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        String passportNumber = randomString("1234567890", 4) + " " + randomString("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 4);

        for (; ; ) {
            click(globalpage.submit);
            if (elementIsNotPresent(globalpage.errorRenterPassportNumber)) {
                break;
            } else {
                findElement(globalpage.renterPassportNumber).clear();
                sendKeys(globalpage.renterPassportNumber, passportNumber);
            }
        }

        String txtRenterPassportNumber = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterPassportNumber.txt";
        File fileRenterPassportNumber = new File(txtRenterPassportNumber);

        try {
            FileWriter writer = new FileWriter(fileRenterPassportNumber);
            writer.write(passportNumber);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            waitBy(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки арендатора");
        }

        String txtRenterID = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterID.txt";
        File fileRenterId = new File(txtRenterID);

        String renterID = findElement(By.xpath("//h1")).getText().replace("Арендатор №", "").trim();

        try {
            FileWriter writer = new FileWriter(fileRenterId);
            writer.write(renterID);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    @Test(description = "Сверка параметров добавленного арендатора", priority = 1)
    public void verificationParamsNewRenter() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        click(globalpage.objectsSection);
        click(globalpage.rentersSection);
        rentersSection();

        // Чтение ID арендатора
        String txtRenterID = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterID.txt";
        File fileRenterId = new File(txtRenterID);
        Scanner scannerRenterId = new Scanner(fileRenterId);
        String renterID = scannerRenterId.nextLine();

        // Чтение номера телефона арендатора
        String txtRenterPhone = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterPhone.txt";
        File fileRenterPhone = new File(txtRenterPhone);
        Scanner scannerRenterPhone = new Scanner(fileRenterPhone);
        String renterPhone = scannerRenterPhone.nextLine();

        // Чтение эл.почты арендатора
        String txtRenterEmail = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterEmail.txt";
        File fileRenterEmail = new File(txtRenterEmail);
        Scanner scannerRenterEmail = new Scanner(fileRenterEmail);
        String renterEmail = scannerRenterEmail.nextLine();

        // Чтение номера паспорта арендатора
        String txtRenterPassportNumber = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterPassportNumber.txt";
        File fileRenterPassportNumber = new File(txtRenterPassportNumber);
        Scanner scannerPassportNumber = new Scanner(fileRenterPassportNumber);
        String renterPassportNumber = scannerPassportNumber.nextLine();

        findElement(globalpage.searchInput).sendKeys(renterID + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner, 30);

        waitBy(page.listRenterId);

        boolean renter = true;

        List<WebElement> elements = driver.findElements(page.listRenterId);
        for (WebElement element : elements) {

            if (element.getText().equals(renterID)) {

                Assert.assertTrue(findElement(page.listSafeRenterFullName).getText().contains("Автотестовый Арендатор Объектович"));
                Assert.assertTrue(findElement(page.listSafeRenterPhone).getText().contains(renterPhone));

                click(globalpage.searchNumber(renterID), 30);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки арендатора");
                }

                renter = false;
                break;

            }

        }

        if (renter) {
            afterExeptionMessage("ID добавленного арендатора не найден");
        }

        Assert.assertTrue(findElement(page.cardRenterFullName).getText().contains("Автотестовый Арендатор Объектович"));
        Assert.assertTrue(findElement(page.cardRenterBornedAtDate).getText().contains("12 дек 1980"));
        Assert.assertTrue(findElement(page.cardRenterPhone).getText().contains(renterPhone));
        Assert.assertTrue(findElement(page.cardRenterEmail).getText().contains(renterEmail));
        Assert.assertTrue(findElement(page.cardRenterPassportData).getText().contains(renterPassportNumber));
        Assert.assertTrue(findElement(page.cardRenterPassportData).getText().contains("выдан Советом Народных Комиссаров СССР"));
        Assert.assertTrue(findElement(page.cardRenterPassportData).getText().contains("01.05.1990, код-код"));
        Assert.assertTrue(findElement(page.cardRenterAddress).getText().contains("адрес регистрации арендатора"));
        Assert.assertTrue(findElement(page.cardRenterBankBIK).getText().contains("БИК"));
        Assert.assertTrue(findElement(page.cardRenterBankCorrAccountId).getText().contains("Корр.счет"));
        Assert.assertTrue(findElement(page.cardRenterBankTitle).getText().contains("Банк"));
        Assert.assertTrue(findElement(page.cardRenterBankAccount).getText().contains("Счет"));
        Assert.assertTrue(findElement(page.cardRenterBankCorrespTitle).getText().contains("Банк-корреспондент"));

        click(page.documents);
        List<WebElement> images = driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 2);
        Assert.assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

        click(page.history);
        Assert.assertTrue(findElement(page.historyStatusNewRenter).getText().contains("Создан арендатор"));

    }

    @Test(description = "Редактирование арендатора", priority = 2)
    public void editRenter() throws InterruptedException, FileNotFoundException, AWTException {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        click(globalpage.objectsSection);
        click(globalpage.rentersSection);
        rentersSection();

        String txtRenterID = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterID.txt";
        File fileRenterId = new File(txtRenterID);
        Scanner scannerRenterId = new Scanner(fileRenterId);
        String renterID = scannerRenterId.nextLine();

        findElement(globalpage.searchInput).sendKeys(renterID + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner, 30);

        waitBy(page.listRenterId);

        boolean renter = true;

        List<WebElement> elements = driver.findElements(page.listRenterId);
        for (WebElement element : elements) {

            if (element.getText().equals(renterID)) {

                click(globalpage.searchNumber(renterID), 30);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки арендатора");
                }

                renter = false;
                break;

            }

        }

        if (renter) {
            afterExeptionMessage("ID добавленного арендатора не найден");
        }

        click(page.editRenter);
        try {
            waitBy(page.editRenterElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы редактирования арендатора");
        }

        sendKeys(globalpage.renterFamily, "Отредактированный");
        sendKeys(globalpage.renterName, "Клиент");
        sendKeys(globalpage.renterPatronymic, "Автотестович");
        sendKeys(globalpage.renterBornedAtDay, "11");
        sendKeys(globalpage.renterBornedAtMonth, "11");
        sendKeys(globalpage.renterBornedAtYear, "1999");
        findElement(globalpage.renterPhone).clear();
        sendKeys(globalpage.renterPhone, "0987654321");
        sendKeys(globalpage.renterEmail, "test@email.com");
        sendKeys(globalpage.renterPassportNumber, "1234 ABCD");
        sleep(500);
        sendKeys(globalpage.renterPassportIssued, "Генералами песчаных карьеров");
        sendKeys(globalpage.renterPassportIssuedDay, "12");
        sendKeys(globalpage.renterPassportIssuedMonth, "12");
        sendKeys(globalpage.renterPassportIssuedYear, "2012");
        sendKeys(globalpage.renterPassportCode, "хот-дог");
        sendKeys(globalpage.renterAddress, "улица Кучера, дом Петуха, квартира Цыпленка, 4 звонка");
        sendKeys(globalpage.renterBankTitle, "Банк изменен");
        sendKeys(globalpage.renterBankAccount, "Счет изменен");
        sendKeys(globalpage.renterBankCorrAccountId, "Корр.счет изменен");
        sendKeys(globalpage.renterBankCorrespTitle, "Банк-корреспондент изменен");
        sendKeys(globalpage.renterBankBIK, "БИК изменен");

        List<WebElement> photos = driver.findElements(By.xpath("//div[@class='form-file-preview ng-scope']/div[@class='item-preview col-sm-3 ng-scope']"));
        WebElement photoPDF = photos.get(0).findElement(By.xpath("//div[contains(@class,'item-preview-inner')]/a"));

        if(photoPDF != null){
            click(page.editDeletePhoto2);
        } else {
            click(page.editDeletePhoto1);
        }

        String jpg = System.getProperty("user.dir") + "\\files\\images\\111.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(2000);

        String png = System.getProperty("user.dir") + "\\files\\images\\222.png";
        setClipboardData(png);
        click(page.addPhoto);

        Robot robotPNG = new Robot();
        robotPNG.delay(250);
        robotPNG.keyPress(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_V);
        robotPNG.keyRelease(KeyEvent.VK_CONTROL);
        robotPNG.keyPress(KeyEvent.VK_ENTER);
        robotPNG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        String phone = randomString("9876543210", 10);

        for (; ; ) {
            click(globalpage.submit);
            if (elementIsNotPresent(globalpage.errorRenterPhone)) {
                break;
            } else {
                findElement(globalpage.renterPhone).clear();
                sendKeys(globalpage.renterPhone, phone);
            }
            phone = findElement(globalpage.renterPhone).getAttribute("value");
        }

        String txtRenterPhone = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterPhone.txt";
        File fileRenterPhone = new File(txtRenterPhone);

        try {
            FileWriter writer = new FileWriter(fileRenterPhone);
            writer.write(phone);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        String email = randomString("abcdefghijklmnopqrstuvwxyz", 7) + "@email.com";

        for (; ; ) {
            click(globalpage.submit);
            if (elementIsNotPresent(globalpage.errorRenterEmail)) {
                break;
            } else {
                findElement(globalpage.renterEmail).clear();
                sendKeys(globalpage.renterEmail, email);
            }
        }

        String txtRenterEmail = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterEmail.txt";
        File fileRenterEmail = new File(txtRenterEmail);

        try {
            FileWriter writer = new FileWriter(fileRenterEmail);
            writer.write(email);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        String passportNumber = randomString("1234567890", 4) + " " + randomString("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 4);

        for (; ; ) {
            click(globalpage.submit);
            if (elementIsNotPresent(page.editErrorRenterPassportNumber)) {
                break;
            } else {
                findElement(globalpage.renterPassportNumber).clear();
                sendKeys(globalpage.renterPassportNumber, passportNumber);
            }
        }

        String txtRenterPassportNumber = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterPassportNumber.txt";
        File fileRenterPassportNumber = new File(txtRenterPassportNumber);

        try {
            FileWriter writer = new FileWriter(fileRenterPassportNumber);
            writer.write(passportNumber);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            waitBy(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки арендатора");
        }

    }

    @Test(description = "Сверка параметров отредактированного арендатора", priority = 3)
    public void verificationParamsEditRenter() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        click(globalpage.objectsSection);
        click(globalpage.rentersSection);
        rentersSection();

        String txtRenterID = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterID.txt";
        File fileRenterId = new File(txtRenterID);
        Scanner scannerRenterId = new Scanner(fileRenterId);
        String renterID = scannerRenterId.nextLine();

        String txtRenterPhone = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterPhone.txt";
        File fileRenterPhone = new File(txtRenterPhone);
        Scanner scannerRenterPhone = new Scanner(fileRenterPhone);
        String renterPhone = scannerRenterPhone.nextLine();

        String txtRenterEmail = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterEmail.txt";
        File fileRenterEmail = new File(txtRenterEmail);
        Scanner scannerRenterEmail = new Scanner(fileRenterEmail);
        String renterEmail = scannerRenterEmail.nextLine();

        String txtRenterPassportNumber = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterPassportNumber.txt";
        File fileRenterPassportNumber = new File(txtRenterPassportNumber);
        Scanner scannerPassportNumber = new Scanner(fileRenterPassportNumber);
        String renterPassportNumber = scannerPassportNumber.nextLine();

        findElement(globalpage.searchInput).sendKeys(renterID + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner, 30);

        waitBy(page.listRenterId);

        boolean renter = true;

        List<WebElement> elements = driver.findElements(page.listRenterId);
        for (WebElement element : elements) {

            if (element.getText().equals(renterID)) {

                Assert.assertTrue(findElement(page.listSafeRenterFullName).getText().contains("Отредактированный Клиент Автотестович"));
                Assert.assertTrue(findElement(page.listSafeRenterPhone).getText().contains(renterPhone));

                click(globalpage.searchNumber(renterID), 30);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки арендатора");
                }

                renter = false;
                break;

            }

        }

        if (renter) {
            afterExeptionMessage("ID добавленного арендатора не найден");
        }

        Assert.assertTrue(findElement(page.cardRenterFullName).getText().contains("Отредактированный Клиент Автотестович"));
        Assert.assertTrue(findElement(page.cardRenterBornedAtDate).getText().contains("11 ноя 1999"));
        Assert.assertTrue(findElement(page.cardRenterPhone).getText().contains(renterPhone));
        Assert.assertTrue(findElement(page.cardRenterEmail).getText().contains(renterEmail));
        Assert.assertTrue(findElement(page.cardRenterPassportData).getText().contains(renterPassportNumber));
        Assert.assertTrue(findElement(page.cardRenterPassportData).getText().contains("выдан Генералами песчаных карьеров"));
        Assert.assertTrue(findElement(page.cardRenterPassportData).getText().contains("12.12.2012, хот-дог"));
        Assert.assertTrue(findElement(page.cardRenterAddress).getText().contains("улица Кучера, дом Петуха, квартира Цыпленка, 4 звонка"));
        Assert.assertTrue(findElement(page.cardRenterBankBIK).getText().contains("БИК изменен"));
        Assert.assertTrue(findElement(page.cardRenterBankCorrAccountId).getText().contains("Корр.счет изменен"));
        Assert.assertTrue(findElement(page.cardRenterBankTitle).getText().contains("Банк изменен"));
        Assert.assertTrue(findElement(page.cardRenterBankAccount).getText().contains("Счет изменен"));
        Assert.assertTrue(findElement(page.cardRenterBankCorrespTitle).getText().contains("Банк-корреспондент изменен"));

        Assert.assertTrue(findElement(page.noApartmentsElement).getText().contains("Нет апартаментов"));
        Assert.assertTrue(findElement(page.noParkingElement).getText().contains("Нет машиномест"));

        click(page.documents);
        List<WebElement> images = driver.findElements(By.xpath("//div/img"));
        Assert.assertTrue(images.size() == 3);
        Assert.assertTrue(findElement(By.xpath("//span[text()='pdf']")).isDisplayed());

    }

}
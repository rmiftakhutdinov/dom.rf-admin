package tests.rentersTests;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.RentersPage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class RenterCard extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход по ссылкам в карточке арендатора")
    public void openLinksInRenterCard() throws InterruptedException {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        objectsSection();
        click(globalpage.rentersSection);
        rentersSection();

        waitBy(page.listRenterId);

        findElement(globalpage.searchInput).sendKeys("202" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.listRenterId);

        boolean renterID = true;

        List<WebElement> elements = driver.findElements(page.listRenterId);
        for (WebElement element : elements) {

            if (element.getText().equals("202")) {

                click(globalpage.searchNumber("202"), 30);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки арендатора");
                }

                renterID = false;
                break;

            }

        }

        if (renterID) {
            afterExeptionMessage("Номер арендатора не найден");
        }

        boolean statusApartment = true;

        List<WebElement> apartments = driver.findElements(By.xpath("//div[@class='panel-body']/div[2]/div[1]"));
        for (WebElement apartment : apartments) {

            if (apartment.getText().contains("ап. 250")) {

                String handleBefore = driver.getWindowHandle();

                click(By.xpath("//span[text()='Снят']/ancestor::p/a"));
                sleep(3000);

                for (String winHandle : driver.getWindowHandles()) {
                    driver.switchTo().window(winHandle);
                }

                try {
                    waitBy(globalpage.apartmentCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки апартамента");
                }

                driver.close();
                driver.switchTo().window(handleBefore);

                statusApartment = false;
                break;

            }

        }

        if (statusApartment) {
            afterExeptionMessage("Номер арендованного апартамента не найден");
        }

        click(page.documents);
        Assert.assertTrue(findElement(page.documents).getAttribute("class").equals("active"));
        click(page.photo);
        try {
            waitBy(page.openPhoto);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент открытого в галерее фото не найден");
        }

        String photo1 = new String(findElement(page.openPhoto).getAttribute("src"));
        findElement(page.nextPhoto).click();
        sleep(1000);
        String photo2 = new String(findElement(page.openPhoto).getAttribute("src"));
        Assert.assertFalse(photo1.equals(photo2));
        findElement(page.prevPhoto).click();
        sleep(1000);
        Assert.assertFalse(photo2.equals(photo1));
        findElement(By.xpath("//body")).sendKeys(Keys.ESCAPE);

        click(page.history);
        Assert.assertTrue(findElement(page.history).getAttribute("class").equals("active"));

        boolean historyStatusApartment = true;

        List<WebElement> historyStatusesApartment = driver.findElements(By.xpath("//tbody/tr/td[3]/a[contains(text(),'250')]"));
        for (WebElement element : historyStatusesApartment){

            if (element.getAttribute("href").contains("/admin/realty-card/apartment/532?n=250")){

                String handleBefore = driver.getWindowHandle();

                click(By.xpath("//tbody/tr/td[3]/a[contains(text(),'250, Москва, Ходынский бульвар, 2, Лайнер')]"));
                sleep(3000);

                for (String winHandle : driver.getWindowHandles()) {
                    driver.switchTo().window(winHandle);
                }

                try {
                    waitBy(globalpage.apartmentCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки апартамента");
                }

                driver.close();
                driver.switchTo().window(handleBefore);

                historyStatusApartment = false;
                break;

            }

        }

        if (historyStatusApartment) {
            afterExeptionMessage("Ссылка на заселенный апартамент не найдена");
        }

        boolean historyStatusParking = true;

        List<WebElement> historyStatusesParking = driver.findElements(By.xpath("//tbody/tr/td[3]/a[contains(text(),'A70')]"));
        for (WebElement element : historyStatusesParking){

            if (element.getAttribute("href").contains("/admin/realty-card/parking/575?n=A70")){

                String handleBefore = driver.getWindowHandle();

                click(By.xpath("//tbody/tr/td[3]/a[contains(text(),'A70, Москва, Ходынский бульвар, 2, Лайнер')]"));
                sleep(3000);

                for (String winHandle : driver.getWindowHandles()) {
                    driver.switchTo().window(winHandle);
                }

                try {
                    waitBy(globalpage.parkingCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки машиноместа");
                }

                driver.close();
                driver.switchTo().window(handleBefore);

                historyStatusParking = false;
                break;

            }

        }

        if (historyStatusParking) {
            afterExeptionMessage("Ссылка на сданное машиноместо не найдена");
        }

        click(page.comments);
        Assert.assertTrue(findElement(page.comments).getAttribute("class").equals("active"));

    }

    @Test(description = "Переход в карточку машиноместа по ссылке из карточки арендатора", priority = 1)
    public void fromRenterCardToParkingCard() throws InterruptedException {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        click(globalpage.objectsSection);
        click(globalpage.rentersSection);
        rentersSection();

        findElement(globalpage.searchInput).sendKeys("202" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.listRenterId);

        boolean renterID = true;

        List<WebElement> elements = driver.findElements(page.listRenterId);
        for (WebElement element : elements) {

            if (element.getText().equals("202")) {

                click(globalpage.searchNumber("202"), 30);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки арендатора");
                }

                renterID = false;
                break;

            }

        }

        if (renterID) {
            afterExeptionMessage("Номер арендатора не найден");
        }

        boolean statusParking = true;

        List<WebElement> parkings = driver.findElements(By.xpath("//div[@class='panel-body']/div[2]/div[2]"));
        for (WebElement parking : parkings) {

            if (parking.getText().contains("м/м A70")) {

                String handleBefore = driver.getWindowHandle();

                click(By.xpath("//span[text()='Сдано']/ancestor::p/a"));
                sleep(3000);
                for (String winHandle : driver.getWindowHandles()) {
                    driver.switchTo().window(winHandle);
                }
                try {
                    waitBy(globalpage.parkingCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки машиноместа");
                }
                driver.close();
                driver.switchTo().window(handleBefore);

                statusParking = false;
                break;

            }

        }

        if (statusParking) {
            afterExeptionMessage("Номер арендованного машиноместа не найден");
        }

    }

    @Test(description = "Отправка приглашения арендатору для регистрации", priority = 2)
    public void sendInviteToRenterForRegistration() {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        click(globalpage.objectsSection);
        click(globalpage.rentersSection);
        rentersSection();

        findElement(globalpage.searchInput).sendKeys("202" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.listRenterId);

        boolean renterID = true;

        List<WebElement> elements = driver.findElements(page.listRenterId);
        for (WebElement element : elements) {

            if (element.getText().equals("202")) {

                click(globalpage.searchNumber("202"), 30);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки арендатора");
                }

                renterID = false;
                break;

            }

        }

        if (renterID) {
            afterExeptionMessage("Номер арендатора не найден");
        }

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.sendInvite)).build().perform();
        String tooltip = new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@uib-tooltip]"))).getAttribute("uib-tooltip");
        Assert.assertTrue(tooltip.equals("Клиенту будет отправлено письмо со ссылкой в личный кабинет жильца"));

        click(page.sendInvite);
        try {
            waitBy(page.toastInvite);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент тоста с успешной отправкой письма не найден");
        }

    }

    @Test(description = "Переход в форму заселения арендатора в апартамент по ссылке из карточки арендатора", priority = 3)
    public void fromRenterCardToSettlementRenterInApartmentForm() {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        click(globalpage.objectsSection);
        click(globalpage.rentersSection);
        rentersSection();

        findElement(globalpage.searchInput).sendKeys("202" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.listRenterId);

        boolean renterID = true;

        List<WebElement> elements = driver.findElements(page.listRenterId);
        for (WebElement element : elements) {

            if (element.getText().equals("202")) {

                click(globalpage.searchNumber("202"), 30);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки арендатора");
                }

                renterID = false;
                break;

            }

        }

        if (renterID) {
            afterExeptionMessage("Номер арендатора не найден");
        }

        click(page.toSettle);
        try {
            waitBy(page.settlementElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы заселения арендатора в апартамент");
        }

        try {
            waitBy(By.xpath("//ol[@id='dynamic-options']/button//span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]"));
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент необходимого арендатора");
        }

        boolean a = true;

        List<WebElement> apartments = driver.findElements(By.xpath("//div[4]//select/option"));
        for (WebElement element : apartments){

            String apartment = element.getText().replace("A", "");
            selectByText(By.xpath("//div[4]//select"), apartment);
            int apart = Integer.parseInt(apartment);

            if (apart >= 1){

                if (findElement(By.xpath("//div[4]/div[2]/span/span")).getText().contains("Свободен")){

                    a = false;
                    break;

                }

            } else {

                apart++;

            }

        }

        if (a) {
            afterExeptionMessage("Свободный апартамент не найден");
        }

    }

    @Test(description = "Переход в форму сдачи машиноместа по ссылке из карточки арендатора", priority = 4)
    public void fromRenterCardToLeaseParkingForm() throws InterruptedException {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        click(globalpage.objectsSection);
        click(globalpage.rentersSection);
        rentersSection();

        findElement(globalpage.searchInput).sendKeys("202" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.listRenterId);

        boolean renterID = true;

        List<WebElement> elements = driver.findElements(page.listRenterId);
        for (WebElement element : elements) {

            if (element.getText().equals("202")) {

                click(globalpage.searchNumber("202"), 30);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки арендатора");
                }

                renterID = false;
                break;

            }

        }

        if (renterID) {
            afterExeptionMessage("Номер арендатора не найден");
        }

        click(page.toLease);
        try {
            waitBy(page.leaseElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент страницы сдачи машиноместа");
        }

        try {
            waitBy(By.xpath("//ol[@id='dynamic-options']/button//span[contains(text(),'Мифтахутдинов Рамиль Рустэмович')]"));
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент необходимого арендатора");
        }

        boolean a = true;

        List<WebElement> apartments = driver.findElements(By.xpath("//div[4]//select/option"));
        for (WebElement element : apartments){

            selectByText(By.xpath("//div[4]//select"), element.getText());
            String parking = element.getText().replace("A", "").trim();
            int park = Integer.parseInt(parking);

            if (park >= 400){

                if (findElement(By.xpath("//div[4]/div[2]/span/span")).getText().contains("Свободен")){

                    a = false;
                    break;

                }

            } else {

                park++;

            }

        }

        if (a) {
            afterExeptionMessage("Свободное машиноместо не найдено");
        }

    }

    @Test(description = "Добавление комментария в карточке арендатора", priority = 5)
    public void addCommentInRenterCard() throws FileNotFoundException {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        click(globalpage.objectsSection);
        click(globalpage.rentersSection);
        rentersSection();

        String txtRenterID = System.getProperty("user.dir") + "\\files\\txtFiles\\renters\\textRenterID.txt";
        File fileRenterId = new File(txtRenterID);
        Scanner scannerRenterId = new Scanner(fileRenterId);
        String renterID = scannerRenterId.nextLine();

        findElement(globalpage.searchInput).sendKeys(renterID + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.listRenterId);

        boolean renter = true;

        List<WebElement> elements = driver.findElements(page.listRenterId);
        for (WebElement element : elements) {

            if (element.getText().equals(renterID)) {

                click(globalpage.searchNumber(renterID), 30);
                try {
                    waitBy(globalpage.renterCardElement);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент карточки арендатора");
                }

                renter = false;
                break;

            }

        }

        if (renter) {
            afterExeptionMessage("ID добавленного арендатора не найден");
        }

        sendKeys(page.commentsTextField, "Комментарий, добавленный в карточке арендатора");
        click(page.addComment);
        try {
            waitBy(page.newCommentElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент добавленного комментария в карточке арендатора");
        }

        click(globalpage.rentersSection);
        rentersSection();

        findElement(globalpage.searchInput).sendKeys(renterID + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.listRenterId);

        renter = true;

        List<WebElement> elements2 = driver.findElements(page.listRenterId);

        for (WebElement element : elements2) {

            if (element.getText().equals(renterID)) {

                try {
                    waitBy(page.listComment);
                } catch (NoSuchElementException ex) {
                    afterExeptionMessage("Не найден элемент добавленного комментария в списке арендаторов");
                }

                renter = false;
                break;

            }

        }

        if (renter) {
            afterExeptionMessage("Номер арендатора не найден");
        }

    }

}
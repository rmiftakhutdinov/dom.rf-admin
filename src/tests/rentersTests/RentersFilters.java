package tests.rentersTests;

import framework.Configuration;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.RentersPage;
import java.util.concurrent.TimeUnit;

public class RentersFilters extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Строка поиска в списке арендаторов")
    public void searchLineRenters() {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        objectsSection();
        click(globalpage.rentersSection);
        rentersSection();

        waitBy(page.listRenterId);

        findElement(globalpage.searchInput).sendKeys("A70" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.listRenterParking).getText().contains("м/м A70"));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("250" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.listRenterApartment).getText().contains("ап. 250"));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("202" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.listRenterId).getText().equals("202"));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("Мифтахутдинов Рамиль" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.listRenterFullName).getText().contains("Мифтахутдинов Рамиль Рустэмович"));

        findElement(globalpage.searchInput).clear();
        findElement(globalpage.searchInput).sendKeys("89272401145" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);
        Assert.assertTrue(findElement(page.listRenterPhone).getText().equals("+7 927 240-11-45"));

    }

}
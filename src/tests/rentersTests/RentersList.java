package tests.rentersTests;

import framework.Configuration;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.RentersPage;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RentersList extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход по ссылкам в списке арендаторов")
    public void openLinksInRentersList() {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        objectsSection();
        click(globalpage.rentersSection);
        rentersSection();

        findElement(globalpage.searchInput).sendKeys("202" + Keys.ENTER);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(globalpage.spinner)));
        elementIsNotVisible(globalpage.spinner,30);

        waitBy(page.listRenterId);

        boolean renterID = true;

        List<WebElement> elements = driver.findElements(page.listRenterId);
        for (WebElement element : elements) {

            if (element.getText().equals("202")) {

                renterID = false;
                break;

            }

        }

        if (renterID) {
            afterExeptionMessage("Номер арендатора не найден");
        }

        click(page.listRenterId);
        try {
            waitBy(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки арендатора");
        }

        driver.navigate().back();
        rentersSection();

        click(page.listRenterFullName);
        try {
            waitBy(globalpage.renterCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Не найден элемент карточки арендатора");
        }

        driver.navigate().back();
        rentersSection();

        click(page.listRenterApartment);
        try {
            waitBy(globalpage.apartmentCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки апартамента не найден");
        }

        driver.navigate().back();
        rentersSection();

        click(page.listRenterParking);
        try {
            waitBy(globalpage.parkingCardElement);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Элемент карточки машиноместа не найден");
        }

    }

    @Test(description = "Пагинация в списке арендаторов", priority = 1)
    public void rentersListPagination() {

        GlobalPage globalpage = new GlobalPage();
        RentersPage page = new RentersPage();

        click(globalpage.rentersSection);
        rentersSection();

        waitBy(globalpage.nextPage, 30);

        String page1 = new String(findElement(page.listRenterId).getAttribute("href"));

        click(globalpage.nextPage,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        String page2 = new String(findElement(page.listRenterId).getAttribute("href"));
        Assert.assertFalse(page1.equals(page2));

        click(globalpage.prevPage,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

        click(globalpage.page2,30);
        try {
            waitBy(globalpage.page2Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на следующую страницу не произошел");
        }

        click(globalpage.page1,30);
        try {
            waitBy(globalpage.page1Active);
        } catch (TimeoutException ex){
            afterExeptionMessage("Переход на предыдущую страницу не произошел");
        }

    }

}
package tests.reportsTests;

import framework.Configuration;
import org.openqa.selenium.Keys;
import org.testng.annotations.*;
import pageObjects.AhmlServicesPage;
import pageObjects.GlobalPage;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ReportFromAhmlServices extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLProd);
        authorizationInAdminProd();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Отчет по всем заказам ДОМ.РФ")
    public void downloadReportAllAhmlServices() throws Exception {

        GlobalPage page = new GlobalPage();

        objectsSection();
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по одному заказу ДОМ.РФ", priority = 1)
    public void downloadReportOneAhmlServices() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        sendKeys(page.searchInput, "157" + Keys.ENTER);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам ДОМ.РФ с типом 'Генеральная уборка'", priority = 2)
    public void downloadReportAhmlServicesTypeGeneral() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        selectByValue(new AhmlServicesPage().filterByType, "number:8");

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy_generalnaia_uborka.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    // Убрать игнор после добавления фильтра по выездной уборке
    @Ignore
    @Test(description = "Отчет по заказам ДОМ.РФ с типом 'Выездная уборка'", priority = 3)
    public void downloadReportAhmlServicesTypeDeparture() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        selectByValue(new AhmlServicesPage().filterByType, "number:???"); // указать нужный номер

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy_???_uborka.xlsx"); // указать название уборки

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам ДОМ.РФ с типом 'Поддерживающая уборка'", priority = 4)
    public void downloadReportAhmlServicesTypeSupportive() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        selectByValue(new AhmlServicesPage().filterByType, "number:1");

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy_podderzhivaiushchaia_uborka.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам ДОМ.РФ с типом 'Технологическая уборка'", priority = 5)
    public void downloadReportAhmlServicesTypeTechnologic() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        selectByValue(new AhmlServicesPage().filterByType, "number:110");

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy_tekhnologicheskaia_uborka.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам ДОМ.РФ со статусом 'Не выполнен'", priority = 6)
    public void downloadReportAhmlServicesStatusNotReady() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        selectByValue(new AhmlServicesPage().filterByStatus, "string:new");

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy_ne_vypolnen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам ДОМ.РФ со статусом 'В обработке'", priority = 7)
    public void downloadReportAhmlServicesStatusProcessed() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        selectByValue(new AhmlServicesPage().filterByStatus, "string:processed");

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy_v_obrabotke.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам ДОМ.РФ со статусом 'Выполнен'", priority = 8)
    public void downloadReportAhmlServicesStatusReady() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        selectByValue(new AhmlServicesPage().filterByStatus, "string:ready");

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy_vypolnen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам ДОМ.РФ со статусом 'Отменен'", priority = 9)
    public void downloadReportAhmlServicesStatusCancelled() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        selectByValue(new AhmlServicesPage().filterByStatus, "string:cancelled");

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy_otmenen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам ДОМ.РФ за период", priority = 10)
    public void downloadReportAhmlServicesByPeriod() throws InterruptedException, ParseException {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.ahmlServicesSection);
        ahmlServicesSection();

        ahmlServiceNumber();

        click(new AhmlServicesPage().periodFilter);
        click(new AhmlServicesPage().periodFilterDiapason);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.periodInputDateFrom);
        click(page.nextMonth);
        click(page.setDateFrom);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        click(page.periodInputDateTo);
        click(page.prevMonth);
        click(page.setDateTo);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        ahmlServiceNumber();

        String inputDate1 = findElement(page.periodInputDateFrom).getAttribute("value");
        String inputDate2 = findElement(page.periodInputDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "vnutrennie_zakazy_" + dateFrom + "_" + dateTo + ".xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

}
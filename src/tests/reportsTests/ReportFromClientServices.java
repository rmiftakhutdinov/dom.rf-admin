package tests.reportsTests;

import framework.Configuration;
import org.openqa.selenium.Keys;
import org.testng.annotations.*;
import pageObjects.ClientServicesPage;
import pageObjects.GlobalPage;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ReportFromClientServices extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLProd);
        authorizationInAdminProd();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Отчет по всем заказам клиентов")
    public void downloadReportAllClientServices() throws InterruptedException {

        GlobalPage page = new GlobalPage();

        objectsSection();
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по одному заказу", priority = 1)
    public void downloadReportOneClientService() throws InterruptedException {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        sendKeys(page.searchInput, "1-65" + Keys.ENTER);
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам с типом 'Уборка' в статусах 'Не выполнен' и 'Не оплачен'", priority = 2)
    public void downloadReportClientServicesTypeCleanStatusNewWaiting() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ClientServicesPage pageserv = new ClientServicesPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        selectByValue(pageserv.typeFilter, "string:clean");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        selectByValue(pageserv.statusFilter, "string:new");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        selectByValue(pageserv.paymentFilter, "string:waiting");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy_uborka_ne_vypolnen_ne_oplachen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам с типом 'Ремонт' в статусах 'Выполнен' и 'Оплачен'", priority = 3)
    public void downloadReportClientServicesTypeRepairStatusReadyPaid() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ClientServicesPage pageserv = new ClientServicesPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        selectByValue(pageserv.typeFilter, "string:repair");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        selectByValue(pageserv.statusFilter, "string:ready");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        selectByValue(pageserv.paymentFilter, "string:paid");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy_remont_vypolnen_oplachen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам с типом 'Пропуск' в статусе 'Бесплатно'", priority = 4)
    public void downloadReportClientServicesTypePassStatusFree() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ClientServicesPage pageserv = new ClientServicesPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        selectByValue(pageserv.typeFilter, "string:pass");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        selectByValue(pageserv.paymentFilter, "string:free");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy_propusk_besplatno.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам с типом 'Прочие' в статусе 'Отменен'", priority = 5)
    public void downloadReportClientServicesTypeOtherStatusCancelled() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ClientServicesPage pageserv = new ClientServicesPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        selectByValue(pageserv.typeFilter, "string:other");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        selectByValue(pageserv.statusFilter, "string:cancelled");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy_prochie_otmenen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам в статусе 'В обработке'", priority = 6)
    public void downloadReportClientServicesStatusProcessed() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ClientServicesPage pageserv = new ClientServicesPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        selectByValue(pageserv.statusFilter, "string:processed");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy_v_obrabotke.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам в статусе 'Возвращено'", priority = 7)
    public void downloadReportClientServicesStatusReturned() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ClientServicesPage pageserv = new ClientServicesPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        selectByValue(pageserv.paymentFilter, "string:returned");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy_vozvrashcheno.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам в статусе 'К возврату'", priority = 8)
    public void downloadReportClientServicesStatusToReturn() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ClientServicesPage pageserv = new ClientServicesPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        selectByValue(pageserv.paymentFilter, "string:toReturn");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy_k_vozvratu.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам с оценкой '5'", priority = 9)
    public void downloadReportClientServicesRating5() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ClientServicesPage pageserv = new ClientServicesPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        click(pageserv.rating5);
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        clientServiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy_otsenka_5.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заказам за период", priority = 10)
    public void downloadReportClientServicesByPeriod() throws InterruptedException, ParseException {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.dropdownMenuServices);
        click(page.clientServicesSection);
        clientServicesSection();

        clientServiceNumber();

        click(page.periodFilter);
        click(page.periodDiapason1);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.periodInputDateFrom);
        click(page.nextMonth);
        click(page.setDateFrom);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.periodInputDateTo);
        click(page.prevMonth);
        click(page.setDateTo);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        String inputDate1 = findElement(page.periodInputDateFrom).getAttribute("value");
        String inputDate2 = findElement(page.periodInputDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zakazy_" + dateFrom + "_" + dateTo + ".xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

}
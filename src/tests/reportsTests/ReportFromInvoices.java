package tests.reportsTests;

import framework.Configuration;
import org.openqa.selenium.Keys;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.InvoicePage;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ReportFromInvoices extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminProd();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Отчет по всем счетам")
    public void downloadReportAllInvoices() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        objectsSection();
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        click(pageinvoice.statusFilter);
        click(pageinvoice.allStatuses);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по одному счету", priority = 1)
    public void downloadReportOneInvoice() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        click(pageinvoice.statusFilter);
        click(pageinvoice.allStatuses);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        sendKeys(page.searchInput, "268-12" + Keys.ENTER);
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по счетам с типом 'Аренда' в статусе 'Не оплачен'", priority = 2)
    public void downloadReportInvoicesTypeRentStatusNotPaid() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        selectByValue(pageinvoice.typeFilter, "string:rent");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_arenda_ne_oplachen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по счетам с типом 'Коммунальные' в статусе 'Оплачен'", priority = 3)
    public void downloadReportInvoicesTypeCommunalStatusPaid() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        selectByValue(pageinvoice.statusFilter, "string:paid");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        selectByValue(pageinvoice.typeFilter, "string:communal");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_kommunalnye_oplachen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по счетам в статусе 'К возврату'", priority = 4)
    public void downloadReportInvoicesStatusToReturn() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        selectByValue(pageinvoice.statusFilter, "string:toReturn");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_k_vozvratu.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по счетам в статусе 'Возвращено'", priority = 5)
    public void downloadReportInvoicesStatusReturned() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        selectByValue(pageinvoice.statusFilter, "string:returned");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_vozvrashcheno.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по счетам с типом 'Депозит' в статусе 'Отменен'", priority = 6)
    public void downloadReportInvoicesTypeDepositStatusCancel() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        selectByValue(pageinvoice.statusFilter, "string:cancel");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        selectByValue(pageinvoice.typeFilter, "string:deposit");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_depozit_otmenen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по счетам с типом 'Услуги'", priority = 7)
    public void downloadReportInvoicesTypeServices() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        click(pageinvoice.statusFilter);
        click(pageinvoice.allStatuses);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        selectByValue(pageinvoice.typeFilter, "string:service");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_uslugi.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по счетам с типом 'Пени'", priority = 8)
    public void downloadReportInvoicesTypeFine() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        click(pageinvoice.statusFilter);
        click(pageinvoice.allStatuses);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        selectByValue(pageinvoice.typeFilter, "string:fine");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_peni.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по счетам с типом 'Задолженность'", priority = 9)
    public void downloadReportInvoicesTypeDebt() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        click(pageinvoice.statusFilter);
        click(pageinvoice.allStatuses);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        selectByValue(pageinvoice.typeFilter, "string:debt");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_zadolzhennost.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по счетам с типом 'Прочее'", priority = 10)
    public void downloadReportInvoicesTypeOther() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        click(pageinvoice.statusFilter);
        click(pageinvoice.allStatuses);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        selectByValue(pageinvoice.typeFilter, "string:other");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_prochee.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по просроченным счетам", priority = 11)
    public void downloadReportDelayInvoices() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        sendKeys(pageinvoice.delayedPaymentFrom, "1");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        sendKeys(pageinvoice.delayedPaymentTo, "10");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_ne_oplachen_prosrochka.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по всем счетам за период", priority = 12)
    public void downloadReportAllInvoicesByPeriod() throws InterruptedException, ParseException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        click(pageinvoice.statusFilter);
        click(pageinvoice.allStatuses);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.periodFilter);
        click(page.periodDiapason1);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(pageinvoice.calendarDateFrom);
        click(pageinvoice.nextMonth);
        click(pageinvoice.setDateFrom);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(pageinvoice.calendarDateTo);
        click(pageinvoice.prevMonth);
        click(pageinvoice.setDateTo);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        String inputDate1 = findElement(page.periodInputDateFrom).getAttribute("value");
        String inputDate2 = findElement(page.periodInputDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_" + dateFrom + "_" + dateTo + ".xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по счетам в статусе 'Не выставлен'", priority = 13)
    public void downloadReportInvoicesStatusNotInvoiced() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        InvoicePage pageinvoice = new InvoicePage();

        click(page.objectsSection);
        click(page.invoiceSection);
        invoiceSection();

        invoiceNumber();

        selectByValue(pageinvoice.statusFilter, "string:notInvoiced");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        invoiceNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "scheta_ne_vystavlen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

}
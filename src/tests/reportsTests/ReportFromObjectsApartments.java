package tests.reportsTests;

import framework.Configuration;
import org.openqa.selenium.Keys;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageObjects.GlobalPage;
import pageObjects.ObjectsPage;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ReportFromObjectsApartments extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();

        driver.get(page.pageURLProd);
        authorizationInAdminProd();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Отчет по всем апартаментам")
    public void downloadReportAllApartments() throws InterruptedException {

        GlobalPage page = new GlobalPage();

        objectsSection();
        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по одному апартаменту", priority = 1)
    public void downloadReportOneApartment() throws InterruptedException {

        GlobalPage page = new GlobalPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        sendKeys(page.searchInput, "250" + Keys.ENTER);
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам - 2 этаж, студия, бежевый", priority = 2)
    public void downloadReportApartmentFoor2StudioBeige() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterFloors, "number:2");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        selectByValue(pageobject.filterRooms, "string:studio");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        selectByValue(pageobject.filterDecoration, "string:beige");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_etazh_2_studiia_bezhevyi.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам - 3 этаж, 2-комнатная, белый", priority = 3)
    public void downloadReportApartmentFoor3Room2White() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterFloors, "number:3");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        selectByValue(pageobject.filterRooms, "string:two");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        selectByValue(pageobject.filterDecoration, "string:white");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_etazh_3_2_komnatnaia_belyi.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам в статусе 'Свободен'", priority = 4)
    public void downloadReportApartmentStatusFree() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterStatus, "string:free");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_svoboden.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по апартаментам в статусе 'Забронирован' за все время", priority = 5)
    public void downloadReportApartmentStatusReservedAllTime() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterStatus, "string:reserved");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_zabronirovan.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по апартаментам в статусе 'Забронирован' за период", priority = 6)
    public void downloadReportApartmentStatusReservedByPeriod() throws InterruptedException, ParseException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(new ObjectsPage().filterStatus, "string:reserved");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(pageobject.periodFilterStatusReserved);
        click(page.periodDiapason3);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        /*click(page.periodInputDateFrom);
        click(page.nextMonth);
        click(page.setDateFrom);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.periodInputDateTo);
        click(page.prevMonth);
        click(page.setDateTo);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();*/

        String inputDate1 = findElement(page.periodInputDateFrom).getAttribute("value");
        String inputDate2 = findElement(page.periodInputDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_" + dateFrom + "_" + dateTo + "_zabronirovan.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по апартаментам в статусе 'Арендован' за все время", priority = 7)
    public void downloadReportApartmentStatusRentedAllTime() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterStatus, "string:none");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_arendovan.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по апартаментам в статусе 'Арендован' за период", priority = 8)
    public void downloadReportApartmentStatusRentedByPeriod() throws InterruptedException, ParseException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(new ObjectsPage().filterStatus, "string:none");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(pageobject.periodFilterStatusRent);
        click(page.periodDiapason3);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        /*click(page.periodInputDateFrom);
        click(page.nextMonth);
        click(page.setDateFrom);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.periodInputDateTo);
        click(page.prevMonth);
        click(page.setDateTo);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();*/

        String inputDate1 = findElement(page.periodInputDateFrom).getAttribute("value");
        String inputDate2 = findElement(page.periodInputDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_" + dateFrom + "_" + dateTo + "_arendovan.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по апартаментам в статусе 'Расторжение' за все время", priority = 9)
    public void downloadReportApartmentStatusTerminationAllTime() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterStatus, "string:termination");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_rastorzhenie.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по апартаментам в статусе 'Расторжение' за период", priority = 10)
    public void downloadReportApartmentStatusTerminationByPeriod() throws InterruptedException, ParseException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(new ObjectsPage().filterStatus, "string:termination");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(pageobject.periodFilterStatusTermination);
        click(page.periodDiapason3);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        /*click(page.periodInputDateFrom);
        click(page.nextMonth);
        click(page.setDateFrom);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.periodInputDateTo);
        click(page.prevMonth);
        click(page.setDateTo);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();*/

        String inputDate1 = findElement(page.periodInputDateFrom).getAttribute("value");
        String inputDate2 = findElement(page.periodInputDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_" + dateFrom + "_" + dateTo + "_rastorzhenie.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по апартаментам в статусе 'Приостановлен' за все время", priority = 11)
    public void downloadReportApartmentStatusPausedAllTime() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterStatus, "string:paused");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_priostanovlen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по апартаментам в статусе 'Приостановлен' за период", priority = 12)
    public void downloadReportApartmentStatusPausedByPeriod() throws InterruptedException, ParseException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(new ObjectsPage().filterStatus, "string:paused");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(pageobject.periodFilterStatusPaused);
        click(page.periodDiapason3);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        /*click(page.periodInputDateFrom);
        click(page.nextMonth);
        click(page.setDateFrom);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.periodInputDateTo);
        click(page.prevMonth);
        click(page.setDateTo);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();*/

        String inputDate1 = findElement(page.periodInputDateFrom).getAttribute("value");
        String inputDate2 = findElement(page.periodInputDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_" + dateFrom + "_" + dateTo + "_priostanovlen.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам в статусе 'Неактивен'", priority = 13)
    public void downloadReportApartmentStatusInactive() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterStatus, "string:inactive");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_neaktiven.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам со статусом заявки 'Сбор документов'", priority = 14)
    public void downloadReportApartmentOrderStatusDocumentsGathering() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterOrderStatus, "string:documentsGathering");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_sbor_dokumentov.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам со статусом заявки 'Проверка СБ'", priority = 15)
    public void downloadReportApartmentOrderStatusSecurity() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterOrderStatus, "string:security");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_proverka_sb.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам со статусом заявки 'Проверка завершена'", priority = 16)
    public void downloadReportApartmentOrderStatusSecurityDone() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterOrderStatus, "string:securityDone");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_proverka_zavershena.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам со статусом заявки 'Запрос доп.инфы'", priority = 17)
    public void downloadReportApartmentOrderStatusSecurityNeedInfo() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterOrderStatus, "string:securityNeedInfo");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_zapros_dop_infy.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам со статусом заявки 'Оформление договора'", priority = 18)
    public void downloadReportApartmentOrderStatusDocumentsExecution() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterOrderStatus, "string:documentsExecution");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_oformlenie_dogovora.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам со статусом заявки 'Согласование СД'", priority = 19)
    public void downloadReportApartmentOrderStatusAgreement() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterOrderStatus, "string:agreement");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_soglasovanie_sd.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по апартаментам со статусом заявки 'Подписан ДБ'", priority = 20)
    public void downloadReportApartmentOrderStatusBookingContractSigned() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.filterOrderStatus, "string:bookingContractSigned");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        apartmentNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "apartamenty_podpisan_db.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

}
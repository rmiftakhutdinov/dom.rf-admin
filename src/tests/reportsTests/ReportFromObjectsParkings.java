package tests.reportsTests;

import framework.Configuration;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageObjects.GlobalPage;
import pageObjects.ObjectsPage;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ReportFromObjectsParkings extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();

        driver.get(page.pageURLProd);
        authorizationInAdminProd();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Отчет по всем машиноместам")
    public void downloadReportAllParkings() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        objectsSection();
        apartmentNumber();

        selectByValue(pageobject.changeTypeObject, "string:parkings");
        parkingNumber();
        Assert.assertTrue(findElement(pageobject.parkingsListElement).isDisplayed());

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "mashinomesta.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по одному машиноместу", priority = 1)
    public void downloadReportOneParking() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.changeTypeObject, "string:parkings");
        parkingNumber();
        Assert.assertTrue(findElement(pageobject.parkingsListElement).isDisplayed());

        sendKeys(page.searchInput, "A20" + Keys.ENTER);
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "mashinomesta.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по машиноместам в статусе 'Свободен'", priority = 2)
    public void downloadReportParkingStatusFree() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.changeTypeObject, "string:parkings");
        parkingNumber();
        Assert.assertTrue(findElement(pageobject.parkingsListElement).isDisplayed());

        selectByValue(pageobject.filterStatus, "string:free");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "mashinomesta_svoboden.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по машиноместам в статусе 'Арендован' за все время", priority = 3)
    public void downloadReportParkingStatusRentedAllTime() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.changeTypeObject, "string:parkings");
        parkingNumber();
        Assert.assertTrue(findElement(pageobject.parkingsListElement).isDisplayed());

        selectByValue(pageobject.filterStatus, "string:none");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "mashinomesta_arendovan.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по машиноместам в статусе 'Арендован' за период", priority = 4)
    public void downloadReportParkingStatusRentedByPeriod() throws InterruptedException, ParseException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.changeTypeObject, "string:parkings");
        parkingNumber();
        Assert.assertTrue(findElement(pageobject.parkingsListElement).isDisplayed());

        selectByValue(new ObjectsPage().filterStatus, "string:none");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();

        click(page.periodFilter);
        click(page.periodDiapason3);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();

        /*click(page.periodInputDateFrom);
        click(page.nextMonth);
        click(page.setDateFrom);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();

        click(page.periodInputDateTo);
        click(page.prevMonth);
        click(page.setDateTo);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();*/

        String inputDate1 = findElement(page.periodInputDateFrom).getAttribute("value");
        String inputDate2 = findElement(page.periodInputDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "mashinomesta_" + dateFrom + "_" + dateTo + "_arendovan.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по машиноместам в статусе 'Расторжение' за все время", priority = 5)
    public void downloadReportParkingStatusTerminationAllTime() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.changeTypeObject, "string:parkings");
        parkingNumber();
        Assert.assertTrue(findElement(pageobject.parkingsListElement).isDisplayed());

        selectByValue(pageobject.filterStatus, "string:termination");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "mashinomesta_rastorzhenie.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по по машиноместам в статусе 'Расторжение' за период", priority = 6)
    public void downloadReportParkingStatusTerminationByPeriod() throws InterruptedException, ParseException {

        GlobalPage page = new GlobalPage();
        ObjectsPage pageobject = new ObjectsPage();

        click(page.ordersSection);
        click(page.objectsSection);
        apartmentNumber();

        selectByValue(pageobject.changeTypeObject, "string:parkings");
        parkingNumber();
        Assert.assertTrue(findElement(pageobject.parkingsListElement).isDisplayed());

        selectByValue(new ObjectsPage().filterStatus, "string:termination");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();

        click(page.periodFilter);
        click(page.periodDiapason3);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();

        /*click(page.periodInputDateFrom);
        click(page.nextMonth);
        click(page.setDateFrom);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();

        click(page.periodInputDateTo);
        click(page.prevMonth);
        click(page.setDateTo);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        parkingNumber();*/

        String inputDate1 = findElement(page.periodInputDateFrom).getAttribute("value");
        String inputDate2 = findElement(page.periodInputDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "mashinomesta_" + dateFrom + "_" + dateTo + "_rastorzhenie.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

}
package tests.reportsTests;

import framework.Configuration;
import org.openqa.selenium.Keys;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.OrdersPage;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ReportFromOrders extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();

        driver.get(page.pageURLProd);
        authorizationInAdminProd();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Отчет по всем заявкам")
    public void downloadReportAllOrders() throws InterruptedException {

        GlobalPage page = new GlobalPage();

        objectsSection();
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по одной заявке", priority = 1)
    public void downloadReportOneOrder() throws InterruptedException {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        sendKeys(page.searchInput, "999" + Keys.ENTER);
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Новая'", priority = 2)
    public void downloadReportOrdersStatusNew() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_new");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_novaia.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Лист ожидания' с типом апартамента 'Студии 32'", priority = 3)
    public void downloadReportOrdersStatusWaitingListTypeStudio32() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_waitingList");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        selectByValue(pageorder.filterByType, "string:studio");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_studii_32_list_ozhidaniia.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Приглашён на просмотр' с типом апартамента 'Студии 25'", priority = 4)
    public void downloadReportOrdersStatusInvitedToShowTypeStudio25() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_invitedToShow");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        selectByValue(pageorder.filterByType, "string:smallStudio");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_studii_25_priglashion_na_prosmotr.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Сбор документов' с типом апартамента '2-комнатные'", priority = 5)
    public void downloadReportOrdersStatusDocumentsGatheringTypeTwoRooms() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_documentsGathering");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        selectByValue(pageorder.filterByType, "string:two");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_2_komnatnye_sbor_dokumentov.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Думает' с типом апартамента '3-комнатные'", priority = 6)
    public void downloadReportOrdersStatusThinkingTypeThreeRooms() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_thinking");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        selectByValue(pageorder.filterByType, "string:three");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_3_komnatnye_dumaet.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Проверка СБ'", priority = 7)
    public void downloadReportOrdersStatusSecurity() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_security");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_proverka_sb.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Проверка завершена'", priority = 8)
    public void downloadReportOrdersStatusSecurityDone() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_securityDone");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_proverka_zavershena.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Запрос доп.инфы'", priority = 9)
    public void downloadReportOrdersStatusSecurityNeedInfo() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_securityNeedInfo");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_zapros_dop_infy.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Оформление договора'", priority = 10)
    public void downloadReportOrdersStatusContractExecution() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_documentsExecution");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_oformlenie_dogovora.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Согласование СД'", priority = 11)
    public void downloadReportOrdersStatusAgreement() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_agreement");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_soglasovanie_sd.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Подписан ДБ'", priority = 12)
    public void downloadReportOrdersStatusBookingContractSigned() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_bookingContractSigned");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_podpisan_db.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Подписан ДА'", priority = 13)
    public void downloadReportOrdersStatusSettlement() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_settlement");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_podpisan_da.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Отказ'", priority = 14)
    public void downloadReportOrdersStatusRefuse() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_refuse");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_otkaz.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Выехал'", priority = 15)
    public void downloadReportOrdersStatusLeft() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_left");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_vyekhal.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам в статусе 'Удалена'", priority = 16)
    public void downloadReportOrdersStatusDeleted() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:status_deleted");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_udalena.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам с состоянием 'Нужно перезвонить'", priority = 17)
    public void downloadReportOrdersStateNeedCall() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:call_call");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_nuzhno_perezvonit.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по заявкам с состоянием 'Нужно заселить'", priority = 18)
    public void downloadReportOrdersStateNeedSettlement() throws InterruptedException {

        GlobalPage page = new GlobalPage();
        OrdersPage pageorder = new OrdersPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        selectByValue(pageorder.filterByStatus, "string:settlement_settlement");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_nuzhno_zaselit.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по всем заявкам за период", priority = 19)
    public void downloadReportAllOrdersByPeriod() throws InterruptedException, ParseException {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.ordersSection);
        ordersSection();

        orderNumber();

        click(page.periodFilter);
        click(page.periodDiapason2);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.periodInputDateFrom);
        click(page.nextMonth);
        click(page.setDateFrom);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        click(page.periodInputDateTo);
        click(page.prevMonth);
        click(page.setDateTo);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        orderNumber();

        String inputDate1 = findElement(page.periodInputDateFrom).getAttribute("value");
        String inputDate2 = findElement(page.periodInputDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "zaiavki_" + dateFrom + "_" + dateTo + ".xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

}
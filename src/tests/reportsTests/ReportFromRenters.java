package tests.reportsTests;

import framework.Configuration;
import org.openqa.selenium.Keys;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.RentersPage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ReportFromRenters extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLProd);
        authorizationInAdminProd();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Отчет по всем арендаторам")
    public void downloadReportAllRenters() throws Exception {

        GlobalPage page = new GlobalPage();

        objectsSection();
        click(page.rentersSection);
        rentersSection();

        renterNumberId();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "arendatory.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по одному арендатору", priority = 1)
    public void downloadReportOneRenter() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.rentersSection);
        rentersSection();

        waitBy(page.searchInput);
        findElement(page.searchInput).sendKeys("202" + Keys.ENTER);
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner,30);

        renterNumberId();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "arendatory.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по арендаторам с машиноместами в статусе 'Снимался'", priority = 2)
    public void downloadReportRentersWithParkingStatusWasRented() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.rentersSection);
        rentersSection();

        renterNumberId();

        selectByValue(new RentersPage().filterByObjects, "string:parking");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        renterNumberId();

        selectByValue(new RentersPage().filterByStatus, "string:wasRented");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        renterNumberId();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "arendatory_mashinomesto_snimalsia.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по арендаторам с апартаментами в статусе 'Снят'", priority = 3)
    public void downloadReportRentersWithApartmentsStatusIsRented() throws Exception {

        GlobalPage page = new GlobalPage();

        click(page.objectsSection);
        click(page.rentersSection);
        rentersSection();

        renterNumberId();

        selectByValue(new RentersPage().filterByObjects, "string:apartment");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        renterNumberId();

        selectByValue(new RentersPage().filterByStatus, "string:isRented");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        renterNumberId();

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "arendatory_apartament_sniat.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

    @Test(description = "Отчет по арендаторам в статусе 'Переселение' за период", priority = 4)
    public void downloadReportRentersStatusResettledPeriod() throws Exception {

        GlobalPage page = new GlobalPage();
        RentersPage pagerenters = new RentersPage();

        click(page.objectsSection);
        click(page.rentersSection);
        rentersSection();

        renterNumberId();

        selectByValue(pagerenters.filterByStatus, "string:resettled");
        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        renterNumberId();

        click(pagerenters.filterByDateResettled);
        click(pagerenters.diapasonByDateResettled);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        renterNumberId();

        click(pagerenters.calendarDateFrom);
        click(pagerenters.nextMonth);
        click(pagerenters.setDateFromResettled);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        renterNumberId();

        click(pagerenters.calendarDateTo);
        click(pagerenters.prevMonth);
        click(pagerenters.setDateToResettled);

        waitBy(page.spinner);
        elementIsNotVisible(page.spinner);

        renterNumberId();

        String inputDate1 = findElement(pagerenters.calendarDateFrom).getAttribute("value");
        String inputDate2 = findElement(pagerenters.calendarDateTo).getAttribute("value");

        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault());

        Date date1 = oldDateFormat.parse(inputDate1);
        String dateFrom = newDateFormat.format(date1);
        Date date2 = oldDateFormat.parse(inputDate2);
        String dateTo = newDateFormat.format(date2);

        click(page.downloadReport);

        domElementIsNotVisible(page.downloadReportSpinner,300);
        sleep(1000);

        String filename = (page.dirWork + "arendatory_" + dateFrom + "_" + dateTo + "_pereselenie.xlsx");

        File file = new File(filename);
        fileExist(file);
        fileDelete(file);

    }

}
package tests.statisticsTests;

import framework.Configuration;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.*;
import pageObjects.GlobalPage;
import pageObjects.StatisticsPage;
import java.util.concurrent.TimeUnit;

public class StatisticsTests extends GlobalPage {

    @BeforeClass
    public void setUp() throws InterruptedException {

        driver = getChromeDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        GlobalPage page = new GlobalPage();
        driver.get(page.pageURLDev);
        authorizationInAdminDev();

    }

    @AfterClass
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переход по ссылкам в блоке 'Заселения'")
    public void openLinksInSettlementsSubsection() {

        GlobalPage page = new GlobalPage();
        StatisticsPage pagestat = new StatisticsPage();

        objectsSection();
        click(page.statisticsSection);
        statisticsSection();

        String handleBefore = driver.getWindowHandle();

        click(pagestat.settlementToday);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String idToday = "http://dev-arenda.ahml.ru/admin/order-list?status_call=settlement_settlement&settlement";
        Assert.assertTrue(driver.getCurrentUrl().contains(idToday));

        ordersSection();
        driver.close();
        driver.switchTo().window(handleBefore);

        click(pagestat.settlementTomorrow);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String idTomorrow = "http://dev-arenda.ahml.ru/admin/order-list?status_call=settlement_settlement&settlement";
        Assert.assertTrue(driver.getCurrentUrl().contains(idTomorrow));

        ordersSection();
        driver.close();
        driver.switchTo().window(handleBefore);

        click(pagestat.settlement30Days);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String id30days = "http://dev-arenda.ahml.ru/admin/order-list?status_call=settlement_settlement&settlement";
        Assert.assertTrue(driver.getCurrentUrl().contains(id30days));

        ordersSection();
        driver.close();
        driver.switchTo().window(handleBefore);

    }

    @Test(description = "Переход по ссылкам в блоке 'Выезды'", priority = 1)
    public void openLinksInEjectmentsSubsection() {

        GlobalPage page = new GlobalPage();
        StatisticsPage pagestat = new StatisticsPage();

        statisticsSection();

        String handleBefore = driver.getWindowHandle();

        click(pagestat.ejectmentToday);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String idToday = "http://dev-arenda.ahml.ru/admin/realty-list?status=block&contractEnded";
        Assert.assertTrue(driver.getCurrentUrl().contains(idToday));

        objectsSection();
        driver.close();
        driver.switchTo().window(handleBefore);

        click(pagestat.ejectmentTomorrow);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String idTomorrow = "http://dev-arenda.ahml.ru/admin/realty-list?status=block&contractEnded";
        Assert.assertTrue(driver.getCurrentUrl().contains(idTomorrow));

        objectsSection();
        driver.close();
        driver.switchTo().window(handleBefore);

        click(pagestat.ejectment30Days);

        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String id30days = "http://dev-arenda.ahml.ru/admin/realty-list?status=block&contractEnded";
        Assert.assertTrue(driver.getCurrentUrl().contains(id30days));

        objectsSection();
        driver.close();
        driver.switchTo().window(handleBefore);

    }

    @Test(description = "Переключение отображения графика понедельно/помесячно", priority = 2)
    public void changeGraphDisplay() {

        GlobalPage page = new GlobalPage();
        StatisticsPage pagestat = new StatisticsPage();

        statisticsSection();

        click(pagestat.monthStatistic);
        try {
            waitBy(pagestat.monthStatisticElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("График не переключился на режим 'Помесячно'");
        }

        click(pagestat.weekStatistic);
        try {
            waitBy(pagestat.weekStatisticElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("График не переключился на режим 'Понедельно'");
        }

    }

}